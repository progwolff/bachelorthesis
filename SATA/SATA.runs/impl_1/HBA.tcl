proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {Common-41} -limit 4294967295
set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000

start_step init_design
set rc [catch {
  create_msg_db init_design.pb
  set_param gui.test TreeTableDev
  set_param iopl.placeGlobalClockInstsInIOPlacer 0
  set_param iopl.placeRegionalClockInstsInIOPlacer 0
  debug::add_scope template.lib 1
  set_property design_mode GateLvl [current_fileset]
  set_property webtalk.parent_dir C:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.cache/wt [current_project]
  set_property parent.project_path C:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.xpr [current_project]
  set_property ip_repo_paths {
  c:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.cache/ip
  C:/Users/wolff/Desktop/bachelorthesis/ip_repo/AXI_SATA_controller_1.0
} [current_project]
  set_property ip_output_repo c:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.cache/ip [current_project]
  add_files -quiet C:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.runs/synth_1/HBA.dcp
  add_files -quiet C:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.runs/gtp_sata_synth_1/gtp_sata.dcp
  set_property netlist_only true [get_files C:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.runs/gtp_sata_synth_1/gtp_sata.dcp]
  read_xdc -mode out_of_context -ref gtp_sata c:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.srcs/sources_1/ip/gtp_sata/gtp_sata_ooc.xdc
  set_property processing_order EARLY [get_files c:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.srcs/sources_1/ip/gtp_sata/gtp_sata_ooc.xdc]
  read_xdc -ref gtp_sata c:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.srcs/sources_1/ip/gtp_sata/gtp_sata.xdc
  set_property processing_order EARLY [get_files c:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.srcs/sources_1/ip/gtp_sata/gtp_sata.xdc]
  read_xdc C:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.srcs/sources_1/ip/gtp_sata/gtp_sata.xdc
  link_design -top HBA -part xc7a200tfbg676-1
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
}

start_step opt_design
set rc [catch {
  create_msg_db opt_design.pb
  read_xdc -unmanaged c:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.srcs/sources_1/ip/gtp_sata/tcl/v7ht.tcl
  catch {write_debug_probes -quiet -force debug_nets}
  opt_design 
  write_checkpoint -force HBA_opt.dcp
  catch {report_drc -file HBA_drc_opted.rpt}
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
}

start_step place_design
set rc [catch {
  create_msg_db place_design.pb
  place_design 
  write_checkpoint -force HBA_placed.dcp
  catch { report_io -file HBA_io_placed.rpt }
  catch { report_clock_utilization -file HBA_clock_utilization_placed.rpt }
  catch { report_utilization -file HBA_utilization_placed.rpt -pb HBA_utilization_placed.pb }
  catch { report_control_sets -verbose -file HBA_control_sets_placed.rpt }
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
}

start_step route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force HBA_routed.dcp
  catch { report_drc -file HBA_drc_routed.rpt -pb HBA_drc_routed.pb }
  catch { report_timing_summary -warn_on_violation -max_paths 10 -file HBA_timing_summary_routed.rpt -rpx HBA_timing_summary_routed.rpx }
  catch { report_power -file HBA_power_routed.rpt -pb HBA_power_summary_routed.pb }
  catch { report_route_status -file HBA_route_status.rpt -pb HBA_route_status.pb }
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
}

