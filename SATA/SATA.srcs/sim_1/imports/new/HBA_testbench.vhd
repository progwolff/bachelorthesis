----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 25.03.2015 11:38:52
-- Design Name: 
-- Module Name: HBA_testbench - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity HBA_testbench is
--  Port ( );
end HBA_testbench;

architecture Behavioral of HBA_testbench is
    component \HBA\
       port (
        -- clock and reset
        hard_reset : in std_logic; -- Active high, reset button pushed on board
        soft_reset : in std_logic; -- Active high, reset button or soft reset
        DRPCLK_IN_N : in std_logic;
        DRPCLK_IN_P : in std_logic;
        SYSCLK_IN_P : in std_logic;
        SYSCLK_IN_N : in std_logic;
        gtp_refclk : out std_logic;-- //150MHz SATA3 clock used for reset link and gtp
        sata_sys_clock : out std_logic;-- // 75/150 MHZ system clock
       
        -- ports that connect to I/O pins of the FPGA
        Q0_CLK1_GTREFCLK_PAD_N_IN : in std_logic;
        Q0_CLK1_GTREFCLK_PAD_P_IN : in std_logic;
        RXP0_IN : in std_logic;-- // Receiver input
        RXN0_IN : in std_logic;--, // Receiver input
        TXP0_OUT : out std_logic;--, // Transceiver output
        TXN0_OUT : out std_logic;--, // Transceiver output
    
        -- HBA main interface: input ports
        cmd : in std_logic_vector(2 downto 0);
        cmd_en : in std_logic;
        lba : in std_logic_vector(47 downto 0);
        sectorcnt : in std_logic_vector(15 downto 0);
        wdata : in std_logic_vector(15 downto 0);
        wdata_en : in std_logic;
        rdata_next : in std_logic; 
    
        -- HBA main interface: output ports
        wdata_full : out std_logic;
        rdata : out std_logic_vector(15 downto 0);
        rdata_empty : out std_logic;
        cmd_failed : out std_logic;
        cmd_success : out std_logic;
    
        -- HBA additional reporting signals
        link_initialized : out std_logic;
        link_gen : out std_logic_vector(1 downto 0);
    
        -- HBA NCQ extension
        ncq_wtag : in std_logic_vector(4 downto 0);
        ncq_rtag : out std_logic_vector(4 downto 0);
        ncq_idle : out std_logic;
        ncq_relinquish : out std_logic;
        ncq_ready_for_wdata : out std_logic;
        ncq_SActive : out std_logic_vector(31 downto 0);
        ncq_SActive_valid : out std_logic
        );
    end component;
    
     -- clock and reset
     signal hard_reset : std_logic := '0'; -- Active high, reset button pushed on board
     signal soft_reset : std_logic := '0'; -- Active high, reset button or soft reset
     signal DRPCLK_IN_N : std_logic;
     signal DRPCLK_IN_P : std_logic;
     signal SYSCLK_IN_P : std_logic;
     signal SYSCLK_IN_N : std_logic;
     
     constant sys_clk_period : time := 17 ns;
     constant gtp_clk_period : time := 7 ns;
           
     signal Q0_CLK1_GTREFCLK_PAD_P_IN : std_logic;
     signal Q0_CLK1_GTREFCLK_PAD_N_IN : std_logic;
     signal RXP0_IN : std_logic := '0';-- // Receiver input
     signal RXN0_IN : std_logic := '0';--, // Receiver input
     signal TXP0_OUT : std_logic;--, // Transceiver output
     signal TXN0_OUT : std_logic;--, // Transceiver output
        
            -- HBA main interface: input ports
     signal cmd : std_logic_vector(2 downto 0) := (others=>'0');
     signal cmd_en : std_logic := '0';
     signal lba : std_logic_vector(47 downto 0) := (others=>'0');
     signal sectorcnt : std_logic_vector(15 downto 0) := (others=>'0');
     signal wdata : std_logic_vector(15 downto 0) := (others=>'0');
     signal wdata_en : std_logic := '0';
     signal rdata_next : std_logic := '0'; 
        
            -- HBA main interface: output ports
     signal wdata_full : std_logic;
     signal rdata : std_logic_vector(15 downto 0);
     signal rdata_empty : std_logic;
     signal cmd_failed : std_logic;
     signal cmd_success : std_logic;
        
            -- HBA additional reporting signals
     signal link_initialized : std_logic;
     signal link_gen : std_logic_vector(1 downto 0);
        
            -- HBA NCQ extension
     signal ncq_wtag : std_logic_vector(4 downto 0);
     signal ncq_rtag : std_logic_vector(4 downto 0);
     signal ncq_idle : std_logic;
     signal ncq_relinquish : std_logic;
     signal ncq_ready_for_wdata : std_logic;
     signal ncq_SActive : std_logic_vector(31 downto 0);
     signal ncq_SActive_valid : std_logic;

begin

    hba_0 : \HBA\
    port map (
    
            -- clock and reset
            hard_reset => hard_reset,
            soft_reset => soft_reset,
            DRPCLK_IN_N => DRPCLK_IN_N,
            DRPCLK_IN_P => DRPCLK_IN_P,
            SYSCLK_IN_P => SYSCLK_IN_P,
            SYSCLK_IN_N => SYSCLK_IN_N,
            gtp_refclk => open,
            sata_sys_clock => open,
           
            -- ports that connect to I/O pins of the FPGA
            Q0_CLK1_GTREFCLK_PAD_N_IN => Q0_CLK1_GTREFCLK_PAD_N_IN,
            Q0_CLK1_GTREFCLK_PAD_P_IN => Q0_CLK1_GTREFCLK_PAD_P_IN,
            RXP0_IN => RXP0_IN,
            RXN0_IN => RXN0_IN,
            TXP0_OUT => TXP0_OUT,
            TXN0_OUT => TXN0_OUT,
        
            -- HBA main interface: input ports
            cmd => cmd,
            cmd_en => cmd_en,
            lba => lba,
            sectorcnt => sectorcnt,
            wdata => wdata,
            wdata_en => wdata_en,
            rdata_next => rdata_next, 
        
            -- HBA main interface: output ports
            wdata_full => wdata_full,
            rdata => rdata,
            rdata_empty => rdata_empty,
            cmd_failed => cmd_failed,
            cmd_success => cmd_success,
        
            -- HBA additional reporting signals
            link_initialized => link_initialized,
            link_gen => link_gen,
        
            -- HBA NCQ extension
            ncq_wtag => ncq_wtag,
            ncq_rtag => ncq_rtag,
            ncq_idle => ncq_idle,
            ncq_relinquish => ncq_relinquish,
            ncq_ready_for_wdata => ncq_ready_for_wdata,
            ncq_SActive => ncq_SActive,
            ncq_SActive_valid => ncq_SActive_valid
       
        
    );
    
    sys_clk_procsss : process
    begin
        SYSCLK_IN_P <= '0';
        SYSCLK_IN_N <= '1';
        wait for sys_clk_period/2;
        SYSCLK_IN_P <= '1';
        SYSCLK_IN_N <= '0';
        wait for sys_clk_period/2;
    end process;
    
    gtp_clk_process : process
    begin
        Q0_CLK1_GTREFCLK_PAD_P_IN <= '0';
        Q0_CLK1_GTREFCLK_PAD_N_IN <= '1';
        wait for gtp_clk_period/2;
        Q0_CLK1_GTREFCLK_PAD_P_IN <= '1';
        Q0_CLK1_GTREFCLK_PAD_N_IN <= '0';
        wait for gtp_clk_period/2;
    end process;
    
    stim_proc : process
    begin
        wait for 10 ns;
        hard_reset <= '1';
        wait for 2 ns;
        hard_reset <= '0';
        wait for 50 ns;
        wait;
    end process;
    
end Behavioral;
