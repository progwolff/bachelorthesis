// Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2014.4 (win64) Build 1071353 Tue Nov 18 18:29:27 MST 2014
// Date        : Tue Mar 31 16:52:35 2015
// Host        : Titow running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.srcs/sources_1/ip/gtp_sata/gtp_sata_stub.v
// Design      : gtp_sata
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a200tfbg676-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "gtp_sata,gtwizard_v3_4,{protocol_file=sata2}" *)
module gtp_sata(SOFT_RESET_IN, DONT_RESET_ON_DATA_ERROR_IN, Q0_CLK1_GTREFCLK_PAD_N_IN, Q0_CLK1_GTREFCLK_PAD_P_IN, GT0_TX_FSM_RESET_DONE_OUT, GT0_RX_FSM_RESET_DONE_OUT, GT0_DATA_VALID_IN, GT0_TXUSRCLK_OUT, GT0_TXUSRCLK2_OUT, GT0_RXUSRCLK_OUT, GT0_RXUSRCLK2_OUT, gt0_drpaddr_in, gt0_drpdi_in, gt0_drpdo_out, gt0_drpen_in, gt0_drprdy_out, gt0_drpwe_in, gt0_eyescanreset_in, gt0_rxuserrdy_in, gt0_eyescandataerror_out, gt0_eyescantrigger_in, gt0_rxdata_out, gt0_rxcharisk_out, gt0_rxdisperr_out, gt0_rxnotintable_out, gt0_gtprxn_in, gt0_gtprxp_in, gt0_rxstatus_out, gt0_rxbyteisaligned_out, gt0_dmonitorout_out, gt0_rxlpmhfhold_in, gt0_rxlpmhfovrden_in, gt0_rxlpmlfhold_in, gt0_gtrxreset_in, gt0_rxlpmreset_in, gt0_rxcomsasdet_out, gt0_rxcomwakedet_out, gt0_rxcominitdet_out, gt0_rxelecidle_out, gt0_rxresetdone_out, gt0_gttxreset_in, gt0_txuserrdy_in, gt0_txdata_in, gt0_txelecidle_in, gt0_txcharisk_in, gt0_gtptxn_out, gt0_gtptxp_out, gt0_txoutclkfabric_out, gt0_txoutclkpcs_out, gt0_txresetdone_out, gt0_txcomfinish_out, gt0_txcominit_in, gt0_txcomsas_in, gt0_txcomwake_in, GT0_PLL0RESET_OUT, GT0_PLL0OUTCLK_OUT, GT0_PLL0OUTREFCLK_OUT, GT0_PLL0LOCK_OUT, GT0_PLL0REFCLKLOST_OUT, GT0_PLL1OUTCLK_OUT, GT0_PLL1OUTREFCLK_OUT, DRP_CLK_O, SYSCLK_IN_P, SYSCLK_IN_N)
/* synthesis syn_black_box black_box_pad_pin="SOFT_RESET_IN,DONT_RESET_ON_DATA_ERROR_IN,Q0_CLK1_GTREFCLK_PAD_N_IN,Q0_CLK1_GTREFCLK_PAD_P_IN,GT0_TX_FSM_RESET_DONE_OUT,GT0_RX_FSM_RESET_DONE_OUT,GT0_DATA_VALID_IN,GT0_TXUSRCLK_OUT,GT0_TXUSRCLK2_OUT,GT0_RXUSRCLK_OUT,GT0_RXUSRCLK2_OUT,gt0_drpaddr_in[8:0],gt0_drpdi_in[15:0],gt0_drpdo_out[15:0],gt0_drpen_in,gt0_drprdy_out,gt0_drpwe_in,gt0_eyescanreset_in,gt0_rxuserrdy_in,gt0_eyescandataerror_out,gt0_eyescantrigger_in,gt0_rxdata_out[15:0],gt0_rxcharisk_out[1:0],gt0_rxdisperr_out[1:0],gt0_rxnotintable_out[1:0],gt0_gtprxn_in,gt0_gtprxp_in,gt0_rxstatus_out[2:0],gt0_rxbyteisaligned_out,gt0_dmonitorout_out[14:0],gt0_rxlpmhfhold_in,gt0_rxlpmhfovrden_in,gt0_rxlpmlfhold_in,gt0_gtrxreset_in,gt0_rxlpmreset_in,gt0_rxcomsasdet_out,gt0_rxcomwakedet_out,gt0_rxcominitdet_out,gt0_rxelecidle_out,gt0_rxresetdone_out,gt0_gttxreset_in,gt0_txuserrdy_in,gt0_txdata_in[15:0],gt0_txelecidle_in,gt0_txcharisk_in[1:0],gt0_gtptxn_out,gt0_gtptxp_out,gt0_txoutclkfabric_out,gt0_txoutclkpcs_out,gt0_txresetdone_out,gt0_txcomfinish_out,gt0_txcominit_in,gt0_txcomsas_in,gt0_txcomwake_in,GT0_PLL0RESET_OUT,GT0_PLL0OUTCLK_OUT,GT0_PLL0OUTREFCLK_OUT,GT0_PLL0LOCK_OUT,GT0_PLL0REFCLKLOST_OUT,GT0_PLL1OUTCLK_OUT,GT0_PLL1OUTREFCLK_OUT,DRP_CLK_O,SYSCLK_IN_P,SYSCLK_IN_N" */;
  input SOFT_RESET_IN;
  input DONT_RESET_ON_DATA_ERROR_IN;
  input Q0_CLK1_GTREFCLK_PAD_N_IN;
  input Q0_CLK1_GTREFCLK_PAD_P_IN;
  output GT0_TX_FSM_RESET_DONE_OUT;
  output GT0_RX_FSM_RESET_DONE_OUT;
  input GT0_DATA_VALID_IN;
  output GT0_TXUSRCLK_OUT;
  output GT0_TXUSRCLK2_OUT;
  output GT0_RXUSRCLK_OUT;
  output GT0_RXUSRCLK2_OUT;
  input [8:0]gt0_drpaddr_in;
  input [15:0]gt0_drpdi_in;
  output [15:0]gt0_drpdo_out;
  input gt0_drpen_in;
  output gt0_drprdy_out;
  input gt0_drpwe_in;
  input gt0_eyescanreset_in;
  input gt0_rxuserrdy_in;
  output gt0_eyescandataerror_out;
  input gt0_eyescantrigger_in;
  output [15:0]gt0_rxdata_out;
  output [1:0]gt0_rxcharisk_out;
  output [1:0]gt0_rxdisperr_out;
  output [1:0]gt0_rxnotintable_out;
  input gt0_gtprxn_in;
  input gt0_gtprxp_in;
  output [2:0]gt0_rxstatus_out;
  output gt0_rxbyteisaligned_out;
  output [14:0]gt0_dmonitorout_out;
  input gt0_rxlpmhfhold_in;
  input gt0_rxlpmhfovrden_in;
  input gt0_rxlpmlfhold_in;
  input gt0_gtrxreset_in;
  input gt0_rxlpmreset_in;
  output gt0_rxcomsasdet_out;
  output gt0_rxcomwakedet_out;
  output gt0_rxcominitdet_out;
  output gt0_rxelecidle_out;
  output gt0_rxresetdone_out;
  input gt0_gttxreset_in;
  input gt0_txuserrdy_in;
  input [15:0]gt0_txdata_in;
  input gt0_txelecidle_in;
  input [1:0]gt0_txcharisk_in;
  output gt0_gtptxn_out;
  output gt0_gtptxp_out;
  output gt0_txoutclkfabric_out;
  output gt0_txoutclkpcs_out;
  output gt0_txresetdone_out;
  output gt0_txcomfinish_out;
  input gt0_txcominit_in;
  input gt0_txcomsas_in;
  input gt0_txcomwake_in;
  output GT0_PLL0RESET_OUT;
  output GT0_PLL0OUTCLK_OUT;
  output GT0_PLL0OUTREFCLK_OUT;
  output GT0_PLL0LOCK_OUT;
  output GT0_PLL0REFCLKLOST_OUT;
  output GT0_PLL1OUTCLK_OUT;
  output GT0_PLL1OUTREFCLK_OUT;
  output DRP_CLK_O;
  input SYSCLK_IN_P;
  input SYSCLK_IN_N;
endmodule
