// Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2014.4 (win64) Build 1071353 Tue Nov 18 18:29:27 MST 2014
// Date        : Tue Mar 31 16:52:35 2015
// Host        : Titow running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               C:/Users/wolff/Desktop/bachelorthesis/SATA/SATA.srcs/sources_1/ip/gtp_sata/gtp_sata_funcsim.v
// Design      : gtp_sata
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tfbg676-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* x_core_info = "gtp_sata,gtwizard_v3_4,{protocol_file=sata2}" *) (* core_generation_info = "gtp_sata,gtwizard_v3_4,{protocol_file=sata2}" *) 
(* NotValidForBitStream *)
module gtp_sata
   (SOFT_RESET_IN,
    DONT_RESET_ON_DATA_ERROR_IN,
    Q0_CLK1_GTREFCLK_PAD_N_IN,
    Q0_CLK1_GTREFCLK_PAD_P_IN,
    GT0_TX_FSM_RESET_DONE_OUT,
    GT0_RX_FSM_RESET_DONE_OUT,
    GT0_DATA_VALID_IN,
    GT0_TXUSRCLK_OUT,
    GT0_TXUSRCLK2_OUT,
    GT0_RXUSRCLK_OUT,
    GT0_RXUSRCLK2_OUT,
    gt0_drpaddr_in,
    gt0_drpdi_in,
    gt0_drpdo_out,
    gt0_drpen_in,
    gt0_drprdy_out,
    gt0_drpwe_in,
    gt0_eyescanreset_in,
    gt0_rxuserrdy_in,
    gt0_eyescandataerror_out,
    gt0_eyescantrigger_in,
    gt0_rxdata_out,
    gt0_rxcharisk_out,
    gt0_rxdisperr_out,
    gt0_rxnotintable_out,
    gt0_gtprxn_in,
    gt0_gtprxp_in,
    gt0_rxstatus_out,
    gt0_rxbyteisaligned_out,
    gt0_dmonitorout_out,
    gt0_rxlpmhfhold_in,
    gt0_rxlpmhfovrden_in,
    gt0_rxlpmlfhold_in,
    gt0_gtrxreset_in,
    gt0_rxlpmreset_in,
    gt0_rxcomsasdet_out,
    gt0_rxcomwakedet_out,
    gt0_rxcominitdet_out,
    gt0_rxelecidle_out,
    gt0_rxresetdone_out,
    gt0_gttxreset_in,
    gt0_txuserrdy_in,
    gt0_txdata_in,
    gt0_txelecidle_in,
    gt0_txcharisk_in,
    gt0_gtptxn_out,
    gt0_gtptxp_out,
    gt0_txoutclkfabric_out,
    gt0_txoutclkpcs_out,
    gt0_txresetdone_out,
    gt0_txcomfinish_out,
    gt0_txcominit_in,
    gt0_txcomsas_in,
    gt0_txcomwake_in,
    GT0_PLL0RESET_OUT,
    GT0_PLL0OUTCLK_OUT,
    GT0_PLL0OUTREFCLK_OUT,
    GT0_PLL0LOCK_OUT,
    GT0_PLL0REFCLKLOST_OUT,
    GT0_PLL1OUTCLK_OUT,
    GT0_PLL1OUTREFCLK_OUT,
    DRP_CLK_O,
    SYSCLK_IN_P,
    SYSCLK_IN_N);
  input SOFT_RESET_IN;
  input DONT_RESET_ON_DATA_ERROR_IN;
  input Q0_CLK1_GTREFCLK_PAD_N_IN;
  input Q0_CLK1_GTREFCLK_PAD_P_IN;
  output GT0_TX_FSM_RESET_DONE_OUT;
  output GT0_RX_FSM_RESET_DONE_OUT;
  input GT0_DATA_VALID_IN;
  output GT0_TXUSRCLK_OUT;
  output GT0_TXUSRCLK2_OUT;
  output GT0_RXUSRCLK_OUT;
  output GT0_RXUSRCLK2_OUT;
  input [8:0]gt0_drpaddr_in;
  input [15:0]gt0_drpdi_in;
  output [15:0]gt0_drpdo_out;
  input gt0_drpen_in;
  output gt0_drprdy_out;
  input gt0_drpwe_in;
  input gt0_eyescanreset_in;
  input gt0_rxuserrdy_in;
  output gt0_eyescandataerror_out;
  input gt0_eyescantrigger_in;
  output [15:0]gt0_rxdata_out;
  output [1:0]gt0_rxcharisk_out;
  output [1:0]gt0_rxdisperr_out;
  output [1:0]gt0_rxnotintable_out;
  input gt0_gtprxn_in;
  input gt0_gtprxp_in;
  output [2:0]gt0_rxstatus_out;
  output gt0_rxbyteisaligned_out;
  output [14:0]gt0_dmonitorout_out;
  input gt0_rxlpmhfhold_in;
  input gt0_rxlpmhfovrden_in;
  input gt0_rxlpmlfhold_in;
  input gt0_gtrxreset_in;
  input gt0_rxlpmreset_in;
  output gt0_rxcomsasdet_out;
  output gt0_rxcomwakedet_out;
  output gt0_rxcominitdet_out;
  output gt0_rxelecidle_out;
  output gt0_rxresetdone_out;
  input gt0_gttxreset_in;
  input gt0_txuserrdy_in;
  input [15:0]gt0_txdata_in;
  input gt0_txelecidle_in;
  input [1:0]gt0_txcharisk_in;
  output gt0_gtptxn_out;
  output gt0_gtptxp_out;
  output gt0_txoutclkfabric_out;
  output gt0_txoutclkpcs_out;
  output gt0_txresetdone_out;
  output gt0_txcomfinish_out;
  input gt0_txcominit_in;
  input gt0_txcomsas_in;
  input gt0_txcomwake_in;
  output GT0_PLL0RESET_OUT;
  output GT0_PLL0OUTCLK_OUT;
  output GT0_PLL0OUTREFCLK_OUT;
  output GT0_PLL0LOCK_OUT;
  output GT0_PLL0REFCLKLOST_OUT;
  output GT0_PLL1OUTCLK_OUT;
  output GT0_PLL1OUTREFCLK_OUT;
  output DRP_CLK_O;
  input SYSCLK_IN_P;
  input SYSCLK_IN_N;

  wire DONT_RESET_ON_DATA_ERROR_IN;
  wire DRP_CLK_O;
  wire GT0_DATA_VALID_IN;
  wire GT0_PLL0LOCK_OUT;
  wire GT0_PLL0OUTCLK_OUT;
  wire GT0_PLL0OUTREFCLK_OUT;
  wire GT0_PLL0REFCLKLOST_OUT;
  wire GT0_PLL0RESET_OUT;
  wire GT0_PLL1OUTCLK_OUT;
  wire GT0_PLL1OUTREFCLK_OUT;
  wire GT0_RXUSRCLK2_OUT;
  wire GT0_RX_FSM_RESET_DONE_OUT;
  wire GT0_TX_FSM_RESET_DONE_OUT;
  wire Q0_CLK1_GTREFCLK_PAD_N_IN;
  wire Q0_CLK1_GTREFCLK_PAD_P_IN;
  wire SOFT_RESET_IN;
(* DIFF_TERM=0 *) (* IBUF_LOW_PWR *)   wire SYSCLK_IN_N;
(* DIFF_TERM=0 *) (* IBUF_LOW_PWR *)   wire SYSCLK_IN_P;
  wire [14:0]gt0_dmonitorout_out;
  wire [8:0]gt0_drpaddr_in;
  wire [15:0]gt0_drpdi_in;
  wire [15:0]gt0_drpdo_out;
  wire gt0_drpen_in;
  wire gt0_drprdy_out;
  wire gt0_drpwe_in;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire gt0_gtprxn_in;
  wire gt0_gtprxp_in;
  wire gt0_gtptxn_out;
  wire gt0_gtptxp_out;
  wire gt0_rxbyteisaligned_out;
  wire [1:0]gt0_rxcharisk_out;
  wire gt0_rxcominitdet_out;
  wire gt0_rxcomsasdet_out;
  wire gt0_rxcomwakedet_out;
  wire [15:0]gt0_rxdata_out;
  wire [1:0]gt0_rxdisperr_out;
  wire gt0_rxelecidle_out;
  wire gt0_rxlpmhfhold_in;
  wire gt0_rxlpmhfovrden_in;
  wire gt0_rxlpmlfhold_in;
  wire gt0_rxlpmreset_in;
  wire [1:0]gt0_rxnotintable_out;
  wire gt0_rxresetdone_out;
  wire [2:0]gt0_rxstatus_out;
  wire [1:0]gt0_txcharisk_in;
  wire gt0_txcomfinish_out;
  wire gt0_txcominit_in;
  wire gt0_txcomsas_in;
  wire gt0_txcomwake_in;
  wire [15:0]gt0_txdata_in;
  wire gt0_txelecidle_in;
  wire gt0_txoutclkfabric_out;
  wire gt0_txoutclkpcs_out;
  wire gt0_txresetdone_out;

  assign GT0_RXUSRCLK_OUT = GT0_RXUSRCLK2_OUT;
  assign GT0_TXUSRCLK2_OUT = GT0_RXUSRCLK2_OUT;
  assign GT0_TXUSRCLK_OUT = GT0_RXUSRCLK2_OUT;
gtp_sata_gtp_sata_support__parameterized0 U0
       (.DONT_RESET_ON_DATA_ERROR_IN(DONT_RESET_ON_DATA_ERROR_IN),
        .DRP_CLK_O(DRP_CLK_O),
        .GT0_DATA_VALID_IN(GT0_DATA_VALID_IN),
        .GT0_PLL0LOCK_OUT(GT0_PLL0LOCK_OUT),
        .GT0_PLL0OUTCLK_OUT(GT0_PLL0OUTCLK_OUT),
        .GT0_PLL0OUTREFCLK_OUT(GT0_PLL0OUTREFCLK_OUT),
        .GT0_PLL0REFCLKLOST_OUT(GT0_PLL0REFCLKLOST_OUT),
        .GT0_PLL0RESET_OUT(GT0_PLL0RESET_OUT),
        .GT0_PLL1OUTCLK_OUT(GT0_PLL1OUTCLK_OUT),
        .GT0_PLL1OUTREFCLK_OUT(GT0_PLL1OUTREFCLK_OUT),
        .GT0_RXUSRCLK2_OUT(GT0_RXUSRCLK2_OUT),
        .GT0_RX_FSM_RESET_DONE_OUT(GT0_RX_FSM_RESET_DONE_OUT),
        .GT0_TX_FSM_RESET_DONE_OUT(GT0_TX_FSM_RESET_DONE_OUT),
        .Q0_CLK1_GTREFCLK_PAD_N_IN(Q0_CLK1_GTREFCLK_PAD_N_IN),
        .Q0_CLK1_GTREFCLK_PAD_P_IN(Q0_CLK1_GTREFCLK_PAD_P_IN),
        .SOFT_RESET_IN(SOFT_RESET_IN),
        .SYSCLK_IN_N(SYSCLK_IN_N),
        .SYSCLK_IN_P(SYSCLK_IN_P),
        .gt0_dmonitorout_out(gt0_dmonitorout_out),
        .gt0_drpaddr_in(gt0_drpaddr_in),
        .gt0_drpdi_in(gt0_drpdi_in),
        .gt0_drpdo_out(gt0_drpdo_out),
        .gt0_drpen_in(gt0_drpen_in),
        .gt0_drprdy_out(gt0_drprdy_out),
        .gt0_drpwe_in(gt0_drpwe_in),
        .gt0_eyescandataerror_out(gt0_eyescandataerror_out),
        .gt0_eyescanreset_in(gt0_eyescanreset_in),
        .gt0_eyescantrigger_in(gt0_eyescantrigger_in),
        .gt0_gtprxn_in(gt0_gtprxn_in),
        .gt0_gtprxp_in(gt0_gtprxp_in),
        .gt0_gtptxn_out(gt0_gtptxn_out),
        .gt0_gtptxp_out(gt0_gtptxp_out),
        .gt0_rxbyteisaligned_out(gt0_rxbyteisaligned_out),
        .gt0_rxcharisk_out(gt0_rxcharisk_out),
        .gt0_rxcominitdet_out(gt0_rxcominitdet_out),
        .gt0_rxcomsasdet_out(gt0_rxcomsasdet_out),
        .gt0_rxcomwakedet_out(gt0_rxcomwakedet_out),
        .gt0_rxdata_out(gt0_rxdata_out),
        .gt0_rxdisperr_out(gt0_rxdisperr_out),
        .gt0_rxelecidle_out(gt0_rxelecidle_out),
        .gt0_rxlpmhfhold_in(gt0_rxlpmhfhold_in),
        .gt0_rxlpmhfovrden_in(gt0_rxlpmhfovrden_in),
        .gt0_rxlpmlfhold_in(gt0_rxlpmlfhold_in),
        .gt0_rxlpmreset_in(gt0_rxlpmreset_in),
        .gt0_rxnotintable_out(gt0_rxnotintable_out),
        .gt0_rxresetdone_out(gt0_rxresetdone_out),
        .gt0_rxstatus_out(gt0_rxstatus_out),
        .gt0_txcharisk_in(gt0_txcharisk_in),
        .gt0_txcomfinish_out(gt0_txcomfinish_out),
        .gt0_txcominit_in(gt0_txcominit_in),
        .gt0_txcomsas_in(gt0_txcomsas_in),
        .gt0_txcomwake_in(gt0_txcomwake_in),
        .gt0_txdata_in(gt0_txdata_in),
        .gt0_txelecidle_in(gt0_txelecidle_in),
        .gt0_txoutclkfabric_out(gt0_txoutclkfabric_out),
        .gt0_txoutclkpcs_out(gt0_txoutclkpcs_out),
        .gt0_txresetdone_out(gt0_txresetdone_out));
endmodule

(* ORIG_REF_NAME = "gtp_sata_GT_USRCLK_SOURCE" *) 
module gtp_sata_gtp_sata_GT_USRCLK_SOURCE
   (Q0_CLK1_GTREFCLK_OUT,
    DRPCLK_OUT,
    GT0_TXUSRCLK_OUT,
    Q0_CLK1_GTREFCLK_PAD_P_IN,
    Q0_CLK1_GTREFCLK_PAD_N_IN,
    SYSCLK_IN_P,
    SYSCLK_IN_N,
    GT0_TXOUTCLK_IN);
  output Q0_CLK1_GTREFCLK_OUT;
  output DRPCLK_OUT;
  output GT0_TXUSRCLK_OUT;
  input Q0_CLK1_GTREFCLK_PAD_P_IN;
  input Q0_CLK1_GTREFCLK_PAD_N_IN;
  input SYSCLK_IN_P;
  input SYSCLK_IN_N;
  input GT0_TXOUTCLK_IN;

  wire DRPCLK_IN;
  wire DRPCLK_OUT;
  wire GT0_TXOUTCLK_IN;
  wire GT0_TXUSRCLK_OUT;
  wire Q0_CLK1_GTREFCLK_OUT;
  wire Q0_CLK1_GTREFCLK_PAD_N_IN;
  wire Q0_CLK1_GTREFCLK_PAD_P_IN;
(* DIFF_TERM=0 *) (* IBUF_LOW_PWR *)   wire SYSCLK_IN_N;
(* DIFF_TERM=0 *) (* IBUF_LOW_PWR *)   wire SYSCLK_IN_P;
  wire NLW_ibufds_instq0_clk1_ODIV2_UNCONNECTED;

(* box_type = "PRIMITIVE" *) 
   BUFG DRP_CLK_BUFG
       (.I(DRPCLK_IN),
        .O(DRPCLK_OUT));
(* CAPACITANCE = "DONT_CARE" *) 
   (* IBUF_DELAY_VALUE = "0" *) 
   (* IFD_DELAY_VALUE = "AUTO" *) 
   (* box_type = "PRIMITIVE" *) 
   IBUFDS #(
    .DQS_BIAS("FALSE"),
    .IOSTANDARD("DEFAULT")) 
     IBUFDS_DRP_CLK
       (.I(SYSCLK_IN_P),
        .IB(SYSCLK_IN_N),
        .O(DRPCLK_IN));
(* box_type = "PRIMITIVE" *) 
   IBUFDS_GTE2 #(
    .CLKCM_CFG("TRUE"),
    .CLKRCV_TRST("TRUE"),
    .CLKSWING_CFG(2'b11)) 
     ibufds_instq0_clk1
       (.CEB(1'b0),
        .I(Q0_CLK1_GTREFCLK_PAD_P_IN),
        .IB(Q0_CLK1_GTREFCLK_PAD_N_IN),
        .O(Q0_CLK1_GTREFCLK_OUT),
        .ODIV2(NLW_ibufds_instq0_clk1_ODIV2_UNCONNECTED));
(* box_type = "PRIMITIVE" *) 
   BUFG txoutclk_bufg0_i
       (.I(GT0_TXOUTCLK_IN),
        .O(GT0_TXUSRCLK_OUT));
endmodule

(* ORIG_REF_NAME = "gtp_sata_GT" *) 
module gtp_sata_gtp_sata_GT__parameterized0
   (O1,
    gt0_eyescandataerror_out,
    gt0_gtptxn_out,
    gt0_gtptxp_out,
    gt0_rxbyteisaligned_out,
    gt0_rxcominitdet_out,
    gt0_rxcomsasdet_out,
    gt0_rxcomwakedet_out,
    gt0_rxelecidle_out,
    gt0_rxresetdone_out,
    gt0_txcomfinish_out,
    gt0_txoutclk_out,
    gt0_txoutclkfabric_out,
    gt0_txoutclkpcs_out,
    gt0_txresetdone_out,
    gt0_dmonitorout_out,
    gt0_drpdo_out,
    gt0_rxstatus_out,
    gt0_rxdata_out,
    gt0_rxcharisk_out,
    gt0_rxdisperr_out,
    gt0_rxnotintable_out,
    CLK,
    gt0_eyescanreset_in,
    gt0_eyescantrigger_in,
    gt0_gtprxn_in,
    gt0_gtprxp_in,
    GTTXRESET,
    GT0_PLL0OUTCLK_OUT,
    GT0_PLL0OUTREFCLK_OUT,
    GT0_PLL1OUTCLK_OUT,
    GT0_PLL1OUTREFCLK_OUT,
    gt0_rxlpmhfhold_in,
    gt0_rxlpmhfovrden_in,
    gt0_rxlpmlfhold_in,
    gt0_rxlpmreset_in,
    RXUSERRDY,
    I1,
    gt0_txcominit_in,
    gt0_txcomsas_in,
    gt0_txcomwake_in,
    gt0_txelecidle_in,
    TXUSERRDY,
    gt0_txdata_in,
    gt0_txcharisk_in,
    SR,
    I2,
    gt0_drpen_in,
    gt0_drpdi_in,
    gt0_drpwe_in,
    gt0_drpaddr_in);
  output O1;
  output gt0_eyescandataerror_out;
  output gt0_gtptxn_out;
  output gt0_gtptxp_out;
  output gt0_rxbyteisaligned_out;
  output gt0_rxcominitdet_out;
  output gt0_rxcomsasdet_out;
  output gt0_rxcomwakedet_out;
  output gt0_rxelecidle_out;
  output gt0_rxresetdone_out;
  output gt0_txcomfinish_out;
  output gt0_txoutclk_out;
  output gt0_txoutclkfabric_out;
  output gt0_txoutclkpcs_out;
  output gt0_txresetdone_out;
  output [14:0]gt0_dmonitorout_out;
  output [15:0]gt0_drpdo_out;
  output [2:0]gt0_rxstatus_out;
  output [15:0]gt0_rxdata_out;
  output [1:0]gt0_rxcharisk_out;
  output [1:0]gt0_rxdisperr_out;
  output [1:0]gt0_rxnotintable_out;
  input CLK;
  input gt0_eyescanreset_in;
  input gt0_eyescantrigger_in;
  input gt0_gtprxn_in;
  input gt0_gtprxp_in;
  input GTTXRESET;
  input GT0_PLL0OUTCLK_OUT;
  input GT0_PLL0OUTREFCLK_OUT;
  input GT0_PLL1OUTCLK_OUT;
  input GT0_PLL1OUTREFCLK_OUT;
  input gt0_rxlpmhfhold_in;
  input gt0_rxlpmhfovrden_in;
  input gt0_rxlpmlfhold_in;
  input gt0_rxlpmreset_in;
  input RXUSERRDY;
  input I1;
  input gt0_txcominit_in;
  input gt0_txcomsas_in;
  input gt0_txcomwake_in;
  input gt0_txelecidle_in;
  input TXUSERRDY;
  input [15:0]gt0_txdata_in;
  input [1:0]gt0_txcharisk_in;
  input [0:0]SR;
  input I2;
  input gt0_drpen_in;
  input [15:0]gt0_drpdi_in;
  input gt0_drpwe_in;
  input [8:0]gt0_drpaddr_in;

  wire CLK;
  wire GT0_PLL0OUTCLK_OUT;
  wire GT0_PLL0OUTREFCLK_OUT;
  wire GT0_PLL1OUTCLK_OUT;
  wire GT0_PLL1OUTREFCLK_OUT;
  wire GTRXRESET_OUT;
  wire GTTXRESET;
  wire I1;
  wire I2;
  wire O1;
  wire RXPMARESETDONE;
  wire RXUSERRDY;
  wire [0:0]SR;
  wire TXPMARESETDONE;
  wire TXUSERRDY;
  wire [14:0]gt0_dmonitorout_out;
  wire [8:0]gt0_drpaddr_in;
  wire [15:0]gt0_drpdi_in;
  wire [15:0]gt0_drpdo_out;
  wire gt0_drpen_in;
  wire gt0_drpwe_in;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire gt0_gtprxn_in;
  wire gt0_gtprxp_in;
  wire gt0_gtptxn_out;
  wire gt0_gtptxp_out;
  wire gt0_rxbyteisaligned_out;
  wire [1:0]gt0_rxcharisk_out;
  wire gt0_rxcominitdet_out;
  wire gt0_rxcomsasdet_out;
  wire gt0_rxcomwakedet_out;
  wire [15:0]gt0_rxdata_out;
  wire [1:0]gt0_rxdisperr_out;
  wire gt0_rxelecidle_out;
  wire gt0_rxlpmhfhold_in;
  wire gt0_rxlpmhfovrden_in;
  wire gt0_rxlpmlfhold_in;
  wire gt0_rxlpmreset_in;
  wire [1:0]gt0_rxnotintable_out;
  wire gt0_rxresetdone_out;
  wire [2:0]gt0_rxstatus_out;
  wire [1:0]gt0_txcharisk_in;
  wire gt0_txcomfinish_out;
  wire gt0_txcominit_in;
  wire gt0_txcomsas_in;
  wire gt0_txcomwake_in;
  wire [15:0]gt0_txdata_in;
  wire gt0_txelecidle_in;
  wire gt0_txoutclk_out;
  wire gt0_txoutclkfabric_out;
  wire gt0_txoutclkpcs_out;
  wire gt0_txresetdone_out;
  wire n_10_gtrxreset_seq_i;
  wire n_11_gtrxreset_seq_i;
  wire n_12_gtrxreset_seq_i;
  wire n_13_gtrxreset_seq_i;
  wire n_14_gtrxreset_seq_i;
  wire n_15_gtrxreset_seq_i;
  wire n_16_gtrxreset_seq_i;
  wire n_17_gtrxreset_seq_i;
  wire n_18_gtrxreset_seq_i;
  wire n_19_gtrxreset_seq_i;
  wire n_1_gtrxreset_seq_i;
  wire n_20_gtrxreset_seq_i;
  wire n_21_gtrxreset_seq_i;
  wire n_22_gtrxreset_seq_i;
  wire n_23_gtrxreset_seq_i;
  wire n_24_gtpe2_i;
  wire n_24_gtrxreset_seq_i;
  wire n_25_gtrxreset_seq_i;
  wire n_26_gtrxreset_seq_i;
  wire n_27_gtrxreset_seq_i;
  wire n_2_gtrxreset_seq_i;
  wire n_3_gtrxreset_seq_i;
  wire n_4_gtrxreset_seq_i;
  wire n_5_gtrxreset_seq_i;
  wire n_6_gtrxreset_seq_i;
  wire n_7_gtrxreset_seq_i;
  wire n_8_gtrxreset_seq_i;
  wire n_9_gtrxreset_seq_i;
  wire NLW_gtpe2_i_PHYSTATUS_UNCONNECTED;
  wire NLW_gtpe2_i_PMARSVDOUT0_UNCONNECTED;
  wire NLW_gtpe2_i_PMARSVDOUT1_UNCONNECTED;
  wire NLW_gtpe2_i_RXBYTEREALIGN_UNCONNECTED;
  wire NLW_gtpe2_i_RXCDRLOCK_UNCONNECTED;
  wire NLW_gtpe2_i_RXCHANBONDSEQ_UNCONNECTED;
  wire NLW_gtpe2_i_RXCHANISALIGNED_UNCONNECTED;
  wire NLW_gtpe2_i_RXCHANREALIGN_UNCONNECTED;
  wire NLW_gtpe2_i_RXCOMMADET_UNCONNECTED;
  wire NLW_gtpe2_i_RXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtpe2_i_RXHEADERVALID_UNCONNECTED;
  wire NLW_gtpe2_i_RXOSINTDONE_UNCONNECTED;
  wire NLW_gtpe2_i_RXOSINTSTARTED_UNCONNECTED;
  wire NLW_gtpe2_i_RXOSINTSTROBEDONE_UNCONNECTED;
  wire NLW_gtpe2_i_RXOSINTSTROBESTARTED_UNCONNECTED;
  wire NLW_gtpe2_i_RXOUTCLKFABRIC_UNCONNECTED;
  wire NLW_gtpe2_i_RXOUTCLKPCS_UNCONNECTED;
  wire NLW_gtpe2_i_RXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtpe2_i_RXPRBSERR_UNCONNECTED;
  wire NLW_gtpe2_i_RXRATEDONE_UNCONNECTED;
  wire NLW_gtpe2_i_RXSYNCDONE_UNCONNECTED;
  wire NLW_gtpe2_i_RXSYNCOUT_UNCONNECTED;
  wire NLW_gtpe2_i_RXVALID_UNCONNECTED;
  wire NLW_gtpe2_i_TXDLYSRESETDONE_UNCONNECTED;
  wire NLW_gtpe2_i_TXGEARBOXREADY_UNCONNECTED;
  wire NLW_gtpe2_i_TXPHALIGNDONE_UNCONNECTED;
  wire NLW_gtpe2_i_TXPHINITDONE_UNCONNECTED;
  wire NLW_gtpe2_i_TXRATEDONE_UNCONNECTED;
  wire NLW_gtpe2_i_TXSYNCDONE_UNCONNECTED;
  wire NLW_gtpe2_i_TXSYNCOUT_UNCONNECTED;
  wire [15:0]NLW_gtpe2_i_PCSRSVDOUT_UNCONNECTED;
  wire [2:0]NLW_gtpe2_i_RXBUFSTATUS_UNCONNECTED;
  wire [3:0]NLW_gtpe2_i_RXCHARISCOMMA_UNCONNECTED;
  wire [3:2]NLW_gtpe2_i_RXCHARISK_UNCONNECTED;
  wire [3:0]NLW_gtpe2_i_RXCHBONDO_UNCONNECTED;
  wire [1:0]NLW_gtpe2_i_RXCLKCORCNT_UNCONNECTED;
  wire [31:16]NLW_gtpe2_i_RXDATA_UNCONNECTED;
  wire [1:0]NLW_gtpe2_i_RXDATAVALID_UNCONNECTED;
  wire [3:2]NLW_gtpe2_i_RXDISPERR_UNCONNECTED;
  wire [2:0]NLW_gtpe2_i_RXHEADER_UNCONNECTED;
  wire [3:2]NLW_gtpe2_i_RXNOTINTABLE_UNCONNECTED;
  wire [4:0]NLW_gtpe2_i_RXPHMONITOR_UNCONNECTED;
  wire [4:0]NLW_gtpe2_i_RXPHSLIPMONITOR_UNCONNECTED;
  wire [1:0]NLW_gtpe2_i_RXSTARTOFSEQ_UNCONNECTED;
  wire [1:0]NLW_gtpe2_i_TXBUFSTATUS_UNCONNECTED;

(* box_type = "PRIMITIVE" *) 
   GTPE2_CHANNEL #(
    .ACJTAG_DEBUG_MODE(1'b0),
    .ACJTAG_MODE(1'b0),
    .ACJTAG_RESET(1'b0),
    .ADAPT_CFG0(20'b00000000000000000000),
    .ALIGN_COMMA_DOUBLE("FALSE"),
    .ALIGN_COMMA_ENABLE(10'b1111111111),
    .ALIGN_COMMA_WORD(1),
    .ALIGN_MCOMMA_DET("TRUE"),
    .ALIGN_MCOMMA_VALUE(10'b1010000011),
    .ALIGN_PCOMMA_DET("TRUE"),
    .ALIGN_PCOMMA_VALUE(10'b0101111100),
    .CBCC_DATA_SOURCE_SEL("DECODED"),
    .CFOK_CFG(43'b1001001000000000000000001000000111010000000),
    .CFOK_CFG2(7'b0100000),
    .CFOK_CFG3(7'b0100000),
    .CFOK_CFG4(1'b0),
    .CFOK_CFG5(2'b00),
    .CFOK_CFG6(4'b0000),
    .CHAN_BOND_KEEP_ALIGN("FALSE"),
    .CHAN_BOND_MAX_SKEW(1),
    .CHAN_BOND_SEQ_1_1(10'b0000000000),
    .CHAN_BOND_SEQ_1_2(10'b0000000000),
    .CHAN_BOND_SEQ_1_3(10'b0000000000),
    .CHAN_BOND_SEQ_1_4(10'b0000000000),
    .CHAN_BOND_SEQ_1_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_1(10'b0000000000),
    .CHAN_BOND_SEQ_2_2(10'b0000000000),
    .CHAN_BOND_SEQ_2_3(10'b0000000000),
    .CHAN_BOND_SEQ_2_4(10'b0000000000),
    .CHAN_BOND_SEQ_2_ENABLE(4'b1111),
    .CHAN_BOND_SEQ_2_USE("FALSE"),
    .CHAN_BOND_SEQ_LEN(1),
    .CLK_COMMON_SWING(1'b0),
    .CLK_CORRECT_USE("FALSE"),
    .CLK_COR_KEEP_IDLE("FALSE"),
    .CLK_COR_MAX_LAT(9),
    .CLK_COR_MIN_LAT(7),
    .CLK_COR_PRECEDENCE("TRUE"),
    .CLK_COR_REPEAT_WAIT(0),
    .CLK_COR_SEQ_1_1(10'b0100000000),
    .CLK_COR_SEQ_1_2(10'b0000000000),
    .CLK_COR_SEQ_1_3(10'b0000000000),
    .CLK_COR_SEQ_1_4(10'b0000000000),
    .CLK_COR_SEQ_1_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_1(10'b0100000000),
    .CLK_COR_SEQ_2_2(10'b0000000000),
    .CLK_COR_SEQ_2_3(10'b0000000000),
    .CLK_COR_SEQ_2_4(10'b0000000000),
    .CLK_COR_SEQ_2_ENABLE(4'b1111),
    .CLK_COR_SEQ_2_USE("FALSE"),
    .CLK_COR_SEQ_LEN(1),
    .DEC_MCOMMA_DETECT("TRUE"),
    .DEC_PCOMMA_DETECT("TRUE"),
    .DEC_VALID_COMMA_ONLY("FALSE"),
    .DMONITOR_CFG(24'h000A00),
    .ES_CLK_PHASE_SEL(1'b0),
    .ES_CONTROL(6'b000000),
    .ES_ERRDET_EN("FALSE"),
    .ES_EYE_SCAN_EN("FALSE"),
    .ES_HORZ_OFFSET(12'h010),
    .ES_PMA_CFG(10'b0000000000),
    .ES_PRESCALE(5'b00000),
    .ES_QUALIFIER(80'h00000000000000000000),
    .ES_QUAL_MASK(80'h00000000000000000000),
    .ES_SDATA_MASK(80'h00000000000000000000),
    .ES_VERT_OFFSET(9'b000000000),
    .FTS_DESKEW_SEQ_ENABLE(4'b1111),
    .FTS_LANE_DESKEW_CFG(4'b1111),
    .FTS_LANE_DESKEW_EN("FALSE"),
    .GEARBOX_MODE(3'b000),
    .IS_CLKRSVD0_INVERTED(1'b0),
    .IS_CLKRSVD1_INVERTED(1'b0),
    .IS_DMONITORCLK_INVERTED(1'b0),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_RXUSRCLK2_INVERTED(1'b0),
    .IS_RXUSRCLK_INVERTED(1'b0),
    .IS_SIGVALIDCLK_INVERTED(1'b0),
    .IS_TXPHDLYTSTCLK_INVERTED(1'b0),
    .IS_TXUSRCLK2_INVERTED(1'b0),
    .IS_TXUSRCLK_INVERTED(1'b0),
    .LOOPBACK_CFG(1'b0),
    .OUTREFCLK_SEL_INV(2'b11),
    .PCS_PCIE_EN("FALSE"),
    .PCS_RSVD_ATTR(48'h000000000100),
    .PD_TRANS_TIME_FROM_P2(12'h03C),
    .PD_TRANS_TIME_NONE_P2(8'h3C),
    .PD_TRANS_TIME_TO_P2(8'h64),
    .PMA_LOOPBACK_CFG(1'b0),
    .PMA_RSV(32'h00000333),
    .PMA_RSV2(32'h00002040),
    .PMA_RSV3(2'b00),
    .PMA_RSV4(4'b0000),
    .PMA_RSV5(1'b0),
    .PMA_RSV6(1'b0),
    .PMA_RSV7(1'b0),
    .RXBUFRESET_TIME(5'b00001),
    .RXBUF_ADDR_MODE("FAST"),
    .RXBUF_EIDLE_HI_CNT(4'b1000),
    .RXBUF_EIDLE_LO_CNT(4'b0000),
    .RXBUF_EN("TRUE"),
    .RXBUF_RESET_ON_CB_CHANGE("TRUE"),
    .RXBUF_RESET_ON_COMMAALIGN("FALSE"),
    .RXBUF_RESET_ON_EIDLE("FALSE"),
    .RXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .RXBUF_THRESH_OVFLW(61),
    .RXBUF_THRESH_OVRD("FALSE"),
    .RXBUF_THRESH_UNDFLW(4),
    .RXCDRFREQRESET_TIME(5'b00001),
    .RXCDRPHRESET_TIME(5'b00001),
    .RXCDR_CFG(83'h0000047FE206024481010),
    .RXCDR_FR_RESET_ON_EIDLE(1'b0),
    .RXCDR_HOLD_DURING_EIDLE(1'b0),
    .RXCDR_LOCK_CFG(6'b001001),
    .RXCDR_PH_RESET_ON_EIDLE(1'b0),
    .RXDLY_CFG(16'h001F),
    .RXDLY_LCFG(9'h030),
    .RXDLY_TAP_CFG(16'h0000),
    .RXGEARBOX_EN("FALSE"),
    .RXISCANRESET_TIME(5'b00001),
    .RXLPMRESET_TIME(7'b0001111),
    .RXLPM_BIAS_STARTUP_DISABLE(1'b0),
    .RXLPM_CFG(4'b0110),
    .RXLPM_CFG1(1'b0),
    .RXLPM_CM_CFG(1'b0),
    .RXLPM_GC_CFG(9'b111100010),
    .RXLPM_GC_CFG2(3'b001),
    .RXLPM_HF_CFG(14'b00001111110000),
    .RXLPM_HF_CFG2(5'b01010),
    .RXLPM_HF_CFG3(4'b0000),
    .RXLPM_HOLD_DURING_EIDLE(1'b0),
    .RXLPM_INCM_CFG(1'b0),
    .RXLPM_IPCM_CFG(1'b1),
    .RXLPM_LF_CFG(18'b000000001111110000),
    .RXLPM_LF_CFG2(5'b01010),
    .RXLPM_OSINT_CFG(3'b100),
    .RXOOB_CFG(7'b0000110),
    .RXOOB_CLK_CFG("PMA"),
    .RXOSCALRESET_TIME(5'b00011),
    .RXOSCALRESET_TIMEOUT(5'b00000),
    .RXOUT_DIV(2),
    .RXPCSRESET_TIME(5'b00001),
    .RXPHDLY_CFG(24'h084020),
    .RXPH_CFG(24'hC00002),
    .RXPH_MONITOR_SEL(5'b00000),
    .RXPI_CFG0(3'b000),
    .RXPI_CFG1(1'b1),
    .RXPI_CFG2(1'b1),
    .RXPMARESET_TIME(5'b00011),
    .RXPRBS_ERR_LOOPBACK(1'b0),
    .RXSLIDE_AUTO_WAIT(7),
    .RXSLIDE_MODE("OFF"),
    .RXSYNC_MULTILANE(1'b0),
    .RXSYNC_OVRD(1'b0),
    .RXSYNC_SKIP_DA(1'b0),
    .RX_BIAS_CFG(16'b0000111100110011),
    .RX_BUFFER_CFG(6'b000000),
    .RX_CLK25_DIV(6),
    .RX_CLKMUX_EN(1'b1),
    .RX_CM_SEL(2'b01),
    .RX_CM_TRIM(4'b1010),
    .RX_DATA_WIDTH(20),
    .RX_DDI_SEL(6'b000000),
    .RX_DEBUG_CFG(14'b00000000000000),
    .RX_DEFER_RESET_BUF_EN("TRUE"),
    .RX_DISPERR_SEQ_MATCH("TRUE"),
    .RX_OS_CFG(13'b0000010000000),
    .RX_SIG_VALID_DLY(10),
    .RX_XCLK_SEL("RXREC"),
    .SAS_MAX_COM(64),
    .SAS_MIN_COM(36),
    .SATA_BURST_SEQ_LEN(4'b0101),
    .SATA_BURST_VAL(3'b110),
    .SATA_EIDLE_VAL(3'b110),
    .SATA_MAX_BURST(8),
    .SATA_MAX_INIT(21),
    .SATA_MAX_WAKE(7),
    .SATA_MIN_BURST(4),
    .SATA_MIN_INIT(12),
    .SATA_MIN_WAKE(4),
    .SATA_PLL_CFG("VCO_3000MHZ"),
    .SHOW_REALIGN_COMMA("TRUE"),
    .SIM_RECEIVER_DETECT_PASS("TRUE"),
    .SIM_RESET_SPEEDUP("FALSE"),
    .SIM_TX_EIDLE_DRIVE_LEVEL("X"),
    .SIM_VERSION("2.0"),
    .TERM_RCAL_CFG(15'b100001000010000),
    .TERM_RCAL_OVRD(3'b000),
    .TRANS_TIME_RATE(8'h0E),
    .TST_RSV(32'h00000000),
    .TXBUF_EN("TRUE"),
    .TXBUF_RESET_ON_RATE_CHANGE("TRUE"),
    .TXDLY_CFG(16'h001F),
    .TXDLY_LCFG(9'h030),
    .TXDLY_TAP_CFG(16'h0000),
    .TXGEARBOX_EN("FALSE"),
    .TXOOB_CFG(1'b0),
    .TXOUT_DIV(2),
    .TXPCSRESET_TIME(5'b00001),
    .TXPHDLY_CFG(24'h084020),
    .TXPH_CFG(16'h0780),
    .TXPH_MONITOR_SEL(5'b00000),
    .TXPI_CFG0(2'b00),
    .TXPI_CFG1(2'b00),
    .TXPI_CFG2(2'b00),
    .TXPI_CFG3(1'b0),
    .TXPI_CFG4(1'b0),
    .TXPI_CFG5(3'b000),
    .TXPI_GREY_SEL(1'b0),
    .TXPI_INVSTROBE_SEL(1'b0),
    .TXPI_PPMCLK_SEL("TXUSRCLK2"),
    .TXPI_PPM_CFG(8'b00000000),
    .TXPI_SYNFREQ_PPM(3'b000),
    .TXPMARESET_TIME(5'b00001),
    .TXSYNC_MULTILANE(1'b0),
    .TXSYNC_OVRD(1'b0),
    .TXSYNC_SKIP_DA(1'b0),
    .TX_CLK25_DIV(6),
    .TX_CLKMUX_EN(1'b1),
    .TX_DATA_WIDTH(20),
    .TX_DEEMPH0(6'b000000),
    .TX_DEEMPH1(6'b000000),
    .TX_DRIVE_MODE("DIRECT"),
    .TX_EIDLE_ASSERT_DELAY(3'b110),
    .TX_EIDLE_DEASSERT_DELAY(3'b100),
    .TX_LOOPBACK_DRIVE_HIZ("FALSE"),
    .TX_MAINCURSOR_SEL(1'b0),
    .TX_MARGIN_FULL_0(7'b1001110),
    .TX_MARGIN_FULL_1(7'b1001001),
    .TX_MARGIN_FULL_2(7'b1000101),
    .TX_MARGIN_FULL_3(7'b1000010),
    .TX_MARGIN_FULL_4(7'b1000000),
    .TX_MARGIN_LOW_0(7'b1000110),
    .TX_MARGIN_LOW_1(7'b1000100),
    .TX_MARGIN_LOW_2(7'b1000010),
    .TX_MARGIN_LOW_3(7'b1000000),
    .TX_MARGIN_LOW_4(7'b1000000),
    .TX_PREDRIVER_MODE(1'b0),
    .TX_RXDETECT_CFG(14'h1832),
    .TX_RXDETECT_REF(3'b100),
    .TX_XCLK_SEL("TXOUT"),
    .UCODEER_CLR(1'b0),
    .USE_PCS_CLK_PHASE_SEL(1'b0)) 
     gtpe2_i
       (.CFGRESET(1'b0),
        .CLKRSVD0(1'b0),
        .CLKRSVD1(1'b0),
        .DMONFIFORESET(1'b0),
        .DMONITORCLK(1'b0),
        .DMONITOROUT(gt0_dmonitorout_out),
        .DRPADDR({n_19_gtrxreset_seq_i,n_20_gtrxreset_seq_i,n_21_gtrxreset_seq_i,n_22_gtrxreset_seq_i,n_23_gtrxreset_seq_i,n_24_gtrxreset_seq_i,n_25_gtrxreset_seq_i,n_26_gtrxreset_seq_i,n_27_gtrxreset_seq_i}),
        .DRPCLK(CLK),
        .DRPDI({n_2_gtrxreset_seq_i,n_3_gtrxreset_seq_i,n_4_gtrxreset_seq_i,n_5_gtrxreset_seq_i,n_6_gtrxreset_seq_i,n_7_gtrxreset_seq_i,n_8_gtrxreset_seq_i,n_9_gtrxreset_seq_i,n_10_gtrxreset_seq_i,n_11_gtrxreset_seq_i,n_12_gtrxreset_seq_i,n_13_gtrxreset_seq_i,n_14_gtrxreset_seq_i,n_15_gtrxreset_seq_i,n_16_gtrxreset_seq_i,n_17_gtrxreset_seq_i}),
        .DRPDO(gt0_drpdo_out),
        .DRPEN(n_1_gtrxreset_seq_i),
        .DRPRDY(O1),
        .DRPWE(n_18_gtrxreset_seq_i),
        .EYESCANDATAERROR(gt0_eyescandataerror_out),
        .EYESCANMODE(1'b0),
        .EYESCANRESET(gt0_eyescanreset_in),
        .EYESCANTRIGGER(gt0_eyescantrigger_in),
        .GTPRXN(gt0_gtprxn_in),
        .GTPRXP(gt0_gtprxp_in),
        .GTPTXN(gt0_gtptxn_out),
        .GTPTXP(gt0_gtptxp_out),
        .GTRESETSEL(1'b0),
        .GTRSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GTRXRESET(GTRXRESET_OUT),
        .GTTXRESET(GTTXRESET),
        .LOOPBACK({1'b0,1'b0,1'b0}),
        .PCSRSVDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCSRSVDOUT(NLW_gtpe2_i_PCSRSVDOUT_UNCONNECTED[15:0]),
        .PHYSTATUS(NLW_gtpe2_i_PHYSTATUS_UNCONNECTED),
        .PLL0CLK(GT0_PLL0OUTCLK_OUT),
        .PLL0REFCLK(GT0_PLL0OUTREFCLK_OUT),
        .PLL1CLK(GT0_PLL1OUTCLK_OUT),
        .PLL1REFCLK(GT0_PLL1OUTREFCLK_OUT),
        .PMARSVDIN0(1'b0),
        .PMARSVDIN1(1'b0),
        .PMARSVDIN2(1'b0),
        .PMARSVDIN3(1'b0),
        .PMARSVDIN4(1'b0),
        .PMARSVDOUT0(NLW_gtpe2_i_PMARSVDOUT0_UNCONNECTED),
        .PMARSVDOUT1(NLW_gtpe2_i_PMARSVDOUT1_UNCONNECTED),
        .RESETOVRD(1'b0),
        .RX8B10BEN(1'b1),
        .RXADAPTSELTEST({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .RXBUFRESET(1'b0),
        .RXBUFSTATUS(NLW_gtpe2_i_RXBUFSTATUS_UNCONNECTED[2:0]),
        .RXBYTEISALIGNED(gt0_rxbyteisaligned_out),
        .RXBYTEREALIGN(NLW_gtpe2_i_RXBYTEREALIGN_UNCONNECTED),
        .RXCDRFREQRESET(1'b0),
        .RXCDRHOLD(1'b0),
        .RXCDRLOCK(NLW_gtpe2_i_RXCDRLOCK_UNCONNECTED),
        .RXCDROVRDEN(1'b0),
        .RXCDRRESET(1'b0),
        .RXCDRRESETRSV(1'b0),
        .RXCHANBONDSEQ(NLW_gtpe2_i_RXCHANBONDSEQ_UNCONNECTED),
        .RXCHANISALIGNED(NLW_gtpe2_i_RXCHANISALIGNED_UNCONNECTED),
        .RXCHANREALIGN(NLW_gtpe2_i_RXCHANREALIGN_UNCONNECTED),
        .RXCHARISCOMMA(NLW_gtpe2_i_RXCHARISCOMMA_UNCONNECTED[3:0]),
        .RXCHARISK({NLW_gtpe2_i_RXCHARISK_UNCONNECTED[3:2],gt0_rxcharisk_out}),
        .RXCHBONDEN(1'b0),
        .RXCHBONDI({1'b0,1'b0,1'b0,1'b0}),
        .RXCHBONDLEVEL({1'b0,1'b0,1'b0}),
        .RXCHBONDMASTER(1'b0),
        .RXCHBONDO(NLW_gtpe2_i_RXCHBONDO_UNCONNECTED[3:0]),
        .RXCHBONDSLAVE(1'b0),
        .RXCLKCORCNT(NLW_gtpe2_i_RXCLKCORCNT_UNCONNECTED[1:0]),
        .RXCOMINITDET(gt0_rxcominitdet_out),
        .RXCOMMADET(NLW_gtpe2_i_RXCOMMADET_UNCONNECTED),
        .RXCOMMADETEN(1'b1),
        .RXCOMSASDET(gt0_rxcomsasdet_out),
        .RXCOMWAKEDET(gt0_rxcomwakedet_out),
        .RXDATA({NLW_gtpe2_i_RXDATA_UNCONNECTED[31:16],gt0_rxdata_out}),
        .RXDATAVALID(NLW_gtpe2_i_RXDATAVALID_UNCONNECTED[1:0]),
        .RXDDIEN(1'b0),
        .RXDFEXYDEN(1'b0),
        .RXDISPERR({NLW_gtpe2_i_RXDISPERR_UNCONNECTED[3:2],gt0_rxdisperr_out}),
        .RXDLYBYPASS(1'b1),
        .RXDLYEN(1'b0),
        .RXDLYOVRDEN(1'b0),
        .RXDLYSRESET(1'b0),
        .RXDLYSRESETDONE(NLW_gtpe2_i_RXDLYSRESETDONE_UNCONNECTED),
        .RXELECIDLE(gt0_rxelecidle_out),
        .RXELECIDLEMODE({1'b0,1'b0}),
        .RXGEARBOXSLIP(1'b0),
        .RXHEADER(NLW_gtpe2_i_RXHEADER_UNCONNECTED[2:0]),
        .RXHEADERVALID(NLW_gtpe2_i_RXHEADERVALID_UNCONNECTED),
        .RXLPMHFHOLD(gt0_rxlpmhfhold_in),
        .RXLPMHFOVRDEN(gt0_rxlpmhfovrden_in),
        .RXLPMLFHOLD(gt0_rxlpmlfhold_in),
        .RXLPMLFOVRDEN(1'b0),
        .RXLPMOSINTNTRLEN(1'b0),
        .RXLPMRESET(gt0_rxlpmreset_in),
        .RXMCOMMAALIGNEN(1'b1),
        .RXNOTINTABLE({NLW_gtpe2_i_RXNOTINTABLE_UNCONNECTED[3:2],gt0_rxnotintable_out}),
        .RXOOBRESET(1'b0),
        .RXOSCALRESET(1'b0),
        .RXOSHOLD(1'b0),
        .RXOSINTCFG({1'b0,1'b0,1'b1,1'b0}),
        .RXOSINTDONE(NLW_gtpe2_i_RXOSINTDONE_UNCONNECTED),
        .RXOSINTEN(1'b1),
        .RXOSINTHOLD(1'b0),
        .RXOSINTID0({1'b0,1'b0,1'b0,1'b0}),
        .RXOSINTNTRLEN(1'b0),
        .RXOSINTOVRDEN(1'b0),
        .RXOSINTPD(1'b0),
        .RXOSINTSTARTED(NLW_gtpe2_i_RXOSINTSTARTED_UNCONNECTED),
        .RXOSINTSTROBE(1'b0),
        .RXOSINTSTROBEDONE(NLW_gtpe2_i_RXOSINTSTROBEDONE_UNCONNECTED),
        .RXOSINTSTROBESTARTED(NLW_gtpe2_i_RXOSINTSTROBESTARTED_UNCONNECTED),
        .RXOSINTTESTOVRDEN(1'b0),
        .RXOSOVRDEN(1'b0),
        .RXOUTCLK(n_24_gtpe2_i),
        .RXOUTCLKFABRIC(NLW_gtpe2_i_RXOUTCLKFABRIC_UNCONNECTED),
        .RXOUTCLKPCS(NLW_gtpe2_i_RXOUTCLKPCS_UNCONNECTED),
        .RXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .RXPCOMMAALIGNEN(1'b1),
        .RXPCSRESET(1'b0),
        .RXPD({1'b0,1'b0}),
        .RXPHALIGN(1'b0),
        .RXPHALIGNDONE(NLW_gtpe2_i_RXPHALIGNDONE_UNCONNECTED),
        .RXPHALIGNEN(1'b0),
        .RXPHDLYPD(1'b0),
        .RXPHDLYRESET(1'b0),
        .RXPHMONITOR(NLW_gtpe2_i_RXPHMONITOR_UNCONNECTED[4:0]),
        .RXPHOVRDEN(1'b0),
        .RXPHSLIPMONITOR(NLW_gtpe2_i_RXPHSLIPMONITOR_UNCONNECTED[4:0]),
        .RXPMARESET(1'b0),
        .RXPMARESETDONE(RXPMARESETDONE),
        .RXPOLARITY(1'b0),
        .RXPRBSCNTRESET(1'b0),
        .RXPRBSERR(NLW_gtpe2_i_RXPRBSERR_UNCONNECTED),
        .RXPRBSSEL({1'b0,1'b0,1'b0}),
        .RXRATE({1'b0,1'b0,1'b0}),
        .RXRATEDONE(NLW_gtpe2_i_RXRATEDONE_UNCONNECTED),
        .RXRATEMODE(1'b0),
        .RXRESETDONE(gt0_rxresetdone_out),
        .RXSLIDE(1'b0),
        .RXSTARTOFSEQ(NLW_gtpe2_i_RXSTARTOFSEQ_UNCONNECTED[1:0]),
        .RXSTATUS(gt0_rxstatus_out),
        .RXSYNCALLIN(1'b0),
        .RXSYNCDONE(NLW_gtpe2_i_RXSYNCDONE_UNCONNECTED),
        .RXSYNCIN(1'b0),
        .RXSYNCMODE(1'b0),
        .RXSYNCOUT(NLW_gtpe2_i_RXSYNCOUT_UNCONNECTED),
        .RXSYSCLKSEL({1'b0,1'b0}),
        .RXUSERRDY(RXUSERRDY),
        .RXUSRCLK(I1),
        .RXUSRCLK2(I1),
        .RXVALID(NLW_gtpe2_i_RXVALID_UNCONNECTED),
        .SETERRSTATUS(1'b0),
        .SIGVALIDCLK(1'b0),
        .TSTIN({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .TX8B10BBYPASS({1'b0,1'b0,1'b0,1'b0}),
        .TX8B10BEN(1'b1),
        .TXBUFDIFFCTRL({1'b1,1'b0,1'b0}),
        .TXBUFSTATUS(NLW_gtpe2_i_TXBUFSTATUS_UNCONNECTED[1:0]),
        .TXCHARDISPMODE({1'b0,1'b0,1'b0,1'b0}),
        .TXCHARDISPVAL({1'b0,1'b0,1'b0,1'b0}),
        .TXCHARISK({1'b0,1'b0,gt0_txcharisk_in}),
        .TXCOMFINISH(gt0_txcomfinish_out),
        .TXCOMINIT(gt0_txcominit_in),
        .TXCOMSAS(gt0_txcomsas_in),
        .TXCOMWAKE(gt0_txcomwake_in),
        .TXDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,gt0_txdata_in}),
        .TXDEEMPH(1'b0),
        .TXDETECTRX(1'b0),
        .TXDIFFCTRL({1'b1,1'b0,1'b0,1'b0}),
        .TXDIFFPD(1'b0),
        .TXDLYBYPASS(1'b1),
        .TXDLYEN(1'b0),
        .TXDLYHOLD(1'b0),
        .TXDLYOVRDEN(1'b0),
        .TXDLYSRESET(1'b0),
        .TXDLYSRESETDONE(NLW_gtpe2_i_TXDLYSRESETDONE_UNCONNECTED),
        .TXDLYUPDOWN(1'b0),
        .TXELECIDLE(gt0_txelecidle_in),
        .TXGEARBOXREADY(NLW_gtpe2_i_TXGEARBOXREADY_UNCONNECTED),
        .TXHEADER({1'b0,1'b0,1'b0}),
        .TXINHIBIT(1'b0),
        .TXMAINCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXMARGIN({1'b0,1'b0,1'b0}),
        .TXOUTCLK(gt0_txoutclk_out),
        .TXOUTCLKFABRIC(gt0_txoutclkfabric_out),
        .TXOUTCLKPCS(gt0_txoutclkpcs_out),
        .TXOUTCLKSEL({1'b0,1'b1,1'b0}),
        .TXPCSRESET(1'b0),
        .TXPD({1'b0,1'b0}),
        .TXPDELECIDLEMODE(1'b0),
        .TXPHALIGN(1'b0),
        .TXPHALIGNDONE(NLW_gtpe2_i_TXPHALIGNDONE_UNCONNECTED),
        .TXPHALIGNEN(1'b0),
        .TXPHDLYPD(1'b0),
        .TXPHDLYRESET(1'b0),
        .TXPHDLYTSTCLK(1'b0),
        .TXPHINIT(1'b0),
        .TXPHINITDONE(NLW_gtpe2_i_TXPHINITDONE_UNCONNECTED),
        .TXPHOVRDEN(1'b0),
        .TXPIPPMEN(1'b0),
        .TXPIPPMOVRDEN(1'b0),
        .TXPIPPMPD(1'b0),
        .TXPIPPMSEL(1'b1),
        .TXPIPPMSTEPSIZE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPISOPD(1'b0),
        .TXPMARESET(1'b0),
        .TXPMARESETDONE(TXPMARESETDONE),
        .TXPOLARITY(1'b0),
        .TXPOSTCURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPOSTCURSORINV(1'b0),
        .TXPRBSFORCEERR(1'b0),
        .TXPRBSSEL({1'b0,1'b0,1'b0}),
        .TXPRECURSOR({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXPRECURSORINV(1'b0),
        .TXRATE({1'b0,1'b0,1'b0}),
        .TXRATEDONE(NLW_gtpe2_i_TXRATEDONE_UNCONNECTED),
        .TXRATEMODE(1'b0),
        .TXRESETDONE(gt0_txresetdone_out),
        .TXSEQUENCE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .TXSTARTSEQ(1'b0),
        .TXSWING(1'b0),
        .TXSYNCALLIN(1'b0),
        .TXSYNCDONE(NLW_gtpe2_i_TXSYNCDONE_UNCONNECTED),
        .TXSYNCIN(1'b0),
        .TXSYNCMODE(1'b0),
        .TXSYNCOUT(NLW_gtpe2_i_TXSYNCOUT_UNCONNECTED),
        .TXSYSCLKSEL({1'b0,1'b0}),
        .TXUSERRDY(TXUSERRDY),
        .TXUSRCLK(I1),
        .TXUSRCLK2(I1));
gtp_sata_gtp_sata_gtrxreset_seq gtrxreset_seq_i
       (.CLK(CLK),
        .DRPADDR({n_19_gtrxreset_seq_i,n_20_gtrxreset_seq_i,n_21_gtrxreset_seq_i,n_22_gtrxreset_seq_i,n_23_gtrxreset_seq_i,n_24_gtrxreset_seq_i,n_25_gtrxreset_seq_i,n_26_gtrxreset_seq_i,n_27_gtrxreset_seq_i}),
        .DRPDI({n_2_gtrxreset_seq_i,n_3_gtrxreset_seq_i,n_4_gtrxreset_seq_i,n_5_gtrxreset_seq_i,n_6_gtrxreset_seq_i,n_7_gtrxreset_seq_i,n_8_gtrxreset_seq_i,n_9_gtrxreset_seq_i,n_10_gtrxreset_seq_i,n_11_gtrxreset_seq_i,n_12_gtrxreset_seq_i,n_13_gtrxreset_seq_i,n_14_gtrxreset_seq_i,n_15_gtrxreset_seq_i,n_16_gtrxreset_seq_i,n_17_gtrxreset_seq_i}),
        .GTRXRESET_OUT(GTRXRESET_OUT),
        .I1(O1),
        .I2(I2),
        .O1(n_1_gtrxreset_seq_i),
        .O2(n_18_gtrxreset_seq_i),
        .SR(SR),
        .data_in(RXPMARESETDONE),
        .gt0_drpaddr_in(gt0_drpaddr_in),
        .gt0_drpdi_in(gt0_drpdi_in),
        .gt0_drpdo_out(gt0_drpdo_out),
        .gt0_drpen_in(gt0_drpen_in),
        .gt0_drpwe_in(gt0_drpwe_in));
endmodule

(* ORIG_REF_NAME = "gtp_sata_RX_STARTUP_FSM" *) 
module gtp_sata_gtp_sata_RX_STARTUP_FSM__parameterized0
   (SR,
    GT0_RX_FSM_RESET_DONE_OUT,
    RXUSERRDY,
    CLK,
    I1,
    SOFT_RESET_IN,
    DONT_RESET_ON_DATA_ERROR_IN,
    TXUSERRDY,
    gt0_rxresetdone_out,
    GT0_DATA_VALID_IN,
    GT0_PLL0LOCK_OUT,
    I2);
  output [0:0]SR;
  output GT0_RX_FSM_RESET_DONE_OUT;
  output RXUSERRDY;
  input CLK;
  input I1;
  input SOFT_RESET_IN;
  input DONT_RESET_ON_DATA_ERROR_IN;
  input TXUSERRDY;
  input gt0_rxresetdone_out;
  input GT0_DATA_VALID_IN;
  input GT0_PLL0LOCK_OUT;
  input I2;

  wire CLK;
  wire DONT_RESET_ON_DATA_ERROR_IN;
  wire GT0_DATA_VALID_IN;
  wire GT0_PLL0LOCK_OUT;
  wire GT0_RX_FSM_RESET_DONE_OUT;
  wire I1;
  wire I2;
  wire RXUSERRDY;
  wire SOFT_RESET_IN;
  wire [0:0]SR;
  wire TXUSERRDY;
  wire data_valid_sync;
  wire gt0_rxresetdone_out;
  wire [5:0]init_wait_count_reg__0;
  wire init_wait_done;
  wire [9:0]mmcm_lock_count_reg__0;
  wire mmcm_lock_reclocked;
  wire \n_0_FSM_sequential_rx_state[0]_i_2 ;
  wire \n_0_FSM_sequential_rx_state[2]_i_1 ;
  wire \n_0_FSM_sequential_rx_state[3]_i_10 ;
  wire \n_0_FSM_sequential_rx_state[3]_i_6 ;
  wire \n_0_FSM_sequential_rx_state[3]_i_7 ;
  wire \n_0_FSM_sequential_rx_state[3]_i_9 ;
  wire n_0_RXUSERRDY_i_1;
  wire n_0_check_tlock_max_i_1;
  wire n_0_check_tlock_max_reg;
  wire n_0_gtrxreset_i_i_1;
  wire \n_0_init_wait_count[0]_i_1__1 ;
  wire \n_0_init_wait_count[5]_i_1__1 ;
  wire n_0_init_wait_done_i_1__1;
  wire n_0_init_wait_done_reg;
  wire \n_0_mmcm_lock_count[9]_i_2__0 ;
  wire \n_0_mmcm_lock_count[9]_i_4__0 ;
  wire n_0_mmcm_lock_reclocked_i_2__0;
  wire n_0_reset_time_out_i_2__0;
  wire n_0_reset_time_out_i_4__0;
  wire n_0_reset_time_out_i_5;
  wire n_0_reset_time_out_reg;
  wire n_0_run_phase_alignment_int_i_1__0;
  wire n_0_run_phase_alignment_int_reg;
  wire n_0_run_phase_alignment_int_s3_reg;
  wire n_0_rx_fsm_reset_done_int_i_2;
  wire n_0_rx_fsm_reset_done_int_i_4;
  wire n_0_rx_fsm_reset_done_int_i_5;
  wire n_0_sync_PLL0LOCK;
  wire n_0_sync_mmcm_lock_reclocked;
  wire n_0_time_out_100us_i_1;
  wire n_0_time_out_100us_i_2;
  wire n_0_time_out_100us_i_3;
  wire n_0_time_out_100us_i_4;
  wire n_0_time_out_100us_i_5;
  wire n_0_time_out_100us_reg;
  wire n_0_time_out_1us_i_1;
  wire n_0_time_out_1us_i_2;
  wire n_0_time_out_1us_i_3;
  wire n_0_time_out_1us_i_4;
  wire n_0_time_out_1us_reg;
  wire n_0_time_out_2ms_i_1;
  wire n_0_time_out_2ms_reg;
  wire \n_0_time_out_counter[0]_i_1 ;
  wire \n_0_time_out_counter[0]_i_10 ;
  wire \n_0_time_out_counter[0]_i_11 ;
  wire \n_0_time_out_counter[0]_i_4__0 ;
  wire \n_0_time_out_counter[0]_i_5__0 ;
  wire \n_0_time_out_counter[0]_i_6__0 ;
  wire \n_0_time_out_counter[0]_i_7__0 ;
  wire \n_0_time_out_counter[0]_i_8__0 ;
  wire \n_0_time_out_counter[0]_i_9__0 ;
  wire \n_0_time_out_counter[12]_i_2__0 ;
  wire \n_0_time_out_counter[12]_i_3__0 ;
  wire \n_0_time_out_counter[12]_i_4__0 ;
  wire \n_0_time_out_counter[12]_i_5__0 ;
  wire \n_0_time_out_counter[16]_i_2__0 ;
  wire \n_0_time_out_counter[16]_i_3 ;
  wire \n_0_time_out_counter[4]_i_2__0 ;
  wire \n_0_time_out_counter[4]_i_3__0 ;
  wire \n_0_time_out_counter[4]_i_4__0 ;
  wire \n_0_time_out_counter[4]_i_5__0 ;
  wire \n_0_time_out_counter[8]_i_2__0 ;
  wire \n_0_time_out_counter[8]_i_3__0 ;
  wire \n_0_time_out_counter[8]_i_4__0 ;
  wire \n_0_time_out_counter[8]_i_5__0 ;
  wire \n_0_time_out_counter_reg[0]_i_2__0 ;
  wire \n_0_time_out_counter_reg[12]_i_1__0 ;
  wire \n_0_time_out_counter_reg[4]_i_1__0 ;
  wire \n_0_time_out_counter_reg[8]_i_1__0 ;
  wire n_0_time_out_wait_bypass_i_1__0;
  wire n_0_time_out_wait_bypass_reg;
  wire n_0_time_tlock_max_i_10;
  wire n_0_time_tlock_max_i_11;
  wire n_0_time_tlock_max_i_12;
  wire n_0_time_tlock_max_i_13;
  wire n_0_time_tlock_max_i_14;
  wire n_0_time_tlock_max_i_15;
  wire n_0_time_tlock_max_i_16;
  wire n_0_time_tlock_max_i_17;
  wire n_0_time_tlock_max_i_18;
  wire n_0_time_tlock_max_i_19;
  wire n_0_time_tlock_max_i_1__0;
  wire n_0_time_tlock_max_i_20;
  wire n_0_time_tlock_max_i_4;
  wire n_0_time_tlock_max_i_5;
  wire n_0_time_tlock_max_i_7;
  wire n_0_time_tlock_max_i_8;
  wire n_0_time_tlock_max_i_9;
  wire n_0_time_tlock_max_reg_i_3;
  wire n_0_time_tlock_max_reg_i_6;
  wire \n_0_wait_bypass_count[0]_i_1__0 ;
  wire \n_0_wait_bypass_count[0]_i_2__0 ;
  wire \n_0_wait_bypass_count[0]_i_4__0 ;
  wire \n_0_wait_bypass_count[0]_i_5 ;
  wire \n_0_wait_bypass_count[0]_i_6__0 ;
  wire \n_0_wait_bypass_count[0]_i_7__0 ;
  wire \n_0_wait_bypass_count[0]_i_8__0 ;
  wire \n_0_wait_bypass_count[0]_i_9 ;
  wire \n_0_wait_bypass_count[12]_i_2__0 ;
  wire \n_0_wait_bypass_count[4]_i_2__0 ;
  wire \n_0_wait_bypass_count[4]_i_3__0 ;
  wire \n_0_wait_bypass_count[4]_i_4__0 ;
  wire \n_0_wait_bypass_count[4]_i_5__0 ;
  wire \n_0_wait_bypass_count[8]_i_2__0 ;
  wire \n_0_wait_bypass_count[8]_i_3__0 ;
  wire \n_0_wait_bypass_count[8]_i_4__0 ;
  wire \n_0_wait_bypass_count[8]_i_5__0 ;
  wire \n_0_wait_bypass_count_reg[0]_i_3__0 ;
  wire \n_0_wait_bypass_count_reg[4]_i_1__0 ;
  wire \n_0_wait_bypass_count_reg[8]_i_1__0 ;
  wire \n_0_wait_time_cnt[1]_i_1__0 ;
  wire \n_0_wait_time_cnt[4]_i_1__0 ;
  wire \n_0_wait_time_cnt[6]_i_1__0 ;
  wire \n_0_wait_time_cnt[6]_i_2__0 ;
  wire \n_0_wait_time_cnt[6]_i_4__0 ;
  wire n_1_sync_PLL0LOCK;
  wire n_1_sync_data_valid;
  wire n_1_sync_mmcm_lock_reclocked;
  wire \n_1_time_out_counter_reg[0]_i_2__0 ;
  wire \n_1_time_out_counter_reg[12]_i_1__0 ;
  wire \n_1_time_out_counter_reg[4]_i_1__0 ;
  wire \n_1_time_out_counter_reg[8]_i_1__0 ;
  wire n_1_time_tlock_max_reg_i_3;
  wire n_1_time_tlock_max_reg_i_6;
  wire \n_1_wait_bypass_count_reg[0]_i_3__0 ;
  wire \n_1_wait_bypass_count_reg[4]_i_1__0 ;
  wire \n_1_wait_bypass_count_reg[8]_i_1__0 ;
  wire n_2_sync_data_valid;
  wire \n_2_time_out_counter_reg[0]_i_2__0 ;
  wire \n_2_time_out_counter_reg[12]_i_1__0 ;
  wire \n_2_time_out_counter_reg[4]_i_1__0 ;
  wire \n_2_time_out_counter_reg[8]_i_1__0 ;
  wire n_2_time_tlock_max_reg_i_3;
  wire n_2_time_tlock_max_reg_i_6;
  wire \n_2_wait_bypass_count_reg[0]_i_3__0 ;
  wire \n_2_wait_bypass_count_reg[4]_i_1__0 ;
  wire \n_2_wait_bypass_count_reg[8]_i_1__0 ;
  wire n_3_sync_data_valid;
  wire \n_3_time_out_counter_reg[0]_i_2__0 ;
  wire \n_3_time_out_counter_reg[12]_i_1__0 ;
  wire \n_3_time_out_counter_reg[16]_i_1__0 ;
  wire \n_3_time_out_counter_reg[4]_i_1__0 ;
  wire \n_3_time_out_counter_reg[8]_i_1__0 ;
  wire n_3_time_tlock_max_reg_i_3;
  wire n_3_time_tlock_max_reg_i_6;
  wire \n_3_wait_bypass_count_reg[0]_i_3__0 ;
  wire \n_3_wait_bypass_count_reg[4]_i_1__0 ;
  wire \n_3_wait_bypass_count_reg[8]_i_1__0 ;
  wire n_4_sync_data_valid;
  wire \n_4_time_out_counter_reg[0]_i_2__0 ;
  wire \n_4_time_out_counter_reg[12]_i_1__0 ;
  wire \n_4_time_out_counter_reg[4]_i_1__0 ;
  wire \n_4_time_out_counter_reg[8]_i_1__0 ;
  wire \n_4_wait_bypass_count_reg[0]_i_3__0 ;
  wire \n_4_wait_bypass_count_reg[4]_i_1__0 ;
  wire \n_4_wait_bypass_count_reg[8]_i_1__0 ;
  wire n_5_sync_data_valid;
  wire \n_5_time_out_counter_reg[0]_i_2__0 ;
  wire \n_5_time_out_counter_reg[12]_i_1__0 ;
  wire \n_5_time_out_counter_reg[4]_i_1__0 ;
  wire \n_5_time_out_counter_reg[8]_i_1__0 ;
  wire \n_5_wait_bypass_count_reg[0]_i_3__0 ;
  wire \n_5_wait_bypass_count_reg[4]_i_1__0 ;
  wire \n_5_wait_bypass_count_reg[8]_i_1__0 ;
  wire \n_6_time_out_counter_reg[0]_i_2__0 ;
  wire \n_6_time_out_counter_reg[12]_i_1__0 ;
  wire \n_6_time_out_counter_reg[16]_i_1__0 ;
  wire \n_6_time_out_counter_reg[4]_i_1__0 ;
  wire \n_6_time_out_counter_reg[8]_i_1__0 ;
  wire \n_6_wait_bypass_count_reg[0]_i_3__0 ;
  wire \n_6_wait_bypass_count_reg[4]_i_1__0 ;
  wire \n_6_wait_bypass_count_reg[8]_i_1__0 ;
  wire \n_7_time_out_counter_reg[0]_i_2__0 ;
  wire \n_7_time_out_counter_reg[12]_i_1__0 ;
  wire \n_7_time_out_counter_reg[16]_i_1__0 ;
  wire \n_7_time_out_counter_reg[4]_i_1__0 ;
  wire \n_7_time_out_counter_reg[8]_i_1__0 ;
  wire \n_7_wait_bypass_count_reg[0]_i_3__0 ;
  wire \n_7_wait_bypass_count_reg[12]_i_1__0 ;
  wire \n_7_wait_bypass_count_reg[4]_i_1__0 ;
  wire \n_7_wait_bypass_count_reg[8]_i_1__0 ;
  wire [5:1]p_0_in__1;
  wire [9:0]p_0_in__2;
  wire run_phase_alignment_int_s2;
  wire rx_fsm_reset_done_int_s2;
  wire rx_fsm_reset_done_int_s3;
(* RTL_KEEP = "yes" *)   wire [3:0]rx_state;
  wire rx_state16_out;
  wire rxresetdone_s2;
  wire rxresetdone_s3;
  wire time_out_2ms;
  wire [17:0]time_out_counter_reg;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire time_tlock_max;
  wire time_tlock_max1;
  wire [12:0]wait_bypass_count_reg;
  wire [6:0]wait_time_cnt0__0;
  wire [6:0]wait_time_cnt_reg__0;
  wire [3:1]\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED ;
  wire [3:2]\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED ;
  wire [3:1]NLW_time_tlock_max_reg_i_2_CO_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max_reg_i_2_O_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max_reg_i_3_O_UNCONNECTED;
  wire [3:0]NLW_time_tlock_max_reg_i_6_O_UNCONNECTED;
  wire [3:0]\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED ;
  wire [3:1]\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED ;

LUT6 #(
    .INIT(64'hCF40CF4F00000000)) 
     \FSM_sequential_rx_state[0]_i_2 
       (.I0(n_0_time_out_2ms_reg),
        .I1(rx_state[2]),
        .I2(rx_state[1]),
        .I3(n_0_reset_time_out_reg),
        .I4(time_tlock_max),
        .I5(rx_state[0]),
        .O(\n_0_FSM_sequential_rx_state[0]_i_2 ));
LUT6 #(
    .INIT(64'h0003005000F000F0)) 
     \FSM_sequential_rx_state[2]_i_1 
       (.I0(rx_state16_out),
        .I1(n_0_time_out_2ms_reg),
        .I2(rx_state[2]),
        .I3(rx_state[3]),
        .I4(rx_state[1]),
        .I5(rx_state[0]),
        .O(\n_0_FSM_sequential_rx_state[2]_i_1 ));
(* SOFT_HLUTNM = "soft_lutpair14" *) 
   LUT2 #(
    .INIT(4'h2)) 
     \FSM_sequential_rx_state[2]_i_2 
       (.I0(time_tlock_max),
        .I1(n_0_reset_time_out_reg),
        .O(rx_state16_out));
LUT4 #(
    .INIT(16'hAA08)) 
     \FSM_sequential_rx_state[3]_i_10 
       (.I0(rx_state[2]),
        .I1(n_0_time_out_2ms_reg),
        .I2(n_0_reset_time_out_reg),
        .I3(rxresetdone_s3),
        .O(\n_0_FSM_sequential_rx_state[3]_i_10 ));
LUT6 #(
    .INIT(64'hFBFBFF00FBFBFFFF)) 
     \FSM_sequential_rx_state[3]_i_6 
       (.I0(wait_time_cnt_reg__0[6]),
        .I1(\n_0_wait_time_cnt[6]_i_4__0 ),
        .I2(rx_state[0]),
        .I3(rx_state[2]),
        .I4(rx_state[1]),
        .I5(n_0_init_wait_done_reg),
        .O(\n_0_FSM_sequential_rx_state[3]_i_6 ));
LUT5 #(
    .INIT(32'h80800080)) 
     \FSM_sequential_rx_state[3]_i_7 
       (.I0(rx_state[0]),
        .I1(rx_state[1]),
        .I2(rx_state[2]),
        .I3(n_0_time_out_2ms_reg),
        .I4(n_0_reset_time_out_reg),
        .O(\n_0_FSM_sequential_rx_state[3]_i_7 ));
LUT4 #(
    .INIT(16'hF2FF)) 
     \FSM_sequential_rx_state[3]_i_9 
       (.I0(time_tlock_max),
        .I1(n_0_reset_time_out_reg),
        .I2(mmcm_lock_reclocked),
        .I3(rx_state[2]),
        .O(\n_0_FSM_sequential_rx_state[3]_i_9 ));
(* KEEP = "yes" *) 
   FDRE \FSM_sequential_rx_state_reg[0] 
       (.C(CLK),
        .CE(n_5_sync_data_valid),
        .D(n_4_sync_data_valid),
        .Q(rx_state[0]),
        .R(SOFT_RESET_IN));
(* KEEP = "yes" *) 
   FDRE \FSM_sequential_rx_state_reg[1] 
       (.C(CLK),
        .CE(n_5_sync_data_valid),
        .D(n_3_sync_data_valid),
        .Q(rx_state[1]),
        .R(SOFT_RESET_IN));
(* KEEP = "yes" *) 
   FDRE \FSM_sequential_rx_state_reg[2] 
       (.C(CLK),
        .CE(n_5_sync_data_valid),
        .D(\n_0_FSM_sequential_rx_state[2]_i_1 ),
        .Q(rx_state[2]),
        .R(SOFT_RESET_IN));
(* KEEP = "yes" *) 
   FDRE \FSM_sequential_rx_state_reg[3] 
       (.C(CLK),
        .CE(n_5_sync_data_valid),
        .D(n_2_sync_data_valid),
        .Q(rx_state[3]),
        .R(SOFT_RESET_IN));
LUT6 #(
    .INIT(64'hFFFBFFFB40000000)) 
     RXUSERRDY_i_1
       (.I0(rx_state[3]),
        .I1(rx_state[0]),
        .I2(rx_state[2]),
        .I3(rx_state[1]),
        .I4(TXUSERRDY),
        .I5(RXUSERRDY),
        .O(n_0_RXUSERRDY_i_1));
FDRE #(
    .INIT(1'b0)) 
     RXUSERRDY_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_RXUSERRDY_i_1),
        .Q(RXUSERRDY),
        .R(SOFT_RESET_IN));
LUT5 #(
    .INIT(32'hFFFB0008)) 
     check_tlock_max_i_1
       (.I0(rx_state[2]),
        .I1(rx_state[0]),
        .I2(rx_state[1]),
        .I3(rx_state[3]),
        .I4(n_0_check_tlock_max_reg),
        .O(n_0_check_tlock_max_i_1));
FDRE #(
    .INIT(1'b0)) 
     check_tlock_max_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_check_tlock_max_i_1),
        .Q(n_0_check_tlock_max_reg),
        .R(SOFT_RESET_IN));
LUT5 #(
    .INIT(32'hFFFB0002)) 
     gtrxreset_i_i_1
       (.I0(rx_state[0]),
        .I1(rx_state[2]),
        .I2(rx_state[1]),
        .I3(rx_state[3]),
        .I4(SR),
        .O(n_0_gtrxreset_i_i_1));
FDRE #(
    .INIT(1'b0)) 
     gtrxreset_i_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_gtrxreset_i_i_1),
        .Q(SR),
        .R(SOFT_RESET_IN));
LUT1 #(
    .INIT(2'h1)) 
     \init_wait_count[0]_i_1__1 
       (.I0(init_wait_count_reg__0[0]),
        .O(\n_0_init_wait_count[0]_i_1__1 ));
(* SOFT_HLUTNM = "soft_lutpair13" *) 
   LUT2 #(
    .INIT(4'h6)) 
     \init_wait_count[1]_i_1__1 
       (.I0(init_wait_count_reg__0[1]),
        .I1(init_wait_count_reg__0[0]),
        .O(p_0_in__1[1]));
(* SOFT_HLUTNM = "soft_lutpair13" *) 
   LUT3 #(
    .INIT(8'h78)) 
     \init_wait_count[2]_i_1__1 
       (.I0(init_wait_count_reg__0[1]),
        .I1(init_wait_count_reg__0[0]),
        .I2(init_wait_count_reg__0[2]),
        .O(p_0_in__1[2]));
(* SOFT_HLUTNM = "soft_lutpair7" *) 
   LUT4 #(
    .INIT(16'h6AAA)) 
     \init_wait_count[3]_i_1__1 
       (.I0(init_wait_count_reg__0[3]),
        .I1(init_wait_count_reg__0[1]),
        .I2(init_wait_count_reg__0[0]),
        .I3(init_wait_count_reg__0[2]),
        .O(p_0_in__1[3]));
(* SOFT_HLUTNM = "soft_lutpair7" *) 
   LUT5 #(
    .INIT(32'h6AAAAAAA)) 
     \init_wait_count[4]_i_1__1 
       (.I0(init_wait_count_reg__0[4]),
        .I1(init_wait_count_reg__0[2]),
        .I2(init_wait_count_reg__0[0]),
        .I3(init_wait_count_reg__0[1]),
        .I4(init_wait_count_reg__0[3]),
        .O(p_0_in__1[4]));
LUT6 #(
    .INIT(64'hFFFFFFFFFFDFFFFF)) 
     \init_wait_count[5]_i_1__1 
       (.I0(init_wait_count_reg__0[0]),
        .I1(init_wait_count_reg__0[1]),
        .I2(init_wait_count_reg__0[3]),
        .I3(init_wait_count_reg__0[2]),
        .I4(init_wait_count_reg__0[5]),
        .I5(init_wait_count_reg__0[4]),
        .O(\n_0_init_wait_count[5]_i_1__1 ));
LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
     \init_wait_count[5]_i_2__0 
       (.I0(init_wait_count_reg__0[5]),
        .I1(init_wait_count_reg__0[3]),
        .I2(init_wait_count_reg__0[1]),
        .I3(init_wait_count_reg__0[0]),
        .I4(init_wait_count_reg__0[2]),
        .I5(init_wait_count_reg__0[4]),
        .O(p_0_in__1[5]));
FDCE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[0] 
       (.C(CLK),
        .CE(\n_0_init_wait_count[5]_i_1__1 ),
        .CLR(SOFT_RESET_IN),
        .D(\n_0_init_wait_count[0]_i_1__1 ),
        .Q(init_wait_count_reg__0[0]));
FDCE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[1] 
       (.C(CLK),
        .CE(\n_0_init_wait_count[5]_i_1__1 ),
        .CLR(SOFT_RESET_IN),
        .D(p_0_in__1[1]),
        .Q(init_wait_count_reg__0[1]));
FDCE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[2] 
       (.C(CLK),
        .CE(\n_0_init_wait_count[5]_i_1__1 ),
        .CLR(SOFT_RESET_IN),
        .D(p_0_in__1[2]),
        .Q(init_wait_count_reg__0[2]));
FDCE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[3] 
       (.C(CLK),
        .CE(\n_0_init_wait_count[5]_i_1__1 ),
        .CLR(SOFT_RESET_IN),
        .D(p_0_in__1[3]),
        .Q(init_wait_count_reg__0[3]));
FDCE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[4] 
       (.C(CLK),
        .CE(\n_0_init_wait_count[5]_i_1__1 ),
        .CLR(SOFT_RESET_IN),
        .D(p_0_in__1[4]),
        .Q(init_wait_count_reg__0[4]));
FDCE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[5] 
       (.C(CLK),
        .CE(\n_0_init_wait_count[5]_i_1__1 ),
        .CLR(SOFT_RESET_IN),
        .D(p_0_in__1[5]),
        .Q(init_wait_count_reg__0[5]));
LUT2 #(
    .INIT(4'hE)) 
     init_wait_done_i_1__1
       (.I0(init_wait_done),
        .I1(n_0_init_wait_done_reg),
        .O(n_0_init_wait_done_i_1__1));
LUT6 #(
    .INIT(64'h0000040000000000)) 
     init_wait_done_i_2__0
       (.I0(init_wait_count_reg__0[4]),
        .I1(init_wait_count_reg__0[5]),
        .I2(init_wait_count_reg__0[2]),
        .I3(init_wait_count_reg__0[3]),
        .I4(init_wait_count_reg__0[1]),
        .I5(init_wait_count_reg__0[0]),
        .O(init_wait_done));
FDCE #(
    .INIT(1'b0)) 
     init_wait_done_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(SOFT_RESET_IN),
        .D(n_0_init_wait_done_i_1__1),
        .Q(n_0_init_wait_done_reg));
(* SOFT_HLUTNM = "soft_lutpair15" *) 
   LUT1 #(
    .INIT(2'h1)) 
     \mmcm_lock_count[0]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[0]),
        .O(p_0_in__2[0]));
(* SOFT_HLUTNM = "soft_lutpair15" *) 
   LUT2 #(
    .INIT(4'h6)) 
     \mmcm_lock_count[1]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[0]),
        .I1(mmcm_lock_count_reg__0[1]),
        .O(p_0_in__2[1]));
(* SOFT_HLUTNM = "soft_lutpair12" *) 
   LUT3 #(
    .INIT(8'h78)) 
     \mmcm_lock_count[2]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[0]),
        .I1(mmcm_lock_count_reg__0[1]),
        .I2(mmcm_lock_count_reg__0[2]),
        .O(p_0_in__2[2]));
(* SOFT_HLUTNM = "soft_lutpair12" *) 
   LUT4 #(
    .INIT(16'h6AAA)) 
     \mmcm_lock_count[3]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[3]),
        .I1(mmcm_lock_count_reg__0[0]),
        .I2(mmcm_lock_count_reg__0[1]),
        .I3(mmcm_lock_count_reg__0[2]),
        .O(p_0_in__2[3]));
(* SOFT_HLUTNM = "soft_lutpair8" *) 
   LUT5 #(
    .INIT(32'h6AAAAAAA)) 
     \mmcm_lock_count[4]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[4]),
        .I1(mmcm_lock_count_reg__0[2]),
        .I2(mmcm_lock_count_reg__0[1]),
        .I3(mmcm_lock_count_reg__0[0]),
        .I4(mmcm_lock_count_reg__0[3]),
        .O(p_0_in__2[4]));
LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
     \mmcm_lock_count[5]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[5]),
        .I1(mmcm_lock_count_reg__0[3]),
        .I2(mmcm_lock_count_reg__0[0]),
        .I3(mmcm_lock_count_reg__0[1]),
        .I4(mmcm_lock_count_reg__0[2]),
        .I5(mmcm_lock_count_reg__0[4]),
        .O(p_0_in__2[5]));
LUT3 #(
    .INIT(8'h6A)) 
     \mmcm_lock_count[6]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[6]),
        .I1(\n_0_mmcm_lock_count[9]_i_4__0 ),
        .I2(mmcm_lock_count_reg__0[5]),
        .O(p_0_in__2[6]));
(* SOFT_HLUTNM = "soft_lutpair9" *) 
   LUT4 #(
    .INIT(16'h6AAA)) 
     \mmcm_lock_count[7]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[7]),
        .I1(mmcm_lock_count_reg__0[5]),
        .I2(mmcm_lock_count_reg__0[6]),
        .I3(\n_0_mmcm_lock_count[9]_i_4__0 ),
        .O(p_0_in__2[7]));
(* SOFT_HLUTNM = "soft_lutpair9" *) 
   LUT5 #(
    .INIT(32'h6AAAAAAA)) 
     \mmcm_lock_count[8]_i_1__0 
       (.I0(mmcm_lock_count_reg__0[8]),
        .I1(mmcm_lock_count_reg__0[7]),
        .I2(\n_0_mmcm_lock_count[9]_i_4__0 ),
        .I3(mmcm_lock_count_reg__0[6]),
        .I4(mmcm_lock_count_reg__0[5]),
        .O(p_0_in__2[8]));
LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
     \mmcm_lock_count[9]_i_2__0 
       (.I0(mmcm_lock_count_reg__0[9]),
        .I1(mmcm_lock_count_reg__0[8]),
        .I2(mmcm_lock_count_reg__0[7]),
        .I3(mmcm_lock_count_reg__0[5]),
        .I4(mmcm_lock_count_reg__0[6]),
        .I5(\n_0_mmcm_lock_count[9]_i_4__0 ),
        .O(\n_0_mmcm_lock_count[9]_i_2__0 ));
LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
     \mmcm_lock_count[9]_i_3__0 
       (.I0(mmcm_lock_count_reg__0[9]),
        .I1(mmcm_lock_count_reg__0[7]),
        .I2(mmcm_lock_count_reg__0[8]),
        .I3(\n_0_mmcm_lock_count[9]_i_4__0 ),
        .I4(mmcm_lock_count_reg__0[6]),
        .I5(mmcm_lock_count_reg__0[5]),
        .O(p_0_in__2[9]));
(* SOFT_HLUTNM = "soft_lutpair8" *) 
   LUT5 #(
    .INIT(32'h80000000)) 
     \mmcm_lock_count[9]_i_4__0 
       (.I0(mmcm_lock_count_reg__0[4]),
        .I1(mmcm_lock_count_reg__0[2]),
        .I2(mmcm_lock_count_reg__0[1]),
        .I3(mmcm_lock_count_reg__0[0]),
        .I4(mmcm_lock_count_reg__0[3]),
        .O(\n_0_mmcm_lock_count[9]_i_4__0 ));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[0] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2__0 ),
        .D(p_0_in__2[0]),
        .Q(mmcm_lock_count_reg__0[0]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[1] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2__0 ),
        .D(p_0_in__2[1]),
        .Q(mmcm_lock_count_reg__0[1]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[2] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2__0 ),
        .D(p_0_in__2[2]),
        .Q(mmcm_lock_count_reg__0[2]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[3] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2__0 ),
        .D(p_0_in__2[3]),
        .Q(mmcm_lock_count_reg__0[3]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[4] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2__0 ),
        .D(p_0_in__2[4]),
        .Q(mmcm_lock_count_reg__0[4]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[5] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2__0 ),
        .D(p_0_in__2[5]),
        .Q(mmcm_lock_count_reg__0[5]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[6] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2__0 ),
        .D(p_0_in__2[6]),
        .Q(mmcm_lock_count_reg__0[6]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[7] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2__0 ),
        .D(p_0_in__2[7]),
        .Q(mmcm_lock_count_reg__0[7]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[8] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2__0 ),
        .D(p_0_in__2[8]),
        .Q(mmcm_lock_count_reg__0[8]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[9] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2__0 ),
        .D(p_0_in__2[9]),
        .Q(mmcm_lock_count_reg__0[9]),
        .R(n_0_sync_mmcm_lock_reclocked));
LUT6 #(
    .INIT(64'h8000000000000000)) 
     mmcm_lock_reclocked_i_2__0
       (.I0(\n_0_mmcm_lock_count[9]_i_4__0 ),
        .I1(mmcm_lock_count_reg__0[6]),
        .I2(mmcm_lock_count_reg__0[5]),
        .I3(mmcm_lock_count_reg__0[7]),
        .I4(mmcm_lock_count_reg__0[8]),
        .I5(mmcm_lock_count_reg__0[9]),
        .O(n_0_mmcm_lock_reclocked_i_2__0));
FDRE #(
    .INIT(1'b0)) 
     mmcm_lock_reclocked_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_1_sync_mmcm_lock_reclocked),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
LUT5 #(
    .INIT(32'h000000E2)) 
     reset_time_out_i_2__0
       (.I0(I2),
        .I1(rx_state[0]),
        .I2(mmcm_lock_reclocked),
        .I3(rx_state[1]),
        .I4(rx_state[3]),
        .O(n_0_reset_time_out_i_2__0));
LUT3 #(
    .INIT(8'h08)) 
     reset_time_out_i_4__0
       (.I0(rxresetdone_s3),
        .I1(rx_state[1]),
        .I2(rx_state[3]),
        .O(n_0_reset_time_out_i_4__0));
LUT5 #(
    .INIT(32'h55227762)) 
     reset_time_out_i_5
       (.I0(rx_state[3]),
        .I1(rx_state[2]),
        .I2(I2),
        .I3(rx_state[0]),
        .I4(rx_state[1]),
        .O(n_0_reset_time_out_i_5));
FDSE #(
    .INIT(1'b0)) 
     reset_time_out_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_sync_PLL0LOCK),
        .Q(n_0_reset_time_out_reg),
        .S(SOFT_RESET_IN));
LUT5 #(
    .INIT(32'hFFFD0004)) 
     run_phase_alignment_int_i_1__0
       (.I0(rx_state[0]),
        .I1(rx_state[3]),
        .I2(rx_state[1]),
        .I3(rx_state[2]),
        .I4(n_0_run_phase_alignment_int_reg),
        .O(n_0_run_phase_alignment_int_i_1__0));
FDRE #(
    .INIT(1'b0)) 
     run_phase_alignment_int_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_run_phase_alignment_int_i_1__0),
        .Q(n_0_run_phase_alignment_int_reg),
        .R(SOFT_RESET_IN));
FDRE #(
    .INIT(1'b0)) 
     run_phase_alignment_int_s3_reg
       (.C(I1),
        .CE(1'b1),
        .D(run_phase_alignment_int_s2),
        .Q(n_0_run_phase_alignment_int_s3_reg),
        .R(1'b0));
LUT2 #(
    .INIT(4'h1)) 
     rx_fsm_reset_done_int_i_2
       (.I0(rx_state[2]),
        .I1(rx_state[0]),
        .O(n_0_rx_fsm_reset_done_int_i_2));
LUT2 #(
    .INIT(4'h2)) 
     rx_fsm_reset_done_int_i_4
       (.I0(n_0_time_out_1us_reg),
        .I1(n_0_reset_time_out_reg),
        .O(n_0_rx_fsm_reset_done_int_i_4));
LUT2 #(
    .INIT(4'hB)) 
     rx_fsm_reset_done_int_i_5
       (.I0(rx_state[2]),
        .I1(rx_state[3]),
        .O(n_0_rx_fsm_reset_done_int_i_5));
FDRE #(
    .INIT(1'b0)) 
     rx_fsm_reset_done_int_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_1_sync_data_valid),
        .Q(GT0_RX_FSM_RESET_DONE_OUT),
        .R(SOFT_RESET_IN));
FDRE #(
    .INIT(1'b0)) 
     rx_fsm_reset_done_int_s3_reg
       (.C(I1),
        .CE(1'b1),
        .D(rx_fsm_reset_done_int_s2),
        .Q(rx_fsm_reset_done_int_s3),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     rxresetdone_s3_reg
       (.C(CLK),
        .CE(1'b1),
        .D(rxresetdone_s2),
        .Q(rxresetdone_s3),
        .R(1'b0));
gtp_sata_gtp_sata_sync_block__parameterized0_6 sync_PLL0LOCK
       (.CLK(CLK),
        .GT0_PLL0LOCK_OUT(GT0_PLL0LOCK_OUT),
        .I1(n_0_reset_time_out_i_2__0),
        .I2(n_0_reset_time_out_i_4__0),
        .I3(n_0_reset_time_out_i_5),
        .I4(n_0_reset_time_out_reg),
        .I5(n_0_time_out_2ms_reg),
        .I6(\n_0_FSM_sequential_rx_state[3]_i_9 ),
        .I7(\n_0_FSM_sequential_rx_state[3]_i_10 ),
        .O1(n_0_sync_PLL0LOCK),
        .O2(n_1_sync_PLL0LOCK),
        .data_out(data_valid_sync),
        .out(rx_state));
gtp_sata_gtp_sata_sync_block__parameterized0_7 sync_RXRESETDONE
       (.CLK(CLK),
        .data_out(rxresetdone_s2),
        .gt0_rxresetdone_out(gt0_rxresetdone_out));
gtp_sata_gtp_sata_sync_block__parameterized0_8 sync_data_valid
       (.CLK(CLK),
        .D({n_2_sync_data_valid,n_3_sync_data_valid,n_4_sync_data_valid}),
        .DONT_RESET_ON_DATA_ERROR_IN(DONT_RESET_ON_DATA_ERROR_IN),
        .E(n_5_sync_data_valid),
        .GT0_DATA_VALID_IN(GT0_DATA_VALID_IN),
        .GT0_RX_FSM_RESET_DONE_OUT(GT0_RX_FSM_RESET_DONE_OUT),
        .I1(n_0_time_out_100us_reg),
        .I10(n_0_rx_fsm_reset_done_int_i_4),
        .I11(n_0_rx_fsm_reset_done_int_i_5),
        .I12(\n_0_FSM_sequential_rx_state[3]_i_7 ),
        .I2(n_0_reset_time_out_reg),
        .I3(n_0_time_out_1us_reg),
        .I4(n_0_rx_fsm_reset_done_int_i_2),
        .I5(\n_0_FSM_sequential_rx_state[0]_i_2 ),
        .I6(n_0_time_out_2ms_reg),
        .I7(n_1_sync_PLL0LOCK),
        .I8(\n_0_FSM_sequential_rx_state[3]_i_6 ),
        .I9(I2),
        .O1(n_1_sync_data_valid),
        .data_out(data_valid_sync),
        .out(rx_state),
        .rx_state16_out(rx_state16_out),
        .time_out_wait_bypass_s3(time_out_wait_bypass_s3));
gtp_sata_gtp_sata_sync_block__parameterized0_9 sync_mmcm_lock_reclocked
       (.CLK(CLK),
        .I1(n_0_mmcm_lock_reclocked_i_2__0),
        .O1(n_1_sync_mmcm_lock_reclocked),
        .SR(n_0_sync_mmcm_lock_reclocked),
        .mmcm_lock_reclocked(mmcm_lock_reclocked));
gtp_sata_gtp_sata_sync_block__parameterized0_10 sync_run_phase_alignment_int
       (.I1(I1),
        .data_in(n_0_run_phase_alignment_int_reg),
        .data_out(run_phase_alignment_int_s2));
gtp_sata_gtp_sata_sync_block__parameterized0_11 sync_rx_fsm_reset_done_int
       (.GT0_RX_FSM_RESET_DONE_OUT(GT0_RX_FSM_RESET_DONE_OUT),
        .I1(I1),
        .data_out(rx_fsm_reset_done_int_s2));
gtp_sata_gtp_sata_sync_block__parameterized0_12 sync_time_out_wait_bypass
       (.CLK(CLK),
        .data_in(n_0_time_out_wait_bypass_reg),
        .data_out(time_out_wait_bypass_s2));
LUT6 #(
    .INIT(64'hFFFFFFFF40000000)) 
     time_out_100us_i_1
       (.I0(time_out_counter_reg[0]),
        .I1(time_out_counter_reg[1]),
        .I2(n_0_time_out_100us_i_2),
        .I3(n_0_time_out_100us_i_3),
        .I4(n_0_time_out_100us_i_4),
        .I5(n_0_time_out_100us_reg),
        .O(n_0_time_out_100us_i_1));
LUT2 #(
    .INIT(4'h1)) 
     time_out_100us_i_2
       (.I0(time_out_counter_reg[8]),
        .I1(time_out_counter_reg[9]),
        .O(n_0_time_out_100us_i_2));
LUT6 #(
    .INIT(64'h0000000000040000)) 
     time_out_100us_i_3
       (.I0(n_0_time_out_100us_i_5),
        .I1(time_out_counter_reg[6]),
        .I2(time_out_counter_reg[7]),
        .I3(\n_0_time_out_counter[0]_i_11 ),
        .I4(time_out_counter_reg[12]),
        .I5(time_out_counter_reg[16]),
        .O(n_0_time_out_100us_i_3));
LUT6 #(
    .INIT(64'h0000000000010000)) 
     time_out_100us_i_4
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[13]),
        .I2(time_out_counter_reg[14]),
        .I3(time_out_counter_reg[15]),
        .I4(time_out_counter_reg[11]),
        .I5(time_out_counter_reg[10]),
        .O(n_0_time_out_100us_i_4));
(* SOFT_HLUTNM = "soft_lutpair11" *) 
   LUT2 #(
    .INIT(4'hB)) 
     time_out_100us_i_5
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .O(n_0_time_out_100us_i_5));
FDRE #(
    .INIT(1'b0)) 
     time_out_100us_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_time_out_100us_i_1),
        .Q(n_0_time_out_100us_reg),
        .R(n_0_reset_time_out_reg));
LUT6 #(
    .INIT(64'hFFFFFFFF00010000)) 
     time_out_1us_i_1
       (.I0(time_out_counter_reg[15]),
        .I1(time_out_counter_reg[14]),
        .I2(time_out_counter_reg[13]),
        .I3(time_out_counter_reg[17]),
        .I4(n_0_time_out_1us_i_2),
        .I5(n_0_time_out_1us_reg),
        .O(n_0_time_out_1us_i_1));
LUT6 #(
    .INIT(64'h0000000800000000)) 
     time_out_1us_i_2
       (.I0(time_out_counter_reg[5]),
        .I1(time_out_counter_reg[4]),
        .I2(time_out_counter_reg[8]),
        .I3(time_out_counter_reg[9]),
        .I4(n_0_time_out_1us_i_3),
        .I5(n_0_time_out_1us_i_4),
        .O(n_0_time_out_1us_i_2));
(* SOFT_HLUTNM = "soft_lutpair11" *) 
   LUT4 #(
    .INIT(16'hFFF7)) 
     time_out_1us_i_3
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[7]),
        .I3(time_out_counter_reg[0]),
        .O(n_0_time_out_1us_i_3));
LUT6 #(
    .INIT(64'h0000000000000004)) 
     time_out_1us_i_4
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[1]),
        .I2(time_out_counter_reg[11]),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_counter_reg[16]),
        .I5(time_out_counter_reg[6]),
        .O(n_0_time_out_1us_i_4));
FDRE #(
    .INIT(1'b0)) 
     time_out_1us_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_time_out_1us_i_1),
        .Q(n_0_time_out_1us_reg),
        .R(n_0_reset_time_out_reg));
LUT2 #(
    .INIT(4'hE)) 
     time_out_2ms_i_1
       (.I0(time_out_2ms),
        .I1(n_0_time_out_2ms_reg),
        .O(n_0_time_out_2ms_i_1));
FDRE #(
    .INIT(1'b0)) 
     time_out_2ms_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_time_out_2ms_i_1),
        .Q(n_0_time_out_2ms_reg),
        .R(n_0_reset_time_out_reg));
LUT1 #(
    .INIT(2'h1)) 
     \time_out_counter[0]_i_1 
       (.I0(time_out_2ms),
        .O(\n_0_time_out_counter[0]_i_1 ));
LUT2 #(
    .INIT(4'h7)) 
     \time_out_counter[0]_i_10 
       (.I0(time_out_counter_reg[11]),
        .I1(time_out_counter_reg[10]),
        .O(\n_0_time_out_counter[0]_i_10 ));
LUT2 #(
    .INIT(4'hB)) 
     \time_out_counter[0]_i_11 
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[5]),
        .O(\n_0_time_out_counter[0]_i_11 ));
LUT6 #(
    .INIT(64'h0000000000000200)) 
     \time_out_counter[0]_i_3 
       (.I0(\n_0_time_out_counter[0]_i_8__0 ),
        .I1(time_out_counter_reg[1]),
        .I2(time_out_counter_reg[8]),
        .I3(\n_0_time_out_counter[0]_i_9__0 ),
        .I4(\n_0_time_out_counter[0]_i_10 ),
        .I5(\n_0_time_out_counter[0]_i_11 ),
        .O(time_out_2ms));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[0]_i_4__0 
       (.I0(time_out_counter_reg[3]),
        .O(\n_0_time_out_counter[0]_i_4__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[0]_i_5__0 
       (.I0(time_out_counter_reg[2]),
        .O(\n_0_time_out_counter[0]_i_5__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[0]_i_6__0 
       (.I0(time_out_counter_reg[1]),
        .O(\n_0_time_out_counter[0]_i_6__0 ));
LUT1 #(
    .INIT(2'h1)) 
     \time_out_counter[0]_i_7__0 
       (.I0(time_out_counter_reg[0]),
        .O(\n_0_time_out_counter[0]_i_7__0 ));
LUT6 #(
    .INIT(64'h0004000000000000)) 
     \time_out_counter[0]_i_8__0 
       (.I0(time_out_counter_reg[13]),
        .I1(time_out_counter_reg[12]),
        .I2(time_out_counter_reg[0]),
        .I3(time_out_counter_reg[7]),
        .I4(time_out_counter_reg[3]),
        .I5(time_out_counter_reg[2]),
        .O(\n_0_time_out_counter[0]_i_8__0 ));
LUT6 #(
    .INIT(64'h1000000000000000)) 
     \time_out_counter[0]_i_9__0 
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[16]),
        .I2(time_out_counter_reg[14]),
        .I3(time_out_counter_reg[15]),
        .I4(time_out_counter_reg[6]),
        .I5(time_out_counter_reg[17]),
        .O(\n_0_time_out_counter[0]_i_9__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[12]_i_2__0 
       (.I0(time_out_counter_reg[15]),
        .O(\n_0_time_out_counter[12]_i_2__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[12]_i_3__0 
       (.I0(time_out_counter_reg[14]),
        .O(\n_0_time_out_counter[12]_i_3__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[12]_i_4__0 
       (.I0(time_out_counter_reg[13]),
        .O(\n_0_time_out_counter[12]_i_4__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[12]_i_5__0 
       (.I0(time_out_counter_reg[12]),
        .O(\n_0_time_out_counter[12]_i_5__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[16]_i_2__0 
       (.I0(time_out_counter_reg[17]),
        .O(\n_0_time_out_counter[16]_i_2__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[16]_i_3 
       (.I0(time_out_counter_reg[16]),
        .O(\n_0_time_out_counter[16]_i_3 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[4]_i_2__0 
       (.I0(time_out_counter_reg[7]),
        .O(\n_0_time_out_counter[4]_i_2__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[4]_i_3__0 
       (.I0(time_out_counter_reg[6]),
        .O(\n_0_time_out_counter[4]_i_3__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[4]_i_4__0 
       (.I0(time_out_counter_reg[5]),
        .O(\n_0_time_out_counter[4]_i_4__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[4]_i_5__0 
       (.I0(time_out_counter_reg[4]),
        .O(\n_0_time_out_counter[4]_i_5__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[8]_i_2__0 
       (.I0(time_out_counter_reg[11]),
        .O(\n_0_time_out_counter[8]_i_2__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[8]_i_3__0 
       (.I0(time_out_counter_reg[10]),
        .O(\n_0_time_out_counter[8]_i_3__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[8]_i_4__0 
       (.I0(time_out_counter_reg[9]),
        .O(\n_0_time_out_counter[8]_i_4__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[8]_i_5__0 
       (.I0(time_out_counter_reg[8]),
        .O(\n_0_time_out_counter[8]_i_5__0 ));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[0] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_7_time_out_counter_reg[0]_i_2__0 ),
        .Q(time_out_counter_reg[0]),
        .R(n_0_reset_time_out_reg));
CARRY4 \time_out_counter_reg[0]_i_2__0 
       (.CI(1'b0),
        .CO({\n_0_time_out_counter_reg[0]_i_2__0 ,\n_1_time_out_counter_reg[0]_i_2__0 ,\n_2_time_out_counter_reg[0]_i_2__0 ,\n_3_time_out_counter_reg[0]_i_2__0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\n_4_time_out_counter_reg[0]_i_2__0 ,\n_5_time_out_counter_reg[0]_i_2__0 ,\n_6_time_out_counter_reg[0]_i_2__0 ,\n_7_time_out_counter_reg[0]_i_2__0 }),
        .S({\n_0_time_out_counter[0]_i_4__0 ,\n_0_time_out_counter[0]_i_5__0 ,\n_0_time_out_counter[0]_i_6__0 ,\n_0_time_out_counter[0]_i_7__0 }));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[10] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_5_time_out_counter_reg[8]_i_1__0 ),
        .Q(time_out_counter_reg[10]),
        .R(n_0_reset_time_out_reg));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[11] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_4_time_out_counter_reg[8]_i_1__0 ),
        .Q(time_out_counter_reg[11]),
        .R(n_0_reset_time_out_reg));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[12] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_7_time_out_counter_reg[12]_i_1__0 ),
        .Q(time_out_counter_reg[12]),
        .R(n_0_reset_time_out_reg));
CARRY4 \time_out_counter_reg[12]_i_1__0 
       (.CI(\n_0_time_out_counter_reg[8]_i_1__0 ),
        .CO({\n_0_time_out_counter_reg[12]_i_1__0 ,\n_1_time_out_counter_reg[12]_i_1__0 ,\n_2_time_out_counter_reg[12]_i_1__0 ,\n_3_time_out_counter_reg[12]_i_1__0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\n_4_time_out_counter_reg[12]_i_1__0 ,\n_5_time_out_counter_reg[12]_i_1__0 ,\n_6_time_out_counter_reg[12]_i_1__0 ,\n_7_time_out_counter_reg[12]_i_1__0 }),
        .S({\n_0_time_out_counter[12]_i_2__0 ,\n_0_time_out_counter[12]_i_3__0 ,\n_0_time_out_counter[12]_i_4__0 ,\n_0_time_out_counter[12]_i_5__0 }));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[13] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_6_time_out_counter_reg[12]_i_1__0 ),
        .Q(time_out_counter_reg[13]),
        .R(n_0_reset_time_out_reg));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[14] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_5_time_out_counter_reg[12]_i_1__0 ),
        .Q(time_out_counter_reg[14]),
        .R(n_0_reset_time_out_reg));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[15] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_4_time_out_counter_reg[12]_i_1__0 ),
        .Q(time_out_counter_reg[15]),
        .R(n_0_reset_time_out_reg));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[16] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_7_time_out_counter_reg[16]_i_1__0 ),
        .Q(time_out_counter_reg[16]),
        .R(n_0_reset_time_out_reg));
CARRY4 \time_out_counter_reg[16]_i_1__0 
       (.CI(\n_0_time_out_counter_reg[12]_i_1__0 ),
        .CO({\NLW_time_out_counter_reg[16]_i_1__0_CO_UNCONNECTED [3:1],\n_3_time_out_counter_reg[16]_i_1__0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1__0_O_UNCONNECTED [3:2],\n_6_time_out_counter_reg[16]_i_1__0 ,\n_7_time_out_counter_reg[16]_i_1__0 }),
        .S({1'b0,1'b0,\n_0_time_out_counter[16]_i_2__0 ,\n_0_time_out_counter[16]_i_3 }));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[17] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_6_time_out_counter_reg[16]_i_1__0 ),
        .Q(time_out_counter_reg[17]),
        .R(n_0_reset_time_out_reg));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[1] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_6_time_out_counter_reg[0]_i_2__0 ),
        .Q(time_out_counter_reg[1]),
        .R(n_0_reset_time_out_reg));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[2] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_5_time_out_counter_reg[0]_i_2__0 ),
        .Q(time_out_counter_reg[2]),
        .R(n_0_reset_time_out_reg));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[3] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_4_time_out_counter_reg[0]_i_2__0 ),
        .Q(time_out_counter_reg[3]),
        .R(n_0_reset_time_out_reg));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[4] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_7_time_out_counter_reg[4]_i_1__0 ),
        .Q(time_out_counter_reg[4]),
        .R(n_0_reset_time_out_reg));
CARRY4 \time_out_counter_reg[4]_i_1__0 
       (.CI(\n_0_time_out_counter_reg[0]_i_2__0 ),
        .CO({\n_0_time_out_counter_reg[4]_i_1__0 ,\n_1_time_out_counter_reg[4]_i_1__0 ,\n_2_time_out_counter_reg[4]_i_1__0 ,\n_3_time_out_counter_reg[4]_i_1__0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\n_4_time_out_counter_reg[4]_i_1__0 ,\n_5_time_out_counter_reg[4]_i_1__0 ,\n_6_time_out_counter_reg[4]_i_1__0 ,\n_7_time_out_counter_reg[4]_i_1__0 }),
        .S({\n_0_time_out_counter[4]_i_2__0 ,\n_0_time_out_counter[4]_i_3__0 ,\n_0_time_out_counter[4]_i_4__0 ,\n_0_time_out_counter[4]_i_5__0 }));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[5] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_6_time_out_counter_reg[4]_i_1__0 ),
        .Q(time_out_counter_reg[5]),
        .R(n_0_reset_time_out_reg));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[6] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_5_time_out_counter_reg[4]_i_1__0 ),
        .Q(time_out_counter_reg[6]),
        .R(n_0_reset_time_out_reg));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[7] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_4_time_out_counter_reg[4]_i_1__0 ),
        .Q(time_out_counter_reg[7]),
        .R(n_0_reset_time_out_reg));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[8] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_7_time_out_counter_reg[8]_i_1__0 ),
        .Q(time_out_counter_reg[8]),
        .R(n_0_reset_time_out_reg));
CARRY4 \time_out_counter_reg[8]_i_1__0 
       (.CI(\n_0_time_out_counter_reg[4]_i_1__0 ),
        .CO({\n_0_time_out_counter_reg[8]_i_1__0 ,\n_1_time_out_counter_reg[8]_i_1__0 ,\n_2_time_out_counter_reg[8]_i_1__0 ,\n_3_time_out_counter_reg[8]_i_1__0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\n_4_time_out_counter_reg[8]_i_1__0 ,\n_5_time_out_counter_reg[8]_i_1__0 ,\n_6_time_out_counter_reg[8]_i_1__0 ,\n_7_time_out_counter_reg[8]_i_1__0 }),
        .S({\n_0_time_out_counter[8]_i_2__0 ,\n_0_time_out_counter[8]_i_3__0 ,\n_0_time_out_counter[8]_i_4__0 ,\n_0_time_out_counter[8]_i_5__0 }));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[9] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1 ),
        .D(\n_6_time_out_counter_reg[8]_i_1__0 ),
        .Q(time_out_counter_reg[9]),
        .R(n_0_reset_time_out_reg));
LUT6 #(
    .INIT(64'hFF00FF0100000000)) 
     time_out_wait_bypass_i_1__0
       (.I0(\n_0_wait_bypass_count[0]_i_4__0 ),
        .I1(wait_bypass_count_reg[3]),
        .I2(\n_0_wait_bypass_count[0]_i_5 ),
        .I3(n_0_time_out_wait_bypass_reg),
        .I4(rx_fsm_reset_done_int_s3),
        .I5(n_0_run_phase_alignment_int_s3_reg),
        .O(n_0_time_out_wait_bypass_i_1__0));
FDRE #(
    .INIT(1'b0)) 
     time_out_wait_bypass_reg
       (.C(I1),
        .CE(1'b1),
        .D(n_0_time_out_wait_bypass_i_1__0),
        .Q(n_0_time_out_wait_bypass_reg),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     time_out_wait_bypass_s3_reg
       (.C(CLK),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
LUT2 #(
    .INIT(4'h1)) 
     time_tlock_max_i_10
       (.I0(time_out_counter_reg[15]),
        .I1(time_out_counter_reg[14]),
        .O(n_0_time_tlock_max_i_10));
LUT2 #(
    .INIT(4'h2)) 
     time_tlock_max_i_11
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[13]),
        .O(n_0_time_tlock_max_i_11));
LUT2 #(
    .INIT(4'h2)) 
     time_tlock_max_i_12
       (.I0(time_out_counter_reg[11]),
        .I1(time_out_counter_reg[10]),
        .O(n_0_time_tlock_max_i_12));
LUT2 #(
    .INIT(4'h1)) 
     time_tlock_max_i_13
       (.I0(time_out_counter_reg[8]),
        .I1(time_out_counter_reg[9]),
        .O(n_0_time_tlock_max_i_13));
LUT2 #(
    .INIT(4'h8)) 
     time_tlock_max_i_14
       (.I0(time_out_counter_reg[4]),
        .I1(time_out_counter_reg[5]),
        .O(n_0_time_tlock_max_i_14));
LUT2 #(
    .INIT(4'h8)) 
     time_tlock_max_i_15
       (.I0(time_out_counter_reg[2]),
        .I1(time_out_counter_reg[3]),
        .O(n_0_time_tlock_max_i_15));
LUT2 #(
    .INIT(4'h8)) 
     time_tlock_max_i_16
       (.I0(time_out_counter_reg[1]),
        .I1(time_out_counter_reg[0]),
        .O(n_0_time_tlock_max_i_16));
LUT2 #(
    .INIT(4'h2)) 
     time_tlock_max_i_17
       (.I0(time_out_counter_reg[6]),
        .I1(time_out_counter_reg[7]),
        .O(n_0_time_tlock_max_i_17));
LUT2 #(
    .INIT(4'h2)) 
     time_tlock_max_i_18
       (.I0(time_out_counter_reg[5]),
        .I1(time_out_counter_reg[4]),
        .O(n_0_time_tlock_max_i_18));
LUT2 #(
    .INIT(4'h2)) 
     time_tlock_max_i_19
       (.I0(time_out_counter_reg[3]),
        .I1(time_out_counter_reg[2]),
        .O(n_0_time_tlock_max_i_19));
(* SOFT_HLUTNM = "soft_lutpair14" *) 
   LUT3 #(
    .INIT(8'hF8)) 
     time_tlock_max_i_1__0
       (.I0(n_0_check_tlock_max_reg),
        .I1(time_tlock_max1),
        .I2(time_tlock_max),
        .O(n_0_time_tlock_max_i_1__0));
LUT2 #(
    .INIT(4'h2)) 
     time_tlock_max_i_20
       (.I0(time_out_counter_reg[1]),
        .I1(time_out_counter_reg[0]),
        .O(n_0_time_tlock_max_i_20));
LUT2 #(
    .INIT(4'hE)) 
     time_tlock_max_i_4
       (.I0(time_out_counter_reg[17]),
        .I1(time_out_counter_reg[16]),
        .O(n_0_time_tlock_max_i_4));
LUT2 #(
    .INIT(4'h1)) 
     time_tlock_max_i_5
       (.I0(time_out_counter_reg[16]),
        .I1(time_out_counter_reg[17]),
        .O(n_0_time_tlock_max_i_5));
LUT2 #(
    .INIT(4'hE)) 
     time_tlock_max_i_7
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[15]),
        .O(n_0_time_tlock_max_i_7));
LUT2 #(
    .INIT(4'h8)) 
     time_tlock_max_i_8
       (.I0(time_out_counter_reg[10]),
        .I1(time_out_counter_reg[11]),
        .O(n_0_time_tlock_max_i_8));
LUT2 #(
    .INIT(4'hE)) 
     time_tlock_max_i_9
       (.I0(time_out_counter_reg[9]),
        .I1(time_out_counter_reg[8]),
        .O(n_0_time_tlock_max_i_9));
FDRE #(
    .INIT(1'b0)) 
     time_tlock_max_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_time_tlock_max_i_1__0),
        .Q(time_tlock_max),
        .R(n_0_reset_time_out_reg));
CARRY4 time_tlock_max_reg_i_2
       (.CI(n_0_time_tlock_max_reg_i_3),
        .CO({NLW_time_tlock_max_reg_i_2_CO_UNCONNECTED[3:1],time_tlock_max1}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,n_0_time_tlock_max_i_4}),
        .O(NLW_time_tlock_max_reg_i_2_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,n_0_time_tlock_max_i_5}));
CARRY4 time_tlock_max_reg_i_3
       (.CI(n_0_time_tlock_max_reg_i_6),
        .CO({n_0_time_tlock_max_reg_i_3,n_1_time_tlock_max_reg_i_3,n_2_time_tlock_max_reg_i_3,n_3_time_tlock_max_reg_i_3}),
        .CYINIT(1'b0),
        .DI({n_0_time_tlock_max_i_7,time_out_counter_reg[13],n_0_time_tlock_max_i_8,n_0_time_tlock_max_i_9}),
        .O(NLW_time_tlock_max_reg_i_3_O_UNCONNECTED[3:0]),
        .S({n_0_time_tlock_max_i_10,n_0_time_tlock_max_i_11,n_0_time_tlock_max_i_12,n_0_time_tlock_max_i_13}));
CARRY4 time_tlock_max_reg_i_6
       (.CI(1'b0),
        .CO({n_0_time_tlock_max_reg_i_6,n_1_time_tlock_max_reg_i_6,n_2_time_tlock_max_reg_i_6,n_3_time_tlock_max_reg_i_6}),
        .CYINIT(1'b0),
        .DI({time_out_counter_reg[7],n_0_time_tlock_max_i_14,n_0_time_tlock_max_i_15,n_0_time_tlock_max_i_16}),
        .O(NLW_time_tlock_max_reg_i_6_O_UNCONNECTED[3:0]),
        .S({n_0_time_tlock_max_i_17,n_0_time_tlock_max_i_18,n_0_time_tlock_max_i_19,n_0_time_tlock_max_i_20}));
LUT1 #(
    .INIT(2'h1)) 
     \wait_bypass_count[0]_i_1__0 
       (.I0(n_0_run_phase_alignment_int_s3_reg),
        .O(\n_0_wait_bypass_count[0]_i_1__0 ));
LUT4 #(
    .INIT(16'h00FE)) 
     \wait_bypass_count[0]_i_2__0 
       (.I0(\n_0_wait_bypass_count[0]_i_4__0 ),
        .I1(wait_bypass_count_reg[3]),
        .I2(\n_0_wait_bypass_count[0]_i_5 ),
        .I3(rx_fsm_reset_done_int_s3),
        .O(\n_0_wait_bypass_count[0]_i_2__0 ));
LUT6 #(
    .INIT(64'hFFFFEFFFFFFFFFFF)) 
     \wait_bypass_count[0]_i_4__0 
       (.I0(wait_bypass_count_reg[11]),
        .I1(wait_bypass_count_reg[4]),
        .I2(wait_bypass_count_reg[0]),
        .I3(wait_bypass_count_reg[9]),
        .I4(wait_bypass_count_reg[10]),
        .I5(wait_bypass_count_reg[2]),
        .O(\n_0_wait_bypass_count[0]_i_4__0 ));
LUT6 #(
    .INIT(64'hFDFFFFFFFFFFFFFF)) 
     \wait_bypass_count[0]_i_5 
       (.I0(wait_bypass_count_reg[1]),
        .I1(wait_bypass_count_reg[6]),
        .I2(wait_bypass_count_reg[5]),
        .I3(wait_bypass_count_reg[12]),
        .I4(wait_bypass_count_reg[8]),
        .I5(wait_bypass_count_reg[7]),
        .O(\n_0_wait_bypass_count[0]_i_5 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[0]_i_6__0 
       (.I0(wait_bypass_count_reg[3]),
        .O(\n_0_wait_bypass_count[0]_i_6__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[0]_i_7__0 
       (.I0(wait_bypass_count_reg[2]),
        .O(\n_0_wait_bypass_count[0]_i_7__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[0]_i_8__0 
       (.I0(wait_bypass_count_reg[1]),
        .O(\n_0_wait_bypass_count[0]_i_8__0 ));
LUT1 #(
    .INIT(2'h1)) 
     \wait_bypass_count[0]_i_9 
       (.I0(wait_bypass_count_reg[0]),
        .O(\n_0_wait_bypass_count[0]_i_9 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[12]_i_2__0 
       (.I0(wait_bypass_count_reg[12]),
        .O(\n_0_wait_bypass_count[12]_i_2__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[4]_i_2__0 
       (.I0(wait_bypass_count_reg[7]),
        .O(\n_0_wait_bypass_count[4]_i_2__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[4]_i_3__0 
       (.I0(wait_bypass_count_reg[6]),
        .O(\n_0_wait_bypass_count[4]_i_3__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[4]_i_4__0 
       (.I0(wait_bypass_count_reg[5]),
        .O(\n_0_wait_bypass_count[4]_i_4__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[4]_i_5__0 
       (.I0(wait_bypass_count_reg[4]),
        .O(\n_0_wait_bypass_count[4]_i_5__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[8]_i_2__0 
       (.I0(wait_bypass_count_reg[11]),
        .O(\n_0_wait_bypass_count[8]_i_2__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[8]_i_3__0 
       (.I0(wait_bypass_count_reg[10]),
        .O(\n_0_wait_bypass_count[8]_i_3__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[8]_i_4__0 
       (.I0(wait_bypass_count_reg[9]),
        .O(\n_0_wait_bypass_count[8]_i_4__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[8]_i_5__0 
       (.I0(wait_bypass_count_reg[8]),
        .O(\n_0_wait_bypass_count[8]_i_5__0 ));
FDRE \wait_bypass_count_reg[0] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2__0 ),
        .D(\n_7_wait_bypass_count_reg[0]_i_3__0 ),
        .Q(wait_bypass_count_reg[0]),
        .R(\n_0_wait_bypass_count[0]_i_1__0 ));
CARRY4 \wait_bypass_count_reg[0]_i_3__0 
       (.CI(1'b0),
        .CO({\n_0_wait_bypass_count_reg[0]_i_3__0 ,\n_1_wait_bypass_count_reg[0]_i_3__0 ,\n_2_wait_bypass_count_reg[0]_i_3__0 ,\n_3_wait_bypass_count_reg[0]_i_3__0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\n_4_wait_bypass_count_reg[0]_i_3__0 ,\n_5_wait_bypass_count_reg[0]_i_3__0 ,\n_6_wait_bypass_count_reg[0]_i_3__0 ,\n_7_wait_bypass_count_reg[0]_i_3__0 }),
        .S({\n_0_wait_bypass_count[0]_i_6__0 ,\n_0_wait_bypass_count[0]_i_7__0 ,\n_0_wait_bypass_count[0]_i_8__0 ,\n_0_wait_bypass_count[0]_i_9 }));
FDRE \wait_bypass_count_reg[10] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2__0 ),
        .D(\n_5_wait_bypass_count_reg[8]_i_1__0 ),
        .Q(wait_bypass_count_reg[10]),
        .R(\n_0_wait_bypass_count[0]_i_1__0 ));
FDRE \wait_bypass_count_reg[11] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2__0 ),
        .D(\n_4_wait_bypass_count_reg[8]_i_1__0 ),
        .Q(wait_bypass_count_reg[11]),
        .R(\n_0_wait_bypass_count[0]_i_1__0 ));
FDRE \wait_bypass_count_reg[12] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2__0 ),
        .D(\n_7_wait_bypass_count_reg[12]_i_1__0 ),
        .Q(wait_bypass_count_reg[12]),
        .R(\n_0_wait_bypass_count[0]_i_1__0 ));
CARRY4 \wait_bypass_count_reg[12]_i_1__0 
       (.CI(\n_0_wait_bypass_count_reg[8]_i_1__0 ),
        .CO(\NLW_wait_bypass_count_reg[12]_i_1__0_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_wait_bypass_count_reg[12]_i_1__0_O_UNCONNECTED [3:1],\n_7_wait_bypass_count_reg[12]_i_1__0 }),
        .S({1'b0,1'b0,1'b0,\n_0_wait_bypass_count[12]_i_2__0 }));
FDRE \wait_bypass_count_reg[1] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2__0 ),
        .D(\n_6_wait_bypass_count_reg[0]_i_3__0 ),
        .Q(wait_bypass_count_reg[1]),
        .R(\n_0_wait_bypass_count[0]_i_1__0 ));
FDRE \wait_bypass_count_reg[2] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2__0 ),
        .D(\n_5_wait_bypass_count_reg[0]_i_3__0 ),
        .Q(wait_bypass_count_reg[2]),
        .R(\n_0_wait_bypass_count[0]_i_1__0 ));
FDRE \wait_bypass_count_reg[3] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2__0 ),
        .D(\n_4_wait_bypass_count_reg[0]_i_3__0 ),
        .Q(wait_bypass_count_reg[3]),
        .R(\n_0_wait_bypass_count[0]_i_1__0 ));
FDRE \wait_bypass_count_reg[4] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2__0 ),
        .D(\n_7_wait_bypass_count_reg[4]_i_1__0 ),
        .Q(wait_bypass_count_reg[4]),
        .R(\n_0_wait_bypass_count[0]_i_1__0 ));
CARRY4 \wait_bypass_count_reg[4]_i_1__0 
       (.CI(\n_0_wait_bypass_count_reg[0]_i_3__0 ),
        .CO({\n_0_wait_bypass_count_reg[4]_i_1__0 ,\n_1_wait_bypass_count_reg[4]_i_1__0 ,\n_2_wait_bypass_count_reg[4]_i_1__0 ,\n_3_wait_bypass_count_reg[4]_i_1__0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\n_4_wait_bypass_count_reg[4]_i_1__0 ,\n_5_wait_bypass_count_reg[4]_i_1__0 ,\n_6_wait_bypass_count_reg[4]_i_1__0 ,\n_7_wait_bypass_count_reg[4]_i_1__0 }),
        .S({\n_0_wait_bypass_count[4]_i_2__0 ,\n_0_wait_bypass_count[4]_i_3__0 ,\n_0_wait_bypass_count[4]_i_4__0 ,\n_0_wait_bypass_count[4]_i_5__0 }));
FDRE \wait_bypass_count_reg[5] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2__0 ),
        .D(\n_6_wait_bypass_count_reg[4]_i_1__0 ),
        .Q(wait_bypass_count_reg[5]),
        .R(\n_0_wait_bypass_count[0]_i_1__0 ));
FDRE \wait_bypass_count_reg[6] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2__0 ),
        .D(\n_5_wait_bypass_count_reg[4]_i_1__0 ),
        .Q(wait_bypass_count_reg[6]),
        .R(\n_0_wait_bypass_count[0]_i_1__0 ));
FDRE \wait_bypass_count_reg[7] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2__0 ),
        .D(\n_4_wait_bypass_count_reg[4]_i_1__0 ),
        .Q(wait_bypass_count_reg[7]),
        .R(\n_0_wait_bypass_count[0]_i_1__0 ));
FDRE \wait_bypass_count_reg[8] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2__0 ),
        .D(\n_7_wait_bypass_count_reg[8]_i_1__0 ),
        .Q(wait_bypass_count_reg[8]),
        .R(\n_0_wait_bypass_count[0]_i_1__0 ));
CARRY4 \wait_bypass_count_reg[8]_i_1__0 
       (.CI(\n_0_wait_bypass_count_reg[4]_i_1__0 ),
        .CO({\n_0_wait_bypass_count_reg[8]_i_1__0 ,\n_1_wait_bypass_count_reg[8]_i_1__0 ,\n_2_wait_bypass_count_reg[8]_i_1__0 ,\n_3_wait_bypass_count_reg[8]_i_1__0 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\n_4_wait_bypass_count_reg[8]_i_1__0 ,\n_5_wait_bypass_count_reg[8]_i_1__0 ,\n_6_wait_bypass_count_reg[8]_i_1__0 ,\n_7_wait_bypass_count_reg[8]_i_1__0 }),
        .S({\n_0_wait_bypass_count[8]_i_2__0 ,\n_0_wait_bypass_count[8]_i_3__0 ,\n_0_wait_bypass_count[8]_i_4__0 ,\n_0_wait_bypass_count[8]_i_5__0 }));
FDRE \wait_bypass_count_reg[9] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2__0 ),
        .D(\n_6_wait_bypass_count_reg[8]_i_1__0 ),
        .Q(wait_bypass_count_reg[9]),
        .R(\n_0_wait_bypass_count[0]_i_1__0 ));
(* SOFT_HLUTNM = "soft_lutpair16" *) 
   LUT1 #(
    .INIT(2'h1)) 
     \wait_time_cnt[0]_i_1__0 
       (.I0(wait_time_cnt_reg__0[0]),
        .O(wait_time_cnt0__0[0]));
(* SOFT_HLUTNM = "soft_lutpair16" *) 
   LUT2 #(
    .INIT(4'h9)) 
     \wait_time_cnt[1]_i_1__0 
       (.I0(wait_time_cnt_reg__0[0]),
        .I1(wait_time_cnt_reg__0[1]),
        .O(\n_0_wait_time_cnt[1]_i_1__0 ));
LUT3 #(
    .INIT(8'hE1)) 
     \wait_time_cnt[2]_i_1__0 
       (.I0(wait_time_cnt_reg__0[0]),
        .I1(wait_time_cnt_reg__0[1]),
        .I2(wait_time_cnt_reg__0[2]),
        .O(wait_time_cnt0__0[2]));
(* SOFT_HLUTNM = "soft_lutpair10" *) 
   LUT4 #(
    .INIT(16'hAAA9)) 
     \wait_time_cnt[3]_i_1__0 
       (.I0(wait_time_cnt_reg__0[3]),
        .I1(wait_time_cnt_reg__0[0]),
        .I2(wait_time_cnt_reg__0[1]),
        .I3(wait_time_cnt_reg__0[2]),
        .O(wait_time_cnt0__0[3]));
(* SOFT_HLUTNM = "soft_lutpair10" *) 
   LUT5 #(
    .INIT(32'hAAAAAAA9)) 
     \wait_time_cnt[4]_i_1__0 
       (.I0(wait_time_cnt_reg__0[4]),
        .I1(wait_time_cnt_reg__0[1]),
        .I2(wait_time_cnt_reg__0[0]),
        .I3(wait_time_cnt_reg__0[2]),
        .I4(wait_time_cnt_reg__0[3]),
        .O(\n_0_wait_time_cnt[4]_i_1__0 ));
LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
     \wait_time_cnt[5]_i_1__0 
       (.I0(wait_time_cnt_reg__0[5]),
        .I1(wait_time_cnt_reg__0[3]),
        .I2(wait_time_cnt_reg__0[2]),
        .I3(wait_time_cnt_reg__0[0]),
        .I4(wait_time_cnt_reg__0[1]),
        .I5(wait_time_cnt_reg__0[4]),
        .O(wait_time_cnt0__0[5]));
LUT3 #(
    .INIT(8'h04)) 
     \wait_time_cnt[6]_i_1__0 
       (.I0(rx_state[1]),
        .I1(rx_state[0]),
        .I2(rx_state[3]),
        .O(\n_0_wait_time_cnt[6]_i_1__0 ));
LUT2 #(
    .INIT(4'hB)) 
     \wait_time_cnt[6]_i_2__0 
       (.I0(wait_time_cnt_reg__0[6]),
        .I1(\n_0_wait_time_cnt[6]_i_4__0 ),
        .O(\n_0_wait_time_cnt[6]_i_2__0 ));
LUT2 #(
    .INIT(4'h6)) 
     \wait_time_cnt[6]_i_3__0 
       (.I0(wait_time_cnt_reg__0[6]),
        .I1(\n_0_wait_time_cnt[6]_i_4__0 ),
        .O(wait_time_cnt0__0[6]));
LUT6 #(
    .INIT(64'h0000000000000001)) 
     \wait_time_cnt[6]_i_4__0 
       (.I0(wait_time_cnt_reg__0[3]),
        .I1(wait_time_cnt_reg__0[2]),
        .I2(wait_time_cnt_reg__0[0]),
        .I3(wait_time_cnt_reg__0[1]),
        .I4(wait_time_cnt_reg__0[4]),
        .I5(wait_time_cnt_reg__0[5]),
        .O(\n_0_wait_time_cnt[6]_i_4__0 ));
FDRE \wait_time_cnt_reg[0] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2__0 ),
        .D(wait_time_cnt0__0[0]),
        .Q(wait_time_cnt_reg__0[0]),
        .R(\n_0_wait_time_cnt[6]_i_1__0 ));
FDRE \wait_time_cnt_reg[1] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2__0 ),
        .D(\n_0_wait_time_cnt[1]_i_1__0 ),
        .Q(wait_time_cnt_reg__0[1]),
        .R(\n_0_wait_time_cnt[6]_i_1__0 ));
FDSE \wait_time_cnt_reg[2] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2__0 ),
        .D(wait_time_cnt0__0[2]),
        .Q(wait_time_cnt_reg__0[2]),
        .S(\n_0_wait_time_cnt[6]_i_1__0 ));
FDRE \wait_time_cnt_reg[3] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2__0 ),
        .D(wait_time_cnt0__0[3]),
        .Q(wait_time_cnt_reg__0[3]),
        .R(\n_0_wait_time_cnt[6]_i_1__0 ));
FDRE \wait_time_cnt_reg[4] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2__0 ),
        .D(\n_0_wait_time_cnt[4]_i_1__0 ),
        .Q(wait_time_cnt_reg__0[4]),
        .R(\n_0_wait_time_cnt[6]_i_1__0 ));
FDSE \wait_time_cnt_reg[5] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2__0 ),
        .D(wait_time_cnt0__0[5]),
        .Q(wait_time_cnt_reg__0[5]),
        .S(\n_0_wait_time_cnt[6]_i_1__0 ));
FDSE \wait_time_cnt_reg[6] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2__0 ),
        .D(wait_time_cnt0__0[6]),
        .Q(wait_time_cnt_reg__0[6]),
        .S(\n_0_wait_time_cnt[6]_i_1__0 ));
endmodule

(* ORIG_REF_NAME = "gtp_sata_TX_STARTUP_FSM" *) 
module gtp_sata_gtp_sata_TX_STARTUP_FSM__parameterized0
   (O1,
    GTTXRESET,
    GT0_TX_FSM_RESET_DONE_OUT,
    TXUSERRDY,
    CLK,
    I1,
    SOFT_RESET_IN,
    gt0_txresetdone_out,
    GT0_PLL0LOCK_OUT);
  output O1;
  output GTTXRESET;
  output GT0_TX_FSM_RESET_DONE_OUT;
  output TXUSERRDY;
  input CLK;
  input I1;
  input SOFT_RESET_IN;
  input gt0_txresetdone_out;
  input GT0_PLL0LOCK_OUT;

  wire CLK;
  wire GT0_PLL0LOCK_OUT;
  wire GT0_TX_FSM_RESET_DONE_OUT;
  wire GTTXRESET;
  wire I1;
  wire O1;
  wire SOFT_RESET_IN;
  wire TXUSERRDY;
  wire clear;
  wire data_out;
  wire gt0_txresetdone_out;
  wire [5:0]init_wait_count_reg__0;
  wire init_wait_done;
  wire [9:0]mmcm_lock_count_reg__0;
  wire mmcm_lock_reclocked;
  wire \n_0_FSM_sequential_tx_state[0]_i_1 ;
  wire \n_0_FSM_sequential_tx_state[0]_i_2 ;
  wire \n_0_FSM_sequential_tx_state[1]_i_1 ;
  wire \n_0_FSM_sequential_tx_state[2]_i_1 ;
  wire \n_0_FSM_sequential_tx_state[2]_i_2 ;
  wire \n_0_FSM_sequential_tx_state[3]_i_2 ;
  wire \n_0_FSM_sequential_tx_state[3]_i_5 ;
  wire \n_0_FSM_sequential_tx_state[3]_i_6 ;
  wire n_0_PLL0_RESET_i_1;
  wire n_0_TXUSERRDY_i_1;
  wire n_0_gttxreset_i_i_1;
  wire \n_0_init_wait_count[0]_i_1__0 ;
  wire \n_0_init_wait_count[5]_i_1__0 ;
  wire n_0_init_wait_done_i_1__0;
  wire n_0_init_wait_done_reg;
  wire \n_0_mmcm_lock_count[9]_i_2 ;
  wire \n_0_mmcm_lock_count[9]_i_4 ;
  wire n_0_mmcm_lock_reclocked_i_2;
  wire n_0_pll_reset_asserted_i_1;
  wire n_0_pll_reset_asserted_reg;
  wire n_0_run_phase_alignment_int_i_1;
  wire n_0_run_phase_alignment_int_reg;
  wire n_0_sync_PLL0LOCK;
  wire n_0_sync_mmcm_lock_reclocked;
  wire n_0_time_out_2ms_i_1__0;
  wire n_0_time_out_2ms_reg;
  wire n_0_time_out_500us_i_1;
  wire n_0_time_out_500us_i_2;
  wire n_0_time_out_500us_i_3;
  wire n_0_time_out_500us_reg;
  wire \n_0_time_out_counter[0]_i_10__0 ;
  wire \n_0_time_out_counter[0]_i_1__0 ;
  wire \n_0_time_out_counter[0]_i_4 ;
  wire \n_0_time_out_counter[0]_i_5 ;
  wire \n_0_time_out_counter[0]_i_6 ;
  wire \n_0_time_out_counter[0]_i_7 ;
  wire \n_0_time_out_counter[0]_i_8 ;
  wire \n_0_time_out_counter[0]_i_9 ;
  wire \n_0_time_out_counter[12]_i_2 ;
  wire \n_0_time_out_counter[12]_i_3 ;
  wire \n_0_time_out_counter[12]_i_4 ;
  wire \n_0_time_out_counter[12]_i_5 ;
  wire \n_0_time_out_counter[16]_i_2 ;
  wire \n_0_time_out_counter[4]_i_2 ;
  wire \n_0_time_out_counter[4]_i_3 ;
  wire \n_0_time_out_counter[4]_i_4 ;
  wire \n_0_time_out_counter[4]_i_5 ;
  wire \n_0_time_out_counter[8]_i_2 ;
  wire \n_0_time_out_counter[8]_i_3 ;
  wire \n_0_time_out_counter[8]_i_4 ;
  wire \n_0_time_out_counter[8]_i_5 ;
  wire \n_0_time_out_counter_reg[0]_i_2 ;
  wire \n_0_time_out_counter_reg[12]_i_1 ;
  wire \n_0_time_out_counter_reg[4]_i_1 ;
  wire \n_0_time_out_counter_reg[8]_i_1 ;
  wire n_0_time_out_wait_bypass_i_1;
  wire n_0_time_out_wait_bypass_reg;
  wire n_0_time_tlock_max_i_1;
  wire n_0_time_tlock_max_i_2;
  wire n_0_time_tlock_max_i_3;
  wire n_0_time_tlock_max_i_4__0;
  wire n_0_time_tlock_max_reg;
  wire n_0_tx_fsm_reset_done_int_i_1;
  wire \n_0_wait_bypass_count[0]_i_1 ;
  wire \n_0_wait_bypass_count[0]_i_10 ;
  wire \n_0_wait_bypass_count[0]_i_2 ;
  wire \n_0_wait_bypass_count[0]_i_4 ;
  wire \n_0_wait_bypass_count[0]_i_5__0 ;
  wire \n_0_wait_bypass_count[0]_i_6 ;
  wire \n_0_wait_bypass_count[0]_i_7 ;
  wire \n_0_wait_bypass_count[0]_i_8 ;
  wire \n_0_wait_bypass_count[0]_i_9__0 ;
  wire \n_0_wait_bypass_count[12]_i_2 ;
  wire \n_0_wait_bypass_count[12]_i_3 ;
  wire \n_0_wait_bypass_count[12]_i_4 ;
  wire \n_0_wait_bypass_count[12]_i_5 ;
  wire \n_0_wait_bypass_count[4]_i_2 ;
  wire \n_0_wait_bypass_count[4]_i_3 ;
  wire \n_0_wait_bypass_count[4]_i_4 ;
  wire \n_0_wait_bypass_count[4]_i_5 ;
  wire \n_0_wait_bypass_count[8]_i_2 ;
  wire \n_0_wait_bypass_count[8]_i_3 ;
  wire \n_0_wait_bypass_count[8]_i_4 ;
  wire \n_0_wait_bypass_count[8]_i_5 ;
  wire \n_0_wait_bypass_count_reg[0]_i_3 ;
  wire \n_0_wait_bypass_count_reg[4]_i_1 ;
  wire \n_0_wait_bypass_count_reg[8]_i_1 ;
  wire \n_0_wait_time_cnt[1]_i_1 ;
  wire \n_0_wait_time_cnt[4]_i_1 ;
  wire \n_0_wait_time_cnt[6]_i_2 ;
  wire \n_0_wait_time_cnt[6]_i_4 ;
  wire n_1_sync_PLL0LOCK;
  wire n_1_sync_mmcm_lock_reclocked;
  wire \n_1_time_out_counter_reg[0]_i_2 ;
  wire \n_1_time_out_counter_reg[12]_i_1 ;
  wire \n_1_time_out_counter_reg[4]_i_1 ;
  wire \n_1_time_out_counter_reg[8]_i_1 ;
  wire \n_1_wait_bypass_count_reg[0]_i_3 ;
  wire \n_1_wait_bypass_count_reg[12]_i_1 ;
  wire \n_1_wait_bypass_count_reg[4]_i_1 ;
  wire \n_1_wait_bypass_count_reg[8]_i_1 ;
  wire \n_2_time_out_counter_reg[0]_i_2 ;
  wire \n_2_time_out_counter_reg[12]_i_1 ;
  wire \n_2_time_out_counter_reg[4]_i_1 ;
  wire \n_2_time_out_counter_reg[8]_i_1 ;
  wire \n_2_wait_bypass_count_reg[0]_i_3 ;
  wire \n_2_wait_bypass_count_reg[12]_i_1 ;
  wire \n_2_wait_bypass_count_reg[4]_i_1 ;
  wire \n_2_wait_bypass_count_reg[8]_i_1 ;
  wire \n_3_time_out_counter_reg[0]_i_2 ;
  wire \n_3_time_out_counter_reg[12]_i_1 ;
  wire \n_3_time_out_counter_reg[4]_i_1 ;
  wire \n_3_time_out_counter_reg[8]_i_1 ;
  wire \n_3_wait_bypass_count_reg[0]_i_3 ;
  wire \n_3_wait_bypass_count_reg[12]_i_1 ;
  wire \n_3_wait_bypass_count_reg[4]_i_1 ;
  wire \n_3_wait_bypass_count_reg[8]_i_1 ;
  wire \n_4_time_out_counter_reg[0]_i_2 ;
  wire \n_4_time_out_counter_reg[12]_i_1 ;
  wire \n_4_time_out_counter_reg[4]_i_1 ;
  wire \n_4_time_out_counter_reg[8]_i_1 ;
  wire \n_4_wait_bypass_count_reg[0]_i_3 ;
  wire \n_4_wait_bypass_count_reg[12]_i_1 ;
  wire \n_4_wait_bypass_count_reg[4]_i_1 ;
  wire \n_4_wait_bypass_count_reg[8]_i_1 ;
  wire \n_5_time_out_counter_reg[0]_i_2 ;
  wire \n_5_time_out_counter_reg[12]_i_1 ;
  wire \n_5_time_out_counter_reg[4]_i_1 ;
  wire \n_5_time_out_counter_reg[8]_i_1 ;
  wire \n_5_wait_bypass_count_reg[0]_i_3 ;
  wire \n_5_wait_bypass_count_reg[12]_i_1 ;
  wire \n_5_wait_bypass_count_reg[4]_i_1 ;
  wire \n_5_wait_bypass_count_reg[8]_i_1 ;
  wire \n_6_time_out_counter_reg[0]_i_2 ;
  wire \n_6_time_out_counter_reg[12]_i_1 ;
  wire \n_6_time_out_counter_reg[4]_i_1 ;
  wire \n_6_time_out_counter_reg[8]_i_1 ;
  wire \n_6_wait_bypass_count_reg[0]_i_3 ;
  wire \n_6_wait_bypass_count_reg[12]_i_1 ;
  wire \n_6_wait_bypass_count_reg[4]_i_1 ;
  wire \n_6_wait_bypass_count_reg[8]_i_1 ;
  wire \n_7_time_out_counter_reg[0]_i_2 ;
  wire \n_7_time_out_counter_reg[12]_i_1 ;
  wire \n_7_time_out_counter_reg[16]_i_1 ;
  wire \n_7_time_out_counter_reg[4]_i_1 ;
  wire \n_7_time_out_counter_reg[8]_i_1 ;
  wire \n_7_wait_bypass_count_reg[0]_i_3 ;
  wire \n_7_wait_bypass_count_reg[12]_i_1 ;
  wire \n_7_wait_bypass_count_reg[4]_i_1 ;
  wire \n_7_wait_bypass_count_reg[8]_i_1 ;
  wire [5:1]p_0_in;
  wire [9:0]p_0_in__0;
  wire reset_time_out;
  wire run_phase_alignment_int_s3;
  wire time_out_2ms;
  wire [16:0]time_out_counter_reg;
  wire time_out_wait_bypass_s2;
  wire time_out_wait_bypass_s3;
  wire tx_fsm_reset_done_int_s2;
  wire tx_fsm_reset_done_int_s3;
(* RTL_KEEP = "yes" *)   wire [3:0]tx_state;
  wire tx_state13_out;
  wire txresetdone_s2;
  wire txresetdone_s3;
  wire [15:0]wait_bypass_count_reg;
  wire [6:0]wait_time_cnt0;
  wire [6:0]wait_time_cnt_reg__0;
  wire wait_time_done;
  wire [3:0]\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED ;
  wire [3:3]\NLW_wait_bypass_count_reg[12]_i_1_CO_UNCONNECTED ;

LUT6 #(
    .INIT(64'h2222220222220A0A)) 
     \FSM_sequential_tx_state[0]_i_1 
       (.I0(\n_0_FSM_sequential_tx_state[0]_i_2 ),
        .I1(tx_state[3]),
        .I2(tx_state[0]),
        .I3(n_0_time_out_2ms_reg),
        .I4(tx_state[2]),
        .I5(tx_state[1]),
        .O(\n_0_FSM_sequential_tx_state[0]_i_1 ));
LUT6 #(
    .INIT(64'h3B33BBBBBBBBBBBB)) 
     \FSM_sequential_tx_state[0]_i_2 
       (.I0(\n_0_FSM_sequential_tx_state[2]_i_2 ),
        .I1(tx_state[0]),
        .I2(reset_time_out),
        .I3(n_0_time_out_500us_reg),
        .I4(tx_state[1]),
        .I5(tx_state[2]),
        .O(\n_0_FSM_sequential_tx_state[0]_i_2 ));
LUT5 #(
    .INIT(32'h11110444)) 
     \FSM_sequential_tx_state[1]_i_1 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state13_out),
        .I3(tx_state[2]),
        .I4(tx_state[1]),
        .O(\n_0_FSM_sequential_tx_state[1]_i_1 ));
(* SOFT_HLUTNM = "soft_lutpair26" *) 
   LUT3 #(
    .INIT(8'h04)) 
     \FSM_sequential_tx_state[1]_i_2 
       (.I0(reset_time_out),
        .I1(n_0_time_tlock_max_reg),
        .I2(mmcm_lock_reclocked),
        .O(tx_state13_out));
LUT6 #(
    .INIT(64'h1111004055550040)) 
     \FSM_sequential_tx_state[2]_i_1 
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[1]),
        .I3(n_0_time_out_2ms_reg),
        .I4(tx_state[2]),
        .I5(\n_0_FSM_sequential_tx_state[2]_i_2 ),
        .O(\n_0_FSM_sequential_tx_state[2]_i_1 ));
LUT4 #(
    .INIT(16'hFF04)) 
     \FSM_sequential_tx_state[2]_i_2 
       (.I0(mmcm_lock_reclocked),
        .I1(n_0_time_tlock_max_reg),
        .I2(reset_time_out),
        .I3(tx_state[1]),
        .O(\n_0_FSM_sequential_tx_state[2]_i_2 ));
LUT5 #(
    .INIT(32'h00A00B00)) 
     \FSM_sequential_tx_state[3]_i_2 
       (.I0(\n_0_FSM_sequential_tx_state[3]_i_6 ),
        .I1(time_out_wait_bypass_s3),
        .I2(tx_state[2]),
        .I3(tx_state[3]),
        .I4(tx_state[1]),
        .O(\n_0_FSM_sequential_tx_state[3]_i_2 ));
(* SOFT_HLUTNM = "soft_lutpair19" *) 
   LUT5 #(
    .INIT(32'h00000010)) 
     \FSM_sequential_tx_state[3]_i_4 
       (.I0(wait_time_cnt_reg__0[5]),
        .I1(wait_time_cnt_reg__0[4]),
        .I2(\n_0_wait_time_cnt[6]_i_4 ),
        .I3(wait_time_cnt_reg__0[3]),
        .I4(wait_time_cnt_reg__0[6]),
        .O(wait_time_done));
LUT2 #(
    .INIT(4'hE)) 
     \FSM_sequential_tx_state[3]_i_5 
       (.I0(tx_state[1]),
        .I1(tx_state[2]),
        .O(\n_0_FSM_sequential_tx_state[3]_i_5 ));
LUT3 #(
    .INIT(8'h8A)) 
     \FSM_sequential_tx_state[3]_i_6 
       (.I0(tx_state[0]),
        .I1(reset_time_out),
        .I2(n_0_time_out_500us_reg),
        .O(\n_0_FSM_sequential_tx_state[3]_i_6 ));
(* KEEP = "yes" *) 
   FDRE \FSM_sequential_tx_state_reg[0] 
       (.C(CLK),
        .CE(n_1_sync_PLL0LOCK),
        .D(\n_0_FSM_sequential_tx_state[0]_i_1 ),
        .Q(tx_state[0]),
        .R(SOFT_RESET_IN));
(* KEEP = "yes" *) 
   FDRE \FSM_sequential_tx_state_reg[1] 
       (.C(CLK),
        .CE(n_1_sync_PLL0LOCK),
        .D(\n_0_FSM_sequential_tx_state[1]_i_1 ),
        .Q(tx_state[1]),
        .R(SOFT_RESET_IN));
(* KEEP = "yes" *) 
   FDRE \FSM_sequential_tx_state_reg[2] 
       (.C(CLK),
        .CE(n_1_sync_PLL0LOCK),
        .D(\n_0_FSM_sequential_tx_state[2]_i_1 ),
        .Q(tx_state[2]),
        .R(SOFT_RESET_IN));
(* KEEP = "yes" *) 
   FDRE \FSM_sequential_tx_state_reg[3] 
       (.C(CLK),
        .CE(n_1_sync_PLL0LOCK),
        .D(\n_0_FSM_sequential_tx_state[3]_i_2 ),
        .Q(tx_state[3]),
        .R(SOFT_RESET_IN));
LUT6 #(
    .INIT(64'hFFFFFDFF00000100)) 
     PLL0_RESET_i_1
       (.I0(n_0_pll_reset_asserted_reg),
        .I1(tx_state[3]),
        .I2(tx_state[2]),
        .I3(tx_state[0]),
        .I4(tx_state[1]),
        .I5(O1),
        .O(n_0_PLL0_RESET_i_1));
FDRE #(
    .INIT(1'b0)) 
     PLL0_RESET_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_PLL0_RESET_i_1),
        .Q(O1),
        .R(SOFT_RESET_IN));
LUT5 #(
    .INIT(32'hFFFB4000)) 
     TXUSERRDY_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[1]),
        .I3(tx_state[2]),
        .I4(TXUSERRDY),
        .O(n_0_TXUSERRDY_i_1));
FDRE #(
    .INIT(1'b0)) 
     TXUSERRDY_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_TXUSERRDY_i_1),
        .Q(TXUSERRDY),
        .R(SOFT_RESET_IN));
LUT5 #(
    .INIT(32'hFFFD0004)) 
     gttxreset_i_i_1
       (.I0(tx_state[2]),
        .I1(tx_state[0]),
        .I2(tx_state[3]),
        .I3(tx_state[1]),
        .I4(GTTXRESET),
        .O(n_0_gttxreset_i_i_1));
FDRE #(
    .INIT(1'b0)) 
     gttxreset_i_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_gttxreset_i_i_1),
        .Q(GTTXRESET),
        .R(SOFT_RESET_IN));
LUT1 #(
    .INIT(2'h1)) 
     \init_wait_count[0]_i_1__0 
       (.I0(init_wait_count_reg__0[0]),
        .O(\n_0_init_wait_count[0]_i_1__0 ));
(* SOFT_HLUTNM = "soft_lutpair24" *) 
   LUT2 #(
    .INIT(4'h6)) 
     \init_wait_count[1]_i_1__0 
       (.I0(init_wait_count_reg__0[1]),
        .I1(init_wait_count_reg__0[0]),
        .O(p_0_in[1]));
(* SOFT_HLUTNM = "soft_lutpair24" *) 
   LUT3 #(
    .INIT(8'h78)) 
     \init_wait_count[2]_i_1__0 
       (.I0(init_wait_count_reg__0[1]),
        .I1(init_wait_count_reg__0[0]),
        .I2(init_wait_count_reg__0[2]),
        .O(p_0_in[2]));
(* SOFT_HLUTNM = "soft_lutpair18" *) 
   LUT4 #(
    .INIT(16'h6AAA)) 
     \init_wait_count[3]_i_1__0 
       (.I0(init_wait_count_reg__0[3]),
        .I1(init_wait_count_reg__0[1]),
        .I2(init_wait_count_reg__0[0]),
        .I3(init_wait_count_reg__0[2]),
        .O(p_0_in[3]));
(* SOFT_HLUTNM = "soft_lutpair18" *) 
   LUT5 #(
    .INIT(32'h6AAAAAAA)) 
     \init_wait_count[4]_i_1__0 
       (.I0(init_wait_count_reg__0[4]),
        .I1(init_wait_count_reg__0[2]),
        .I2(init_wait_count_reg__0[0]),
        .I3(init_wait_count_reg__0[1]),
        .I4(init_wait_count_reg__0[3]),
        .O(p_0_in[4]));
LUT6 #(
    .INIT(64'hFFFFFFFFFFDFFFFF)) 
     \init_wait_count[5]_i_1__0 
       (.I0(init_wait_count_reg__0[0]),
        .I1(init_wait_count_reg__0[1]),
        .I2(init_wait_count_reg__0[3]),
        .I3(init_wait_count_reg__0[2]),
        .I4(init_wait_count_reg__0[5]),
        .I5(init_wait_count_reg__0[4]),
        .O(\n_0_init_wait_count[5]_i_1__0 ));
LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
     \init_wait_count[5]_i_2 
       (.I0(init_wait_count_reg__0[5]),
        .I1(init_wait_count_reg__0[3]),
        .I2(init_wait_count_reg__0[1]),
        .I3(init_wait_count_reg__0[0]),
        .I4(init_wait_count_reg__0[2]),
        .I5(init_wait_count_reg__0[4]),
        .O(p_0_in[5]));
FDCE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[0] 
       (.C(CLK),
        .CE(\n_0_init_wait_count[5]_i_1__0 ),
        .CLR(SOFT_RESET_IN),
        .D(\n_0_init_wait_count[0]_i_1__0 ),
        .Q(init_wait_count_reg__0[0]));
FDCE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[1] 
       (.C(CLK),
        .CE(\n_0_init_wait_count[5]_i_1__0 ),
        .CLR(SOFT_RESET_IN),
        .D(p_0_in[1]),
        .Q(init_wait_count_reg__0[1]));
FDCE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[2] 
       (.C(CLK),
        .CE(\n_0_init_wait_count[5]_i_1__0 ),
        .CLR(SOFT_RESET_IN),
        .D(p_0_in[2]),
        .Q(init_wait_count_reg__0[2]));
FDCE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[3] 
       (.C(CLK),
        .CE(\n_0_init_wait_count[5]_i_1__0 ),
        .CLR(SOFT_RESET_IN),
        .D(p_0_in[3]),
        .Q(init_wait_count_reg__0[3]));
FDCE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[4] 
       (.C(CLK),
        .CE(\n_0_init_wait_count[5]_i_1__0 ),
        .CLR(SOFT_RESET_IN),
        .D(p_0_in[4]),
        .Q(init_wait_count_reg__0[4]));
FDCE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[5] 
       (.C(CLK),
        .CE(\n_0_init_wait_count[5]_i_1__0 ),
        .CLR(SOFT_RESET_IN),
        .D(p_0_in[5]),
        .Q(init_wait_count_reg__0[5]));
LUT2 #(
    .INIT(4'hE)) 
     init_wait_done_i_1__0
       (.I0(init_wait_done),
        .I1(n_0_init_wait_done_reg),
        .O(n_0_init_wait_done_i_1__0));
LUT6 #(
    .INIT(64'h0000040000000000)) 
     init_wait_done_i_2
       (.I0(init_wait_count_reg__0[4]),
        .I1(init_wait_count_reg__0[5]),
        .I2(init_wait_count_reg__0[2]),
        .I3(init_wait_count_reg__0[3]),
        .I4(init_wait_count_reg__0[1]),
        .I5(init_wait_count_reg__0[0]),
        .O(init_wait_done));
FDCE #(
    .INIT(1'b0)) 
     init_wait_done_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(SOFT_RESET_IN),
        .D(n_0_init_wait_done_i_1__0),
        .Q(n_0_init_wait_done_reg));
(* SOFT_HLUTNM = "soft_lutpair28" *) 
   LUT1 #(
    .INIT(2'h1)) 
     \mmcm_lock_count[0]_i_1 
       (.I0(mmcm_lock_count_reg__0[0]),
        .O(p_0_in__0[0]));
(* SOFT_HLUTNM = "soft_lutpair28" *) 
   LUT2 #(
    .INIT(4'h6)) 
     \mmcm_lock_count[1]_i_1 
       (.I0(mmcm_lock_count_reg__0[0]),
        .I1(mmcm_lock_count_reg__0[1]),
        .O(p_0_in__0[1]));
(* SOFT_HLUTNM = "soft_lutpair22" *) 
   LUT3 #(
    .INIT(8'h78)) 
     \mmcm_lock_count[2]_i_1 
       (.I0(mmcm_lock_count_reg__0[0]),
        .I1(mmcm_lock_count_reg__0[1]),
        .I2(mmcm_lock_count_reg__0[2]),
        .O(p_0_in__0[2]));
(* SOFT_HLUTNM = "soft_lutpair22" *) 
   LUT4 #(
    .INIT(16'h6AAA)) 
     \mmcm_lock_count[3]_i_1 
       (.I0(mmcm_lock_count_reg__0[3]),
        .I1(mmcm_lock_count_reg__0[0]),
        .I2(mmcm_lock_count_reg__0[1]),
        .I3(mmcm_lock_count_reg__0[2]),
        .O(p_0_in__0[3]));
(* SOFT_HLUTNM = "soft_lutpair20" *) 
   LUT5 #(
    .INIT(32'h6AAAAAAA)) 
     \mmcm_lock_count[4]_i_1 
       (.I0(mmcm_lock_count_reg__0[4]),
        .I1(mmcm_lock_count_reg__0[2]),
        .I2(mmcm_lock_count_reg__0[1]),
        .I3(mmcm_lock_count_reg__0[0]),
        .I4(mmcm_lock_count_reg__0[3]),
        .O(p_0_in__0[4]));
LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
     \mmcm_lock_count[5]_i_1 
       (.I0(mmcm_lock_count_reg__0[5]),
        .I1(mmcm_lock_count_reg__0[3]),
        .I2(mmcm_lock_count_reg__0[0]),
        .I3(mmcm_lock_count_reg__0[1]),
        .I4(mmcm_lock_count_reg__0[2]),
        .I5(mmcm_lock_count_reg__0[4]),
        .O(p_0_in__0[5]));
(* SOFT_HLUTNM = "soft_lutpair23" *) 
   LUT3 #(
    .INIT(8'h6A)) 
     \mmcm_lock_count[6]_i_1 
       (.I0(mmcm_lock_count_reg__0[6]),
        .I1(\n_0_mmcm_lock_count[9]_i_4 ),
        .I2(mmcm_lock_count_reg__0[5]),
        .O(p_0_in__0[6]));
(* SOFT_HLUTNM = "soft_lutpair21" *) 
   LUT4 #(
    .INIT(16'h6AAA)) 
     \mmcm_lock_count[7]_i_1 
       (.I0(mmcm_lock_count_reg__0[7]),
        .I1(mmcm_lock_count_reg__0[5]),
        .I2(mmcm_lock_count_reg__0[6]),
        .I3(\n_0_mmcm_lock_count[9]_i_4 ),
        .O(p_0_in__0[7]));
(* SOFT_HLUTNM = "soft_lutpair21" *) 
   LUT5 #(
    .INIT(32'h6AAAAAAA)) 
     \mmcm_lock_count[8]_i_1 
       (.I0(mmcm_lock_count_reg__0[8]),
        .I1(\n_0_mmcm_lock_count[9]_i_4 ),
        .I2(mmcm_lock_count_reg__0[6]),
        .I3(mmcm_lock_count_reg__0[5]),
        .I4(mmcm_lock_count_reg__0[7]),
        .O(p_0_in__0[8]));
LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
     \mmcm_lock_count[9]_i_2 
       (.I0(mmcm_lock_count_reg__0[5]),
        .I1(mmcm_lock_count_reg__0[6]),
        .I2(\n_0_mmcm_lock_count[9]_i_4 ),
        .I3(mmcm_lock_count_reg__0[8]),
        .I4(mmcm_lock_count_reg__0[7]),
        .I5(mmcm_lock_count_reg__0[9]),
        .O(\n_0_mmcm_lock_count[9]_i_2 ));
LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
     \mmcm_lock_count[9]_i_3 
       (.I0(mmcm_lock_count_reg__0[9]),
        .I1(mmcm_lock_count_reg__0[7]),
        .I2(mmcm_lock_count_reg__0[8]),
        .I3(\n_0_mmcm_lock_count[9]_i_4 ),
        .I4(mmcm_lock_count_reg__0[6]),
        .I5(mmcm_lock_count_reg__0[5]),
        .O(p_0_in__0[9]));
(* SOFT_HLUTNM = "soft_lutpair20" *) 
   LUT5 #(
    .INIT(32'h80000000)) 
     \mmcm_lock_count[9]_i_4 
       (.I0(mmcm_lock_count_reg__0[4]),
        .I1(mmcm_lock_count_reg__0[2]),
        .I2(mmcm_lock_count_reg__0[1]),
        .I3(mmcm_lock_count_reg__0[0]),
        .I4(mmcm_lock_count_reg__0[3]),
        .O(\n_0_mmcm_lock_count[9]_i_4 ));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[0] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2 ),
        .D(p_0_in__0[0]),
        .Q(mmcm_lock_count_reg__0[0]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[1] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2 ),
        .D(p_0_in__0[1]),
        .Q(mmcm_lock_count_reg__0[1]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[2] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2 ),
        .D(p_0_in__0[2]),
        .Q(mmcm_lock_count_reg__0[2]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[3] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2 ),
        .D(p_0_in__0[3]),
        .Q(mmcm_lock_count_reg__0[3]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[4] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2 ),
        .D(p_0_in__0[4]),
        .Q(mmcm_lock_count_reg__0[4]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[5] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2 ),
        .D(p_0_in__0[5]),
        .Q(mmcm_lock_count_reg__0[5]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[6] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2 ),
        .D(p_0_in__0[6]),
        .Q(mmcm_lock_count_reg__0[6]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[7] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2 ),
        .D(p_0_in__0[7]),
        .Q(mmcm_lock_count_reg__0[7]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[8] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2 ),
        .D(p_0_in__0[8]),
        .Q(mmcm_lock_count_reg__0[8]),
        .R(n_0_sync_mmcm_lock_reclocked));
FDRE #(
    .INIT(1'b0)) 
     \mmcm_lock_count_reg[9] 
       (.C(CLK),
        .CE(\n_0_mmcm_lock_count[9]_i_2 ),
        .D(p_0_in__0[9]),
        .Q(mmcm_lock_count_reg__0[9]),
        .R(n_0_sync_mmcm_lock_reclocked));
(* SOFT_HLUTNM = "soft_lutpair23" *) 
   LUT3 #(
    .INIT(8'h80)) 
     mmcm_lock_reclocked_i_2
       (.I0(\n_0_mmcm_lock_count[9]_i_4 ),
        .I1(mmcm_lock_count_reg__0[6]),
        .I2(mmcm_lock_count_reg__0[5]),
        .O(n_0_mmcm_lock_reclocked_i_2));
FDRE #(
    .INIT(1'b0)) 
     mmcm_lock_reclocked_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_1_sync_mmcm_lock_reclocked),
        .Q(mmcm_lock_reclocked),
        .R(1'b0));
LUT5 #(
    .INIT(32'hEF00FF10)) 
     pll_reset_asserted_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[2]),
        .I2(tx_state[0]),
        .I3(n_0_pll_reset_asserted_reg),
        .I4(tx_state[1]),
        .O(n_0_pll_reset_asserted_i_1));
FDRE #(
    .INIT(1'b0)) 
     pll_reset_asserted_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_pll_reset_asserted_i_1),
        .Q(n_0_pll_reset_asserted_reg),
        .R(SOFT_RESET_IN));
FDRE #(
    .INIT(1'b0)) 
     reset_time_out_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_sync_PLL0LOCK),
        .Q(reset_time_out),
        .R(SOFT_RESET_IN));
LUT5 #(
    .INIT(32'hFFFB0002)) 
     run_phase_alignment_int_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(n_0_run_phase_alignment_int_reg),
        .O(n_0_run_phase_alignment_int_i_1));
FDRE #(
    .INIT(1'b0)) 
     run_phase_alignment_int_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_run_phase_alignment_int_i_1),
        .Q(n_0_run_phase_alignment_int_reg),
        .R(SOFT_RESET_IN));
FDRE #(
    .INIT(1'b0)) 
     run_phase_alignment_int_s3_reg
       (.C(I1),
        .CE(1'b1),
        .D(data_out),
        .Q(run_phase_alignment_int_s3),
        .R(1'b0));
gtp_sata_gtp_sata_sync_block__parameterized0_0 sync_PLL0LOCK
       (.CLK(CLK),
        .E(n_1_sync_PLL0LOCK),
        .GT0_PLL0LOCK_OUT(GT0_PLL0LOCK_OUT),
        .I1(n_0_init_wait_done_reg),
        .I2(\n_0_FSM_sequential_tx_state[3]_i_5 ),
        .I3(n_0_time_tlock_max_reg),
        .I4(n_0_pll_reset_asserted_reg),
        .I5(n_0_time_out_500us_reg),
        .I6(n_0_time_out_2ms_reg),
        .O1(n_0_sync_PLL0LOCK),
        .mmcm_lock_reclocked(mmcm_lock_reclocked),
        .out(tx_state),
        .reset_time_out(reset_time_out),
        .txresetdone_s3(txresetdone_s3),
        .wait_time_done(wait_time_done));
gtp_sata_gtp_sata_sync_block__parameterized0_1 sync_TXRESETDONE
       (.CLK(CLK),
        .data_out(txresetdone_s2),
        .gt0_txresetdone_out(gt0_txresetdone_out));
gtp_sata_gtp_sata_sync_block__parameterized0_2 sync_mmcm_lock_reclocked
       (.CLK(CLK),
        .I1(n_0_mmcm_lock_reclocked_i_2),
        .O1(n_1_sync_mmcm_lock_reclocked),
        .Q(mmcm_lock_count_reg__0[9:7]),
        .SR(n_0_sync_mmcm_lock_reclocked),
        .mmcm_lock_reclocked(mmcm_lock_reclocked));
gtp_sata_gtp_sata_sync_block__parameterized0_3 sync_run_phase_alignment_int
       (.I1(I1),
        .data_in(n_0_run_phase_alignment_int_reg),
        .data_out(data_out));
gtp_sata_gtp_sata_sync_block__parameterized0_4 sync_time_out_wait_bypass
       (.CLK(CLK),
        .data_in(n_0_time_out_wait_bypass_reg),
        .data_out(time_out_wait_bypass_s2));
gtp_sata_gtp_sata_sync_block__parameterized0_5 sync_tx_fsm_reset_done_int
       (.GT0_TX_FSM_RESET_DONE_OUT(GT0_TX_FSM_RESET_DONE_OUT),
        .I1(I1),
        .data_out(tx_fsm_reset_done_int_s2));
(* SOFT_HLUTNM = "soft_lutpair26" *) 
   LUT3 #(
    .INIT(8'h0E)) 
     time_out_2ms_i_1__0
       (.I0(n_0_time_out_2ms_reg),
        .I1(time_out_2ms),
        .I2(reset_time_out),
        .O(n_0_time_out_2ms_i_1__0));
FDRE #(
    .INIT(1'b0)) 
     time_out_2ms_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_time_out_2ms_i_1__0),
        .Q(n_0_time_out_2ms_reg),
        .R(1'b0));
LUT5 #(
    .INIT(32'h0000ABAA)) 
     time_out_500us_i_1
       (.I0(n_0_time_out_500us_reg),
        .I1(time_out_counter_reg[10]),
        .I2(time_out_counter_reg[15]),
        .I3(n_0_time_out_500us_i_2),
        .I4(reset_time_out),
        .O(n_0_time_out_500us_i_1));
LUT5 #(
    .INIT(32'h00800000)) 
     time_out_500us_i_2
       (.I0(n_0_time_tlock_max_i_3),
        .I1(n_0_time_out_500us_i_3),
        .I2(time_out_counter_reg[13]),
        .I3(time_out_counter_reg[5]),
        .I4(time_out_counter_reg[4]),
        .O(n_0_time_out_500us_i_2));
LUT6 #(
    .INIT(64'h0000000000004000)) 
     time_out_500us_i_3
       (.I0(time_out_counter_reg[6]),
        .I1(time_out_counter_reg[11]),
        .I2(time_out_counter_reg[14]),
        .I3(time_out_counter_reg[9]),
        .I4(time_out_counter_reg[16]),
        .I5(time_out_counter_reg[3]),
        .O(n_0_time_out_500us_i_3));
FDRE #(
    .INIT(1'b0)) 
     time_out_500us_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_time_out_500us_i_1),
        .Q(n_0_time_out_500us_reg),
        .R(1'b0));
LUT6 #(
    .INIT(64'h0040000000000000)) 
     \time_out_counter[0]_i_10__0 
       (.I0(time_out_counter_reg[5]),
        .I1(time_out_counter_reg[6]),
        .I2(time_out_counter_reg[15]),
        .I3(time_out_counter_reg[0]),
        .I4(time_out_counter_reg[11]),
        .I5(time_out_counter_reg[16]),
        .O(\n_0_time_out_counter[0]_i_10__0 ));
LUT1 #(
    .INIT(2'h1)) 
     \time_out_counter[0]_i_1__0 
       (.I0(time_out_2ms),
        .O(\n_0_time_out_counter[0]_i_1__0 ));
LUT6 #(
    .INIT(64'h0010000000000000)) 
     \time_out_counter[0]_i_3__0 
       (.I0(\n_0_time_out_counter[0]_i_8 ),
        .I1(time_out_counter_reg[4]),
        .I2(time_out_counter_reg[13]),
        .I3(time_out_counter_reg[1]),
        .I4(\n_0_time_out_counter[0]_i_9 ),
        .I5(\n_0_time_out_counter[0]_i_10__0 ),
        .O(time_out_2ms));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[0]_i_4 
       (.I0(time_out_counter_reg[3]),
        .O(\n_0_time_out_counter[0]_i_4 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[0]_i_5 
       (.I0(time_out_counter_reg[2]),
        .O(\n_0_time_out_counter[0]_i_5 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[0]_i_6 
       (.I0(time_out_counter_reg[1]),
        .O(\n_0_time_out_counter[0]_i_6 ));
LUT1 #(
    .INIT(2'h1)) 
     \time_out_counter[0]_i_7 
       (.I0(time_out_counter_reg[0]),
        .O(\n_0_time_out_counter[0]_i_7 ));
LUT2 #(
    .INIT(4'hE)) 
     \time_out_counter[0]_i_8 
       (.I0(time_out_counter_reg[7]),
        .I1(time_out_counter_reg[8]),
        .O(\n_0_time_out_counter[0]_i_8 ));
LUT6 #(
    .INIT(64'h0000000000000008)) 
     \time_out_counter[0]_i_9 
       (.I0(time_out_counter_reg[14]),
        .I1(time_out_counter_reg[3]),
        .I2(time_out_counter_reg[9]),
        .I3(time_out_counter_reg[10]),
        .I4(time_out_counter_reg[2]),
        .I5(time_out_counter_reg[12]),
        .O(\n_0_time_out_counter[0]_i_9 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[12]_i_2 
       (.I0(time_out_counter_reg[15]),
        .O(\n_0_time_out_counter[12]_i_2 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[12]_i_3 
       (.I0(time_out_counter_reg[14]),
        .O(\n_0_time_out_counter[12]_i_3 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[12]_i_4 
       (.I0(time_out_counter_reg[13]),
        .O(\n_0_time_out_counter[12]_i_4 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[12]_i_5 
       (.I0(time_out_counter_reg[12]),
        .O(\n_0_time_out_counter[12]_i_5 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[16]_i_2 
       (.I0(time_out_counter_reg[16]),
        .O(\n_0_time_out_counter[16]_i_2 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[4]_i_2 
       (.I0(time_out_counter_reg[7]),
        .O(\n_0_time_out_counter[4]_i_2 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[4]_i_3 
       (.I0(time_out_counter_reg[6]),
        .O(\n_0_time_out_counter[4]_i_3 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[4]_i_4 
       (.I0(time_out_counter_reg[5]),
        .O(\n_0_time_out_counter[4]_i_4 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[4]_i_5 
       (.I0(time_out_counter_reg[4]),
        .O(\n_0_time_out_counter[4]_i_5 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[8]_i_2 
       (.I0(time_out_counter_reg[11]),
        .O(\n_0_time_out_counter[8]_i_2 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[8]_i_3 
       (.I0(time_out_counter_reg[10]),
        .O(\n_0_time_out_counter[8]_i_3 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[8]_i_4 
       (.I0(time_out_counter_reg[9]),
        .O(\n_0_time_out_counter[8]_i_4 ));
LUT1 #(
    .INIT(2'h2)) 
     \time_out_counter[8]_i_5 
       (.I0(time_out_counter_reg[8]),
        .O(\n_0_time_out_counter[8]_i_5 ));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[0] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_7_time_out_counter_reg[0]_i_2 ),
        .Q(time_out_counter_reg[0]),
        .R(reset_time_out));
CARRY4 \time_out_counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\n_0_time_out_counter_reg[0]_i_2 ,\n_1_time_out_counter_reg[0]_i_2 ,\n_2_time_out_counter_reg[0]_i_2 ,\n_3_time_out_counter_reg[0]_i_2 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\n_4_time_out_counter_reg[0]_i_2 ,\n_5_time_out_counter_reg[0]_i_2 ,\n_6_time_out_counter_reg[0]_i_2 ,\n_7_time_out_counter_reg[0]_i_2 }),
        .S({\n_0_time_out_counter[0]_i_4 ,\n_0_time_out_counter[0]_i_5 ,\n_0_time_out_counter[0]_i_6 ,\n_0_time_out_counter[0]_i_7 }));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[10] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_5_time_out_counter_reg[8]_i_1 ),
        .Q(time_out_counter_reg[10]),
        .R(reset_time_out));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[11] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_4_time_out_counter_reg[8]_i_1 ),
        .Q(time_out_counter_reg[11]),
        .R(reset_time_out));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[12] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_7_time_out_counter_reg[12]_i_1 ),
        .Q(time_out_counter_reg[12]),
        .R(reset_time_out));
CARRY4 \time_out_counter_reg[12]_i_1 
       (.CI(\n_0_time_out_counter_reg[8]_i_1 ),
        .CO({\n_0_time_out_counter_reg[12]_i_1 ,\n_1_time_out_counter_reg[12]_i_1 ,\n_2_time_out_counter_reg[12]_i_1 ,\n_3_time_out_counter_reg[12]_i_1 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\n_4_time_out_counter_reg[12]_i_1 ,\n_5_time_out_counter_reg[12]_i_1 ,\n_6_time_out_counter_reg[12]_i_1 ,\n_7_time_out_counter_reg[12]_i_1 }),
        .S({\n_0_time_out_counter[12]_i_2 ,\n_0_time_out_counter[12]_i_3 ,\n_0_time_out_counter[12]_i_4 ,\n_0_time_out_counter[12]_i_5 }));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[13] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_6_time_out_counter_reg[12]_i_1 ),
        .Q(time_out_counter_reg[13]),
        .R(reset_time_out));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[14] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_5_time_out_counter_reg[12]_i_1 ),
        .Q(time_out_counter_reg[14]),
        .R(reset_time_out));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[15] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_4_time_out_counter_reg[12]_i_1 ),
        .Q(time_out_counter_reg[15]),
        .R(reset_time_out));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[16] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_7_time_out_counter_reg[16]_i_1 ),
        .Q(time_out_counter_reg[16]),
        .R(reset_time_out));
CARRY4 \time_out_counter_reg[16]_i_1 
       (.CI(\n_0_time_out_counter_reg[12]_i_1 ),
        .CO(\NLW_time_out_counter_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_time_out_counter_reg[16]_i_1_O_UNCONNECTED [3:1],\n_7_time_out_counter_reg[16]_i_1 }),
        .S({1'b0,1'b0,1'b0,\n_0_time_out_counter[16]_i_2 }));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[1] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_6_time_out_counter_reg[0]_i_2 ),
        .Q(time_out_counter_reg[1]),
        .R(reset_time_out));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[2] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_5_time_out_counter_reg[0]_i_2 ),
        .Q(time_out_counter_reg[2]),
        .R(reset_time_out));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[3] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_4_time_out_counter_reg[0]_i_2 ),
        .Q(time_out_counter_reg[3]),
        .R(reset_time_out));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[4] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_7_time_out_counter_reg[4]_i_1 ),
        .Q(time_out_counter_reg[4]),
        .R(reset_time_out));
CARRY4 \time_out_counter_reg[4]_i_1 
       (.CI(\n_0_time_out_counter_reg[0]_i_2 ),
        .CO({\n_0_time_out_counter_reg[4]_i_1 ,\n_1_time_out_counter_reg[4]_i_1 ,\n_2_time_out_counter_reg[4]_i_1 ,\n_3_time_out_counter_reg[4]_i_1 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\n_4_time_out_counter_reg[4]_i_1 ,\n_5_time_out_counter_reg[4]_i_1 ,\n_6_time_out_counter_reg[4]_i_1 ,\n_7_time_out_counter_reg[4]_i_1 }),
        .S({\n_0_time_out_counter[4]_i_2 ,\n_0_time_out_counter[4]_i_3 ,\n_0_time_out_counter[4]_i_4 ,\n_0_time_out_counter[4]_i_5 }));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[5] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_6_time_out_counter_reg[4]_i_1 ),
        .Q(time_out_counter_reg[5]),
        .R(reset_time_out));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[6] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_5_time_out_counter_reg[4]_i_1 ),
        .Q(time_out_counter_reg[6]),
        .R(reset_time_out));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[7] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_4_time_out_counter_reg[4]_i_1 ),
        .Q(time_out_counter_reg[7]),
        .R(reset_time_out));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[8] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_7_time_out_counter_reg[8]_i_1 ),
        .Q(time_out_counter_reg[8]),
        .R(reset_time_out));
CARRY4 \time_out_counter_reg[8]_i_1 
       (.CI(\n_0_time_out_counter_reg[4]_i_1 ),
        .CO({\n_0_time_out_counter_reg[8]_i_1 ,\n_1_time_out_counter_reg[8]_i_1 ,\n_2_time_out_counter_reg[8]_i_1 ,\n_3_time_out_counter_reg[8]_i_1 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\n_4_time_out_counter_reg[8]_i_1 ,\n_5_time_out_counter_reg[8]_i_1 ,\n_6_time_out_counter_reg[8]_i_1 ,\n_7_time_out_counter_reg[8]_i_1 }),
        .S({\n_0_time_out_counter[8]_i_2 ,\n_0_time_out_counter[8]_i_3 ,\n_0_time_out_counter[8]_i_4 ,\n_0_time_out_counter[8]_i_5 }));
FDRE #(
    .INIT(1'b0)) 
     \time_out_counter_reg[9] 
       (.C(CLK),
        .CE(\n_0_time_out_counter[0]_i_1__0 ),
        .D(\n_6_time_out_counter_reg[8]_i_1 ),
        .Q(time_out_counter_reg[9]),
        .R(reset_time_out));
LUT4 #(
    .INIT(16'hAB00)) 
     time_out_wait_bypass_i_1
       (.I0(n_0_time_out_wait_bypass_reg),
        .I1(tx_fsm_reset_done_int_s3),
        .I2(\n_0_wait_bypass_count[0]_i_4 ),
        .I3(run_phase_alignment_int_s3),
        .O(n_0_time_out_wait_bypass_i_1));
FDRE #(
    .INIT(1'b0)) 
     time_out_wait_bypass_reg
       (.C(I1),
        .CE(1'b1),
        .D(n_0_time_out_wait_bypass_i_1),
        .Q(n_0_time_out_wait_bypass_reg),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     time_out_wait_bypass_s3_reg
       (.C(CLK),
        .CE(1'b1),
        .D(time_out_wait_bypass_s2),
        .Q(time_out_wait_bypass_s3),
        .R(1'b0));
LUT5 #(
    .INIT(32'h0000ABAA)) 
     time_tlock_max_i_1
       (.I0(n_0_time_tlock_max_reg),
        .I1(time_out_counter_reg[10]),
        .I2(time_out_counter_reg[15]),
        .I3(n_0_time_tlock_max_i_2),
        .I4(reset_time_out),
        .O(n_0_time_tlock_max_i_1));
LUT5 #(
    .INIT(32'h00000800)) 
     time_tlock_max_i_2
       (.I0(n_0_time_tlock_max_i_3),
        .I1(n_0_time_tlock_max_i_4__0),
        .I2(time_out_counter_reg[4]),
        .I3(time_out_counter_reg[5]),
        .I4(time_out_counter_reg[13]),
        .O(n_0_time_tlock_max_i_2));
LUT6 #(
    .INIT(64'h0000000000000008)) 
     time_tlock_max_i_3
       (.I0(time_out_counter_reg[12]),
        .I1(time_out_counter_reg[1]),
        .I2(time_out_counter_reg[0]),
        .I3(time_out_counter_reg[2]),
        .I4(time_out_counter_reg[7]),
        .I5(time_out_counter_reg[8]),
        .O(n_0_time_tlock_max_i_3));
LUT6 #(
    .INIT(64'h0000000000080000)) 
     time_tlock_max_i_4__0
       (.I0(time_out_counter_reg[11]),
        .I1(time_out_counter_reg[6]),
        .I2(time_out_counter_reg[14]),
        .I3(time_out_counter_reg[9]),
        .I4(time_out_counter_reg[3]),
        .I5(time_out_counter_reg[16]),
        .O(n_0_time_tlock_max_i_4__0));
FDRE #(
    .INIT(1'b0)) 
     time_tlock_max_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_time_tlock_max_i_1),
        .Q(n_0_time_tlock_max_reg),
        .R(1'b0));
LUT5 #(
    .INIT(32'hFFFF0008)) 
     tx_fsm_reset_done_int_i_1
       (.I0(tx_state[3]),
        .I1(tx_state[0]),
        .I2(tx_state[2]),
        .I3(tx_state[1]),
        .I4(GT0_TX_FSM_RESET_DONE_OUT),
        .O(n_0_tx_fsm_reset_done_int_i_1));
FDRE #(
    .INIT(1'b0)) 
     tx_fsm_reset_done_int_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_tx_fsm_reset_done_int_i_1),
        .Q(GT0_TX_FSM_RESET_DONE_OUT),
        .R(SOFT_RESET_IN));
FDRE #(
    .INIT(1'b0)) 
     tx_fsm_reset_done_int_s3_reg
       (.C(I1),
        .CE(1'b1),
        .D(tx_fsm_reset_done_int_s2),
        .Q(tx_fsm_reset_done_int_s3),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     txresetdone_s3_reg
       (.C(CLK),
        .CE(1'b1),
        .D(txresetdone_s2),
        .Q(txresetdone_s3),
        .R(1'b0));
LUT1 #(
    .INIT(2'h1)) 
     \wait_bypass_count[0]_i_1 
       (.I0(run_phase_alignment_int_s3),
        .O(\n_0_wait_bypass_count[0]_i_1 ));
LUT6 #(
    .INIT(64'hFFFFDFFFFFFFFFFF)) 
     \wait_bypass_count[0]_i_10 
       (.I0(wait_bypass_count_reg[6]),
        .I1(wait_bypass_count_reg[11]),
        .I2(wait_bypass_count_reg[13]),
        .I3(wait_bypass_count_reg[12]),
        .I4(wait_bypass_count_reg[10]),
        .I5(wait_bypass_count_reg[4]),
        .O(\n_0_wait_bypass_count[0]_i_10 ));
LUT2 #(
    .INIT(4'h2)) 
     \wait_bypass_count[0]_i_2 
       (.I0(\n_0_wait_bypass_count[0]_i_4 ),
        .I1(tx_fsm_reset_done_int_s3),
        .O(\n_0_wait_bypass_count[0]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFFFFFF7FFF)) 
     \wait_bypass_count[0]_i_4 
       (.I0(wait_bypass_count_reg[9]),
        .I1(wait_bypass_count_reg[1]),
        .I2(wait_bypass_count_reg[0]),
        .I3(wait_bypass_count_reg[15]),
        .I4(\n_0_wait_bypass_count[0]_i_9__0 ),
        .I5(\n_0_wait_bypass_count[0]_i_10 ),
        .O(\n_0_wait_bypass_count[0]_i_4 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[0]_i_5__0 
       (.I0(wait_bypass_count_reg[3]),
        .O(\n_0_wait_bypass_count[0]_i_5__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[0]_i_6 
       (.I0(wait_bypass_count_reg[2]),
        .O(\n_0_wait_bypass_count[0]_i_6 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[0]_i_7 
       (.I0(wait_bypass_count_reg[1]),
        .O(\n_0_wait_bypass_count[0]_i_7 ));
LUT1 #(
    .INIT(2'h1)) 
     \wait_bypass_count[0]_i_8 
       (.I0(wait_bypass_count_reg[0]),
        .O(\n_0_wait_bypass_count[0]_i_8 ));
LUT6 #(
    .INIT(64'hFFFFF7FFFFFFFFFF)) 
     \wait_bypass_count[0]_i_9__0 
       (.I0(wait_bypass_count_reg[3]),
        .I1(wait_bypass_count_reg[2]),
        .I2(wait_bypass_count_reg[8]),
        .I3(wait_bypass_count_reg[7]),
        .I4(wait_bypass_count_reg[14]),
        .I5(wait_bypass_count_reg[5]),
        .O(\n_0_wait_bypass_count[0]_i_9__0 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[12]_i_2 
       (.I0(wait_bypass_count_reg[15]),
        .O(\n_0_wait_bypass_count[12]_i_2 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[12]_i_3 
       (.I0(wait_bypass_count_reg[14]),
        .O(\n_0_wait_bypass_count[12]_i_3 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[12]_i_4 
       (.I0(wait_bypass_count_reg[13]),
        .O(\n_0_wait_bypass_count[12]_i_4 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[12]_i_5 
       (.I0(wait_bypass_count_reg[12]),
        .O(\n_0_wait_bypass_count[12]_i_5 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[4]_i_2 
       (.I0(wait_bypass_count_reg[7]),
        .O(\n_0_wait_bypass_count[4]_i_2 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[4]_i_3 
       (.I0(wait_bypass_count_reg[6]),
        .O(\n_0_wait_bypass_count[4]_i_3 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[4]_i_4 
       (.I0(wait_bypass_count_reg[5]),
        .O(\n_0_wait_bypass_count[4]_i_4 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[4]_i_5 
       (.I0(wait_bypass_count_reg[4]),
        .O(\n_0_wait_bypass_count[4]_i_5 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[8]_i_2 
       (.I0(wait_bypass_count_reg[11]),
        .O(\n_0_wait_bypass_count[8]_i_2 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[8]_i_3 
       (.I0(wait_bypass_count_reg[10]),
        .O(\n_0_wait_bypass_count[8]_i_3 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[8]_i_4 
       (.I0(wait_bypass_count_reg[9]),
        .O(\n_0_wait_bypass_count[8]_i_4 ));
LUT1 #(
    .INIT(2'h2)) 
     \wait_bypass_count[8]_i_5 
       (.I0(wait_bypass_count_reg[8]),
        .O(\n_0_wait_bypass_count[8]_i_5 ));
FDRE \wait_bypass_count_reg[0] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_7_wait_bypass_count_reg[0]_i_3 ),
        .Q(wait_bypass_count_reg[0]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
CARRY4 \wait_bypass_count_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\n_0_wait_bypass_count_reg[0]_i_3 ,\n_1_wait_bypass_count_reg[0]_i_3 ,\n_2_wait_bypass_count_reg[0]_i_3 ,\n_3_wait_bypass_count_reg[0]_i_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\n_4_wait_bypass_count_reg[0]_i_3 ,\n_5_wait_bypass_count_reg[0]_i_3 ,\n_6_wait_bypass_count_reg[0]_i_3 ,\n_7_wait_bypass_count_reg[0]_i_3 }),
        .S({\n_0_wait_bypass_count[0]_i_5__0 ,\n_0_wait_bypass_count[0]_i_6 ,\n_0_wait_bypass_count[0]_i_7 ,\n_0_wait_bypass_count[0]_i_8 }));
FDRE \wait_bypass_count_reg[10] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_5_wait_bypass_count_reg[8]_i_1 ),
        .Q(wait_bypass_count_reg[10]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
FDRE \wait_bypass_count_reg[11] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_4_wait_bypass_count_reg[8]_i_1 ),
        .Q(wait_bypass_count_reg[11]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
FDRE \wait_bypass_count_reg[12] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_7_wait_bypass_count_reg[12]_i_1 ),
        .Q(wait_bypass_count_reg[12]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
CARRY4 \wait_bypass_count_reg[12]_i_1 
       (.CI(\n_0_wait_bypass_count_reg[8]_i_1 ),
        .CO({\NLW_wait_bypass_count_reg[12]_i_1_CO_UNCONNECTED [3],\n_1_wait_bypass_count_reg[12]_i_1 ,\n_2_wait_bypass_count_reg[12]_i_1 ,\n_3_wait_bypass_count_reg[12]_i_1 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\n_4_wait_bypass_count_reg[12]_i_1 ,\n_5_wait_bypass_count_reg[12]_i_1 ,\n_6_wait_bypass_count_reg[12]_i_1 ,\n_7_wait_bypass_count_reg[12]_i_1 }),
        .S({\n_0_wait_bypass_count[12]_i_2 ,\n_0_wait_bypass_count[12]_i_3 ,\n_0_wait_bypass_count[12]_i_4 ,\n_0_wait_bypass_count[12]_i_5 }));
FDRE \wait_bypass_count_reg[13] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_6_wait_bypass_count_reg[12]_i_1 ),
        .Q(wait_bypass_count_reg[13]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
FDRE \wait_bypass_count_reg[14] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_5_wait_bypass_count_reg[12]_i_1 ),
        .Q(wait_bypass_count_reg[14]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
FDRE \wait_bypass_count_reg[15] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_4_wait_bypass_count_reg[12]_i_1 ),
        .Q(wait_bypass_count_reg[15]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
FDRE \wait_bypass_count_reg[1] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_6_wait_bypass_count_reg[0]_i_3 ),
        .Q(wait_bypass_count_reg[1]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
FDRE \wait_bypass_count_reg[2] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_5_wait_bypass_count_reg[0]_i_3 ),
        .Q(wait_bypass_count_reg[2]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
FDRE \wait_bypass_count_reg[3] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_4_wait_bypass_count_reg[0]_i_3 ),
        .Q(wait_bypass_count_reg[3]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
FDRE \wait_bypass_count_reg[4] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_7_wait_bypass_count_reg[4]_i_1 ),
        .Q(wait_bypass_count_reg[4]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
CARRY4 \wait_bypass_count_reg[4]_i_1 
       (.CI(\n_0_wait_bypass_count_reg[0]_i_3 ),
        .CO({\n_0_wait_bypass_count_reg[4]_i_1 ,\n_1_wait_bypass_count_reg[4]_i_1 ,\n_2_wait_bypass_count_reg[4]_i_1 ,\n_3_wait_bypass_count_reg[4]_i_1 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\n_4_wait_bypass_count_reg[4]_i_1 ,\n_5_wait_bypass_count_reg[4]_i_1 ,\n_6_wait_bypass_count_reg[4]_i_1 ,\n_7_wait_bypass_count_reg[4]_i_1 }),
        .S({\n_0_wait_bypass_count[4]_i_2 ,\n_0_wait_bypass_count[4]_i_3 ,\n_0_wait_bypass_count[4]_i_4 ,\n_0_wait_bypass_count[4]_i_5 }));
FDRE \wait_bypass_count_reg[5] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_6_wait_bypass_count_reg[4]_i_1 ),
        .Q(wait_bypass_count_reg[5]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
FDRE \wait_bypass_count_reg[6] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_5_wait_bypass_count_reg[4]_i_1 ),
        .Q(wait_bypass_count_reg[6]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
FDRE \wait_bypass_count_reg[7] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_4_wait_bypass_count_reg[4]_i_1 ),
        .Q(wait_bypass_count_reg[7]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
FDRE \wait_bypass_count_reg[8] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_7_wait_bypass_count_reg[8]_i_1 ),
        .Q(wait_bypass_count_reg[8]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
CARRY4 \wait_bypass_count_reg[8]_i_1 
       (.CI(\n_0_wait_bypass_count_reg[4]_i_1 ),
        .CO({\n_0_wait_bypass_count_reg[8]_i_1 ,\n_1_wait_bypass_count_reg[8]_i_1 ,\n_2_wait_bypass_count_reg[8]_i_1 ,\n_3_wait_bypass_count_reg[8]_i_1 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\n_4_wait_bypass_count_reg[8]_i_1 ,\n_5_wait_bypass_count_reg[8]_i_1 ,\n_6_wait_bypass_count_reg[8]_i_1 ,\n_7_wait_bypass_count_reg[8]_i_1 }),
        .S({\n_0_wait_bypass_count[8]_i_2 ,\n_0_wait_bypass_count[8]_i_3 ,\n_0_wait_bypass_count[8]_i_4 ,\n_0_wait_bypass_count[8]_i_5 }));
FDRE \wait_bypass_count_reg[9] 
       (.C(I1),
        .CE(\n_0_wait_bypass_count[0]_i_2 ),
        .D(\n_6_wait_bypass_count_reg[8]_i_1 ),
        .Q(wait_bypass_count_reg[9]),
        .R(\n_0_wait_bypass_count[0]_i_1 ));
(* SOFT_HLUTNM = "soft_lutpair27" *) 
   LUT1 #(
    .INIT(2'h1)) 
     \wait_time_cnt[0]_i_1 
       (.I0(wait_time_cnt_reg__0[0]),
        .O(wait_time_cnt0[0]));
(* SOFT_HLUTNM = "soft_lutpair27" *) 
   LUT2 #(
    .INIT(4'h9)) 
     \wait_time_cnt[1]_i_1 
       (.I0(wait_time_cnt_reg__0[0]),
        .I1(wait_time_cnt_reg__0[1]),
        .O(\n_0_wait_time_cnt[1]_i_1 ));
(* SOFT_HLUTNM = "soft_lutpair25" *) 
   LUT3 #(
    .INIT(8'hE1)) 
     \wait_time_cnt[2]_i_1 
       (.I0(wait_time_cnt_reg__0[0]),
        .I1(wait_time_cnt_reg__0[1]),
        .I2(wait_time_cnt_reg__0[2]),
        .O(wait_time_cnt0[2]));
(* SOFT_HLUTNM = "soft_lutpair17" *) 
   LUT4 #(
    .INIT(16'hAAA9)) 
     \wait_time_cnt[3]_i_1 
       (.I0(wait_time_cnt_reg__0[3]),
        .I1(wait_time_cnt_reg__0[0]),
        .I2(wait_time_cnt_reg__0[1]),
        .I3(wait_time_cnt_reg__0[2]),
        .O(wait_time_cnt0[3]));
(* SOFT_HLUTNM = "soft_lutpair17" *) 
   LUT5 #(
    .INIT(32'hAAAAAAA9)) 
     \wait_time_cnt[4]_i_1 
       (.I0(wait_time_cnt_reg__0[4]),
        .I1(wait_time_cnt_reg__0[2]),
        .I2(wait_time_cnt_reg__0[1]),
        .I3(wait_time_cnt_reg__0[0]),
        .I4(wait_time_cnt_reg__0[3]),
        .O(\n_0_wait_time_cnt[4]_i_1 ));
LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
     \wait_time_cnt[5]_i_1 
       (.I0(wait_time_cnt_reg__0[5]),
        .I1(wait_time_cnt_reg__0[3]),
        .I2(wait_time_cnt_reg__0[0]),
        .I3(wait_time_cnt_reg__0[1]),
        .I4(wait_time_cnt_reg__0[2]),
        .I5(wait_time_cnt_reg__0[4]),
        .O(wait_time_cnt0[5]));
LUT4 #(
    .INIT(16'h002A)) 
     \wait_time_cnt[6]_i_1 
       (.I0(tx_state[0]),
        .I1(tx_state[1]),
        .I2(tx_state[2]),
        .I3(tx_state[3]),
        .O(clear));
LUT5 #(
    .INIT(32'hFFFFFFEF)) 
     \wait_time_cnt[6]_i_2 
       (.I0(wait_time_cnt_reg__0[6]),
        .I1(wait_time_cnt_reg__0[3]),
        .I2(\n_0_wait_time_cnt[6]_i_4 ),
        .I3(wait_time_cnt_reg__0[4]),
        .I4(wait_time_cnt_reg__0[5]),
        .O(\n_0_wait_time_cnt[6]_i_2 ));
(* SOFT_HLUTNM = "soft_lutpair19" *) 
   LUT5 #(
    .INIT(32'hAAAAA9AA)) 
     \wait_time_cnt[6]_i_3 
       (.I0(wait_time_cnt_reg__0[6]),
        .I1(wait_time_cnt_reg__0[5]),
        .I2(wait_time_cnt_reg__0[4]),
        .I3(\n_0_wait_time_cnt[6]_i_4 ),
        .I4(wait_time_cnt_reg__0[3]),
        .O(wait_time_cnt0[6]));
(* SOFT_HLUTNM = "soft_lutpair25" *) 
   LUT3 #(
    .INIT(8'h01)) 
     \wait_time_cnt[6]_i_4 
       (.I0(wait_time_cnt_reg__0[2]),
        .I1(wait_time_cnt_reg__0[1]),
        .I2(wait_time_cnt_reg__0[0]),
        .O(\n_0_wait_time_cnt[6]_i_4 ));
FDRE \wait_time_cnt_reg[0] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2 ),
        .D(wait_time_cnt0[0]),
        .Q(wait_time_cnt_reg__0[0]),
        .R(clear));
FDRE \wait_time_cnt_reg[1] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2 ),
        .D(\n_0_wait_time_cnt[1]_i_1 ),
        .Q(wait_time_cnt_reg__0[1]),
        .R(clear));
FDSE \wait_time_cnt_reg[2] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2 ),
        .D(wait_time_cnt0[2]),
        .Q(wait_time_cnt_reg__0[2]),
        .S(clear));
FDRE \wait_time_cnt_reg[3] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2 ),
        .D(wait_time_cnt0[3]),
        .Q(wait_time_cnt_reg__0[3]),
        .R(clear));
FDRE \wait_time_cnt_reg[4] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2 ),
        .D(\n_0_wait_time_cnt[4]_i_1 ),
        .Q(wait_time_cnt_reg__0[4]),
        .R(clear));
FDSE \wait_time_cnt_reg[5] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2 ),
        .D(wait_time_cnt0[5]),
        .Q(wait_time_cnt_reg__0[5]),
        .S(clear));
FDSE \wait_time_cnt_reg[6] 
       (.C(CLK),
        .CE(\n_0_wait_time_cnt[6]_i_2 ),
        .D(wait_time_cnt0[6]),
        .Q(wait_time_cnt_reg__0[6]),
        .S(clear));
endmodule

(* ORIG_REF_NAME = "gtp_sata_common" *) 
module gtp_sata_gtp_sata_common__parameterized0
   (GT0_PLL0LOCK_OUT,
    GT0_PLL0OUTCLK_OUT,
    GT0_PLL0OUTREFCLK_OUT,
    GT0_PLL0REFCLKLOST_OUT,
    GT0_PLL1OUTCLK_OUT,
    GT0_PLL1OUTREFCLK_OUT,
    Q0_CLK1_GTREFCLK_OUT,
    DRP_CLK_O,
    GT0_PLL0RESET_OUT);
  output GT0_PLL0LOCK_OUT;
  output GT0_PLL0OUTCLK_OUT;
  output GT0_PLL0OUTREFCLK_OUT;
  output GT0_PLL0REFCLKLOST_OUT;
  output GT0_PLL1OUTCLK_OUT;
  output GT0_PLL1OUTREFCLK_OUT;
  input Q0_CLK1_GTREFCLK_OUT;
  input DRP_CLK_O;
  input GT0_PLL0RESET_OUT;

  wire DRP_CLK_O;
  wire GT0_PLL0LOCK_OUT;
  wire GT0_PLL0OUTCLK_OUT;
  wire GT0_PLL0OUTREFCLK_OUT;
  wire GT0_PLL0REFCLKLOST_OUT;
  wire GT0_PLL0RESET_OUT;
  wire GT0_PLL1OUTCLK_OUT;
  wire GT0_PLL1OUTREFCLK_OUT;
  wire Q0_CLK1_GTREFCLK_OUT;
  wire n_60_gtpe2_common_i;
  wire n_61_gtpe2_common_i;
  wire n_62_gtpe2_common_i;
  wire n_63_gtpe2_common_i;
  wire n_65_gtpe2_common_i;
  wire n_66_gtpe2_common_i;
  wire n_67_gtpe2_common_i;
  wire NLW_gtpe2_common_i_DRPRDY_UNCONNECTED;
  wire NLW_gtpe2_common_i_PLL0FBCLKLOST_UNCONNECTED;
  wire NLW_gtpe2_common_i_PLL1FBCLKLOST_UNCONNECTED;
  wire NLW_gtpe2_common_i_PLL1LOCK_UNCONNECTED;
  wire NLW_gtpe2_common_i_PLL1REFCLKLOST_UNCONNECTED;
  wire NLW_gtpe2_common_i_REFCLKOUTMONITOR0_UNCONNECTED;
  wire NLW_gtpe2_common_i_REFCLKOUTMONITOR1_UNCONNECTED;
  wire [7:0]NLW_gtpe2_common_i_DMONITOROUT_UNCONNECTED;
  wire [15:0]NLW_gtpe2_common_i_DRPDO_UNCONNECTED;
  wire [15:0]NLW_gtpe2_common_i_PMARSVDOUT_UNCONNECTED;

(* box_type = "PRIMITIVE" *) 
   GTPE2_COMMON #(
    .BIAS_CFG(64'h0000000000050001),
    .COMMON_CFG(32'h00000000),
    .IS_DRPCLK_INVERTED(1'b0),
    .IS_GTGREFCLK0_INVERTED(1'b0),
    .IS_GTGREFCLK1_INVERTED(1'b0),
    .IS_PLL0LOCKDETCLK_INVERTED(1'b0),
    .IS_PLL1LOCKDETCLK_INVERTED(1'b0),
    .PLL0_CFG(27'h01F03DC),
    .PLL0_DMON_CFG(1'b0),
    .PLL0_FBDIV(4),
    .PLL0_FBDIV_45(5),
    .PLL0_INIT_CFG(24'h00001E),
    .PLL0_LOCK_CFG(9'h1E8),
    .PLL0_REFCLK_DIV(1),
    .PLL1_CFG(27'h01F03DC),
    .PLL1_DMON_CFG(1'b0),
    .PLL1_FBDIV(1),
    .PLL1_FBDIV_45(4),
    .PLL1_INIT_CFG(24'h00001E),
    .PLL1_LOCK_CFG(9'h1E8),
    .PLL1_REFCLK_DIV(1),
    .PLL_CLKOUT_CFG(8'b00000000),
    .RSVD_ATTR0(16'h0000),
    .RSVD_ATTR1(16'h0000),
    .SIM_PLL0REFCLK_SEL(3'b001),
    .SIM_PLL1REFCLK_SEL(3'b001),
    .SIM_RESET_SPEEDUP("FALSE"),
    .SIM_VERSION("2.0")) 
     gtpe2_common_i
       (.BGBYPASSB(1'b1),
        .BGMONITORENB(1'b1),
        .BGPDB(1'b1),
        .BGRCALOVRD({1'b1,1'b1,1'b1,1'b1,1'b1}),
        .BGRCALOVRDENB(1'b1),
        .DMONITOROUT(NLW_gtpe2_common_i_DMONITOROUT_UNCONNECTED[7:0]),
        .DRPADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPCLK(1'b0),
        .DRPDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DRPDO(NLW_gtpe2_common_i_DRPDO_UNCONNECTED[15:0]),
        .DRPEN(1'b0),
        .DRPRDY(NLW_gtpe2_common_i_DRPRDY_UNCONNECTED),
        .DRPWE(1'b0),
        .GTEASTREFCLK0(n_60_gtpe2_common_i),
        .GTEASTREFCLK1(n_61_gtpe2_common_i),
        .GTGREFCLK0(n_62_gtpe2_common_i),
        .GTGREFCLK1(n_63_gtpe2_common_i),
        .GTREFCLK0(Q0_CLK1_GTREFCLK_OUT),
        .GTREFCLK1(n_65_gtpe2_common_i),
        .GTWESTREFCLK0(n_66_gtpe2_common_i),
        .GTWESTREFCLK1(n_67_gtpe2_common_i),
        .PLL0FBCLKLOST(NLW_gtpe2_common_i_PLL0FBCLKLOST_UNCONNECTED),
        .PLL0LOCK(GT0_PLL0LOCK_OUT),
        .PLL0LOCKDETCLK(DRP_CLK_O),
        .PLL0LOCKEN(1'b1),
        .PLL0OUTCLK(GT0_PLL0OUTCLK_OUT),
        .PLL0OUTREFCLK(GT0_PLL0OUTREFCLK_OUT),
        .PLL0PD(1'b0),
        .PLL0REFCLKLOST(GT0_PLL0REFCLKLOST_OUT),
        .PLL0REFCLKSEL({1'b0,1'b0,1'b1}),
        .PLL0RESET(GT0_PLL0RESET_OUT),
        .PLL1FBCLKLOST(NLW_gtpe2_common_i_PLL1FBCLKLOST_UNCONNECTED),
        .PLL1LOCK(NLW_gtpe2_common_i_PLL1LOCK_UNCONNECTED),
        .PLL1LOCKDETCLK(1'b0),
        .PLL1LOCKEN(1'b1),
        .PLL1OUTCLK(GT0_PLL1OUTCLK_OUT),
        .PLL1OUTREFCLK(GT0_PLL1OUTREFCLK_OUT),
        .PLL1PD(1'b1),
        .PLL1REFCLKLOST(NLW_gtpe2_common_i_PLL1REFCLKLOST_UNCONNECTED),
        .PLL1REFCLKSEL({1'b0,1'b0,1'b1}),
        .PLL1RESET(1'b0),
        .PLLRSVD1({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PLLRSVD2({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PMARSVD({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PMARSVDOUT(NLW_gtpe2_common_i_PMARSVDOUT_UNCONNECTED[15:0]),
        .RCALENB(1'b1),
        .REFCLKOUTMONITOR0(NLW_gtpe2_common_i_REFCLKOUTMONITOR0_UNCONNECTED),
        .REFCLKOUTMONITOR1(NLW_gtpe2_common_i_REFCLKOUTMONITOR1_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "gtp_sata_common_reset" *) 
module gtp_sata_gtp_sata_common_reset__parameterized0
   (GT0_PLL0RESET_OUT,
    DRPCLK_OUT,
    SOFT_RESET_IN,
    AR);
  output GT0_PLL0RESET_OUT;
  input DRPCLK_OUT;
  input SOFT_RESET_IN;
  input [0:0]AR;

  wire [0:0]AR;
  wire COMMON_RESET;
  wire DRPCLK_OUT;
  wire GT0_PLL0RESET_OUT;
  wire SOFT_RESET_IN;
  wire common_reset_asserted;
  wire [7:0]init_wait_count_reg__0;
  wire init_wait_done;
  wire n_0_COMMON_RESET_i_1;
  wire n_0_common_reset_asserted_i_1;
  wire \n_0_init_wait_count[7]_i_1 ;
  wire \n_0_init_wait_count[7]_i_3 ;
  wire \n_0_init_wait_count[7]_i_4 ;
  wire n_0_init_wait_done_i_1;
  wire n_0_state_i_1;
  wire [7:0]plusOp;
  wire state;

(* SOFT_HLUTNM = "soft_lutpair1" *) 
   LUT4 #(
    .INIT(16'h0704)) 
     COMMON_RESET_i_1
       (.I0(common_reset_asserted),
        .I1(state),
        .I2(SOFT_RESET_IN),
        .I3(COMMON_RESET),
        .O(n_0_COMMON_RESET_i_1));
FDRE #(
    .INIT(1'b0)) 
     COMMON_RESET_reg
       (.C(DRPCLK_OUT),
        .CE(1'b1),
        .D(n_0_COMMON_RESET_i_1),
        .Q(COMMON_RESET),
        .R(1'b0));
(* SOFT_HLUTNM = "soft_lutpair1" *) 
   LUT2 #(
    .INIT(4'hE)) 
     GT0_PLL0RESET_OUT_INST_0
       (.I0(COMMON_RESET),
        .I1(AR),
        .O(GT0_PLL0RESET_OUT));
(* SOFT_HLUTNM = "soft_lutpair3" *) 
   LUT2 #(
    .INIT(4'hE)) 
     common_reset_asserted_i_1
       (.I0(state),
        .I1(common_reset_asserted),
        .O(n_0_common_reset_asserted_i_1));
FDRE #(
    .INIT(1'b0)) 
     common_reset_asserted_reg
       (.C(DRPCLK_OUT),
        .CE(1'b1),
        .D(n_0_common_reset_asserted_i_1),
        .Q(common_reset_asserted),
        .R(SOFT_RESET_IN));
(* SOFT_HLUTNM = "soft_lutpair4" *) 
   LUT1 #(
    .INIT(2'h1)) 
     \init_wait_count[0]_i_1 
       (.I0(init_wait_count_reg__0[0]),
        .O(plusOp[0]));
(* SOFT_HLUTNM = "soft_lutpair4" *) 
   LUT2 #(
    .INIT(4'h6)) 
     \init_wait_count[1]_i_1 
       (.I0(init_wait_count_reg__0[0]),
        .I1(init_wait_count_reg__0[1]),
        .O(plusOp[1]));
(* SOFT_HLUTNM = "soft_lutpair2" *) 
   LUT3 #(
    .INIT(8'h78)) 
     \init_wait_count[2]_i_1 
       (.I0(init_wait_count_reg__0[0]),
        .I1(init_wait_count_reg__0[1]),
        .I2(init_wait_count_reg__0[2]),
        .O(plusOp[2]));
(* SOFT_HLUTNM = "soft_lutpair0" *) 
   LUT4 #(
    .INIT(16'h6AAA)) 
     \init_wait_count[3]_i_1 
       (.I0(init_wait_count_reg__0[3]),
        .I1(init_wait_count_reg__0[0]),
        .I2(init_wait_count_reg__0[1]),
        .I3(init_wait_count_reg__0[2]),
        .O(plusOp[3]));
(* SOFT_HLUTNM = "soft_lutpair0" *) 
   LUT5 #(
    .INIT(32'h6AAAAAAA)) 
     \init_wait_count[4]_i_1 
       (.I0(init_wait_count_reg__0[4]),
        .I1(init_wait_count_reg__0[2]),
        .I2(init_wait_count_reg__0[1]),
        .I3(init_wait_count_reg__0[0]),
        .I4(init_wait_count_reg__0[3]),
        .O(plusOp[4]));
LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
     \init_wait_count[5]_i_1 
       (.I0(init_wait_count_reg__0[5]),
        .I1(init_wait_count_reg__0[3]),
        .I2(init_wait_count_reg__0[0]),
        .I3(init_wait_count_reg__0[1]),
        .I4(init_wait_count_reg__0[2]),
        .I5(init_wait_count_reg__0[4]),
        .O(plusOp[5]));
LUT5 #(
    .INIT(32'h6AAAAAAA)) 
     \init_wait_count[6]_i_1 
       (.I0(init_wait_count_reg__0[6]),
        .I1(init_wait_count_reg__0[4]),
        .I2(\n_0_init_wait_count[7]_i_4 ),
        .I3(init_wait_count_reg__0[3]),
        .I4(init_wait_count_reg__0[5]),
        .O(plusOp[6]));
LUT3 #(
    .INIT(8'hFD)) 
     \init_wait_count[7]_i_1 
       (.I0(\n_0_init_wait_count[7]_i_3 ),
        .I1(init_wait_count_reg__0[2]),
        .I2(init_wait_count_reg__0[4]),
        .O(\n_0_init_wait_count[7]_i_1 ));
LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
     \init_wait_count[7]_i_2 
       (.I0(init_wait_count_reg__0[7]),
        .I1(init_wait_count_reg__0[5]),
        .I2(init_wait_count_reg__0[3]),
        .I3(\n_0_init_wait_count[7]_i_4 ),
        .I4(init_wait_count_reg__0[4]),
        .I5(init_wait_count_reg__0[6]),
        .O(plusOp[7]));
LUT6 #(
    .INIT(64'h0000000000000080)) 
     \init_wait_count[7]_i_3 
       (.I0(init_wait_count_reg__0[5]),
        .I1(init_wait_count_reg__0[3]),
        .I2(init_wait_count_reg__0[0]),
        .I3(init_wait_count_reg__0[1]),
        .I4(init_wait_count_reg__0[6]),
        .I5(init_wait_count_reg__0[7]),
        .O(\n_0_init_wait_count[7]_i_3 ));
(* SOFT_HLUTNM = "soft_lutpair2" *) 
   LUT3 #(
    .INIT(8'h80)) 
     \init_wait_count[7]_i_4 
       (.I0(init_wait_count_reg__0[2]),
        .I1(init_wait_count_reg__0[1]),
        .I2(init_wait_count_reg__0[0]),
        .O(\n_0_init_wait_count[7]_i_4 ));
FDRE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[0] 
       (.C(DRPCLK_OUT),
        .CE(\n_0_init_wait_count[7]_i_1 ),
        .D(plusOp[0]),
        .Q(init_wait_count_reg__0[0]),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[1] 
       (.C(DRPCLK_OUT),
        .CE(\n_0_init_wait_count[7]_i_1 ),
        .D(plusOp[1]),
        .Q(init_wait_count_reg__0[1]),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[2] 
       (.C(DRPCLK_OUT),
        .CE(\n_0_init_wait_count[7]_i_1 ),
        .D(plusOp[2]),
        .Q(init_wait_count_reg__0[2]),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[3] 
       (.C(DRPCLK_OUT),
        .CE(\n_0_init_wait_count[7]_i_1 ),
        .D(plusOp[3]),
        .Q(init_wait_count_reg__0[3]),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[4] 
       (.C(DRPCLK_OUT),
        .CE(\n_0_init_wait_count[7]_i_1 ),
        .D(plusOp[4]),
        .Q(init_wait_count_reg__0[4]),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[5] 
       (.C(DRPCLK_OUT),
        .CE(\n_0_init_wait_count[7]_i_1 ),
        .D(plusOp[5]),
        .Q(init_wait_count_reg__0[5]),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[6] 
       (.C(DRPCLK_OUT),
        .CE(\n_0_init_wait_count[7]_i_1 ),
        .D(plusOp[6]),
        .Q(init_wait_count_reg__0[6]),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     \init_wait_count_reg[7] 
       (.C(DRPCLK_OUT),
        .CE(\n_0_init_wait_count[7]_i_1 ),
        .D(plusOp[7]),
        .Q(init_wait_count_reg__0[7]),
        .R(1'b0));
LUT4 #(
    .INIT(16'hAAAE)) 
     init_wait_done_i_1
       (.I0(init_wait_done),
        .I1(\n_0_init_wait_count[7]_i_3 ),
        .I2(init_wait_count_reg__0[2]),
        .I3(init_wait_count_reg__0[4]),
        .O(n_0_init_wait_done_i_1));
FDRE #(
    .INIT(1'b0)) 
     init_wait_done_reg
       (.C(DRPCLK_OUT),
        .CE(1'b1),
        .D(n_0_init_wait_done_i_1),
        .Q(init_wait_done),
        .R(1'b0));
(* SOFT_HLUTNM = "soft_lutpair3" *) 
   LUT2 #(
    .INIT(4'hE)) 
     state_i_1
       (.I0(init_wait_done),
        .I1(state),
        .O(n_0_state_i_1));
FDRE #(
    .INIT(1'b0)) 
     state_reg
       (.C(DRPCLK_OUT),
        .CE(1'b1),
        .D(n_0_state_i_1),
        .Q(state),
        .R(SOFT_RESET_IN));
endmodule

(* ORIG_REF_NAME = "gtp_sata_gtrxreset_seq" *) 
module gtp_sata_gtp_sata_gtrxreset_seq
   (GTRXRESET_OUT,
    O1,
    DRPDI,
    O2,
    DRPADDR,
    SR,
    CLK,
    I2,
    I1,
    data_in,
    gt0_drpdo_out,
    gt0_drpen_in,
    gt0_drpdi_in,
    gt0_drpwe_in,
    gt0_drpaddr_in);
  output GTRXRESET_OUT;
  output O1;
  output [15:0]DRPDI;
  output O2;
  output [8:0]DRPADDR;
  input [0:0]SR;
  input CLK;
  input I2;
  input I1;
  input data_in;
  input [15:0]gt0_drpdo_out;
  input gt0_drpen_in;
  input [15:0]gt0_drpdi_in;
  input gt0_drpwe_in;
  input [8:0]gt0_drpaddr_in;

  wire CLK;
  wire [8:0]DRPADDR;
  wire [15:0]DRPDI;
  wire DRP_OP_DONE;
  wire GTRXRESET_OUT;
  wire I1;
  wire I2;
  wire O1;
  wire O2;
  wire [0:0]SR;
  wire data_in;
  wire data_out;
  wire flag;
  wire [8:0]gt0_drpaddr_in;
  wire [15:0]gt0_drpdi_in;
  wire [15:0]gt0_drpdo_out;
  wire gt0_drpen_in;
  wire gt0_drpwe_in;
  wire gtrxreset_i;
  wire gtrxreset_s;
  wire gtrxreset_ss;
  wire n_0_drp_op_done_o_i_1;
  wire n_0_flag_i_1;
  wire \n_0_rd_data[15]_i_1 ;
  wire \n_0_state_reg[0] ;
  wire \n_0_state_reg[1] ;
  wire \n_0_state_reg[2] ;
  wire [2:0]next_state;
  wire [15:0]original_rd_data;
  wire original_rd_data0;
  wire [15:0]p_1_in;
  wire [15:0]rd_data;
  wire rxpmaresetdone_sss;

LUT5 #(
    .INIT(32'hFFFF8000)) 
     drp_op_done_o_i_1
       (.I0(\n_0_state_reg[0] ),
        .I1(I1),
        .I2(\n_0_state_reg[1] ),
        .I3(\n_0_state_reg[2] ),
        .I4(DRP_OP_DONE),
        .O(n_0_drp_op_done_o_i_1));
FDCE drp_op_done_o_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(SR),
        .D(n_0_drp_op_done_o_i_1),
        .Q(DRP_OP_DONE));
LUT4 #(
    .INIT(16'h3EEE)) 
     flag_i_1
       (.I0(flag),
        .I1(\n_0_state_reg[2] ),
        .I2(\n_0_state_reg[0] ),
        .I3(\n_0_state_reg[1] ),
        .O(n_0_flag_i_1));
FDRE #(
    .INIT(1'b0)) 
     flag_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_flag_i_1),
        .Q(flag),
        .R(1'b0));
LUT5 #(
    .INIT(32'hFF640064)) 
     gtpe2_i_i_1
       (.I0(\n_0_state_reg[2] ),
        .I1(\n_0_state_reg[0] ),
        .I2(\n_0_state_reg[1] ),
        .I3(DRP_OP_DONE),
        .I4(gt0_drpen_in),
        .O(O1));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_10
       (.I0(gt0_drpdi_in[8]),
        .I1(rd_data[8]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[8]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_11
       (.I0(gt0_drpdi_in[7]),
        .I1(rd_data[7]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[7]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_12
       (.I0(gt0_drpdi_in[6]),
        .I1(rd_data[6]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[6]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_13
       (.I0(gt0_drpdi_in[5]),
        .I1(rd_data[5]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[5]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_14
       (.I0(gt0_drpdi_in[4]),
        .I1(rd_data[4]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[4]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_15
       (.I0(gt0_drpdi_in[3]),
        .I1(rd_data[3]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[3]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_16
       (.I0(gt0_drpdi_in[2]),
        .I1(rd_data[2]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[2]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_17
       (.I0(gt0_drpdi_in[1]),
        .I1(rd_data[1]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[1]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_18
       (.I0(gt0_drpdi_in[0]),
        .I1(rd_data[0]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[0]));
(* SOFT_HLUTNM = "soft_lutpair30" *) 
   LUT2 #(
    .INIT(4'h8)) 
     gtpe2_i_i_19
       (.I0(DRP_OP_DONE),
        .I1(gt0_drpaddr_in[8]),
        .O(DRPADDR[8]));
LUT5 #(
    .INIT(32'hAAAA3C00)) 
     gtpe2_i_i_2
       (.I0(gt0_drpwe_in),
        .I1(\n_0_state_reg[0] ),
        .I2(\n_0_state_reg[2] ),
        .I3(\n_0_state_reg[1] ),
        .I4(DRP_OP_DONE),
        .O(O2));
(* SOFT_HLUTNM = "soft_lutpair30" *) 
   LUT2 #(
    .INIT(4'h8)) 
     gtpe2_i_i_20
       (.I0(DRP_OP_DONE),
        .I1(gt0_drpaddr_in[7]),
        .O(DRPADDR[7]));
(* SOFT_HLUTNM = "soft_lutpair31" *) 
   LUT2 #(
    .INIT(4'h8)) 
     gtpe2_i_i_21
       (.I0(DRP_OP_DONE),
        .I1(gt0_drpaddr_in[6]),
        .O(DRPADDR[6]));
(* SOFT_HLUTNM = "soft_lutpair31" *) 
   LUT2 #(
    .INIT(4'h8)) 
     gtpe2_i_i_22
       (.I0(DRP_OP_DONE),
        .I1(gt0_drpaddr_in[5]),
        .O(DRPADDR[5]));
(* SOFT_HLUTNM = "soft_lutpair32" *) 
   LUT2 #(
    .INIT(4'hB)) 
     gtpe2_i_i_23
       (.I0(gt0_drpaddr_in[4]),
        .I1(DRP_OP_DONE),
        .O(DRPADDR[4]));
(* SOFT_HLUTNM = "soft_lutpair32" *) 
   LUT2 #(
    .INIT(4'h8)) 
     gtpe2_i_i_24
       (.I0(DRP_OP_DONE),
        .I1(gt0_drpaddr_in[3]),
        .O(DRPADDR[3]));
(* SOFT_HLUTNM = "soft_lutpair33" *) 
   LUT2 #(
    .INIT(4'h8)) 
     gtpe2_i_i_25
       (.I0(DRP_OP_DONE),
        .I1(gt0_drpaddr_in[2]),
        .O(DRPADDR[2]));
(* SOFT_HLUTNM = "soft_lutpair33" *) 
   LUT2 #(
    .INIT(4'h8)) 
     gtpe2_i_i_26
       (.I0(DRP_OP_DONE),
        .I1(gt0_drpaddr_in[1]),
        .O(DRPADDR[1]));
LUT2 #(
    .INIT(4'hB)) 
     gtpe2_i_i_27
       (.I0(gt0_drpaddr_in[0]),
        .I1(DRP_OP_DONE),
        .O(DRPADDR[0]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_3
       (.I0(gt0_drpdi_in[15]),
        .I1(rd_data[15]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[15]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_4
       (.I0(gt0_drpdi_in[14]),
        .I1(rd_data[14]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[14]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_5
       (.I0(gt0_drpdi_in[13]),
        .I1(rd_data[13]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[13]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_6
       (.I0(gt0_drpdi_in[12]),
        .I1(rd_data[12]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[12]));
LUT6 #(
    .INIT(64'hAA0CAA00AA00AA00)) 
     gtpe2_i_i_7
       (.I0(gt0_drpdi_in[11]),
        .I1(\n_0_state_reg[2] ),
        .I2(\n_0_state_reg[0] ),
        .I3(DRP_OP_DONE),
        .I4(\n_0_state_reg[1] ),
        .I5(rd_data[11]),
        .O(DRPDI[11]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_8
       (.I0(gt0_drpdi_in[10]),
        .I1(rd_data[10]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[10]));
LUT6 #(
    .INIT(64'hA0A0ACA0ACA0A0A0)) 
     gtpe2_i_i_9
       (.I0(gt0_drpdi_in[9]),
        .I1(rd_data[9]),
        .I2(DRP_OP_DONE),
        .I3(\n_0_state_reg[1] ),
        .I4(\n_0_state_reg[2] ),
        .I5(\n_0_state_reg[0] ),
        .O(DRPDI[9]));
(* SOFT_HLUTNM = "soft_lutpair29" *) 
   LUT4 #(
    .INIT(16'h0DFA)) 
     gtrxreset_o_i_1
       (.I0(\n_0_state_reg[0] ),
        .I1(gtrxreset_ss),
        .I2(\n_0_state_reg[1] ),
        .I3(\n_0_state_reg[2] ),
        .O(gtrxreset_i));
FDCE gtrxreset_o_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(I2),
        .D(gtrxreset_i),
        .Q(GTRXRESET_OUT));
FDCE gtrxreset_s_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(I2),
        .D(SR),
        .Q(gtrxreset_s));
FDCE gtrxreset_ss_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(I2),
        .D(gtrxreset_s),
        .Q(gtrxreset_ss));
LUT5 #(
    .INIT(32'h00040000)) 
     \original_rd_data[15]_i_1 
       (.I0(\n_0_state_reg[0] ),
        .I1(I1),
        .I2(flag),
        .I3(\n_0_state_reg[2] ),
        .I4(\n_0_state_reg[1] ),
        .O(original_rd_data0));
FDRE \original_rd_data_reg[0] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[0]),
        .Q(original_rd_data[0]),
        .R(1'b0));
FDRE \original_rd_data_reg[10] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[10]),
        .Q(original_rd_data[10]),
        .R(1'b0));
FDRE \original_rd_data_reg[11] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[11]),
        .Q(original_rd_data[11]),
        .R(1'b0));
FDRE \original_rd_data_reg[12] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[12]),
        .Q(original_rd_data[12]),
        .R(1'b0));
FDRE \original_rd_data_reg[13] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[13]),
        .Q(original_rd_data[13]),
        .R(1'b0));
FDRE \original_rd_data_reg[14] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[14]),
        .Q(original_rd_data[14]),
        .R(1'b0));
FDRE \original_rd_data_reg[15] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[15]),
        .Q(original_rd_data[15]),
        .R(1'b0));
FDRE \original_rd_data_reg[1] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[1]),
        .Q(original_rd_data[1]),
        .R(1'b0));
FDRE \original_rd_data_reg[2] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[2]),
        .Q(original_rd_data[2]),
        .R(1'b0));
FDRE \original_rd_data_reg[3] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[3]),
        .Q(original_rd_data[3]),
        .R(1'b0));
FDRE \original_rd_data_reg[4] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[4]),
        .Q(original_rd_data[4]),
        .R(1'b0));
FDRE \original_rd_data_reg[5] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[5]),
        .Q(original_rd_data[5]),
        .R(1'b0));
FDRE \original_rd_data_reg[6] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[6]),
        .Q(original_rd_data[6]),
        .R(1'b0));
FDRE \original_rd_data_reg[7] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[7]),
        .Q(original_rd_data[7]),
        .R(1'b0));
FDRE \original_rd_data_reg[8] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[8]),
        .Q(original_rd_data[8]),
        .R(1'b0));
FDRE \original_rd_data_reg[9] 
       (.C(CLK),
        .CE(original_rd_data0),
        .D(gt0_drpdo_out[9]),
        .Q(original_rd_data[9]),
        .R(1'b0));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[0]_i_1 
       (.I0(gt0_drpdo_out[0]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[0]),
        .O(p_1_in[0]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[10]_i_1 
       (.I0(gt0_drpdo_out[10]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[10]),
        .O(p_1_in[10]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[11]_i_1 
       (.I0(gt0_drpdo_out[11]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[11]),
        .O(p_1_in[11]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[12]_i_1 
       (.I0(gt0_drpdo_out[12]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[12]),
        .O(p_1_in[12]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[13]_i_1 
       (.I0(gt0_drpdo_out[13]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[13]),
        .O(p_1_in[13]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[14]_i_1 
       (.I0(gt0_drpdo_out[14]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[14]),
        .O(p_1_in[14]));
LUT4 #(
    .INIT(16'h0400)) 
     \rd_data[15]_i_1 
       (.I0(\n_0_state_reg[0] ),
        .I1(\n_0_state_reg[1] ),
        .I2(\n_0_state_reg[2] ),
        .I3(I1),
        .O(\n_0_rd_data[15]_i_1 ));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[15]_i_2 
       (.I0(gt0_drpdo_out[15]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[15]),
        .O(p_1_in[15]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[1]_i_1 
       (.I0(gt0_drpdo_out[1]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[1]),
        .O(p_1_in[1]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[2]_i_1 
       (.I0(gt0_drpdo_out[2]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[2]),
        .O(p_1_in[2]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[3]_i_1 
       (.I0(gt0_drpdo_out[3]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[3]),
        .O(p_1_in[3]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[4]_i_1 
       (.I0(gt0_drpdo_out[4]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[4]),
        .O(p_1_in[4]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[5]_i_1 
       (.I0(gt0_drpdo_out[5]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[5]),
        .O(p_1_in[5]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[6]_i_1 
       (.I0(gt0_drpdo_out[6]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[6]),
        .O(p_1_in[6]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[7]_i_1 
       (.I0(gt0_drpdo_out[7]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[7]),
        .O(p_1_in[7]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[8]_i_1 
       (.I0(gt0_drpdo_out[8]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[8]),
        .O(p_1_in[8]));
LUT4 #(
    .INIT(16'hFB08)) 
     \rd_data[9]_i_1 
       (.I0(gt0_drpdo_out[9]),
        .I1(I1),
        .I2(flag),
        .I3(original_rd_data[9]),
        .O(p_1_in[9]));
FDCE \rd_data_reg[0] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[0]),
        .Q(rd_data[0]));
FDCE \rd_data_reg[10] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[10]),
        .Q(rd_data[10]));
FDCE \rd_data_reg[11] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[11]),
        .Q(rd_data[11]));
FDCE \rd_data_reg[12] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[12]),
        .Q(rd_data[12]));
FDCE \rd_data_reg[13] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[13]),
        .Q(rd_data[13]));
FDCE \rd_data_reg[14] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[14]),
        .Q(rd_data[14]));
FDCE \rd_data_reg[15] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[15]),
        .Q(rd_data[15]));
FDCE \rd_data_reg[1] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[1]),
        .Q(rd_data[1]));
FDCE \rd_data_reg[2] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[2]),
        .Q(rd_data[2]));
FDCE \rd_data_reg[3] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[3]),
        .Q(rd_data[3]));
FDCE \rd_data_reg[4] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[4]),
        .Q(rd_data[4]));
FDCE \rd_data_reg[5] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[5]),
        .Q(rd_data[5]));
FDCE \rd_data_reg[6] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[6]),
        .Q(rd_data[6]));
FDCE \rd_data_reg[7] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[7]),
        .Q(rd_data[7]));
FDCE \rd_data_reg[8] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[8]),
        .Q(rd_data[8]));
FDCE \rd_data_reg[9] 
       (.C(CLK),
        .CE(\n_0_rd_data[15]_i_1 ),
        .CLR(I2),
        .D(p_1_in[9]),
        .Q(rd_data[9]));
FDCE rxpmaresetdone_sss_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(I2),
        .D(data_out),
        .Q(rxpmaresetdone_sss));
(* SOFT_HLUTNM = "soft_lutpair29" *) 
   LUT4 #(
    .INIT(16'h7CCC)) 
     \state[2]_i_1 
       (.I0(I1),
        .I1(\n_0_state_reg[2] ),
        .I2(\n_0_state_reg[1] ),
        .I3(\n_0_state_reg[0] ),
        .O(next_state[2]));
FDCE #(
    .INIT(1'b0)) 
     \state_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(I2),
        .D(next_state[0]),
        .Q(\n_0_state_reg[0] ));
FDCE #(
    .INIT(1'b0)) 
     \state_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(I2),
        .D(next_state[1]),
        .Q(\n_0_state_reg[1] ));
FDCE #(
    .INIT(1'b0)) 
     \state_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(I2),
        .D(next_state[2]),
        .Q(\n_0_state_reg[2] ));
gtp_sata_gtp_sata_sync_block__parameterized0 sync0_RXPMARESETDONE
       (.CLK(CLK),
        .D(next_state[1:0]),
        .I1(I1),
        .Q({\n_0_state_reg[2] ,\n_0_state_reg[1] ,\n_0_state_reg[0] }),
        .data_in(data_in),
        .data_out(data_out),
        .gtrxreset_ss(gtrxreset_ss),
        .rxpmaresetdone_sss(rxpmaresetdone_sss));
endmodule

(* ORIG_REF_NAME = "gtp_sata_init" *) 
module gtp_sata_gtp_sata_init
   (O1,
    gt0_eyescandataerror_out,
    gt0_gtptxn_out,
    gt0_gtptxp_out,
    gt0_rxbyteisaligned_out,
    gt0_rxcominitdet_out,
    gt0_rxcomsasdet_out,
    gt0_rxcomwakedet_out,
    gt0_rxelecidle_out,
    gt0_rxresetdone_out,
    gt0_txcomfinish_out,
    gt0_txoutclk_out,
    gt0_txoutclkfabric_out,
    gt0_txoutclkpcs_out,
    gt0_txresetdone_out,
    gt0_dmonitorout_out,
    gt0_drpdo_out,
    gt0_rxstatus_out,
    gt0_rxdata_out,
    gt0_rxcharisk_out,
    gt0_rxdisperr_out,
    gt0_rxnotintable_out,
    AR,
    GT0_TX_FSM_RESET_DONE_OUT,
    GT0_RX_FSM_RESET_DONE_OUT,
    CLK,
    gt0_eyescanreset_in,
    gt0_eyescantrigger_in,
    gt0_gtprxn_in,
    gt0_gtprxp_in,
    GT0_PLL0OUTCLK_OUT,
    GT0_PLL0OUTREFCLK_OUT,
    GT0_PLL1OUTCLK_OUT,
    GT0_PLL1OUTREFCLK_OUT,
    gt0_rxlpmhfhold_in,
    gt0_rxlpmhfovrden_in,
    gt0_rxlpmlfhold_in,
    gt0_rxlpmreset_in,
    I1,
    gt0_txcominit_in,
    gt0_txcomsas_in,
    gt0_txcomwake_in,
    gt0_txelecidle_in,
    gt0_txdata_in,
    gt0_txcharisk_in,
    SOFT_RESET_IN,
    DONT_RESET_ON_DATA_ERROR_IN,
    GT0_PLL0LOCK_OUT,
    GT0_DATA_VALID_IN,
    gt0_drpen_in,
    gt0_drpdi_in,
    gt0_drpwe_in,
    gt0_drpaddr_in);
  output O1;
  output gt0_eyescandataerror_out;
  output gt0_gtptxn_out;
  output gt0_gtptxp_out;
  output gt0_rxbyteisaligned_out;
  output gt0_rxcominitdet_out;
  output gt0_rxcomsasdet_out;
  output gt0_rxcomwakedet_out;
  output gt0_rxelecidle_out;
  output gt0_rxresetdone_out;
  output gt0_txcomfinish_out;
  output gt0_txoutclk_out;
  output gt0_txoutclkfabric_out;
  output gt0_txoutclkpcs_out;
  output gt0_txresetdone_out;
  output [14:0]gt0_dmonitorout_out;
  output [15:0]gt0_drpdo_out;
  output [2:0]gt0_rxstatus_out;
  output [15:0]gt0_rxdata_out;
  output [1:0]gt0_rxcharisk_out;
  output [1:0]gt0_rxdisperr_out;
  output [1:0]gt0_rxnotintable_out;
  output [0:0]AR;
  output GT0_TX_FSM_RESET_DONE_OUT;
  output GT0_RX_FSM_RESET_DONE_OUT;
  input CLK;
  input gt0_eyescanreset_in;
  input gt0_eyescantrigger_in;
  input gt0_gtprxn_in;
  input gt0_gtprxp_in;
  input GT0_PLL0OUTCLK_OUT;
  input GT0_PLL0OUTREFCLK_OUT;
  input GT0_PLL1OUTCLK_OUT;
  input GT0_PLL1OUTREFCLK_OUT;
  input gt0_rxlpmhfhold_in;
  input gt0_rxlpmhfovrden_in;
  input gt0_rxlpmlfhold_in;
  input gt0_rxlpmreset_in;
  input I1;
  input gt0_txcominit_in;
  input gt0_txcomsas_in;
  input gt0_txcomwake_in;
  input gt0_txelecidle_in;
  input [15:0]gt0_txdata_in;
  input [1:0]gt0_txcharisk_in;
  input SOFT_RESET_IN;
  input DONT_RESET_ON_DATA_ERROR_IN;
  input GT0_PLL0LOCK_OUT;
  input GT0_DATA_VALID_IN;
  input gt0_drpen_in;
  input [15:0]gt0_drpdi_in;
  input gt0_drpwe_in;
  input [8:0]gt0_drpaddr_in;

  wire [0:0]AR;
  wire CLK;
  wire DONT_RESET_ON_DATA_ERROR_IN;
  wire GT0_DATA_VALID_IN;
  wire GT0_PLL0LOCK_OUT;
  wire GT0_PLL0OUTCLK_OUT;
  wire GT0_PLL0OUTREFCLK_OUT;
  wire GT0_PLL1OUTCLK_OUT;
  wire GT0_PLL1OUTREFCLK_OUT;
  wire GT0_RX_FSM_RESET_DONE_OUT;
  wire GT0_TX_FSM_RESET_DONE_OUT;
  wire GTRXRESET;
  wire GTTXRESET;
  wire I1;
  wire O1;
  wire RXUSERRDY;
  wire SOFT_RESET_IN;
  wire TXUSERRDY;
  wire [14:0]gt0_dmonitorout_out;
  wire [8:0]gt0_drpaddr_in;
  wire [15:0]gt0_drpdi_in;
  wire [15:0]gt0_drpdo_out;
  wire gt0_drpen_in;
  wire gt0_drpwe_in;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire gt0_gtprxn_in;
  wire gt0_gtprxp_in;
  wire gt0_gtptxn_out;
  wire gt0_gtptxp_out;
  wire [10:0]gt0_rx_cdrlock_counter;
  wire [10:0]gt0_rx_cdrlock_counter_0;
  wire gt0_rx_cdrlocked;
  wire gt0_rxbyteisaligned_out;
  wire [1:0]gt0_rxcharisk_out;
  wire gt0_rxcominitdet_out;
  wire gt0_rxcomsasdet_out;
  wire gt0_rxcomwakedet_out;
  wire [15:0]gt0_rxdata_out;
  wire [1:0]gt0_rxdisperr_out;
  wire gt0_rxelecidle_out;
  wire gt0_rxlpmhfhold_in;
  wire gt0_rxlpmhfovrden_in;
  wire gt0_rxlpmlfhold_in;
  wire gt0_rxlpmreset_in;
  wire [1:0]gt0_rxnotintable_out;
  wire gt0_rxresetdone_out;
  wire [2:0]gt0_rxstatus_out;
  wire [1:0]gt0_txcharisk_in;
  wire gt0_txcomfinish_out;
  wire gt0_txcominit_in;
  wire gt0_txcomsas_in;
  wire gt0_txcomwake_in;
  wire [15:0]gt0_txdata_in;
  wire gt0_txelecidle_in;
  wire gt0_txoutclk_out;
  wire gt0_txoutclkfabric_out;
  wire gt0_txoutclkpcs_out;
  wire gt0_txresetdone_out;
  wire \n_0_gt0_rx_cdrlock_counter[10]_i_2 ;
  wire \n_0_gt0_rx_cdrlock_counter[1]_i_3 ;
  wire \n_0_gt0_rx_cdrlock_counter[5]_i_1 ;
  wire \n_0_gt0_rx_cdrlock_counter[9]_i_2 ;
  wire n_0_gt0_rx_cdrlocked_i_1;
  wire n_0_gt0_rx_cdrlocked_reg;

(* SOFT_HLUTNM = "soft_lutpair37" *) 
   LUT2 #(
    .INIT(4'hB)) 
     \gt0_rx_cdrlock_counter[0]_i_1 
       (.I0(gt0_rx_cdrlocked),
        .I1(gt0_rx_cdrlock_counter[0]),
        .O(gt0_rx_cdrlock_counter_0[0]));
LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
     \gt0_rx_cdrlock_counter[10]_i_1 
       (.I0(gt0_rx_cdrlock_counter[10]),
        .I1(gt0_rx_cdrlock_counter[8]),
        .I2(gt0_rx_cdrlock_counter[6]),
        .I3(\n_0_gt0_rx_cdrlock_counter[10]_i_2 ),
        .I4(gt0_rx_cdrlock_counter[7]),
        .I5(gt0_rx_cdrlock_counter[9]),
        .O(gt0_rx_cdrlock_counter_0[10]));
LUT6 #(
    .INIT(64'h8000000000000000)) 
     \gt0_rx_cdrlock_counter[10]_i_2 
       (.I0(gt0_rx_cdrlock_counter[5]),
        .I1(gt0_rx_cdrlock_counter[4]),
        .I2(gt0_rx_cdrlock_counter[2]),
        .I3(gt0_rx_cdrlock_counter[1]),
        .I4(gt0_rx_cdrlock_counter[0]),
        .I5(gt0_rx_cdrlock_counter[3]),
        .O(\n_0_gt0_rx_cdrlock_counter[10]_i_2 ));
(* SOFT_HLUTNM = "soft_lutpair37" *) 
   LUT3 #(
    .INIT(8'h06)) 
     \gt0_rx_cdrlock_counter[1]_i_1 
       (.I0(gt0_rx_cdrlock_counter[0]),
        .I1(gt0_rx_cdrlock_counter[1]),
        .I2(gt0_rx_cdrlocked),
        .O(gt0_rx_cdrlock_counter_0[1]));
LUT6 #(
    .INIT(64'h0001000000000000)) 
     \gt0_rx_cdrlock_counter[1]_i_2 
       (.I0(gt0_rx_cdrlock_counter[8]),
        .I1(gt0_rx_cdrlock_counter[9]),
        .I2(gt0_rx_cdrlock_counter[6]),
        .I3(gt0_rx_cdrlock_counter[7]),
        .I4(gt0_rx_cdrlock_counter[10]),
        .I5(\n_0_gt0_rx_cdrlock_counter[1]_i_3 ),
        .O(gt0_rx_cdrlocked));
LUT6 #(
    .INIT(64'h0000000000100000)) 
     \gt0_rx_cdrlock_counter[1]_i_3 
       (.I0(gt0_rx_cdrlock_counter[1]),
        .I1(gt0_rx_cdrlock_counter[5]),
        .I2(gt0_rx_cdrlock_counter[4]),
        .I3(gt0_rx_cdrlock_counter[3]),
        .I4(gt0_rx_cdrlock_counter[0]),
        .I5(gt0_rx_cdrlock_counter[2]),
        .O(\n_0_gt0_rx_cdrlock_counter[1]_i_3 ));
(* SOFT_HLUTNM = "soft_lutpair36" *) 
   LUT3 #(
    .INIT(8'h6A)) 
     \gt0_rx_cdrlock_counter[2]_i_1 
       (.I0(gt0_rx_cdrlock_counter[2]),
        .I1(gt0_rx_cdrlock_counter[1]),
        .I2(gt0_rx_cdrlock_counter[0]),
        .O(gt0_rx_cdrlock_counter_0[2]));
(* SOFT_HLUTNM = "soft_lutpair36" *) 
   LUT4 #(
    .INIT(16'h6AAA)) 
     \gt0_rx_cdrlock_counter[3]_i_1 
       (.I0(gt0_rx_cdrlock_counter[3]),
        .I1(gt0_rx_cdrlock_counter[0]),
        .I2(gt0_rx_cdrlock_counter[1]),
        .I3(gt0_rx_cdrlock_counter[2]),
        .O(gt0_rx_cdrlock_counter_0[3]));
(* SOFT_HLUTNM = "soft_lutpair34" *) 
   LUT5 #(
    .INIT(32'h6AAAAAAA)) 
     \gt0_rx_cdrlock_counter[4]_i_1 
       (.I0(gt0_rx_cdrlock_counter[4]),
        .I1(gt0_rx_cdrlock_counter[2]),
        .I2(gt0_rx_cdrlock_counter[1]),
        .I3(gt0_rx_cdrlock_counter[0]),
        .I4(gt0_rx_cdrlock_counter[3]),
        .O(gt0_rx_cdrlock_counter_0[4]));
LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
     \gt0_rx_cdrlock_counter[5]_i_1 
       (.I0(gt0_rx_cdrlock_counter[5]),
        .I1(gt0_rx_cdrlock_counter[4]),
        .I2(gt0_rx_cdrlock_counter[2]),
        .I3(gt0_rx_cdrlock_counter[1]),
        .I4(gt0_rx_cdrlock_counter[0]),
        .I5(gt0_rx_cdrlock_counter[3]),
        .O(\n_0_gt0_rx_cdrlock_counter[5]_i_1 ));
LUT3 #(
    .INIT(8'h9A)) 
     \gt0_rx_cdrlock_counter[6]_i_1 
       (.I0(gt0_rx_cdrlock_counter[6]),
        .I1(\n_0_gt0_rx_cdrlock_counter[9]_i_2 ),
        .I2(gt0_rx_cdrlock_counter[5]),
        .O(gt0_rx_cdrlock_counter_0[6]));
(* SOFT_HLUTNM = "soft_lutpair35" *) 
   LUT4 #(
    .INIT(16'hA6AA)) 
     \gt0_rx_cdrlock_counter[7]_i_1 
       (.I0(gt0_rx_cdrlock_counter[7]),
        .I1(gt0_rx_cdrlock_counter[5]),
        .I2(\n_0_gt0_rx_cdrlock_counter[9]_i_2 ),
        .I3(gt0_rx_cdrlock_counter[6]),
        .O(gt0_rx_cdrlock_counter_0[7]));
(* SOFT_HLUTNM = "soft_lutpair35" *) 
   LUT5 #(
    .INIT(32'hA6AAAAAA)) 
     \gt0_rx_cdrlock_counter[8]_i_1 
       (.I0(gt0_rx_cdrlock_counter[8]),
        .I1(gt0_rx_cdrlock_counter[6]),
        .I2(\n_0_gt0_rx_cdrlock_counter[9]_i_2 ),
        .I3(gt0_rx_cdrlock_counter[5]),
        .I4(gt0_rx_cdrlock_counter[7]),
        .O(gt0_rx_cdrlock_counter_0[8]));
LUT6 #(
    .INIT(64'hAA6AAAAAAAAAAAAA)) 
     \gt0_rx_cdrlock_counter[9]_i_1 
       (.I0(gt0_rx_cdrlock_counter[9]),
        .I1(gt0_rx_cdrlock_counter[7]),
        .I2(gt0_rx_cdrlock_counter[5]),
        .I3(\n_0_gt0_rx_cdrlock_counter[9]_i_2 ),
        .I4(gt0_rx_cdrlock_counter[6]),
        .I5(gt0_rx_cdrlock_counter[8]),
        .O(gt0_rx_cdrlock_counter_0[9]));
(* SOFT_HLUTNM = "soft_lutpair34" *) 
   LUT5 #(
    .INIT(32'h7FFFFFFF)) 
     \gt0_rx_cdrlock_counter[9]_i_2 
       (.I0(gt0_rx_cdrlock_counter[3]),
        .I1(gt0_rx_cdrlock_counter[0]),
        .I2(gt0_rx_cdrlock_counter[1]),
        .I3(gt0_rx_cdrlock_counter[2]),
        .I4(gt0_rx_cdrlock_counter[4]),
        .O(\n_0_gt0_rx_cdrlock_counter[9]_i_2 ));
FDRE #(
    .INIT(1'b0)) 
     \gt0_rx_cdrlock_counter_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[0]),
        .Q(gt0_rx_cdrlock_counter[0]),
        .R(GTRXRESET));
FDRE #(
    .INIT(1'b0)) 
     \gt0_rx_cdrlock_counter_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[10]),
        .Q(gt0_rx_cdrlock_counter[10]),
        .R(GTRXRESET));
FDRE #(
    .INIT(1'b0)) 
     \gt0_rx_cdrlock_counter_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[1]),
        .Q(gt0_rx_cdrlock_counter[1]),
        .R(GTRXRESET));
FDRE #(
    .INIT(1'b0)) 
     \gt0_rx_cdrlock_counter_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[2]),
        .Q(gt0_rx_cdrlock_counter[2]),
        .R(GTRXRESET));
FDRE #(
    .INIT(1'b0)) 
     \gt0_rx_cdrlock_counter_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[3]),
        .Q(gt0_rx_cdrlock_counter[3]),
        .R(GTRXRESET));
FDRE #(
    .INIT(1'b0)) 
     \gt0_rx_cdrlock_counter_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[4]),
        .Q(gt0_rx_cdrlock_counter[4]),
        .R(GTRXRESET));
FDRE #(
    .INIT(1'b0)) 
     \gt0_rx_cdrlock_counter_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\n_0_gt0_rx_cdrlock_counter[5]_i_1 ),
        .Q(gt0_rx_cdrlock_counter[5]),
        .R(GTRXRESET));
FDRE #(
    .INIT(1'b0)) 
     \gt0_rx_cdrlock_counter_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[6]),
        .Q(gt0_rx_cdrlock_counter[6]),
        .R(GTRXRESET));
FDRE #(
    .INIT(1'b0)) 
     \gt0_rx_cdrlock_counter_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[7]),
        .Q(gt0_rx_cdrlock_counter[7]),
        .R(GTRXRESET));
FDRE #(
    .INIT(1'b0)) 
     \gt0_rx_cdrlock_counter_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[8]),
        .Q(gt0_rx_cdrlock_counter[8]),
        .R(GTRXRESET));
FDRE #(
    .INIT(1'b0)) 
     \gt0_rx_cdrlock_counter_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(gt0_rx_cdrlock_counter_0[9]),
        .Q(gt0_rx_cdrlock_counter[9]),
        .R(GTRXRESET));
LUT2 #(
    .INIT(4'hE)) 
     gt0_rx_cdrlocked_i_1
       (.I0(gt0_rx_cdrlocked),
        .I1(n_0_gt0_rx_cdrlocked_reg),
        .O(n_0_gt0_rx_cdrlocked_i_1));
FDRE gt0_rx_cdrlocked_reg
       (.C(CLK),
        .CE(1'b1),
        .D(n_0_gt0_rx_cdrlocked_i_1),
        .Q(n_0_gt0_rx_cdrlocked_reg),
        .R(GTRXRESET));
gtp_sata_gtp_sata_RX_STARTUP_FSM__parameterized0 gt0_rxresetfsm_i
       (.CLK(CLK),
        .DONT_RESET_ON_DATA_ERROR_IN(DONT_RESET_ON_DATA_ERROR_IN),
        .GT0_DATA_VALID_IN(GT0_DATA_VALID_IN),
        .GT0_PLL0LOCK_OUT(GT0_PLL0LOCK_OUT),
        .GT0_RX_FSM_RESET_DONE_OUT(GT0_RX_FSM_RESET_DONE_OUT),
        .I1(I1),
        .I2(n_0_gt0_rx_cdrlocked_reg),
        .RXUSERRDY(RXUSERRDY),
        .SOFT_RESET_IN(SOFT_RESET_IN),
        .SR(GTRXRESET),
        .TXUSERRDY(TXUSERRDY),
        .gt0_rxresetdone_out(gt0_rxresetdone_out));
gtp_sata_gtp_sata_TX_STARTUP_FSM__parameterized0 gt0_txresetfsm_i
       (.CLK(CLK),
        .GT0_PLL0LOCK_OUT(GT0_PLL0LOCK_OUT),
        .GT0_TX_FSM_RESET_DONE_OUT(GT0_TX_FSM_RESET_DONE_OUT),
        .GTTXRESET(GTTXRESET),
        .I1(I1),
        .O1(AR),
        .SOFT_RESET_IN(SOFT_RESET_IN),
        .TXUSERRDY(TXUSERRDY),
        .gt0_txresetdone_out(gt0_txresetdone_out));
gtp_sata_gtp_sata_multi_gt__parameterized0 gtp_sata_i
       (.CLK(CLK),
        .GT0_PLL0OUTCLK_OUT(GT0_PLL0OUTCLK_OUT),
        .GT0_PLL0OUTREFCLK_OUT(GT0_PLL0OUTREFCLK_OUT),
        .GT0_PLL1OUTCLK_OUT(GT0_PLL1OUTCLK_OUT),
        .GT0_PLL1OUTREFCLK_OUT(GT0_PLL1OUTREFCLK_OUT),
        .GTTXRESET(GTTXRESET),
        .I1(I1),
        .I2(AR),
        .O1(O1),
        .RXUSERRDY(RXUSERRDY),
        .SR(GTRXRESET),
        .TXUSERRDY(TXUSERRDY),
        .gt0_dmonitorout_out(gt0_dmonitorout_out),
        .gt0_drpaddr_in(gt0_drpaddr_in),
        .gt0_drpdi_in(gt0_drpdi_in),
        .gt0_drpdo_out(gt0_drpdo_out),
        .gt0_drpen_in(gt0_drpen_in),
        .gt0_drpwe_in(gt0_drpwe_in),
        .gt0_eyescandataerror_out(gt0_eyescandataerror_out),
        .gt0_eyescanreset_in(gt0_eyescanreset_in),
        .gt0_eyescantrigger_in(gt0_eyescantrigger_in),
        .gt0_gtprxn_in(gt0_gtprxn_in),
        .gt0_gtprxp_in(gt0_gtprxp_in),
        .gt0_gtptxn_out(gt0_gtptxn_out),
        .gt0_gtptxp_out(gt0_gtptxp_out),
        .gt0_rxbyteisaligned_out(gt0_rxbyteisaligned_out),
        .gt0_rxcharisk_out(gt0_rxcharisk_out),
        .gt0_rxcominitdet_out(gt0_rxcominitdet_out),
        .gt0_rxcomsasdet_out(gt0_rxcomsasdet_out),
        .gt0_rxcomwakedet_out(gt0_rxcomwakedet_out),
        .gt0_rxdata_out(gt0_rxdata_out),
        .gt0_rxdisperr_out(gt0_rxdisperr_out),
        .gt0_rxelecidle_out(gt0_rxelecidle_out),
        .gt0_rxlpmhfhold_in(gt0_rxlpmhfhold_in),
        .gt0_rxlpmhfovrden_in(gt0_rxlpmhfovrden_in),
        .gt0_rxlpmlfhold_in(gt0_rxlpmlfhold_in),
        .gt0_rxlpmreset_in(gt0_rxlpmreset_in),
        .gt0_rxnotintable_out(gt0_rxnotintable_out),
        .gt0_rxresetdone_out(gt0_rxresetdone_out),
        .gt0_rxstatus_out(gt0_rxstatus_out),
        .gt0_txcharisk_in(gt0_txcharisk_in),
        .gt0_txcomfinish_out(gt0_txcomfinish_out),
        .gt0_txcominit_in(gt0_txcominit_in),
        .gt0_txcomsas_in(gt0_txcomsas_in),
        .gt0_txcomwake_in(gt0_txcomwake_in),
        .gt0_txdata_in(gt0_txdata_in),
        .gt0_txelecidle_in(gt0_txelecidle_in),
        .gt0_txoutclk_out(gt0_txoutclk_out),
        .gt0_txoutclkfabric_out(gt0_txoutclkfabric_out),
        .gt0_txoutclkpcs_out(gt0_txoutclkpcs_out),
        .gt0_txresetdone_out(gt0_txresetdone_out));
endmodule

(* ORIG_REF_NAME = "gtp_sata_multi_gt" *) 
module gtp_sata_gtp_sata_multi_gt__parameterized0
   (O1,
    gt0_eyescandataerror_out,
    gt0_gtptxn_out,
    gt0_gtptxp_out,
    gt0_rxbyteisaligned_out,
    gt0_rxcominitdet_out,
    gt0_rxcomsasdet_out,
    gt0_rxcomwakedet_out,
    gt0_rxelecidle_out,
    gt0_rxresetdone_out,
    gt0_txcomfinish_out,
    gt0_txoutclk_out,
    gt0_txoutclkfabric_out,
    gt0_txoutclkpcs_out,
    gt0_txresetdone_out,
    gt0_dmonitorout_out,
    gt0_drpdo_out,
    gt0_rxstatus_out,
    gt0_rxdata_out,
    gt0_rxcharisk_out,
    gt0_rxdisperr_out,
    gt0_rxnotintable_out,
    CLK,
    gt0_eyescanreset_in,
    gt0_eyescantrigger_in,
    gt0_gtprxn_in,
    gt0_gtprxp_in,
    GTTXRESET,
    GT0_PLL0OUTCLK_OUT,
    GT0_PLL0OUTREFCLK_OUT,
    GT0_PLL1OUTCLK_OUT,
    GT0_PLL1OUTREFCLK_OUT,
    gt0_rxlpmhfhold_in,
    gt0_rxlpmhfovrden_in,
    gt0_rxlpmlfhold_in,
    gt0_rxlpmreset_in,
    RXUSERRDY,
    I1,
    gt0_txcominit_in,
    gt0_txcomsas_in,
    gt0_txcomwake_in,
    gt0_txelecidle_in,
    TXUSERRDY,
    gt0_txdata_in,
    gt0_txcharisk_in,
    SR,
    I2,
    gt0_drpen_in,
    gt0_drpdi_in,
    gt0_drpwe_in,
    gt0_drpaddr_in);
  output O1;
  output gt0_eyescandataerror_out;
  output gt0_gtptxn_out;
  output gt0_gtptxp_out;
  output gt0_rxbyteisaligned_out;
  output gt0_rxcominitdet_out;
  output gt0_rxcomsasdet_out;
  output gt0_rxcomwakedet_out;
  output gt0_rxelecidle_out;
  output gt0_rxresetdone_out;
  output gt0_txcomfinish_out;
  output gt0_txoutclk_out;
  output gt0_txoutclkfabric_out;
  output gt0_txoutclkpcs_out;
  output gt0_txresetdone_out;
  output [14:0]gt0_dmonitorout_out;
  output [15:0]gt0_drpdo_out;
  output [2:0]gt0_rxstatus_out;
  output [15:0]gt0_rxdata_out;
  output [1:0]gt0_rxcharisk_out;
  output [1:0]gt0_rxdisperr_out;
  output [1:0]gt0_rxnotintable_out;
  input CLK;
  input gt0_eyescanreset_in;
  input gt0_eyescantrigger_in;
  input gt0_gtprxn_in;
  input gt0_gtprxp_in;
  input GTTXRESET;
  input GT0_PLL0OUTCLK_OUT;
  input GT0_PLL0OUTREFCLK_OUT;
  input GT0_PLL1OUTCLK_OUT;
  input GT0_PLL1OUTREFCLK_OUT;
  input gt0_rxlpmhfhold_in;
  input gt0_rxlpmhfovrden_in;
  input gt0_rxlpmlfhold_in;
  input gt0_rxlpmreset_in;
  input RXUSERRDY;
  input I1;
  input gt0_txcominit_in;
  input gt0_txcomsas_in;
  input gt0_txcomwake_in;
  input gt0_txelecidle_in;
  input TXUSERRDY;
  input [15:0]gt0_txdata_in;
  input [1:0]gt0_txcharisk_in;
  input [0:0]SR;
  input I2;
  input gt0_drpen_in;
  input [15:0]gt0_drpdi_in;
  input gt0_drpwe_in;
  input [8:0]gt0_drpaddr_in;

  wire CLK;
  wire GT0_PLL0OUTCLK_OUT;
  wire GT0_PLL0OUTREFCLK_OUT;
  wire GT0_PLL1OUTCLK_OUT;
  wire GT0_PLL1OUTREFCLK_OUT;
  wire GTTXRESET;
  wire I1;
  wire I2;
  wire O1;
  wire RXUSERRDY;
  wire [0:0]SR;
  wire TXUSERRDY;
  wire [14:0]gt0_dmonitorout_out;
  wire [8:0]gt0_drpaddr_in;
  wire [15:0]gt0_drpdi_in;
  wire [15:0]gt0_drpdo_out;
  wire gt0_drpen_in;
  wire gt0_drpwe_in;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire gt0_gtprxn_in;
  wire gt0_gtprxp_in;
  wire gt0_gtptxn_out;
  wire gt0_gtptxp_out;
  wire gt0_rxbyteisaligned_out;
  wire [1:0]gt0_rxcharisk_out;
  wire gt0_rxcominitdet_out;
  wire gt0_rxcomsasdet_out;
  wire gt0_rxcomwakedet_out;
  wire [15:0]gt0_rxdata_out;
  wire [1:0]gt0_rxdisperr_out;
  wire gt0_rxelecidle_out;
  wire gt0_rxlpmhfhold_in;
  wire gt0_rxlpmhfovrden_in;
  wire gt0_rxlpmlfhold_in;
  wire gt0_rxlpmreset_in;
  wire [1:0]gt0_rxnotintable_out;
  wire gt0_rxresetdone_out;
  wire [2:0]gt0_rxstatus_out;
  wire [1:0]gt0_txcharisk_in;
  wire gt0_txcomfinish_out;
  wire gt0_txcominit_in;
  wire gt0_txcomsas_in;
  wire gt0_txcomwake_in;
  wire [15:0]gt0_txdata_in;
  wire gt0_txelecidle_in;
  wire gt0_txoutclk_out;
  wire gt0_txoutclkfabric_out;
  wire gt0_txoutclkpcs_out;
  wire gt0_txresetdone_out;

gtp_sata_gtp_sata_GT__parameterized0 gt0_gtp_sata_i
       (.CLK(CLK),
        .GT0_PLL0OUTCLK_OUT(GT0_PLL0OUTCLK_OUT),
        .GT0_PLL0OUTREFCLK_OUT(GT0_PLL0OUTREFCLK_OUT),
        .GT0_PLL1OUTCLK_OUT(GT0_PLL1OUTCLK_OUT),
        .GT0_PLL1OUTREFCLK_OUT(GT0_PLL1OUTREFCLK_OUT),
        .GTTXRESET(GTTXRESET),
        .I1(I1),
        .I2(I2),
        .O1(O1),
        .RXUSERRDY(RXUSERRDY),
        .SR(SR),
        .TXUSERRDY(TXUSERRDY),
        .gt0_dmonitorout_out(gt0_dmonitorout_out),
        .gt0_drpaddr_in(gt0_drpaddr_in),
        .gt0_drpdi_in(gt0_drpdi_in),
        .gt0_drpdo_out(gt0_drpdo_out),
        .gt0_drpen_in(gt0_drpen_in),
        .gt0_drpwe_in(gt0_drpwe_in),
        .gt0_eyescandataerror_out(gt0_eyescandataerror_out),
        .gt0_eyescanreset_in(gt0_eyescanreset_in),
        .gt0_eyescantrigger_in(gt0_eyescantrigger_in),
        .gt0_gtprxn_in(gt0_gtprxn_in),
        .gt0_gtprxp_in(gt0_gtprxp_in),
        .gt0_gtptxn_out(gt0_gtptxn_out),
        .gt0_gtptxp_out(gt0_gtptxp_out),
        .gt0_rxbyteisaligned_out(gt0_rxbyteisaligned_out),
        .gt0_rxcharisk_out(gt0_rxcharisk_out),
        .gt0_rxcominitdet_out(gt0_rxcominitdet_out),
        .gt0_rxcomsasdet_out(gt0_rxcomsasdet_out),
        .gt0_rxcomwakedet_out(gt0_rxcomwakedet_out),
        .gt0_rxdata_out(gt0_rxdata_out),
        .gt0_rxdisperr_out(gt0_rxdisperr_out),
        .gt0_rxelecidle_out(gt0_rxelecidle_out),
        .gt0_rxlpmhfhold_in(gt0_rxlpmhfhold_in),
        .gt0_rxlpmhfovrden_in(gt0_rxlpmhfovrden_in),
        .gt0_rxlpmlfhold_in(gt0_rxlpmlfhold_in),
        .gt0_rxlpmreset_in(gt0_rxlpmreset_in),
        .gt0_rxnotintable_out(gt0_rxnotintable_out),
        .gt0_rxresetdone_out(gt0_rxresetdone_out),
        .gt0_rxstatus_out(gt0_rxstatus_out),
        .gt0_txcharisk_in(gt0_txcharisk_in),
        .gt0_txcomfinish_out(gt0_txcomfinish_out),
        .gt0_txcominit_in(gt0_txcominit_in),
        .gt0_txcomsas_in(gt0_txcomsas_in),
        .gt0_txcomwake_in(gt0_txcomwake_in),
        .gt0_txdata_in(gt0_txdata_in),
        .gt0_txelecidle_in(gt0_txelecidle_in),
        .gt0_txoutclk_out(gt0_txoutclk_out),
        .gt0_txoutclkfabric_out(gt0_txoutclkfabric_out),
        .gt0_txoutclkpcs_out(gt0_txoutclkpcs_out),
        .gt0_txresetdone_out(gt0_txresetdone_out));
endmodule

(* ORIG_REF_NAME = "gtp_sata_support" *) 
module gtp_sata_gtp_sata_support__parameterized0
   (DRP_CLK_O,
    GT0_RXUSRCLK2_OUT,
    GT0_PLL0LOCK_OUT,
    GT0_PLL0OUTCLK_OUT,
    GT0_PLL0OUTREFCLK_OUT,
    GT0_PLL0REFCLKLOST_OUT,
    GT0_PLL1OUTCLK_OUT,
    GT0_PLL1OUTREFCLK_OUT,
    GT0_PLL0RESET_OUT,
    gt0_drprdy_out,
    gt0_eyescandataerror_out,
    gt0_gtptxn_out,
    gt0_gtptxp_out,
    gt0_rxbyteisaligned_out,
    gt0_rxcominitdet_out,
    gt0_rxcomsasdet_out,
    gt0_rxcomwakedet_out,
    gt0_rxelecidle_out,
    gt0_rxresetdone_out,
    gt0_txcomfinish_out,
    gt0_txoutclkfabric_out,
    gt0_txoutclkpcs_out,
    gt0_txresetdone_out,
    gt0_dmonitorout_out,
    gt0_drpdo_out,
    gt0_rxstatus_out,
    gt0_rxdata_out,
    gt0_rxcharisk_out,
    gt0_rxdisperr_out,
    gt0_rxnotintable_out,
    GT0_TX_FSM_RESET_DONE_OUT,
    GT0_RX_FSM_RESET_DONE_OUT,
    Q0_CLK1_GTREFCLK_PAD_P_IN,
    Q0_CLK1_GTREFCLK_PAD_N_IN,
    SYSCLK_IN_P,
    SYSCLK_IN_N,
    gt0_eyescanreset_in,
    gt0_eyescantrigger_in,
    gt0_gtprxn_in,
    gt0_gtprxp_in,
    gt0_rxlpmhfhold_in,
    gt0_rxlpmhfovrden_in,
    gt0_rxlpmlfhold_in,
    gt0_rxlpmreset_in,
    gt0_txcominit_in,
    gt0_txcomsas_in,
    gt0_txcomwake_in,
    gt0_txelecidle_in,
    gt0_txdata_in,
    gt0_txcharisk_in,
    SOFT_RESET_IN,
    GT0_DATA_VALID_IN,
    DONT_RESET_ON_DATA_ERROR_IN,
    gt0_drpen_in,
    gt0_drpdi_in,
    gt0_drpwe_in,
    gt0_drpaddr_in);
  output DRP_CLK_O;
  output GT0_RXUSRCLK2_OUT;
  output GT0_PLL0LOCK_OUT;
  output GT0_PLL0OUTCLK_OUT;
  output GT0_PLL0OUTREFCLK_OUT;
  output GT0_PLL0REFCLKLOST_OUT;
  output GT0_PLL1OUTCLK_OUT;
  output GT0_PLL1OUTREFCLK_OUT;
  output GT0_PLL0RESET_OUT;
  output gt0_drprdy_out;
  output gt0_eyescandataerror_out;
  output gt0_gtptxn_out;
  output gt0_gtptxp_out;
  output gt0_rxbyteisaligned_out;
  output gt0_rxcominitdet_out;
  output gt0_rxcomsasdet_out;
  output gt0_rxcomwakedet_out;
  output gt0_rxelecidle_out;
  output gt0_rxresetdone_out;
  output gt0_txcomfinish_out;
  output gt0_txoutclkfabric_out;
  output gt0_txoutclkpcs_out;
  output gt0_txresetdone_out;
  output [14:0]gt0_dmonitorout_out;
  output [15:0]gt0_drpdo_out;
  output [2:0]gt0_rxstatus_out;
  output [15:0]gt0_rxdata_out;
  output [1:0]gt0_rxcharisk_out;
  output [1:0]gt0_rxdisperr_out;
  output [1:0]gt0_rxnotintable_out;
  output GT0_TX_FSM_RESET_DONE_OUT;
  output GT0_RX_FSM_RESET_DONE_OUT;
  input Q0_CLK1_GTREFCLK_PAD_P_IN;
  input Q0_CLK1_GTREFCLK_PAD_N_IN;
  input SYSCLK_IN_P;
  input SYSCLK_IN_N;
  input gt0_eyescanreset_in;
  input gt0_eyescantrigger_in;
  input gt0_gtprxn_in;
  input gt0_gtprxp_in;
  input gt0_rxlpmhfhold_in;
  input gt0_rxlpmhfovrden_in;
  input gt0_rxlpmlfhold_in;
  input gt0_rxlpmreset_in;
  input gt0_txcominit_in;
  input gt0_txcomsas_in;
  input gt0_txcomwake_in;
  input gt0_txelecidle_in;
  input [15:0]gt0_txdata_in;
  input [1:0]gt0_txcharisk_in;
  input SOFT_RESET_IN;
  input GT0_DATA_VALID_IN;
  input DONT_RESET_ON_DATA_ERROR_IN;
  input gt0_drpen_in;
  input [15:0]gt0_drpdi_in;
  input gt0_drpwe_in;
  input [8:0]gt0_drpaddr_in;

  wire DONT_RESET_ON_DATA_ERROR_IN;
  wire DRP_CLK_O;
  wire GT0_DATA_VALID_IN;
  wire GT0_PLL0LOCK_OUT;
  wire GT0_PLL0OUTCLK_OUT;
  wire GT0_PLL0OUTREFCLK_OUT;
  wire GT0_PLL0REFCLKLOST_OUT;
  wire GT0_PLL0RESET_OUT;
  wire GT0_PLL1OUTCLK_OUT;
  wire GT0_PLL1OUTREFCLK_OUT;
  wire GT0_RXUSRCLK2_OUT;
  wire GT0_RX_FSM_RESET_DONE_OUT;
  wire GT0_TX_FSM_RESET_DONE_OUT;
  wire GTREFCLK0_IN;
  wire Q0_CLK1_GTREFCLK_PAD_N_IN;
  wire Q0_CLK1_GTREFCLK_PAD_P_IN;
  wire SOFT_RESET_IN;
(* DIFF_TERM=0 *) (* IBUF_LOW_PWR *)   wire SYSCLK_IN_N;
(* DIFF_TERM=0 *) (* IBUF_LOW_PWR *)   wire SYSCLK_IN_P;
  wire [14:0]gt0_dmonitorout_out;
  wire [8:0]gt0_drpaddr_in;
  wire [15:0]gt0_drpdi_in;
  wire [15:0]gt0_drpdo_out;
  wire gt0_drpen_in;
  wire gt0_drprdy_out;
  wire gt0_drpwe_in;
  wire gt0_eyescandataerror_out;
  wire gt0_eyescanreset_in;
  wire gt0_eyescantrigger_in;
  wire gt0_gtprxn_in;
  wire gt0_gtprxp_in;
  wire gt0_gtptxn_out;
  wire gt0_gtptxp_out;
  wire gt0_rxbyteisaligned_out;
  wire [1:0]gt0_rxcharisk_out;
  wire gt0_rxcominitdet_out;
  wire gt0_rxcomsasdet_out;
  wire gt0_rxcomwakedet_out;
  wire [15:0]gt0_rxdata_out;
  wire [1:0]gt0_rxdisperr_out;
  wire gt0_rxelecidle_out;
  wire gt0_rxlpmhfhold_in;
  wire gt0_rxlpmhfovrden_in;
  wire gt0_rxlpmlfhold_in;
  wire gt0_rxlpmreset_in;
  wire [1:0]gt0_rxnotintable_out;
  wire gt0_rxresetdone_out;
  wire [2:0]gt0_rxstatus_out;
  wire [1:0]gt0_txcharisk_in;
  wire gt0_txcomfinish_out;
  wire gt0_txcominit_in;
  wire gt0_txcomsas_in;
  wire gt0_txcomwake_in;
  wire [15:0]gt0_txdata_in;
  wire gt0_txelecidle_in;
  wire gt0_txoutclk_out;
  wire gt0_txoutclkfabric_out;
  wire gt0_txoutclkpcs_out;
  wire gt0_txresetdone_out;
  wire n_71_gtp_sata_init_i;

gtp_sata_gtp_sata_common__parameterized0 common0_i
       (.DRP_CLK_O(DRP_CLK_O),
        .GT0_PLL0LOCK_OUT(GT0_PLL0LOCK_OUT),
        .GT0_PLL0OUTCLK_OUT(GT0_PLL0OUTCLK_OUT),
        .GT0_PLL0OUTREFCLK_OUT(GT0_PLL0OUTREFCLK_OUT),
        .GT0_PLL0REFCLKLOST_OUT(GT0_PLL0REFCLKLOST_OUT),
        .GT0_PLL0RESET_OUT(GT0_PLL0RESET_OUT),
        .GT0_PLL1OUTCLK_OUT(GT0_PLL1OUTCLK_OUT),
        .GT0_PLL1OUTREFCLK_OUT(GT0_PLL1OUTREFCLK_OUT),
        .Q0_CLK1_GTREFCLK_OUT(GTREFCLK0_IN));
gtp_sata_gtp_sata_common_reset__parameterized0 common_reset_i
       (.AR(n_71_gtp_sata_init_i),
        .DRPCLK_OUT(DRP_CLK_O),
        .GT0_PLL0RESET_OUT(GT0_PLL0RESET_OUT),
        .SOFT_RESET_IN(SOFT_RESET_IN));
gtp_sata_gtp_sata_GT_USRCLK_SOURCE gt_usrclk_source
       (.DRPCLK_OUT(DRP_CLK_O),
        .GT0_TXOUTCLK_IN(gt0_txoutclk_out),
        .GT0_TXUSRCLK_OUT(GT0_RXUSRCLK2_OUT),
        .Q0_CLK1_GTREFCLK_OUT(GTREFCLK0_IN),
        .Q0_CLK1_GTREFCLK_PAD_N_IN(Q0_CLK1_GTREFCLK_PAD_N_IN),
        .Q0_CLK1_GTREFCLK_PAD_P_IN(Q0_CLK1_GTREFCLK_PAD_P_IN),
        .SYSCLK_IN_N(SYSCLK_IN_N),
        .SYSCLK_IN_P(SYSCLK_IN_P));
gtp_sata_gtp_sata_init gtp_sata_init_i
       (.AR(n_71_gtp_sata_init_i),
        .CLK(DRP_CLK_O),
        .DONT_RESET_ON_DATA_ERROR_IN(DONT_RESET_ON_DATA_ERROR_IN),
        .GT0_DATA_VALID_IN(GT0_DATA_VALID_IN),
        .GT0_PLL0LOCK_OUT(GT0_PLL0LOCK_OUT),
        .GT0_PLL0OUTCLK_OUT(GT0_PLL0OUTCLK_OUT),
        .GT0_PLL0OUTREFCLK_OUT(GT0_PLL0OUTREFCLK_OUT),
        .GT0_PLL1OUTCLK_OUT(GT0_PLL1OUTCLK_OUT),
        .GT0_PLL1OUTREFCLK_OUT(GT0_PLL1OUTREFCLK_OUT),
        .GT0_RX_FSM_RESET_DONE_OUT(GT0_RX_FSM_RESET_DONE_OUT),
        .GT0_TX_FSM_RESET_DONE_OUT(GT0_TX_FSM_RESET_DONE_OUT),
        .I1(GT0_RXUSRCLK2_OUT),
        .O1(gt0_drprdy_out),
        .SOFT_RESET_IN(SOFT_RESET_IN),
        .gt0_dmonitorout_out(gt0_dmonitorout_out),
        .gt0_drpaddr_in(gt0_drpaddr_in),
        .gt0_drpdi_in(gt0_drpdi_in),
        .gt0_drpdo_out(gt0_drpdo_out),
        .gt0_drpen_in(gt0_drpen_in),
        .gt0_drpwe_in(gt0_drpwe_in),
        .gt0_eyescandataerror_out(gt0_eyescandataerror_out),
        .gt0_eyescanreset_in(gt0_eyescanreset_in),
        .gt0_eyescantrigger_in(gt0_eyescantrigger_in),
        .gt0_gtprxn_in(gt0_gtprxn_in),
        .gt0_gtprxp_in(gt0_gtprxp_in),
        .gt0_gtptxn_out(gt0_gtptxn_out),
        .gt0_gtptxp_out(gt0_gtptxp_out),
        .gt0_rxbyteisaligned_out(gt0_rxbyteisaligned_out),
        .gt0_rxcharisk_out(gt0_rxcharisk_out),
        .gt0_rxcominitdet_out(gt0_rxcominitdet_out),
        .gt0_rxcomsasdet_out(gt0_rxcomsasdet_out),
        .gt0_rxcomwakedet_out(gt0_rxcomwakedet_out),
        .gt0_rxdata_out(gt0_rxdata_out),
        .gt0_rxdisperr_out(gt0_rxdisperr_out),
        .gt0_rxelecidle_out(gt0_rxelecidle_out),
        .gt0_rxlpmhfhold_in(gt0_rxlpmhfhold_in),
        .gt0_rxlpmhfovrden_in(gt0_rxlpmhfovrden_in),
        .gt0_rxlpmlfhold_in(gt0_rxlpmlfhold_in),
        .gt0_rxlpmreset_in(gt0_rxlpmreset_in),
        .gt0_rxnotintable_out(gt0_rxnotintable_out),
        .gt0_rxresetdone_out(gt0_rxresetdone_out),
        .gt0_rxstatus_out(gt0_rxstatus_out),
        .gt0_txcharisk_in(gt0_txcharisk_in),
        .gt0_txcomfinish_out(gt0_txcomfinish_out),
        .gt0_txcominit_in(gt0_txcominit_in),
        .gt0_txcomsas_in(gt0_txcomsas_in),
        .gt0_txcomwake_in(gt0_txcomwake_in),
        .gt0_txdata_in(gt0_txdata_in),
        .gt0_txelecidle_in(gt0_txelecidle_in),
        .gt0_txoutclk_out(gt0_txoutclk_out),
        .gt0_txoutclkfabric_out(gt0_txoutclkfabric_out),
        .gt0_txoutclkpcs_out(gt0_txoutclkpcs_out),
        .gt0_txresetdone_out(gt0_txresetdone_out));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0
   (D,
    data_out,
    I1,
    rxpmaresetdone_sss,
    Q,
    gtrxreset_ss,
    data_in,
    CLK);
  output [1:0]D;
  output data_out;
  input I1;
  input rxpmaresetdone_sss;
  input [2:0]Q;
  input gtrxreset_ss;
  input data_in;
  input CLK;

  wire CLK;
  wire [1:0]D;
  wire I1;
  wire [2:0]Q;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire gtrxreset_ss;
  wire \n_0_state[0]_i_2 ;
  wire rxpmaresetdone_sss;

(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
LUT6 #(
    .INIT(64'h33F23332F0C2F002)) 
     \state[0]_i_1 
       (.I0(gtrxreset_ss),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(\n_0_state[0]_i_2 ),
        .I5(I1),
        .O(D[0]));
LUT2 #(
    .INIT(4'hB)) 
     \state[0]_i_2 
       (.I0(data_out),
        .I1(rxpmaresetdone_sss),
        .O(\n_0_state[0]_i_2 ));
LUT6 #(
    .INIT(64'h553000FFFF00FF00)) 
     \state[1]_i_1 
       (.I0(I1),
        .I1(data_out),
        .I2(rxpmaresetdone_sss),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[0]),
        .O(D[1]));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0_0
   (O1,
    E,
    reset_time_out,
    out,
    I1,
    wait_time_done,
    I2,
    mmcm_lock_reclocked,
    I3,
    I4,
    txresetdone_s3,
    I5,
    I6,
    GT0_PLL0LOCK_OUT,
    CLK);
  output O1;
  output [0:0]E;
  input reset_time_out;
  input [3:0]out;
  input I1;
  input wait_time_done;
  input I2;
  input mmcm_lock_reclocked;
  input I3;
  input I4;
  input txresetdone_s3;
  input I5;
  input I6;
  input GT0_PLL0LOCK_OUT;
  input CLK;

  wire CLK;
  wire [0:0]E;
  wire GT0_PLL0LOCK_OUT;
  wire I1;
  wire I2;
  wire I3;
  wire I4;
  wire I5;
  wire I6;
  wire O1;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire mmcm_lock_reclocked;
  wire \n_0_FSM_sequential_tx_state[3]_i_7 ;
  wire \n_0_FSM_sequential_tx_state[3]_i_8 ;
  wire \n_0_FSM_sequential_tx_state_reg[3]_i_3 ;
  wire n_0_reset_time_out_i_3;
  wire n_0_reset_time_out_i_4;
  wire [3:0]out;
  wire pll0lock_sync;
  wire reset_time_out;
  wire reset_time_out_0;
  wire txresetdone_s3;
  wire wait_time_done;

LUT6 #(
    .INIT(64'h0033B8BB0033B888)) 
     \FSM_sequential_tx_state[3]_i_1 
       (.I0(\n_0_FSM_sequential_tx_state_reg[3]_i_3 ),
        .I1(out[0]),
        .I2(wait_time_done),
        .I3(I2),
        .I4(out[3]),
        .I5(I1),
        .O(E));
LUT6 #(
    .INIT(64'hBA00BA00BAFFBA00)) 
     \FSM_sequential_tx_state[3]_i_7 
       (.I0(mmcm_lock_reclocked),
        .I1(reset_time_out),
        .I2(I3),
        .I3(out[2]),
        .I4(I4),
        .I5(pll0lock_sync),
        .O(\n_0_FSM_sequential_tx_state[3]_i_7 ));
LUT6 #(
    .INIT(64'hBAFFBAFFBAFFBA00)) 
     \FSM_sequential_tx_state[3]_i_8 
       (.I0(txresetdone_s3),
        .I1(reset_time_out),
        .I2(I5),
        .I3(out[2]),
        .I4(I6),
        .I5(pll0lock_sync),
        .O(\n_0_FSM_sequential_tx_state[3]_i_8 ));
MUXF7 \FSM_sequential_tx_state_reg[3]_i_3 
       (.I0(\n_0_FSM_sequential_tx_state[3]_i_7 ),
        .I1(\n_0_FSM_sequential_tx_state[3]_i_8 ),
        .O(\n_0_FSM_sequential_tx_state_reg[3]_i_3 ),
        .S(out[1]));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(GT0_PLL0LOCK_OUT),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(pll0lock_sync),
        .R(1'b0));
LUT3 #(
    .INIT(8'hB8)) 
     reset_time_out_i_1
       (.I0(reset_time_out_0),
        .I1(n_0_reset_time_out_i_3),
        .I2(reset_time_out),
        .O(O1));
LUT6 #(
    .INIT(64'h1F0F1F0F100F1000)) 
     reset_time_out_i_2
       (.I0(out[2]),
        .I1(out[1]),
        .I2(out[3]),
        .I3(out[0]),
        .I4(I1),
        .I5(n_0_reset_time_out_i_4),
        .O(reset_time_out_0));
LUT6 #(
    .INIT(64'h303030302020FFFC)) 
     reset_time_out_i_3
       (.I0(pll0lock_sync),
        .I1(out[3]),
        .I2(out[0]),
        .I3(I1),
        .I4(out[1]),
        .I5(out[2]),
        .O(n_0_reset_time_out_i_3));
LUT5 #(
    .INIT(32'hAFC0A0C0)) 
     reset_time_out_i_4
       (.I0(txresetdone_s3),
        .I1(pll0lock_sync),
        .I2(out[1]),
        .I3(out[2]),
        .I4(mmcm_lock_reclocked),
        .O(n_0_reset_time_out_i_4));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0_1
   (data_out,
    gt0_txresetdone_out,
    CLK);
  output data_out;
  input gt0_txresetdone_out;
  input CLK;

  wire CLK;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire gt0_txresetdone_out;

(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(gt0_txresetdone_out),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0_10
   (data_out,
    data_in,
    I1);
  output data_out;
  input data_in;
  input I1;

  wire I1;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(I1),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(I1),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(I1),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(I1),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(I1),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(I1),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0_11
   (data_out,
    GT0_RX_FSM_RESET_DONE_OUT,
    I1);
  output data_out;
  input GT0_RX_FSM_RESET_DONE_OUT;
  input I1;

  wire GT0_RX_FSM_RESET_DONE_OUT;
  wire I1;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(I1),
        .CE(1'b1),
        .D(GT0_RX_FSM_RESET_DONE_OUT),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(I1),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(I1),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(I1),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(I1),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(I1),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0_12
   (data_out,
    data_in,
    CLK);
  output data_out;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0_2
   (SR,
    O1,
    mmcm_lock_reclocked,
    Q,
    I1,
    CLK);
  output [0:0]SR;
  output O1;
  input mmcm_lock_reclocked;
  input [2:0]Q;
  input I1;
  input CLK;

  wire CLK;
  wire I1;
  wire O1;
  wire [2:0]Q;
  wire [0:0]SR;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;

(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(1'b1),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(mmcm_lock_i),
        .R(1'b0));
LUT1 #(
    .INIT(2'h1)) 
     \mmcm_lock_count[9]_i_1 
       (.I0(mmcm_lock_i),
        .O(SR));
LUT6 #(
    .INIT(64'hEAAAAAAA00000000)) 
     mmcm_lock_reclocked_i_1
       (.I0(mmcm_lock_reclocked),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(I1),
        .I5(mmcm_lock_i),
        .O(O1));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0_3
   (data_out,
    data_in,
    I1);
  output data_out;
  input data_in;
  input I1;

  wire I1;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(I1),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(I1),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(I1),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(I1),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(I1),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(I1),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0_4
   (data_out,
    data_in,
    CLK);
  output data_out;
  input data_in;
  input CLK;

  wire CLK;
  wire data_in;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(data_in),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0_5
   (data_out,
    GT0_TX_FSM_RESET_DONE_OUT,
    I1);
  output data_out;
  input GT0_TX_FSM_RESET_DONE_OUT;
  input I1;

  wire GT0_TX_FSM_RESET_DONE_OUT;
  wire I1;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;

(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(I1),
        .CE(1'b1),
        .D(GT0_TX_FSM_RESET_DONE_OUT),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(I1),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(I1),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(I1),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(I1),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(I1),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0_6
   (O1,
    O2,
    I1,
    out,
    I2,
    I3,
    I4,
    I5,
    I6,
    I7,
    data_out,
    GT0_PLL0LOCK_OUT,
    CLK);
  output O1;
  output O2;
  input I1;
  input [3:0]out;
  input I2;
  input I3;
  input I4;
  input I5;
  input I6;
  input I7;
  input data_out;
  input GT0_PLL0LOCK_OUT;
  input CLK;

  wire CLK;
  wire GT0_PLL0LOCK_OUT;
  wire I1;
  wire I2;
  wire I3;
  wire I4;
  wire I5;
  wire I6;
  wire I7;
  wire O1;
  wire O2;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire n_0_reset_time_out_i_3__0;
  wire [3:0]out;
  wire pll0lock_sync;

LUT6 #(
    .INIT(64'h000000F1F1F100F1)) 
     \FSM_sequential_rx_state[3]_i_3 
       (.I0(pll0lock_sync),
        .I1(I5),
        .I2(out[2]),
        .I3(I6),
        .I4(out[1]),
        .I5(I7),
        .O(O2));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(GT0_PLL0LOCK_OUT),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(pll0lock_sync),
        .R(1'b0));
LUT6 #(
    .INIT(64'hFEAEFFFFFEAE0000)) 
     reset_time_out_i_1__0
       (.I0(I1),
        .I1(n_0_reset_time_out_i_3__0),
        .I2(out[2]),
        .I3(I2),
        .I4(I3),
        .I5(I4),
        .O(O1));
LUT5 #(
    .INIT(32'h4F4F45FF)) 
     reset_time_out_i_3__0
       (.I0(out[3]),
        .I1(pll0lock_sync),
        .I2(out[1]),
        .I3(out[0]),
        .I4(data_out),
        .O(n_0_reset_time_out_i_3__0));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0_7
   (data_out,
    gt0_rxresetdone_out,
    CLK);
  output data_out;
  input gt0_rxresetdone_out;
  input CLK;

  wire CLK;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire gt0_rxresetdone_out;

(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(gt0_rxresetdone_out),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0_8
   (data_out,
    O1,
    D,
    E,
    DONT_RESET_ON_DATA_ERROR_IN,
    I1,
    I2,
    I3,
    I4,
    GT0_RX_FSM_RESET_DONE_OUT,
    I5,
    out,
    I6,
    I7,
    I8,
    I9,
    I10,
    I11,
    I12,
    rx_state16_out,
    time_out_wait_bypass_s3,
    GT0_DATA_VALID_IN,
    CLK);
  output data_out;
  output O1;
  output [2:0]D;
  output [0:0]E;
  input DONT_RESET_ON_DATA_ERROR_IN;
  input I1;
  input I2;
  input I3;
  input I4;
  input GT0_RX_FSM_RESET_DONE_OUT;
  input I5;
  input [3:0]out;
  input I6;
  input I7;
  input I8;
  input I9;
  input I10;
  input I11;
  input I12;
  input rx_state16_out;
  input time_out_wait_bypass_s3;
  input GT0_DATA_VALID_IN;
  input CLK;

  wire CLK;
  wire [2:0]D;
  wire DONT_RESET_ON_DATA_ERROR_IN;
  wire [0:0]E;
  wire GT0_DATA_VALID_IN;
  wire GT0_RX_FSM_RESET_DONE_OUT;
  wire I1;
  wire I10;
  wire I11;
  wire I12;
  wire I2;
  wire I3;
  wire I4;
  wire I5;
  wire I6;
  wire I7;
  wire I8;
  wire I9;
  wire O1;
  wire data_out;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire \n_0_FSM_sequential_rx_state[0]_i_3 ;
  wire \n_0_FSM_sequential_rx_state[3]_i_11 ;
  wire \n_0_FSM_sequential_rx_state[3]_i_4 ;
  wire \n_0_FSM_sequential_rx_state[3]_i_5 ;
  wire \n_0_FSM_sequential_rx_state[3]_i_8 ;
  wire n_0_rx_fsm_reset_done_int_i_3;
  wire [3:0]out;
  wire rx_state1;
  wire rx_state16_out;
  wire time_out_wait_bypass_s3;

LUT6 #(
    .INIT(64'hFFFFFFFF00005540)) 
     \FSM_sequential_rx_state[0]_i_1 
       (.I0(I5),
        .I1(out[1]),
        .I2(I6),
        .I3(out[2]),
        .I4(out[3]),
        .I5(\n_0_FSM_sequential_rx_state[0]_i_3 ),
        .O(D[0]));
LUT5 #(
    .INIT(32'h07050505)) 
     \FSM_sequential_rx_state[0]_i_3 
       (.I0(out[0]),
        .I1(out[1]),
        .I2(out[2]),
        .I3(out[3]),
        .I4(rx_state1),
        .O(\n_0_FSM_sequential_rx_state[0]_i_3 ));
LUT6 #(
    .INIT(64'h00005555151F0000)) 
     \FSM_sequential_rx_state[1]_i_1 
       (.I0(out[3]),
        .I1(rx_state16_out),
        .I2(out[2]),
        .I3(rx_state1),
        .I4(out[0]),
        .I5(out[1]),
        .O(D[1]));
(* SOFT_HLUTNM = "soft_lutpair5" *) 
   LUT4 #(
    .INIT(16'h0004)) 
     \FSM_sequential_rx_state[1]_i_2 
       (.I0(I2),
        .I1(I1),
        .I2(DONT_RESET_ON_DATA_ERROR_IN),
        .I3(data_out),
        .O(rx_state1));
LUT6 #(
    .INIT(64'hFFF0FFF4FFF0FFFF)) 
     \FSM_sequential_rx_state[3]_i_1 
       (.I0(I7),
        .I1(out[0]),
        .I2(\n_0_FSM_sequential_rx_state[3]_i_4 ),
        .I3(\n_0_FSM_sequential_rx_state[3]_i_5 ),
        .I4(out[3]),
        .I5(I8),
        .O(E));
(* SOFT_HLUTNM = "soft_lutpair5" *) 
   LUT4 #(
    .INIT(16'hAABA)) 
     \FSM_sequential_rx_state[3]_i_11 
       (.I0(data_out),
        .I1(DONT_RESET_ON_DATA_ERROR_IN),
        .I2(I1),
        .I3(I2),
        .O(\n_0_FSM_sequential_rx_state[3]_i_11 ));
LUT5 #(
    .INIT(32'h4488448A)) 
     \FSM_sequential_rx_state[3]_i_2 
       (.I0(out[3]),
        .I1(I12),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\n_0_FSM_sequential_rx_state[3]_i_8 ),
        .O(D[2]));
LUT5 #(
    .INIT(32'h00000700)) 
     \FSM_sequential_rx_state[3]_i_4 
       (.I0(out[1]),
        .I1(data_out),
        .I2(out[0]),
        .I3(out[3]),
        .I4(out[2]),
        .O(\n_0_FSM_sequential_rx_state[3]_i_4 ));
LUT6 #(
    .INIT(64'h00000A0A000000C0)) 
     \FSM_sequential_rx_state[3]_i_5 
       (.I0(\n_0_FSM_sequential_rx_state[3]_i_11 ),
        .I1(I9),
        .I2(out[2]),
        .I3(out[3]),
        .I4(out[1]),
        .I5(out[0]),
        .O(\n_0_FSM_sequential_rx_state[3]_i_5 ));
LUT6 #(
    .INIT(64'h0004FFFF00040000)) 
     \FSM_sequential_rx_state[3]_i_8 
       (.I0(I2),
        .I1(I1),
        .I2(DONT_RESET_ON_DATA_ERROR_IN),
        .I3(data_out),
        .I4(out[0]),
        .I5(time_out_wait_bypass_s3),
        .O(\n_0_FSM_sequential_rx_state[3]_i_8 ));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(GT0_DATA_VALID_IN),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(data_out),
        .R(1'b0));
LUT6 #(
    .INIT(64'h0800FFFF08000000)) 
     rx_fsm_reset_done_int_i_1
       (.I0(data_out),
        .I1(I3),
        .I2(I2),
        .I3(I4),
        .I4(n_0_rx_fsm_reset_done_int_i_3),
        .I5(GT0_RX_FSM_RESET_DONE_OUT),
        .O(O1));
LUT6 #(
    .INIT(64'h0000000000FADD00)) 
     rx_fsm_reset_done_int_i_3
       (.I0(data_out),
        .I1(I10),
        .I2(rx_state1),
        .I3(out[1]),
        .I4(out[0]),
        .I5(I11),
        .O(n_0_rx_fsm_reset_done_int_i_3));
endmodule

(* ORIG_REF_NAME = "gtp_sata_sync_block" *) 
module gtp_sata_gtp_sata_sync_block__parameterized0_9
   (SR,
    O1,
    mmcm_lock_reclocked,
    I1,
    CLK);
  output [0:0]SR;
  output O1;
  input mmcm_lock_reclocked;
  input I1;
  input CLK;

  wire CLK;
  wire I1;
  wire O1;
  wire [0:0]SR;
  wire data_sync1;
  wire data_sync2;
  wire data_sync3;
  wire data_sync4;
  wire data_sync5;
  wire mmcm_lock_i;
  wire mmcm_lock_reclocked;

(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg1
       (.C(CLK),
        .CE(1'b1),
        .D(1'b1),
        .Q(data_sync1),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg2
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync1),
        .Q(data_sync2),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg3
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync2),
        .Q(data_sync3),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg4
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync3),
        .Q(data_sync4),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg5
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync4),
        .Q(data_sync5),
        .R(1'b0));
(* ASYNC_REG *) 
   (* SHREG_EXTRACT = "no" *) 
   (* XILINX_LEGACY_PRIM = "FD" *) 
   (* box_type = "PRIMITIVE" *) 
   FDRE #(
    .INIT(1'b0)) 
     data_sync_reg6
       (.C(CLK),
        .CE(1'b1),
        .D(data_sync5),
        .Q(mmcm_lock_i),
        .R(1'b0));
(* SOFT_HLUTNM = "soft_lutpair6" *) 
   LUT1 #(
    .INIT(2'h1)) 
     \mmcm_lock_count[9]_i_1__0 
       (.I0(mmcm_lock_i),
        .O(SR));
(* SOFT_HLUTNM = "soft_lutpair6" *) 
   LUT3 #(
    .INIT(8'hE0)) 
     mmcm_lock_reclocked_i_1__0
       (.I0(mmcm_lock_reclocked),
        .I1(I1),
        .I2(mmcm_lock_i),
        .O(O1));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
