proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000

start_step init_design
set rc [catch {
  create_msg_db init_design.pb
  set_param gui.test TreeTableDev
  debug::add_scope template.lib 1
  set_property design_mode GateLvl [current_fileset]
  set_property webtalk.parent_dir C:/Users/wolff/Desktop/bachelorthesis/XillybusAXI/edit_Xillybus_AXI_Master_v1_0.cache/wt [current_project]
  set_property parent.project_path C:/Users/wolff/Desktop/bachelorthesis/XillybusAXI/edit_Xillybus_AXI_Master_v1_0.xpr [current_project]
  set_property ip_repo_paths {
  c:/Users/wolff/Desktop/bachelorthesis/XillybusAXI/edit_Xillybus_AXI_Master_v1_0.cache/ip
  C:/Users/wolff/Desktop/bachelorthesis/XillybusAXI/Xillybus_AXI_Master_1.0
} [current_project]
  set_property ip_output_repo c:/Users/wolff/Desktop/bachelorthesis/XillybusAXI/edit_Xillybus_AXI_Master_v1_0.cache/ip [current_project]
  add_files -quiet C:/Users/wolff/Desktop/bachelorthesis/XillybusAXI/edit_Xillybus_AXI_Master_v1_0.runs/synth_1/Xillybus_AXI_Master_v1_0.dcp
  add_files -quiet C:/Users/wolff/Desktop/bachelorthesis/XillybusAXI/edit_Xillybus_AXI_Master_v1_0.runs/fifo_8x2048_synth_1/fifo_8x2048.dcp
  set_property netlist_only true [get_files C:/Users/wolff/Desktop/bachelorthesis/XillybusAXI/edit_Xillybus_AXI_Master_v1_0.runs/fifo_8x2048_synth_1/fifo_8x2048.dcp]
  add_files -quiet C:/Users/wolff/Desktop/bachelorthesis/XillybusAXI/edit_Xillybus_AXI_Master_v1_0.runs/fifo_32x512_synth_1/fifo_32x512.dcp
  set_property netlist_only true [get_files C:/Users/wolff/Desktop/bachelorthesis/XillybusAXI/edit_Xillybus_AXI_Master_v1_0.runs/fifo_32x512_synth_1/fifo_32x512.dcp]
  read_xdc -mode out_of_context -ref fifo_8x2048 -cells U0 c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vivado-essentials/fifo_8x2048/fifo_8x2048_ooc.xdc
  set_property processing_order EARLY [get_files c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vivado-essentials/fifo_8x2048/fifo_8x2048_ooc.xdc]
  read_xdc -ref fifo_8x2048 -cells U0 c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vivado-essentials/fifo_8x2048/fifo_8x2048/fifo_8x2048.xdc
  set_property processing_order EARLY [get_files c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vivado-essentials/fifo_8x2048/fifo_8x2048/fifo_8x2048.xdc]
  read_xdc -mode out_of_context -ref fifo_32x512 -cells U0 c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vivado-essentials/fifo_32x512/fifo_32x512_ooc.xdc
  set_property processing_order EARLY [get_files c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vivado-essentials/fifo_32x512/fifo_32x512_ooc.xdc]
  read_xdc -ref fifo_32x512 -cells U0 c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vivado-essentials/fifo_32x512/fifo_32x512/fifo_32x512.xdc
  set_property processing_order EARLY [get_files c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vivado-essentials/fifo_32x512/fifo_32x512/fifo_32x512.xdc]
  link_design -top Xillybus_AXI_Master_v1_0 -part xc7a200tfbg676-2
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
}

start_step opt_design
set rc [catch {
  create_msg_db opt_design.pb
  catch {write_debug_probes -quiet -force debug_nets}
  opt_design 
  write_checkpoint -force Xillybus_AXI_Master_v1_0_opt.dcp
  catch {report_drc -file Xillybus_AXI_Master_v1_0_drc_opted.rpt}
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
}

start_step place_design
set rc [catch {
  create_msg_db place_design.pb
  place_design 
  write_checkpoint -force Xillybus_AXI_Master_v1_0_placed.dcp
  catch { report_io -file Xillybus_AXI_Master_v1_0_io_placed.rpt }
  catch { report_clock_utilization -file Xillybus_AXI_Master_v1_0_clock_utilization_placed.rpt }
  catch { report_utilization -file Xillybus_AXI_Master_v1_0_utilization_placed.rpt -pb Xillybus_AXI_Master_v1_0_utilization_placed.pb }
  catch { report_control_sets -verbose -file Xillybus_AXI_Master_v1_0_control_sets_placed.rpt }
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
}

start_step route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force Xillybus_AXI_Master_v1_0_routed.dcp
  catch { report_drc -file Xillybus_AXI_Master_v1_0_drc_routed.rpt -pb Xillybus_AXI_Master_v1_0_drc_routed.pb }
  catch { report_timing_summary -warn_on_violation -max_paths 10 -file Xillybus_AXI_Master_v1_0_timing_summary_routed.rpt -rpx Xillybus_AXI_Master_v1_0_timing_summary_routed.rpx }
  catch { report_power -file Xillybus_AXI_Master_v1_0_power_routed.rpt -pb Xillybus_AXI_Master_v1_0_power_summary_routed.pb }
  catch { report_route_status -file Xillybus_AXI_Master_v1_0_route_status.rpt -pb Xillybus_AXI_Master_v1_0_route_status.pb }
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
}

