library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity Xillybus_AXI_Master_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Master Bus Interface SATA_AXI
		C_SATA_AXI_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"40000000";
		C_SATA_AXI_BURST_LEN	: integer	:= 16;
		C_SATA_AXI_ID_WIDTH	: integer	:= 1;
		C_SATA_AXI_ADDR_WIDTH	: integer	:= 64;
		C_SATA_AXI_DATA_WIDTH	: integer	:= 512;
		C_SATA_AXI_AWUSER_WIDTH	: integer	:= 0;
		C_SATA_AXI_ARUSER_WIDTH	: integer	:= 0;
		C_SATA_AXI_WUSER_WIDTH	: integer	:= 0;
		C_SATA_AXI_RUSER_WIDTH	: integer	:= 0;
		C_SATA_AXI_BUSER_WIDTH	: integer	:= 0;

		-- Parameters of Axi Master Bus Interface BRAM_AXI
		C_BRAM_AXI_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"40000000";
		C_BRAM_AXI_BURST_LEN	: integer	:= 16;
		C_BRAM_AXI_ID_WIDTH	: integer	:= 1;
		C_BRAM_AXI_ADDR_WIDTH	: integer	:= 32;
		C_BRAM_AXI_DATA_WIDTH	: integer	:= 32;
		C_BRAM_AXI_AWUSER_WIDTH	: integer	:= 0;
		C_BRAM_AXI_ARUSER_WIDTH	: integer	:= 0;
		C_BRAM_AXI_WUSER_WIDTH	: integer	:= 0;
		C_BRAM_AXI_RUSER_WIDTH	: integer	:= 0;
		C_BRAM_AXI_BUSER_WIDTH	: integer	:= 0;

		-- Parameters of Axi Master Bus Interface DRAM_AXI
		C_DRAM_AXI_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"40000000";
		C_DRAM_AXI_BURST_LEN	: integer	:= 16;
		C_DRAM_AXI_ID_WIDTH	: integer	:= 1;
		C_DRAM_AXI_ADDR_WIDTH	: integer	:= 32;
		C_DRAM_AXI_DATA_WIDTH	: integer	:= 32;
		C_DRAM_AXI_AWUSER_WIDTH	: integer	:= 0;
		C_DRAM_AXI_ARUSER_WIDTH	: integer	:= 0;
		C_DRAM_AXI_WUSER_WIDTH	: integer	:= 0;
		C_DRAM_AXI_RUSER_WIDTH	: integer	:= 0;
		C_DRAM_AXI_BUSER_WIDTH	: integer	:= 0
	);
	port (
		-- Users to add ports here
        -- ports from xillybus (or similar)
        user_r_mem_8_rden : IN std_logic;
        user_r_mem_8_empty : OUT std_logic;
        user_r_mem_8_data : OUT std_logic_vector(7 DOWNTO 0);
        user_r_mem_8_eof : OUT std_logic;
        user_r_mem_8_open : IN std_logic;
        user_w_mem_8_wren : IN std_logic;
        user_w_mem_8_full : OUT std_logic;
        user_w_mem_8_data : IN std_logic_vector(7 DOWNTO 0);
        user_w_mem_8_open : IN std_logic;
        user_mem_8_addr : IN std_logic_vector(4 DOWNTO 0);
        user_mem_8_addr_update : IN std_logic;
        user_r_read_32_rden : IN std_logic;
        user_r_read_32_empty : OUT std_logic;
        user_r_read_32_data : OUT std_logic_vector(31 DOWNTO 0);
        user_r_read_32_eof : OUT std_logic;
        user_r_read_32_open : IN std_logic;
        user_r_read_8_rden : IN std_logic;
        user_r_read_8_empty : OUT std_logic;
        user_r_read_8_data : OUT std_logic_vector(7 DOWNTO 0);
        user_r_read_8_eof : OUT std_logic;
        user_r_read_8_open : IN std_logic;
        user_w_write_32_wren : IN std_logic;
        user_w_write_32_full : OUT std_logic;
        user_w_write_32_data : IN std_logic_vector(31 DOWNTO 0);
        user_w_write_32_open : IN std_logic;
        user_w_write_8_wren : IN std_logic;
        user_w_write_8_full : OUT std_logic;
        user_w_write_8_data : IN std_logic_vector(7 DOWNTO 0);
        user_w_write_8_open : IN std_logic;
        
        bus_clk : IN std_logic;
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Master Bus Interface SATA_AXI
		sata_axi_init_axi_txn	: in std_logic;
		sata_axi_txn_done	: out std_logic;
		sata_axi_error	: out std_logic;
		sata_axi_aclk	: in std_logic;
		sata_axi_aresetn	: in std_logic;
		sata_axi_awid	: out std_logic_vector(C_SATA_AXI_ID_WIDTH-1 downto 0);
		sata_axi_awaddr	: out std_logic_vector(C_SATA_AXI_ADDR_WIDTH-1 downto 0);
		sata_axi_awlen	: out std_logic_vector(7 downto 0);
		sata_axi_awsize	: out std_logic_vector(2 downto 0);
		sata_axi_awburst	: out std_logic_vector(1 downto 0);
		sata_axi_awlock	: out std_logic;
		sata_axi_awcache	: out std_logic_vector(3 downto 0);
		sata_axi_awprot	: out std_logic_vector(2 downto 0);
		sata_axi_awqos	: out std_logic_vector(3 downto 0);
		sata_axi_awuser	: out std_logic_vector(C_SATA_AXI_AWUSER_WIDTH-1 downto 0);
		sata_axi_awvalid	: out std_logic;
		sata_axi_awready	: in std_logic;
		sata_axi_wdata	: out std_logic_vector(C_SATA_AXI_DATA_WIDTH-1 downto 0);
		sata_axi_wstrb	: out std_logic_vector(C_SATA_AXI_DATA_WIDTH/8-1 downto 0);
		sata_axi_wlast	: out std_logic;
		sata_axi_wuser	: out std_logic_vector(C_SATA_AXI_WUSER_WIDTH-1 downto 0);
		sata_axi_wvalid	: out std_logic;
		sata_axi_wready	: in std_logic;
		sata_axi_bid	: in std_logic_vector(C_SATA_AXI_ID_WIDTH-1 downto 0);
		sata_axi_bresp	: in std_logic_vector(1 downto 0);
		sata_axi_buser	: in std_logic_vector(C_SATA_AXI_BUSER_WIDTH-1 downto 0);
		sata_axi_bvalid	: in std_logic;
		sata_axi_bready	: out std_logic;
		sata_axi_arid	: out std_logic_vector(C_SATA_AXI_ID_WIDTH-1 downto 0);
		sata_axi_araddr	: out std_logic_vector(C_SATA_AXI_ADDR_WIDTH-1 downto 0);
		sata_axi_arlen	: out std_logic_vector(7 downto 0);
		sata_axi_arsize	: out std_logic_vector(2 downto 0);
		sata_axi_arburst	: out std_logic_vector(1 downto 0);
		sata_axi_arlock	: out std_logic;
		sata_axi_arcache	: out std_logic_vector(3 downto 0);
		sata_axi_arprot	: out std_logic_vector(2 downto 0);
		sata_axi_arqos	: out std_logic_vector(3 downto 0);
		sata_axi_aruser	: out std_logic_vector(C_SATA_AXI_ARUSER_WIDTH-1 downto 0);
		sata_axi_arvalid	: out std_logic;
		sata_axi_arready	: in std_logic;
		sata_axi_rid	: in std_logic_vector(C_SATA_AXI_ID_WIDTH-1 downto 0);
		sata_axi_rdata	: in std_logic_vector(C_SATA_AXI_DATA_WIDTH-1 downto 0);
		sata_axi_rresp	: in std_logic_vector(1 downto 0);
		sata_axi_rlast	: in std_logic;
		sata_axi_ruser	: in std_logic_vector(C_SATA_AXI_RUSER_WIDTH-1 downto 0);
		sata_axi_rvalid	: in std_logic;
		sata_axi_rready	: out std_logic;

		-- Ports of Axi Master Bus Interface BRAM_AXI
		bram_axi_init_axi_txn	: in std_logic;
		bram_axi_txn_done	: out std_logic;
		bram_axi_error	: out std_logic;
		bram_axi_aclk	: in std_logic;
		bram_axi_aresetn	: in std_logic;
		bram_axi_awid	: out std_logic_vector(C_BRAM_AXI_ID_WIDTH-1 downto 0);
		bram_axi_awaddr	: out std_logic_vector(C_BRAM_AXI_ADDR_WIDTH-1 downto 0);
		bram_axi_awlen	: out std_logic_vector(7 downto 0);
		bram_axi_awsize	: out std_logic_vector(2 downto 0);
		bram_axi_awburst	: out std_logic_vector(1 downto 0);
		bram_axi_awlock	: out std_logic;
		bram_axi_awcache	: out std_logic_vector(3 downto 0);
		bram_axi_awprot	: out std_logic_vector(2 downto 0);
		bram_axi_awqos	: out std_logic_vector(3 downto 0);
		bram_axi_awuser	: out std_logic_vector(C_BRAM_AXI_AWUSER_WIDTH-1 downto 0);
		bram_axi_awvalid	: out std_logic;
		bram_axi_awready	: in std_logic;
		bram_axi_wdata	: out std_logic_vector(C_BRAM_AXI_DATA_WIDTH-1 downto 0);
		bram_axi_wstrb	: out std_logic_vector(C_BRAM_AXI_DATA_WIDTH/8-1 downto 0);
		bram_axi_wlast	: out std_logic;
		bram_axi_wuser	: out std_logic_vector(C_BRAM_AXI_WUSER_WIDTH-1 downto 0);
		bram_axi_wvalid	: out std_logic;
		bram_axi_wready	: in std_logic;
		bram_axi_bid	: in std_logic_vector(C_BRAM_AXI_ID_WIDTH-1 downto 0);
		bram_axi_bresp	: in std_logic_vector(1 downto 0);
		bram_axi_buser	: in std_logic_vector(C_BRAM_AXI_BUSER_WIDTH-1 downto 0);
		bram_axi_bvalid	: in std_logic;
		bram_axi_bready	: out std_logic;
		bram_axi_arid	: out std_logic_vector(C_BRAM_AXI_ID_WIDTH-1 downto 0);
		bram_axi_araddr	: out std_logic_vector(C_BRAM_AXI_ADDR_WIDTH-1 downto 0);
		bram_axi_arlen	: out std_logic_vector(7 downto 0);
		bram_axi_arsize	: out std_logic_vector(2 downto 0);
		bram_axi_arburst	: out std_logic_vector(1 downto 0);
		bram_axi_arlock	: out std_logic;
		bram_axi_arcache	: out std_logic_vector(3 downto 0);
		bram_axi_arprot	: out std_logic_vector(2 downto 0);
		bram_axi_arqos	: out std_logic_vector(3 downto 0);
		bram_axi_aruser	: out std_logic_vector(C_BRAM_AXI_ARUSER_WIDTH-1 downto 0);
		bram_axi_arvalid	: out std_logic;
		bram_axi_arready	: in std_logic;
		bram_axi_rid	: in std_logic_vector(C_BRAM_AXI_ID_WIDTH-1 downto 0);
		bram_axi_rdata	: in std_logic_vector(C_BRAM_AXI_DATA_WIDTH-1 downto 0);
		bram_axi_rresp	: in std_logic_vector(1 downto 0);
		bram_axi_rlast	: in std_logic;
		bram_axi_ruser	: in std_logic_vector(C_BRAM_AXI_RUSER_WIDTH-1 downto 0);
		bram_axi_rvalid	: in std_logic;
		bram_axi_rready	: out std_logic;

		-- Ports of Axi Master Bus Interface DRAM_AXI
		dram_axi_init_axi_txn	: in std_logic;
		dram_axi_txn_done	: out std_logic;
		dram_axi_error	: out std_logic;
		dram_axi_aclk	: in std_logic;
		dram_axi_aresetn	: in std_logic;
		dram_axi_awid	: out std_logic_vector(C_DRAM_AXI_ID_WIDTH-1 downto 0);
		dram_axi_awaddr	: out std_logic_vector(C_DRAM_AXI_ADDR_WIDTH-1 downto 0);
		dram_axi_awlen	: out std_logic_vector(7 downto 0);
		dram_axi_awsize	: out std_logic_vector(2 downto 0);
		dram_axi_awburst	: out std_logic_vector(1 downto 0);
		dram_axi_awlock	: out std_logic;
		dram_axi_awcache	: out std_logic_vector(3 downto 0);
		dram_axi_awprot	: out std_logic_vector(2 downto 0);
		dram_axi_awqos	: out std_logic_vector(3 downto 0);
		dram_axi_awuser	: out std_logic_vector(C_DRAM_AXI_AWUSER_WIDTH-1 downto 0);
		dram_axi_awvalid	: out std_logic;
		dram_axi_awready	: in std_logic;
		dram_axi_wdata	: out std_logic_vector(C_DRAM_AXI_DATA_WIDTH-1 downto 0);
		dram_axi_wstrb	: out std_logic_vector(C_DRAM_AXI_DATA_WIDTH/8-1 downto 0);
		dram_axi_wlast	: out std_logic;
		dram_axi_wuser	: out std_logic_vector(C_DRAM_AXI_WUSER_WIDTH-1 downto 0);
		dram_axi_wvalid	: out std_logic;
		dram_axi_wready	: in std_logic;
		dram_axi_bid	: in std_logic_vector(C_DRAM_AXI_ID_WIDTH-1 downto 0);
		dram_axi_bresp	: in std_logic_vector(1 downto 0);
		dram_axi_buser	: in std_logic_vector(C_DRAM_AXI_BUSER_WIDTH-1 downto 0);
		dram_axi_bvalid	: in std_logic;
		dram_axi_bready	: out std_logic;
		dram_axi_arid	: out std_logic_vector(C_DRAM_AXI_ID_WIDTH-1 downto 0);
		dram_axi_araddr	: out std_logic_vector(C_DRAM_AXI_ADDR_WIDTH-1 downto 0);
		dram_axi_arlen	: out std_logic_vector(7 downto 0);
		dram_axi_arsize	: out std_logic_vector(2 downto 0);
		dram_axi_arburst	: out std_logic_vector(1 downto 0);
		dram_axi_arlock	: out std_logic;
		dram_axi_arcache	: out std_logic_vector(3 downto 0);
		dram_axi_arprot	: out std_logic_vector(2 downto 0);
		dram_axi_arqos	: out std_logic_vector(3 downto 0);
		dram_axi_aruser	: out std_logic_vector(C_DRAM_AXI_ARUSER_WIDTH-1 downto 0);
		dram_axi_arvalid	: out std_logic;
		dram_axi_arready	: in std_logic;
		dram_axi_rid	: in std_logic_vector(C_DRAM_AXI_ID_WIDTH-1 downto 0);
		dram_axi_rdata	: in std_logic_vector(C_DRAM_AXI_DATA_WIDTH-1 downto 0);
		dram_axi_rresp	: in std_logic_vector(1 downto 0);
		dram_axi_rlast	: in std_logic;
		dram_axi_ruser	: in std_logic_vector(C_DRAM_AXI_RUSER_WIDTH-1 downto 0);
		dram_axi_rvalid	: in std_logic;
		dram_axi_rready	: out std_logic
	);
end Xillybus_AXI_Master_v1_0;

architecture arch_imp of Xillybus_AXI_Master_v1_0 is


   
    signal count_data : integer range 0 to 1 := 0;
    signal sata_addr_l : std_logic_vector(31 downto 0);
    signal sata_addr_h : std_logic_vector(31 downto 0);

	-- component declaration
	component Xillybus_AXI_Master_v1_0_SATA_AXI is
		generic (
		C_M_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"40000000";
		C_M_AXI_BURST_LEN	: integer	:= 16;
		C_M_AXI_ID_WIDTH	: integer	:= 1;
		C_M_AXI_ADDR_WIDTH	: integer	:= 64;
		C_M_AXI_DATA_WIDTH	: integer	:= 512;
		C_M_AXI_AWUSER_WIDTH	: integer	:= 0;
		C_M_AXI_ARUSER_WIDTH	: integer	:= 0;
		C_M_AXI_WUSER_WIDTH	: integer	:= 0;
		C_M_AXI_RUSER_WIDTH	: integer	:= 0;
		C_M_AXI_BUSER_WIDTH	: integer	:= 0
		);
		port (
		INIT_AXI_TXN	: in std_logic;
		TXN_DONE	: out std_logic;
		ERROR	: out std_logic;
		M_AXI_ACLK	: in std_logic;
		M_AXI_ARESETN	: in std_logic;
		M_AXI_AWID	: out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_AWADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_AWLEN	: out std_logic_vector(7 downto 0);
		M_AXI_AWSIZE	: out std_logic_vector(2 downto 0);
		M_AXI_AWBURST	: out std_logic_vector(1 downto 0);
		M_AXI_AWLOCK	: out std_logic;
		M_AXI_AWCACHE	: out std_logic_vector(3 downto 0);
		M_AXI_AWPROT	: out std_logic_vector(2 downto 0);
		M_AXI_AWQOS	: out std_logic_vector(3 downto 0);
		M_AXI_AWUSER	: out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
		M_AXI_AWVALID	: out std_logic;
		M_AXI_AWREADY	: in std_logic;
		M_AXI_WDATA	: out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_WSTRB	: out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
		M_AXI_WLAST	: out std_logic;
		M_AXI_WUSER	: out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
		M_AXI_WVALID	: out std_logic;
		M_AXI_WREADY	: in std_logic;
		M_AXI_BID	: in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_BRESP	: in std_logic_vector(1 downto 0);
		M_AXI_BUSER	: in std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
		M_AXI_BVALID	: in std_logic;
		M_AXI_BREADY	: out std_logic;
		M_AXI_ARID	: out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_ARADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_ARLEN	: out std_logic_vector(7 downto 0);
		M_AXI_ARSIZE	: out std_logic_vector(2 downto 0);
		M_AXI_ARBURST	: out std_logic_vector(1 downto 0);
		M_AXI_ARLOCK	: out std_logic;
		M_AXI_ARCACHE	: out std_logic_vector(3 downto 0);
		M_AXI_ARPROT	: out std_logic_vector(2 downto 0);
		M_AXI_ARQOS	: out std_logic_vector(3 downto 0);
		M_AXI_ARUSER	: out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
		M_AXI_ARVALID	: out std_logic;
		M_AXI_ARREADY	: in std_logic;
		M_AXI_RID	: in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_RDATA	: in std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_RRESP	: in std_logic_vector(1 downto 0);
		M_AXI_RLAST	: in std_logic;
		M_AXI_RUSER	: in std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
		M_AXI_RVALID	: in std_logic;
		M_AXI_RREADY	: out std_logic
		);
	end component Xillybus_AXI_Master_v1_0_SATA_AXI;

	component Xillybus_AXI_Master_v1_0_BRAM_AXI is
		generic (
		C_M_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"40000000";
		C_M_AXI_BURST_LEN	: integer	:= 16;
		C_M_AXI_ID_WIDTH	: integer	:= 1;
		C_M_AXI_ADDR_WIDTH	: integer	:= 32;
		C_M_AXI_DATA_WIDTH	: integer	:= 32;
		C_M_AXI_AWUSER_WIDTH	: integer	:= 0;
		C_M_AXI_ARUSER_WIDTH	: integer	:= 0;
		C_M_AXI_WUSER_WIDTH	: integer	:= 0;
		C_M_AXI_RUSER_WIDTH	: integer	:= 0;
		C_M_AXI_BUSER_WIDTH	: integer	:= 0
		);
		port (
		INIT_AXI_TXN	: in std_logic;
		TXN_DONE	: out std_logic;
		ERROR	: out std_logic;
		M_AXI_ACLK	: in std_logic;
		M_AXI_ARESETN	: in std_logic;
		M_AXI_AWID	: out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_AWADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_AWLEN	: out std_logic_vector(7 downto 0);
		M_AXI_AWSIZE	: out std_logic_vector(2 downto 0);
		M_AXI_AWBURST	: out std_logic_vector(1 downto 0);
		M_AXI_AWLOCK	: out std_logic;
		M_AXI_AWCACHE	: out std_logic_vector(3 downto 0);
		M_AXI_AWPROT	: out std_logic_vector(2 downto 0);
		M_AXI_AWQOS	: out std_logic_vector(3 downto 0);
		M_AXI_AWUSER	: out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
		M_AXI_AWVALID	: out std_logic;
		M_AXI_AWREADY	: in std_logic;
		M_AXI_WDATA	: out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_WSTRB	: out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
		M_AXI_WLAST	: out std_logic;
		M_AXI_WUSER	: out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
		M_AXI_WVALID	: out std_logic;
		M_AXI_WREADY	: in std_logic;
		M_AXI_BID	: in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_BRESP	: in std_logic_vector(1 downto 0);
		M_AXI_BUSER	: in std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
		M_AXI_BVALID	: in std_logic;
		M_AXI_BREADY	: out std_logic;
		M_AXI_ARID	: out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_ARADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_ARLEN	: out std_logic_vector(7 downto 0);
		M_AXI_ARSIZE	: out std_logic_vector(2 downto 0);
		M_AXI_ARBURST	: out std_logic_vector(1 downto 0);
		M_AXI_ARLOCK	: out std_logic;
		M_AXI_ARCACHE	: out std_logic_vector(3 downto 0);
		M_AXI_ARPROT	: out std_logic_vector(2 downto 0);
		M_AXI_ARQOS	: out std_logic_vector(3 downto 0);
		M_AXI_ARUSER	: out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
		M_AXI_ARVALID	: out std_logic;
		M_AXI_ARREADY	: in std_logic;
		M_AXI_RID	: in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_RDATA	: in std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_RRESP	: in std_logic_vector(1 downto 0);
		M_AXI_RLAST	: in std_logic;
		M_AXI_RUSER	: in std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
		M_AXI_RVALID	: in std_logic;
		M_AXI_RREADY	: out std_logic
		);
	end component Xillybus_AXI_Master_v1_0_BRAM_AXI;

	component Xillybus_AXI_Master_v1_0_DRAM_AXI is
		generic (
		C_M_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"40000000";
		C_M_AXI_BURST_LEN	: integer	:= 16;
		C_M_AXI_ID_WIDTH	: integer	:= 1;
		C_M_AXI_ADDR_WIDTH	: integer	:= 32;
		C_M_AXI_DATA_WIDTH	: integer	:= 32;
		C_M_AXI_AWUSER_WIDTH	: integer	:= 0;
		C_M_AXI_ARUSER_WIDTH	: integer	:= 0;
		C_M_AXI_WUSER_WIDTH	: integer	:= 0;
		C_M_AXI_RUSER_WIDTH	: integer	:= 0;
		C_M_AXI_BUSER_WIDTH	: integer	:= 0
		);
		port (
		INIT_AXI_TXN	: in std_logic;
		TXN_DONE	: out std_logic;
		ERROR	: out std_logic;
		M_AXI_ACLK	: in std_logic;
		M_AXI_ARESETN	: in std_logic;
		M_AXI_AWID	: out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_AWADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_AWLEN	: out std_logic_vector(7 downto 0);
		M_AXI_AWSIZE	: out std_logic_vector(2 downto 0);
		M_AXI_AWBURST	: out std_logic_vector(1 downto 0);
		M_AXI_AWLOCK	: out std_logic;
		M_AXI_AWCACHE	: out std_logic_vector(3 downto 0);
		M_AXI_AWPROT	: out std_logic_vector(2 downto 0);
		M_AXI_AWQOS	: out std_logic_vector(3 downto 0);
		M_AXI_AWUSER	: out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
		M_AXI_AWVALID	: out std_logic;
		M_AXI_AWREADY	: in std_logic;
		M_AXI_WDATA	: out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_WSTRB	: out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
		M_AXI_WLAST	: out std_logic;
		M_AXI_WUSER	: out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
		M_AXI_WVALID	: out std_logic;
		M_AXI_WREADY	: in std_logic;
		M_AXI_BID	: in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_BRESP	: in std_logic_vector(1 downto 0);
		M_AXI_BUSER	: in std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
		M_AXI_BVALID	: in std_logic;
		M_AXI_BREADY	: out std_logic;
		M_AXI_ARID	: out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_ARADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_ARLEN	: out std_logic_vector(7 downto 0);
		M_AXI_ARSIZE	: out std_logic_vector(2 downto 0);
		M_AXI_ARBURST	: out std_logic_vector(1 downto 0);
		M_AXI_ARLOCK	: out std_logic;
		M_AXI_ARCACHE	: out std_logic_vector(3 downto 0);
		M_AXI_ARPROT	: out std_logic_vector(2 downto 0);
		M_AXI_ARQOS	: out std_logic_vector(3 downto 0);
		M_AXI_ARUSER	: out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
		M_AXI_ARVALID	: out std_logic;
		M_AXI_ARREADY	: in std_logic;
		M_AXI_RID	: in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_RDATA	: in std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_RRESP	: in std_logic_vector(1 downto 0);
		M_AXI_RLAST	: in std_logic;
		M_AXI_RUSER	: in std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
		M_AXI_RVALID	: in std_logic;
		M_AXI_RREADY	: out std_logic
		);
	end component Xillybus_AXI_Master_v1_0_DRAM_AXI;
    
    component fifo_8x2048
        port (
          clk: IN std_logic;
          srst: IN std_logic;
          din: IN std_logic_VECTOR(7 downto 0);
          wr_en: IN std_logic;
          rd_en: IN std_logic;
          dout: OUT std_logic_VECTOR(7 downto 0);
          full: OUT std_logic;
          empty: OUT std_logic);
      end component;
    
      component fifo_32x512
        port (
          clk: IN std_logic;
          srst: IN std_logic;
          din: IN std_logic_VECTOR(31 downto 0);
          wr_en: IN std_logic;
          rd_en: IN std_logic;
          dout: OUT std_logic_VECTOR(31 downto 0);
          full: OUT std_logic;
          empty: OUT std_logic);
      end component;
    
    -- Synplicity black box declaration
      attribute syn_black_box : boolean;
      attribute syn_black_box of fifo_32x512: component is true;
      attribute syn_black_box of fifo_8x2048: component is true;
     
      
      signal reset_8 : std_logic;
      signal reset_32 : std_logic;
      
      signal data_to_host : std_logic_vector(31 downto 0);
      signal data_from_host : std_logic_vector(31 downto 0);
      signal data_to_host_en : std_logic;
      signal data_from_host_en : std_logic;
      signal data_from_host_empty : std_logic;
      signal data_to_host_full : std_logic;
begin

-- Instantiation of Axi Bus Interface SATA_AXI
Xillybus_AXI_Master_v1_0_SATA_AXI_inst : Xillybus_AXI_Master_v1_0_SATA_AXI
	generic map (
		C_M_TARGET_SLAVE_BASE_ADDR	=> C_SATA_AXI_TARGET_SLAVE_BASE_ADDR,
		C_M_AXI_BURST_LEN	=> C_SATA_AXI_BURST_LEN,
		C_M_AXI_ID_WIDTH	=> C_SATA_AXI_ID_WIDTH,
		C_M_AXI_ADDR_WIDTH	=> C_SATA_AXI_ADDR_WIDTH,
		C_M_AXI_DATA_WIDTH	=> C_SATA_AXI_DATA_WIDTH,
		C_M_AXI_AWUSER_WIDTH	=> C_SATA_AXI_AWUSER_WIDTH,
		C_M_AXI_ARUSER_WIDTH	=> C_SATA_AXI_ARUSER_WIDTH,
		C_M_AXI_WUSER_WIDTH	=> C_SATA_AXI_WUSER_WIDTH,
		C_M_AXI_RUSER_WIDTH	=> C_SATA_AXI_RUSER_WIDTH,
		C_M_AXI_BUSER_WIDTH	=> C_SATA_AXI_BUSER_WIDTH
	)
	port map (
		INIT_AXI_TXN	=> sata_axi_init_axi_txn,
		TXN_DONE	=> sata_axi_txn_done,
		ERROR	=> sata_axi_error,
		M_AXI_ACLK	=> sata_axi_aclk,
		M_AXI_ARESETN	=> sata_axi_aresetn,
		M_AXI_AWID	=> sata_axi_awid,
		M_AXI_AWADDR	=> sata_axi_awaddr,
		M_AXI_AWLEN	=> sata_axi_awlen,
		M_AXI_AWSIZE	=> sata_axi_awsize,
		M_AXI_AWBURST	=> sata_axi_awburst,
		M_AXI_AWLOCK	=> sata_axi_awlock,
		M_AXI_AWCACHE	=> sata_axi_awcache,
		M_AXI_AWPROT	=> sata_axi_awprot,
		M_AXI_AWQOS	=> sata_axi_awqos,
		M_AXI_AWUSER	=> sata_axi_awuser,
		M_AXI_AWVALID	=> sata_axi_awvalid,
		M_AXI_AWREADY	=> sata_axi_awready,
		M_AXI_WDATA	=> sata_axi_wdata,
		M_AXI_WSTRB	=> sata_axi_wstrb,
		M_AXI_WLAST	=> sata_axi_wlast,
		M_AXI_WUSER	=> sata_axi_wuser,
		M_AXI_WVALID	=> sata_axi_wvalid,
		M_AXI_WREADY	=> sata_axi_wready,
		M_AXI_BID	=> sata_axi_bid,
		M_AXI_BRESP	=> sata_axi_bresp,
		M_AXI_BUSER	=> sata_axi_buser,
		M_AXI_BVALID	=> sata_axi_bvalid,
		M_AXI_BREADY	=> sata_axi_bready,
		M_AXI_ARID	=> sata_axi_arid,
		M_AXI_ARADDR	=> sata_axi_araddr,
		M_AXI_ARLEN	=> sata_axi_arlen,
		M_AXI_ARSIZE	=> sata_axi_arsize,
		M_AXI_ARBURST	=> sata_axi_arburst,
		M_AXI_ARLOCK	=> sata_axi_arlock,
		M_AXI_ARCACHE	=> sata_axi_arcache,
		M_AXI_ARPROT	=> sata_axi_arprot,
		M_AXI_ARQOS	=> sata_axi_arqos,
		M_AXI_ARUSER	=> sata_axi_aruser,
		M_AXI_ARVALID	=> sata_axi_arvalid,
		M_AXI_ARREADY	=> sata_axi_arready,
		M_AXI_RID	=> sata_axi_rid,
		M_AXI_RDATA	=> sata_axi_rdata,
		M_AXI_RRESP	=> sata_axi_rresp,
		M_AXI_RLAST	=> sata_axi_rlast,
		M_AXI_RUSER	=> sata_axi_ruser,
		M_AXI_RVALID	=> sata_axi_rvalid,
		M_AXI_RREADY	=> sata_axi_rready
	);

-- Instantiation of Axi Bus Interface BRAM_AXI
Xillybus_AXI_Master_v1_0_BRAM_AXI_inst : Xillybus_AXI_Master_v1_0_BRAM_AXI
	generic map (
		C_M_TARGET_SLAVE_BASE_ADDR	=> C_BRAM_AXI_TARGET_SLAVE_BASE_ADDR,
		C_M_AXI_BURST_LEN	=> C_BRAM_AXI_BURST_LEN,
		C_M_AXI_ID_WIDTH	=> C_BRAM_AXI_ID_WIDTH,
		C_M_AXI_ADDR_WIDTH	=> C_BRAM_AXI_ADDR_WIDTH,
		C_M_AXI_DATA_WIDTH	=> C_BRAM_AXI_DATA_WIDTH,
		C_M_AXI_AWUSER_WIDTH	=> C_BRAM_AXI_AWUSER_WIDTH,
		C_M_AXI_ARUSER_WIDTH	=> C_BRAM_AXI_ARUSER_WIDTH,
		C_M_AXI_WUSER_WIDTH	=> C_BRAM_AXI_WUSER_WIDTH,
		C_M_AXI_RUSER_WIDTH	=> C_BRAM_AXI_RUSER_WIDTH,
		C_M_AXI_BUSER_WIDTH	=> C_BRAM_AXI_BUSER_WIDTH
	)
	port map (
		INIT_AXI_TXN	=> bram_axi_init_axi_txn,
		TXN_DONE	=> bram_axi_txn_done,
		ERROR	=> bram_axi_error,
		M_AXI_ACLK	=> bram_axi_aclk,
		M_AXI_ARESETN	=> bram_axi_aresetn,
		M_AXI_AWID	=> bram_axi_awid,
		M_AXI_AWADDR	=> bram_axi_awaddr,
		M_AXI_AWLEN	=> bram_axi_awlen,
		M_AXI_AWSIZE	=> bram_axi_awsize,
		M_AXI_AWBURST	=> bram_axi_awburst,
		M_AXI_AWLOCK	=> bram_axi_awlock,
		M_AXI_AWCACHE	=> bram_axi_awcache,
		M_AXI_AWPROT	=> bram_axi_awprot,
		M_AXI_AWQOS	=> bram_axi_awqos,
		M_AXI_AWUSER	=> bram_axi_awuser,
		M_AXI_AWVALID	=> bram_axi_awvalid,
		M_AXI_AWREADY	=> bram_axi_awready,
		M_AXI_WDATA	=> bram_axi_wdata,
		M_AXI_WSTRB	=> bram_axi_wstrb,
		M_AXI_WLAST	=> bram_axi_wlast,
		M_AXI_WUSER	=> bram_axi_wuser,
		M_AXI_WVALID	=> bram_axi_wvalid,
		M_AXI_WREADY	=> bram_axi_wready,
		M_AXI_BID	=> bram_axi_bid,
		M_AXI_BRESP	=> bram_axi_bresp,
		M_AXI_BUSER	=> bram_axi_buser,
		M_AXI_BVALID	=> bram_axi_bvalid,
		M_AXI_BREADY	=> bram_axi_bready,
		M_AXI_ARID	=> bram_axi_arid,
		M_AXI_ARADDR	=> bram_axi_araddr,
		M_AXI_ARLEN	=> bram_axi_arlen,
		M_AXI_ARSIZE	=> bram_axi_arsize,
		M_AXI_ARBURST	=> bram_axi_arburst,
		M_AXI_ARLOCK	=> bram_axi_arlock,
		M_AXI_ARCACHE	=> bram_axi_arcache,
		M_AXI_ARPROT	=> bram_axi_arprot,
		M_AXI_ARQOS	=> bram_axi_arqos,
		M_AXI_ARUSER	=> bram_axi_aruser,
		M_AXI_ARVALID	=> bram_axi_arvalid,
		M_AXI_ARREADY	=> bram_axi_arready,
		M_AXI_RID	=> bram_axi_rid,
		M_AXI_RDATA	=> bram_axi_rdata,
		M_AXI_RRESP	=> bram_axi_rresp,
		M_AXI_RLAST	=> bram_axi_rlast,
		M_AXI_RUSER	=> bram_axi_ruser,
		M_AXI_RVALID	=> bram_axi_rvalid,
		M_AXI_RREADY	=> bram_axi_rready
	);

-- Instantiation of Axi Bus Interface DRAM_AXI
Xillybus_AXI_Master_v1_0_DRAM_AXI_inst : Xillybus_AXI_Master_v1_0_DRAM_AXI
	generic map (
		C_M_TARGET_SLAVE_BASE_ADDR	=> C_DRAM_AXI_TARGET_SLAVE_BASE_ADDR,
		C_M_AXI_BURST_LEN	=> C_DRAM_AXI_BURST_LEN,
		C_M_AXI_ID_WIDTH	=> C_DRAM_AXI_ID_WIDTH,
		C_M_AXI_ADDR_WIDTH	=> C_DRAM_AXI_ADDR_WIDTH,
		C_M_AXI_DATA_WIDTH	=> C_DRAM_AXI_DATA_WIDTH,
		C_M_AXI_AWUSER_WIDTH	=> C_DRAM_AXI_AWUSER_WIDTH,
		C_M_AXI_ARUSER_WIDTH	=> C_DRAM_AXI_ARUSER_WIDTH,
		C_M_AXI_WUSER_WIDTH	=> C_DRAM_AXI_WUSER_WIDTH,
		C_M_AXI_RUSER_WIDTH	=> C_DRAM_AXI_RUSER_WIDTH,
		C_M_AXI_BUSER_WIDTH	=> C_DRAM_AXI_BUSER_WIDTH
	)
	port map (
		INIT_AXI_TXN	=> dram_axi_init_axi_txn,
		TXN_DONE	=> dram_axi_txn_done,
		ERROR	=> dram_axi_error,
		M_AXI_ACLK	=> dram_axi_aclk,
		M_AXI_ARESETN	=> dram_axi_aresetn,
		M_AXI_AWID	=> dram_axi_awid,
		M_AXI_AWADDR	=> dram_axi_awaddr,
		M_AXI_AWLEN	=> dram_axi_awlen,
		M_AXI_AWSIZE	=> dram_axi_awsize,
		M_AXI_AWBURST	=> dram_axi_awburst,
		M_AXI_AWLOCK	=> dram_axi_awlock,
		M_AXI_AWCACHE	=> dram_axi_awcache,
		M_AXI_AWPROT	=> dram_axi_awprot,
		M_AXI_AWQOS	=> dram_axi_awqos,
		M_AXI_AWUSER	=> dram_axi_awuser,
		M_AXI_AWVALID	=> dram_axi_awvalid,
		M_AXI_AWREADY	=> dram_axi_awready,
		M_AXI_WDATA	=> dram_axi_wdata,
		M_AXI_WSTRB	=> dram_axi_wstrb,
		M_AXI_WLAST	=> dram_axi_wlast,
		M_AXI_WUSER	=> dram_axi_wuser,
		M_AXI_WVALID	=> dram_axi_wvalid,
		M_AXI_WREADY	=> dram_axi_wready,
		M_AXI_BID	=> dram_axi_bid,
		M_AXI_BRESP	=> dram_axi_bresp,
		M_AXI_BUSER	=> dram_axi_buser,
		M_AXI_BVALID	=> dram_axi_bvalid,
		M_AXI_BREADY	=> dram_axi_bready,
		M_AXI_ARID	=> dram_axi_arid,
		M_AXI_ARADDR	=> dram_axi_araddr,
		M_AXI_ARLEN	=> dram_axi_arlen,
		M_AXI_ARSIZE	=> dram_axi_arsize,
		M_AXI_ARBURST	=> dram_axi_arburst,
		M_AXI_ARLOCK	=> dram_axi_arlock,
		M_AXI_ARCACHE	=> dram_axi_arcache,
		M_AXI_ARPROT	=> dram_axi_arprot,
		M_AXI_ARQOS	=> dram_axi_arqos,
		M_AXI_ARUSER	=> dram_axi_aruser,
		M_AXI_ARVALID	=> dram_axi_arvalid,
		M_AXI_ARREADY	=> dram_axi_arready,
		M_AXI_RID	=> dram_axi_rid,
		M_AXI_RDATA	=> dram_axi_rdata,
		M_AXI_RRESP	=> dram_axi_rresp,
		M_AXI_RLAST	=> dram_axi_rlast,
		M_AXI_RUSER	=> dram_axi_ruser,
		M_AXI_RVALID	=> dram_axi_rvalid,
		M_AXI_RREADY	=> dram_axi_rready
	);

	-- Add user logic here
    fifo_32_to_host : fifo_32x512
        port map(
          clk        => bus_clk,
          srst       => not user_r_read_32_open,
          din        => data_to_host,
          wr_en      => data_to_host_en,
          rd_en      => user_r_read_32_rden,
          dout       => user_r_read_32_data,
          full       => data_to_host_full,
          empty      => user_r_read_32_empty
          );
    
      user_r_read_32_eof <= '0';
      
     fifo_32_from_host : fifo_32x512
      port map(
        clk        => bus_clk,
        srst       => not user_w_write_32_open,
        din        => user_w_write_32_data,
        wr_en      => user_w_write_32_wren,
        rd_en      => data_from_host_en,
        dout       => data_from_host,
        full       => user_w_write_32_full,
        empty      => data_from_host_empty
        );
          
     user_r_read_32_eof <= '0';
      
    --  8-bit loopback
    
      fifo_8 : fifo_8x2048
        port map(
          clk        => bus_clk,
          srst       => reset_8,
          din        => user_w_write_8_data,
          wr_en      => user_w_write_8_wren,
          rd_en      => user_r_read_8_rden,
          dout       => user_r_read_8_data,
          full       => user_w_write_8_full,
          empty      => user_r_read_8_empty
          );
    
        reset_8 <= not (user_w_write_8_open or user_r_read_8_open);
    
        user_r_read_8_eof <= '0';
	-- User logic ends

end arch_imp;
