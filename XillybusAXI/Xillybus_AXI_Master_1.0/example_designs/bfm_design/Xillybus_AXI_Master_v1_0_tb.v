
`timescale 1 ns / 1 ps

`include "Xillybus_AXI_Master_v1_0_tb_include.vh"

module Xillybus_AXI_Master_v1_0_tb;
	reg tb_ACLK;
	reg tb_ARESETn;

	reg SATA_AXI_INIT_AXI_TXN;
	wire SATA_AXI_TXN_DONE;
	wire SATA_AXI_ERROR;

	reg BRAM_AXI_INIT_AXI_TXN;
	wire BRAM_AXI_TXN_DONE;
	wire BRAM_AXI_ERROR;

	reg DRAM_AXI_INIT_AXI_TXN;
	wire DRAM_AXI_TXN_DONE;
	wire DRAM_AXI_ERROR;

	// Create an instance of the example tb
	`BD_WRAPPER dut (.ACLK(tb_ACLK),
				.ARESETN(tb_ARESETn),
				.SATA_AXI_TXN_DONE(SATA_AXI_TXN_DONE),
				.SATA_AXI_ERROR(SATA_AXI_ERROR),
				.SATA_AXI_INIT_AXI_TXN(SATA_AXI_INIT_AXI_TXN),
				.BRAM_AXI_TXN_DONE(BRAM_AXI_TXN_DONE),
				.BRAM_AXI_ERROR(BRAM_AXI_ERROR),
				.BRAM_AXI_INIT_AXI_TXN(BRAM_AXI_INIT_AXI_TXN),
				.DRAM_AXI_TXN_DONE(DRAM_AXI_TXN_DONE),
				.DRAM_AXI_ERROR(DRAM_AXI_ERROR),
				.DRAM_AXI_INIT_AXI_TXN(DRAM_AXI_INIT_AXI_TXN));

	// Simple Reset Generator and test
	initial begin
		tb_ARESETn = 1'b0;
	  #500;
		// Release the reset on the posedge of the clk.
		@(posedge tb_ACLK);
	  tb_ARESETn = 1'b1;
		@(posedge tb_ACLK);
	end

	// Simple Clock Generator
	initial tb_ACLK = 1'b0;
	always #10 tb_ACLK = !tb_ACLK;

	// Drive the BFM
	initial begin
		// Wait for end of reset
		wait(tb_ARESETn === 0) @(posedge tb_ACLK);
		wait(tb_ARESETn === 1) @(posedge tb_ACLK);
		wait(tb_ARESETn === 1) @(posedge tb_ACLK);     
		wait(tb_ARESETn === 1) @(posedge tb_ACLK);     
		wait(tb_ARESETn === 1) @(posedge tb_ACLK);     

		SATA_AXI_INIT_AXI_TXN = 1'b0;
		#500 SATA_AXI_INIT_AXI_TXN = 1'b1;

		$display("EXAMPLE TEST SATA_AXI:");
		wait( SATA_AXI_TXN_DONE == 1'b1);
		$display("SATA_AXI: PTGEN_TEST_FINISHED!");
		if ( SATA_AXI_ERROR ) begin
		  $display("PTGEN_TEST: FAILED!");
		end else begin
		  $display("PTGEN_TEST: PASSED!");
		end

	end

	// Drive the BFM
	initial begin
		// Wait for end of reset
		wait(tb_ARESETn === 0) @(posedge tb_ACLK);
		wait(tb_ARESETn === 1) @(posedge tb_ACLK);
		wait(tb_ARESETn === 1) @(posedge tb_ACLK);     
		wait(tb_ARESETn === 1) @(posedge tb_ACLK);     
		wait(tb_ARESETn === 1) @(posedge tb_ACLK);     

		BRAM_AXI_INIT_AXI_TXN = 1'b0;
		#500 BRAM_AXI_INIT_AXI_TXN = 1'b1;

		$display("EXAMPLE TEST BRAM_AXI:");
		wait( BRAM_AXI_TXN_DONE == 1'b1);
		$display("BRAM_AXI: PTGEN_TEST_FINISHED!");
		if ( BRAM_AXI_ERROR ) begin
		  $display("PTGEN_TEST: FAILED!");
		end else begin
		  $display("PTGEN_TEST: PASSED!");
		end

	end

	// Drive the BFM
	initial begin
		// Wait for end of reset
		wait(tb_ARESETn === 0) @(posedge tb_ACLK);
		wait(tb_ARESETn === 1) @(posedge tb_ACLK);
		wait(tb_ARESETn === 1) @(posedge tb_ACLK);     
		wait(tb_ARESETn === 1) @(posedge tb_ACLK);     
		wait(tb_ARESETn === 1) @(posedge tb_ACLK);     

		DRAM_AXI_INIT_AXI_TXN = 1'b0;
		#500 DRAM_AXI_INIT_AXI_TXN = 1'b1;

		$display("EXAMPLE TEST DRAM_AXI:");
		wait( DRAM_AXI_TXN_DONE == 1'b1);
		$display("DRAM_AXI: PTGEN_TEST_FINISHED!");
		if ( DRAM_AXI_ERROR ) begin
		  $display("PTGEN_TEST: FAILED!");
		end else begin
		  $display("PTGEN_TEST: PASSED!");
		end

	end

endmodule
