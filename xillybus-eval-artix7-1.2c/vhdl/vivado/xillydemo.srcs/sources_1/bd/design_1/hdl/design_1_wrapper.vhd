--Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2014.4 (win64) Build 1071353 Tue Nov 18 18:29:27 MST 2014
--Date        : Wed Apr 01 16:04:19 2015
--Host        : Titow running 64-bit major release  (build 9200)
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    DRP_CLK_IN_N : in STD_LOGIC;
    DRP_CLK_IN_P : in STD_LOGIC;
    GPIO_LED : out STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_PERST_B_LS : in STD_LOGIC;
    PCIE_REFCLK_N : in STD_LOGIC;
    PCIE_REFCLK_P : in STD_LOGIC;
    PCIE_RX_N : in STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_RX_P : in STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_TX_N : out STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_TX_P : out STD_LOGIC_VECTOR ( 3 downto 0 );
    Q0_CLK1_GTREFCLK_PAD_N_IN : in STD_LOGIC;
    Q0_CLK1_GTREFCLK_PAD_P_IN : in STD_LOGIC;
    RXN0_IN : in STD_LOGIC;
    RXP0_IN : in STD_LOGIC;
    TXN0_OUT : out STD_LOGIC;
    TXP0_OUT : out STD_LOGIC;
    bus_clk : out STD_LOGIC;
    clock_rtl : in STD_LOGIC;
    quiesce : out STD_LOGIC;
    reset_rtl : in STD_LOGIC;
    reset_rtl_0 : in STD_LOGIC
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    clock_rtl : in STD_LOGIC;
    reset_rtl : in STD_LOGIC;
    reset_rtl_0 : in STD_LOGIC;
    PCIE_TX_P : out STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_TX_N : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bus_clk : out STD_LOGIC;
    quiesce : out STD_LOGIC;
    GPIO_LED : out STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_RX_P : in STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_RX_N : in STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_REFCLK_P : in STD_LOGIC;
    PCIE_REFCLK_N : in STD_LOGIC;
    PCIE_PERST_B_LS : in STD_LOGIC;
    DRP_CLK_IN_N : in STD_LOGIC;
    DRP_CLK_IN_P : in STD_LOGIC;
    Q0_CLK1_GTREFCLK_PAD_N_IN : in STD_LOGIC;
    Q0_CLK1_GTREFCLK_PAD_P_IN : in STD_LOGIC;
    RXP0_IN : in STD_LOGIC;
    RXN0_IN : in STD_LOGIC;
    TXP0_OUT : out STD_LOGIC;
    TXN0_OUT : out STD_LOGIC
  );
  end component design_1;
begin
design_1_i: component design_1
    port map (
      DRP_CLK_IN_N => DRP_CLK_IN_N,
      DRP_CLK_IN_P => DRP_CLK_IN_P,
      GPIO_LED(3 downto 0) => GPIO_LED(3 downto 0),
      PCIE_PERST_B_LS => PCIE_PERST_B_LS,
      PCIE_REFCLK_N => PCIE_REFCLK_N,
      PCIE_REFCLK_P => PCIE_REFCLK_P,
      PCIE_RX_N(3 downto 0) => PCIE_RX_N(3 downto 0),
      PCIE_RX_P(3 downto 0) => PCIE_RX_P(3 downto 0),
      PCIE_TX_N(3 downto 0) => PCIE_TX_N(3 downto 0),
      PCIE_TX_P(3 downto 0) => PCIE_TX_P(3 downto 0),
      Q0_CLK1_GTREFCLK_PAD_N_IN => Q0_CLK1_GTREFCLK_PAD_N_IN,
      Q0_CLK1_GTREFCLK_PAD_P_IN => Q0_CLK1_GTREFCLK_PAD_P_IN,
      RXN0_IN => RXN0_IN,
      RXP0_IN => RXP0_IN,
      TXN0_OUT => TXN0_OUT,
      TXP0_OUT => TXP0_OUT,
      bus_clk => bus_clk,
      clock_rtl => clock_rtl,
      quiesce => quiesce,
      reset_rtl => reset_rtl,
      reset_rtl_0 => reset_rtl_0
    );
end STRUCTURE;
