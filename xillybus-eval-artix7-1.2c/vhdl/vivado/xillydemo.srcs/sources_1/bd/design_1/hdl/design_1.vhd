--Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2014.4 (win64) Build 1071353 Tue Nov 18 18:29:27 MST 2014
--Date        : Wed Apr 01 16:04:18 2015
--Host        : Titow running 64-bit major release  (build 9200)
--Command     : generate_target design_1.bd
--Design      : design_1
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1 is
  port (
    DRP_CLK_IN_N : in STD_LOGIC;
    DRP_CLK_IN_P : in STD_LOGIC;
    GPIO_LED : out STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_PERST_B_LS : in STD_LOGIC;
    PCIE_REFCLK_N : in STD_LOGIC;
    PCIE_REFCLK_P : in STD_LOGIC;
    PCIE_RX_N : in STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_RX_P : in STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_TX_N : out STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_TX_P : out STD_LOGIC_VECTOR ( 3 downto 0 );
    Q0_CLK1_GTREFCLK_PAD_N_IN : in STD_LOGIC;
    Q0_CLK1_GTREFCLK_PAD_P_IN : in STD_LOGIC;
    RXN0_IN : in STD_LOGIC;
    RXP0_IN : in STD_LOGIC;
    TXN0_OUT : out STD_LOGIC;
    TXP0_OUT : out STD_LOGIC;
    bus_clk : out STD_LOGIC;
    clock_rtl : in STD_LOGIC;
    quiesce : out STD_LOGIC;
    reset_rtl : in STD_LOGIC;
    reset_rtl_0 : in STD_LOGIC
  );
end design_1;

architecture STRUCTURE of design_1 is
  component design_1_AXI4_groundhog_SATA_0_0 is
  port (
    DRP_CLK_IN_N : in STD_LOGIC;
    DRP_CLK_IN_P : in STD_LOGIC;
    Q0_CLK1_GTREFCLK_PAD_N_IN : in STD_LOGIC;
    Q0_CLK1_GTREFCLK_PAD_P_IN : in STD_LOGIC;
    RXP0_IN : in STD_LOGIC;
    RXN0_IN : in STD_LOGIC;
    TXP0_OUT : out STD_LOGIC;
    TXN0_OUT : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_awid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s00_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s00_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_awlock : in STD_LOGIC;
    s00_axi_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_awregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_awuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s00_axi_wlast : in STD_LOGIC;
    s00_axi_wuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bid : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_buser : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_arid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s00_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s00_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_arlock : in STD_LOGIC;
    s00_axi_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aruser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rid : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rlast : out STD_LOGIC;
    s00_axi_ruser : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  end component design_1_AXI4_groundhog_SATA_0_0;
  component design_1_clk_wiz_0 is
  port (
    clk_in1 : in STD_LOGIC;
    clk_out1 : out STD_LOGIC;
    reset : in STD_LOGIC;
    locked : out STD_LOGIC
  );
  end component design_1_clk_wiz_0;
  component design_1_rst_clk_wiz_100M_0 is
  port (
    slowest_sync_clk : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    aux_reset_in : in STD_LOGIC;
    mb_debug_sys_rst : in STD_LOGIC;
    dcm_locked : in STD_LOGIC;
    mb_reset : out STD_LOGIC;
    bus_struct_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    interconnect_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_rst_clk_wiz_100M_0;
  component design_1_Xillybus_AXI_Master_0_4 is
  port (
    user_r_mem_8_rden : in STD_LOGIC;
    user_r_mem_8_empty : out STD_LOGIC;
    user_r_mem_8_data : out STD_LOGIC_VECTOR ( 7 downto 0 );
    user_r_mem_8_eof : out STD_LOGIC;
    user_r_mem_8_open : in STD_LOGIC;
    user_w_mem_8_wren : in STD_LOGIC;
    user_w_mem_8_full : out STD_LOGIC;
    user_w_mem_8_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    user_w_mem_8_open : in STD_LOGIC;
    user_mem_8_addr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    user_mem_8_addr_update : in STD_LOGIC;
    user_r_read_32_rden : in STD_LOGIC;
    user_r_read_32_empty : out STD_LOGIC;
    user_r_read_32_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    user_r_read_32_eof : out STD_LOGIC;
    user_r_read_32_open : in STD_LOGIC;
    user_r_read_8_rden : in STD_LOGIC;
    user_r_read_8_empty : out STD_LOGIC;
    user_r_read_8_data : out STD_LOGIC_VECTOR ( 7 downto 0 );
    user_r_read_8_eof : out STD_LOGIC;
    user_r_read_8_open : in STD_LOGIC;
    user_w_write_32_wren : in STD_LOGIC;
    user_w_write_32_full : out STD_LOGIC;
    user_w_write_32_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_w_write_32_open : in STD_LOGIC;
    user_w_write_8_wren : in STD_LOGIC;
    user_w_write_8_full : out STD_LOGIC;
    user_w_write_8_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    user_w_write_8_open : in STD_LOGIC;
    sata_axi_init_axi_txn : in STD_LOGIC;
    sata_axi_txn_done : out STD_LOGIC;
    sata_axi_error : out STD_LOGIC;
    sata_axi_aclk : in STD_LOGIC;
    sata_axi_aresetn : in STD_LOGIC;
    sata_axi_awid : out STD_LOGIC_VECTOR ( 0 to 0 );
    sata_axi_awaddr : out STD_LOGIC_VECTOR ( 63 downto 0 );
    sata_axi_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    sata_axi_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    sata_axi_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    sata_axi_awlock : out STD_LOGIC;
    sata_axi_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    sata_axi_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    sata_axi_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    sata_axi_awuser : out STD_LOGIC_VECTOR ( 0 to 0 );
    sata_axi_awvalid : out STD_LOGIC;
    sata_axi_awready : in STD_LOGIC;
    sata_axi_wdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    sata_axi_wstrb : out STD_LOGIC_VECTOR ( 63 downto 0 );
    sata_axi_wlast : out STD_LOGIC;
    sata_axi_wuser : out STD_LOGIC_VECTOR ( 0 to 0 );
    sata_axi_wvalid : out STD_LOGIC;
    sata_axi_wready : in STD_LOGIC;
    sata_axi_bid : in STD_LOGIC_VECTOR ( 0 to 0 );
    sata_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    sata_axi_buser : in STD_LOGIC_VECTOR ( 0 to 0 );
    sata_axi_bvalid : in STD_LOGIC;
    sata_axi_bready : out STD_LOGIC;
    sata_axi_arid : out STD_LOGIC_VECTOR ( 0 to 0 );
    sata_axi_araddr : out STD_LOGIC_VECTOR ( 63 downto 0 );
    sata_axi_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    sata_axi_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    sata_axi_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    sata_axi_arlock : out STD_LOGIC;
    sata_axi_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    sata_axi_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    sata_axi_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    sata_axi_aruser : out STD_LOGIC_VECTOR ( 0 to 0 );
    sata_axi_arvalid : out STD_LOGIC;
    sata_axi_arready : in STD_LOGIC;
    sata_axi_rid : in STD_LOGIC_VECTOR ( 0 to 0 );
    sata_axi_rdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    sata_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    sata_axi_rlast : in STD_LOGIC;
    sata_axi_ruser : in STD_LOGIC_VECTOR ( 0 to 0 );
    sata_axi_rvalid : in STD_LOGIC;
    sata_axi_rready : out STD_LOGIC;
    bram_axi_init_axi_txn : in STD_LOGIC;
    bram_axi_txn_done : out STD_LOGIC;
    bram_axi_error : out STD_LOGIC;
    bram_axi_aclk : in STD_LOGIC;
    bram_axi_aresetn : in STD_LOGIC;
    bram_axi_awid : out STD_LOGIC_VECTOR ( 0 to 0 );
    bram_axi_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_axi_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    bram_axi_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    bram_axi_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    bram_axi_awlock : out STD_LOGIC;
    bram_axi_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_axi_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    bram_axi_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_axi_awuser : out STD_LOGIC_VECTOR ( 0 to 0 );
    bram_axi_awvalid : out STD_LOGIC;
    bram_axi_awready : in STD_LOGIC;
    bram_axi_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_axi_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_axi_wlast : out STD_LOGIC;
    bram_axi_wuser : out STD_LOGIC_VECTOR ( 0 to 0 );
    bram_axi_wvalid : out STD_LOGIC;
    bram_axi_wready : in STD_LOGIC;
    bram_axi_bid : in STD_LOGIC_VECTOR ( 0 to 0 );
    bram_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    bram_axi_buser : in STD_LOGIC_VECTOR ( 0 to 0 );
    bram_axi_bvalid : in STD_LOGIC;
    bram_axi_bready : out STD_LOGIC;
    bram_axi_arid : out STD_LOGIC_VECTOR ( 0 to 0 );
    bram_axi_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_axi_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    bram_axi_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    bram_axi_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    bram_axi_arlock : out STD_LOGIC;
    bram_axi_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_axi_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    bram_axi_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_axi_aruser : out STD_LOGIC_VECTOR ( 0 to 0 );
    bram_axi_arvalid : out STD_LOGIC;
    bram_axi_arready : in STD_LOGIC;
    bram_axi_rid : in STD_LOGIC_VECTOR ( 0 to 0 );
    bram_axi_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    bram_axi_rlast : in STD_LOGIC;
    bram_axi_ruser : in STD_LOGIC_VECTOR ( 0 to 0 );
    bram_axi_rvalid : in STD_LOGIC;
    bram_axi_rready : out STD_LOGIC;
    dram_axi_init_axi_txn : in STD_LOGIC;
    dram_axi_txn_done : out STD_LOGIC;
    dram_axi_error : out STD_LOGIC;
    dram_axi_aclk : in STD_LOGIC;
    dram_axi_aresetn : in STD_LOGIC;
    dram_axi_awid : out STD_LOGIC_VECTOR ( 0 to 0 );
    dram_axi_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    dram_axi_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dram_axi_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    dram_axi_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    dram_axi_awlock : out STD_LOGIC;
    dram_axi_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    dram_axi_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    dram_axi_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    dram_axi_awuser : out STD_LOGIC_VECTOR ( 0 to 0 );
    dram_axi_awvalid : out STD_LOGIC;
    dram_axi_awready : in STD_LOGIC;
    dram_axi_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    dram_axi_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    dram_axi_wlast : out STD_LOGIC;
    dram_axi_wuser : out STD_LOGIC_VECTOR ( 0 to 0 );
    dram_axi_wvalid : out STD_LOGIC;
    dram_axi_wready : in STD_LOGIC;
    dram_axi_bid : in STD_LOGIC_VECTOR ( 0 to 0 );
    dram_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    dram_axi_buser : in STD_LOGIC_VECTOR ( 0 to 0 );
    dram_axi_bvalid : in STD_LOGIC;
    dram_axi_bready : out STD_LOGIC;
    dram_axi_arid : out STD_LOGIC_VECTOR ( 0 to 0 );
    dram_axi_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    dram_axi_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dram_axi_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    dram_axi_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    dram_axi_arlock : out STD_LOGIC;
    dram_axi_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    dram_axi_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    dram_axi_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    dram_axi_aruser : out STD_LOGIC_VECTOR ( 0 to 0 );
    dram_axi_arvalid : out STD_LOGIC;
    dram_axi_arready : in STD_LOGIC;
    dram_axi_rid : in STD_LOGIC_VECTOR ( 0 to 0 );
    dram_axi_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dram_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    dram_axi_rlast : in STD_LOGIC;
    dram_axi_ruser : in STD_LOGIC_VECTOR ( 0 to 0 );
    dram_axi_rvalid : in STD_LOGIC;
    dram_axi_rready : out STD_LOGIC
  );
  end component design_1_Xillybus_AXI_Master_0_4;
  component design_1_xillybus_0_1 is
  port (
    PCIE_TX_P : out STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_TX_N : out STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_RX_P : in STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_RX_N : in STD_LOGIC_VECTOR ( 3 downto 0 );
    PCIE_REFCLK_P : in STD_LOGIC;
    PCIE_REFCLK_N : in STD_LOGIC;
    PCIE_PERST_B_LS : in STD_LOGIC;
    bus_clk : out STD_LOGIC;
    quiesce : out STD_LOGIC;
    GPIO_LED : out STD_LOGIC_VECTOR ( 3 downto 0 );
    user_r_read_32_rden : out STD_LOGIC;
    user_r_read_32_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    user_r_read_32_empty : in STD_LOGIC;
    user_r_read_32_eof : in STD_LOGIC;
    user_r_read_32_open : out STD_LOGIC;
    user_w_write_32_wren : out STD_LOGIC;
    user_w_write_32_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    user_w_write_32_full : in STD_LOGIC;
    user_w_write_32_open : out STD_LOGIC;
    user_r_read_8_rden : out STD_LOGIC;
    user_r_read_8_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    user_r_read_8_empty : in STD_LOGIC;
    user_r_read_8_eof : in STD_LOGIC;
    user_r_read_8_open : out STD_LOGIC;
    user_w_write_8_wren : out STD_LOGIC;
    user_w_write_8_data : out STD_LOGIC_VECTOR ( 7 downto 0 );
    user_w_write_8_full : in STD_LOGIC;
    user_w_write_8_open : out STD_LOGIC;
    user_r_mem_8_rden : out STD_LOGIC;
    user_r_mem_8_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    user_r_mem_8_empty : in STD_LOGIC;
    user_r_mem_8_eof : in STD_LOGIC;
    user_r_mem_8_open : out STD_LOGIC;
    user_w_mem_8_wren : out STD_LOGIC;
    user_w_mem_8_data : out STD_LOGIC_VECTOR ( 7 downto 0 );
    user_w_mem_8_full : in STD_LOGIC;
    user_w_mem_8_open : out STD_LOGIC;
    user_mem_8_addr : out STD_LOGIC_VECTOR ( 4 downto 0 );
    user_mem_8_addr_update : out STD_LOGIC
  );
  end component design_1_xillybus_0_1;
  signal AXI4_groundhog_SATA_0_TXN0_OUT : STD_LOGIC;
  signal AXI4_groundhog_SATA_0_TXP0_OUT : STD_LOGIC;
  signal DRP_CLK_IN_N_1 : STD_LOGIC;
  signal DRP_CLK_IN_P_1 : STD_LOGIC;
  signal GND_1 : STD_LOGIC;
  signal Q0_CLK1_GTREFCLK_PAD_N_IN_1 : STD_LOGIC;
  signal Q0_CLK1_GTREFCLK_PAD_P_IN_1 : STD_LOGIC;
  signal RXN0_IN_1 : STD_LOGIC;
  signal RXP0_IN_1 : STD_LOGIC;
  signal VCC_1 : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_ARADDR : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_ARBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_ARCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_ARID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_ARLEN : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_ARLOCK : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_ARQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_ARREADY : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_ARSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_ARUSER : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_ARVALID : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_AWADDR : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_AWBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_AWCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_AWID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_AWLEN : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_AWLOCK : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_AWQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_AWREADY : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_AWSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_AWUSER : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_AWVALID : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_BID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_BREADY : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_BUSER : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_BVALID : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_RDATA : STD_LOGIC_VECTOR ( 511 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_RID : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_RLAST : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_RREADY : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_RUSER : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_RVALID : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_WDATA : STD_LOGIC_VECTOR ( 511 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_WLAST : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_WREADY : STD_LOGIC;
  signal Xillybus_AXI_Master_0_SATA_AXI_WSTRB : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_WUSER : STD_LOGIC_VECTOR ( 0 to 0 );
  signal Xillybus_AXI_Master_0_SATA_AXI_WVALID : STD_LOGIC;
  signal Xillybus_AXI_Master_0_user_r_mem_8_data : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal Xillybus_AXI_Master_0_user_r_mem_8_empty : STD_LOGIC;
  signal Xillybus_AXI_Master_0_user_r_mem_8_eof : STD_LOGIC;
  signal Xillybus_AXI_Master_0_user_r_read_32_data : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Xillybus_AXI_Master_0_user_r_read_32_empty : STD_LOGIC;
  signal Xillybus_AXI_Master_0_user_r_read_32_eof : STD_LOGIC;
  signal Xillybus_AXI_Master_0_user_r_read_8_data : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal Xillybus_AXI_Master_0_user_r_read_8_empty : STD_LOGIC;
  signal Xillybus_AXI_Master_0_user_r_read_8_eof : STD_LOGIC;
  signal Xillybus_AXI_Master_0_user_w_mem_8_full : STD_LOGIC;
  signal Xillybus_AXI_Master_0_user_w_write_32_full : STD_LOGIC;
  signal Xillybus_AXI_Master_0_user_w_write_8_full : STD_LOGIC;
  signal clk_wiz_clk_out1 : STD_LOGIC;
  signal clk_wiz_locked : STD_LOGIC;
  signal clock_rtl_1 : STD_LOGIC;
  signal reset_rtl_0_1 : STD_LOGIC;
  signal reset_rtl_1 : STD_LOGIC;
  signal rst_clk_wiz_100M_peripheral_aresetn : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xillybus_0_user_mem_8_addr : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal xillybus_0_user_mem_8_addr_update : STD_LOGIC;
  signal xillybus_0_user_r_mem_8_open : STD_LOGIC;
  signal xillybus_0_user_r_mem_8_rden : STD_LOGIC;
  signal xillybus_0_user_r_read_32_open : STD_LOGIC;
  signal xillybus_0_user_r_read_32_rden : STD_LOGIC;
  signal xillybus_0_user_r_read_8_open : STD_LOGIC;
  signal xillybus_0_user_r_read_8_rden : STD_LOGIC;
  signal xillybus_0_user_w_mem_8_data : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal xillybus_0_user_w_mem_8_open : STD_LOGIC;
  signal xillybus_0_user_w_mem_8_wren : STD_LOGIC;
  signal xillybus_0_user_w_write_32_data : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal xillybus_0_user_w_write_32_open : STD_LOGIC;
  signal xillybus_0_user_w_write_32_wren : STD_LOGIC;
  signal xillybus_0_user_w_write_8_data : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal xillybus_0_user_w_write_8_open : STD_LOGIC;
  signal xillybus_0_user_w_write_8_wren : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_bram_axi_arlock_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_bram_axi_arvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_bram_axi_awlock_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_bram_axi_awvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_bram_axi_bready_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_bram_axi_error_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_bram_axi_rready_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_bram_axi_txn_done_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_bram_axi_wlast_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_bram_axi_wvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_dram_axi_arlock_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_dram_axi_arvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_dram_axi_awlock_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_dram_axi_awvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_dram_axi_bready_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_dram_axi_error_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_dram_axi_rready_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_dram_axi_txn_done_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_dram_axi_wlast_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_dram_axi_wvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_sata_axi_error_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_sata_axi_txn_done_UNCONNECTED : STD_LOGIC;
  signal NLW_Xillybus_AXI_Master_0_bram_axi_araddr_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_arburst_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_arcache_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_arid_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_arlen_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_arprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_arqos_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_arsize_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_aruser_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_awaddr_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_awburst_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_awcache_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_awid_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_awlen_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_awprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_awqos_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_awsize_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_awuser_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_wdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_wstrb_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_bram_axi_wuser_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_araddr_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_arburst_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_arcache_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_arid_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_arlen_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_arprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_arqos_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_arsize_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_aruser_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_awaddr_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_awburst_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_awcache_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_awid_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_awlen_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_awprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_awqos_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_awsize_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_awuser_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_wdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_wstrb_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_Xillybus_AXI_Master_0_dram_axi_wuser_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_rst_clk_wiz_100M_mb_reset_UNCONNECTED : STD_LOGIC;
  signal NLW_rst_clk_wiz_100M_bus_struct_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_rst_clk_wiz_100M_interconnect_aresetn_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_rst_clk_wiz_100M_peripheral_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_xillybus_0_bus_clk_UNCONNECTED : STD_LOGIC;
  signal NLW_xillybus_0_quiesce_UNCONNECTED : STD_LOGIC;
  signal NLW_xillybus_0_GPIO_LED_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_xillybus_0_PCIE_TX_N_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_xillybus_0_PCIE_TX_P_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
  DRP_CLK_IN_N_1 <= DRP_CLK_IN_N;
  DRP_CLK_IN_P_1 <= DRP_CLK_IN_P;
  Q0_CLK1_GTREFCLK_PAD_N_IN_1 <= Q0_CLK1_GTREFCLK_PAD_N_IN;
  Q0_CLK1_GTREFCLK_PAD_P_IN_1 <= Q0_CLK1_GTREFCLK_PAD_P_IN;
  RXN0_IN_1 <= RXN0_IN;
  RXP0_IN_1 <= RXP0_IN;
  TXN0_OUT <= AXI4_groundhog_SATA_0_TXN0_OUT;
  TXP0_OUT <= AXI4_groundhog_SATA_0_TXP0_OUT;
  clock_rtl_1 <= clock_rtl;
  reset_rtl_0_1 <= reset_rtl_0;
  reset_rtl_1 <= reset_rtl;
  bus_clk <= 'Z';
  quiesce <= 'Z';
  GPIO_LED(0) <= 'Z';
  GPIO_LED(1) <= 'Z';
  GPIO_LED(2) <= 'Z';
  GPIO_LED(3) <= 'Z';
  PCIE_TX_N(0) <= 'Z';
  PCIE_TX_N(1) <= 'Z';
  PCIE_TX_N(2) <= 'Z';
  PCIE_TX_N(3) <= 'Z';
  PCIE_TX_P(0) <= 'Z';
  PCIE_TX_P(1) <= 'Z';
  PCIE_TX_P(2) <= 'Z';
  PCIE_TX_P(3) <= 'Z';
AXI4_groundhog_SATA_0: component design_1_AXI4_groundhog_SATA_0_0
    port map (
      DRP_CLK_IN_N => DRP_CLK_IN_N_1,
      DRP_CLK_IN_P => DRP_CLK_IN_P_1,
      Q0_CLK1_GTREFCLK_PAD_N_IN => Q0_CLK1_GTREFCLK_PAD_N_IN_1,
      Q0_CLK1_GTREFCLK_PAD_P_IN => Q0_CLK1_GTREFCLK_PAD_P_IN_1,
      RXN0_IN => RXN0_IN_1,
      RXP0_IN => RXP0_IN_1,
      TXN0_OUT => AXI4_groundhog_SATA_0_TXN0_OUT,
      TXP0_OUT => AXI4_groundhog_SATA_0_TXP0_OUT,
      s00_axi_aclk => clk_wiz_clk_out1,
      s00_axi_araddr(63 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARADDR(63 downto 0),
      s00_axi_arburst(1 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARBURST(1 downto 0),
      s00_axi_arcache(3 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARCACHE(3 downto 0),
      s00_axi_aresetn => rst_clk_wiz_100M_peripheral_aresetn(0),
      s00_axi_arid(0) => Xillybus_AXI_Master_0_SATA_AXI_ARID(0),
      s00_axi_arlen(7 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARLEN(7 downto 0),
      s00_axi_arlock => Xillybus_AXI_Master_0_SATA_AXI_ARLOCK,
      s00_axi_arprot(2 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARPROT(2 downto 0),
      s00_axi_arqos(3 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARQOS(3 downto 0),
      s00_axi_arready => Xillybus_AXI_Master_0_SATA_AXI_ARREADY,
      s00_axi_arregion(3) => GND_1,
      s00_axi_arregion(2) => GND_1,
      s00_axi_arregion(1) => GND_1,
      s00_axi_arregion(0) => GND_1,
      s00_axi_arsize(2 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARSIZE(2 downto 0),
      s00_axi_aruser(0) => Xillybus_AXI_Master_0_SATA_AXI_ARUSER(0),
      s00_axi_arvalid => Xillybus_AXI_Master_0_SATA_AXI_ARVALID,
      s00_axi_awaddr(63 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWADDR(63 downto 0),
      s00_axi_awburst(1 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWBURST(1 downto 0),
      s00_axi_awcache(3 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWCACHE(3 downto 0),
      s00_axi_awid(0) => Xillybus_AXI_Master_0_SATA_AXI_AWID(0),
      s00_axi_awlen(7 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWLEN(7 downto 0),
      s00_axi_awlock => Xillybus_AXI_Master_0_SATA_AXI_AWLOCK,
      s00_axi_awprot(2 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWPROT(2 downto 0),
      s00_axi_awqos(3 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWQOS(3 downto 0),
      s00_axi_awready => Xillybus_AXI_Master_0_SATA_AXI_AWREADY,
      s00_axi_awregion(3) => GND_1,
      s00_axi_awregion(2) => GND_1,
      s00_axi_awregion(1) => GND_1,
      s00_axi_awregion(0) => GND_1,
      s00_axi_awsize(2 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWSIZE(2 downto 0),
      s00_axi_awuser(0) => Xillybus_AXI_Master_0_SATA_AXI_AWUSER(0),
      s00_axi_awvalid => Xillybus_AXI_Master_0_SATA_AXI_AWVALID,
      s00_axi_bid(0) => Xillybus_AXI_Master_0_SATA_AXI_BID(0),
      s00_axi_bready => Xillybus_AXI_Master_0_SATA_AXI_BREADY,
      s00_axi_bresp(1 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_BRESP(1 downto 0),
      s00_axi_buser(0) => Xillybus_AXI_Master_0_SATA_AXI_BUSER(0),
      s00_axi_bvalid => Xillybus_AXI_Master_0_SATA_AXI_BVALID,
      s00_axi_rdata(511 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_RDATA(511 downto 0),
      s00_axi_rid(0) => Xillybus_AXI_Master_0_SATA_AXI_RID(0),
      s00_axi_rlast => Xillybus_AXI_Master_0_SATA_AXI_RLAST,
      s00_axi_rready => Xillybus_AXI_Master_0_SATA_AXI_RREADY,
      s00_axi_rresp(1 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_RRESP(1 downto 0),
      s00_axi_ruser(0) => Xillybus_AXI_Master_0_SATA_AXI_RUSER(0),
      s00_axi_rvalid => Xillybus_AXI_Master_0_SATA_AXI_RVALID,
      s00_axi_wdata(511 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_WDATA(511 downto 0),
      s00_axi_wlast => Xillybus_AXI_Master_0_SATA_AXI_WLAST,
      s00_axi_wready => Xillybus_AXI_Master_0_SATA_AXI_WREADY,
      s00_axi_wstrb(63 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_WSTRB(63 downto 0),
      s00_axi_wuser(0) => Xillybus_AXI_Master_0_SATA_AXI_WUSER(0),
      s00_axi_wvalid => Xillybus_AXI_Master_0_SATA_AXI_WVALID
    );
GND: unisim.vcomponents.GND
    port map (
      G => GND_1
    );
VCC: unisim.vcomponents.VCC
    port map (
      P => VCC_1
    );
Xillybus_AXI_Master_0: component design_1_Xillybus_AXI_Master_0_4
    port map (
      bram_axi_aclk => clk_wiz_clk_out1,
      bram_axi_araddr(31 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_araddr_UNCONNECTED(31 downto 0),
      bram_axi_arburst(1 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_arburst_UNCONNECTED(1 downto 0),
      bram_axi_arcache(3 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_arcache_UNCONNECTED(3 downto 0),
      bram_axi_aresetn => GND_1,
      bram_axi_arid(0) => NLW_Xillybus_AXI_Master_0_bram_axi_arid_UNCONNECTED(0),
      bram_axi_arlen(7 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_arlen_UNCONNECTED(7 downto 0),
      bram_axi_arlock => NLW_Xillybus_AXI_Master_0_bram_axi_arlock_UNCONNECTED,
      bram_axi_arprot(2 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_arprot_UNCONNECTED(2 downto 0),
      bram_axi_arqos(3 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_arqos_UNCONNECTED(3 downto 0),
      bram_axi_arready => GND_1,
      bram_axi_arsize(2 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_arsize_UNCONNECTED(2 downto 0),
      bram_axi_aruser(0) => NLW_Xillybus_AXI_Master_0_bram_axi_aruser_UNCONNECTED(0),
      bram_axi_arvalid => NLW_Xillybus_AXI_Master_0_bram_axi_arvalid_UNCONNECTED,
      bram_axi_awaddr(31 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_awaddr_UNCONNECTED(31 downto 0),
      bram_axi_awburst(1 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_awburst_UNCONNECTED(1 downto 0),
      bram_axi_awcache(3 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_awcache_UNCONNECTED(3 downto 0),
      bram_axi_awid(0) => NLW_Xillybus_AXI_Master_0_bram_axi_awid_UNCONNECTED(0),
      bram_axi_awlen(7 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_awlen_UNCONNECTED(7 downto 0),
      bram_axi_awlock => NLW_Xillybus_AXI_Master_0_bram_axi_awlock_UNCONNECTED,
      bram_axi_awprot(2 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_awprot_UNCONNECTED(2 downto 0),
      bram_axi_awqos(3 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_awqos_UNCONNECTED(3 downto 0),
      bram_axi_awready => GND_1,
      bram_axi_awsize(2 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_awsize_UNCONNECTED(2 downto 0),
      bram_axi_awuser(0) => NLW_Xillybus_AXI_Master_0_bram_axi_awuser_UNCONNECTED(0),
      bram_axi_awvalid => NLW_Xillybus_AXI_Master_0_bram_axi_awvalid_UNCONNECTED,
      bram_axi_bid(0) => GND_1,
      bram_axi_bready => NLW_Xillybus_AXI_Master_0_bram_axi_bready_UNCONNECTED,
      bram_axi_bresp(1) => GND_1,
      bram_axi_bresp(0) => GND_1,
      bram_axi_buser(0) => GND_1,
      bram_axi_bvalid => GND_1,
      bram_axi_error => NLW_Xillybus_AXI_Master_0_bram_axi_error_UNCONNECTED,
      bram_axi_init_axi_txn => GND_1,
      bram_axi_rdata(31) => GND_1,
      bram_axi_rdata(30) => GND_1,
      bram_axi_rdata(29) => GND_1,
      bram_axi_rdata(28) => GND_1,
      bram_axi_rdata(27) => GND_1,
      bram_axi_rdata(26) => GND_1,
      bram_axi_rdata(25) => GND_1,
      bram_axi_rdata(24) => GND_1,
      bram_axi_rdata(23) => GND_1,
      bram_axi_rdata(22) => GND_1,
      bram_axi_rdata(21) => GND_1,
      bram_axi_rdata(20) => GND_1,
      bram_axi_rdata(19) => GND_1,
      bram_axi_rdata(18) => GND_1,
      bram_axi_rdata(17) => GND_1,
      bram_axi_rdata(16) => GND_1,
      bram_axi_rdata(15) => GND_1,
      bram_axi_rdata(14) => GND_1,
      bram_axi_rdata(13) => GND_1,
      bram_axi_rdata(12) => GND_1,
      bram_axi_rdata(11) => GND_1,
      bram_axi_rdata(10) => GND_1,
      bram_axi_rdata(9) => GND_1,
      bram_axi_rdata(8) => GND_1,
      bram_axi_rdata(7) => GND_1,
      bram_axi_rdata(6) => GND_1,
      bram_axi_rdata(5) => GND_1,
      bram_axi_rdata(4) => GND_1,
      bram_axi_rdata(3) => GND_1,
      bram_axi_rdata(2) => GND_1,
      bram_axi_rdata(1) => GND_1,
      bram_axi_rdata(0) => GND_1,
      bram_axi_rid(0) => GND_1,
      bram_axi_rlast => GND_1,
      bram_axi_rready => NLW_Xillybus_AXI_Master_0_bram_axi_rready_UNCONNECTED,
      bram_axi_rresp(1) => GND_1,
      bram_axi_rresp(0) => GND_1,
      bram_axi_ruser(0) => GND_1,
      bram_axi_rvalid => GND_1,
      bram_axi_txn_done => NLW_Xillybus_AXI_Master_0_bram_axi_txn_done_UNCONNECTED,
      bram_axi_wdata(31 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_wdata_UNCONNECTED(31 downto 0),
      bram_axi_wlast => NLW_Xillybus_AXI_Master_0_bram_axi_wlast_UNCONNECTED,
      bram_axi_wready => GND_1,
      bram_axi_wstrb(3 downto 0) => NLW_Xillybus_AXI_Master_0_bram_axi_wstrb_UNCONNECTED(3 downto 0),
      bram_axi_wuser(0) => NLW_Xillybus_AXI_Master_0_bram_axi_wuser_UNCONNECTED(0),
      bram_axi_wvalid => NLW_Xillybus_AXI_Master_0_bram_axi_wvalid_UNCONNECTED,
      dram_axi_aclk => clk_wiz_clk_out1,
      dram_axi_araddr(31 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_araddr_UNCONNECTED(31 downto 0),
      dram_axi_arburst(1 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_arburst_UNCONNECTED(1 downto 0),
      dram_axi_arcache(3 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_arcache_UNCONNECTED(3 downto 0),
      dram_axi_aresetn => rst_clk_wiz_100M_peripheral_aresetn(0),
      dram_axi_arid(0) => NLW_Xillybus_AXI_Master_0_dram_axi_arid_UNCONNECTED(0),
      dram_axi_arlen(7 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_arlen_UNCONNECTED(7 downto 0),
      dram_axi_arlock => NLW_Xillybus_AXI_Master_0_dram_axi_arlock_UNCONNECTED,
      dram_axi_arprot(2 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_arprot_UNCONNECTED(2 downto 0),
      dram_axi_arqos(3 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_arqos_UNCONNECTED(3 downto 0),
      dram_axi_arready => GND_1,
      dram_axi_arsize(2 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_arsize_UNCONNECTED(2 downto 0),
      dram_axi_aruser(0) => NLW_Xillybus_AXI_Master_0_dram_axi_aruser_UNCONNECTED(0),
      dram_axi_arvalid => NLW_Xillybus_AXI_Master_0_dram_axi_arvalid_UNCONNECTED,
      dram_axi_awaddr(31 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_awaddr_UNCONNECTED(31 downto 0),
      dram_axi_awburst(1 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_awburst_UNCONNECTED(1 downto 0),
      dram_axi_awcache(3 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_awcache_UNCONNECTED(3 downto 0),
      dram_axi_awid(0) => NLW_Xillybus_AXI_Master_0_dram_axi_awid_UNCONNECTED(0),
      dram_axi_awlen(7 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_awlen_UNCONNECTED(7 downto 0),
      dram_axi_awlock => NLW_Xillybus_AXI_Master_0_dram_axi_awlock_UNCONNECTED,
      dram_axi_awprot(2 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_awprot_UNCONNECTED(2 downto 0),
      dram_axi_awqos(3 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_awqos_UNCONNECTED(3 downto 0),
      dram_axi_awready => GND_1,
      dram_axi_awsize(2 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_awsize_UNCONNECTED(2 downto 0),
      dram_axi_awuser(0) => NLW_Xillybus_AXI_Master_0_dram_axi_awuser_UNCONNECTED(0),
      dram_axi_awvalid => NLW_Xillybus_AXI_Master_0_dram_axi_awvalid_UNCONNECTED,
      dram_axi_bid(0) => GND_1,
      dram_axi_bready => NLW_Xillybus_AXI_Master_0_dram_axi_bready_UNCONNECTED,
      dram_axi_bresp(1) => GND_1,
      dram_axi_bresp(0) => GND_1,
      dram_axi_buser(0) => GND_1,
      dram_axi_bvalid => GND_1,
      dram_axi_error => NLW_Xillybus_AXI_Master_0_dram_axi_error_UNCONNECTED,
      dram_axi_init_axi_txn => GND_1,
      dram_axi_rdata(31) => GND_1,
      dram_axi_rdata(30) => GND_1,
      dram_axi_rdata(29) => GND_1,
      dram_axi_rdata(28) => GND_1,
      dram_axi_rdata(27) => GND_1,
      dram_axi_rdata(26) => GND_1,
      dram_axi_rdata(25) => GND_1,
      dram_axi_rdata(24) => GND_1,
      dram_axi_rdata(23) => GND_1,
      dram_axi_rdata(22) => GND_1,
      dram_axi_rdata(21) => GND_1,
      dram_axi_rdata(20) => GND_1,
      dram_axi_rdata(19) => GND_1,
      dram_axi_rdata(18) => GND_1,
      dram_axi_rdata(17) => GND_1,
      dram_axi_rdata(16) => GND_1,
      dram_axi_rdata(15) => GND_1,
      dram_axi_rdata(14) => GND_1,
      dram_axi_rdata(13) => GND_1,
      dram_axi_rdata(12) => GND_1,
      dram_axi_rdata(11) => GND_1,
      dram_axi_rdata(10) => GND_1,
      dram_axi_rdata(9) => GND_1,
      dram_axi_rdata(8) => GND_1,
      dram_axi_rdata(7) => GND_1,
      dram_axi_rdata(6) => GND_1,
      dram_axi_rdata(5) => GND_1,
      dram_axi_rdata(4) => GND_1,
      dram_axi_rdata(3) => GND_1,
      dram_axi_rdata(2) => GND_1,
      dram_axi_rdata(1) => GND_1,
      dram_axi_rdata(0) => GND_1,
      dram_axi_rid(0) => GND_1,
      dram_axi_rlast => GND_1,
      dram_axi_rready => NLW_Xillybus_AXI_Master_0_dram_axi_rready_UNCONNECTED,
      dram_axi_rresp(1) => GND_1,
      dram_axi_rresp(0) => GND_1,
      dram_axi_ruser(0) => GND_1,
      dram_axi_rvalid => GND_1,
      dram_axi_txn_done => NLW_Xillybus_AXI_Master_0_dram_axi_txn_done_UNCONNECTED,
      dram_axi_wdata(31 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_wdata_UNCONNECTED(31 downto 0),
      dram_axi_wlast => NLW_Xillybus_AXI_Master_0_dram_axi_wlast_UNCONNECTED,
      dram_axi_wready => GND_1,
      dram_axi_wstrb(3 downto 0) => NLW_Xillybus_AXI_Master_0_dram_axi_wstrb_UNCONNECTED(3 downto 0),
      dram_axi_wuser(0) => NLW_Xillybus_AXI_Master_0_dram_axi_wuser_UNCONNECTED(0),
      dram_axi_wvalid => NLW_Xillybus_AXI_Master_0_dram_axi_wvalid_UNCONNECTED,
      sata_axi_aclk => clk_wiz_clk_out1,
      sata_axi_araddr(63 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARADDR(63 downto 0),
      sata_axi_arburst(1 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARBURST(1 downto 0),
      sata_axi_arcache(3 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARCACHE(3 downto 0),
      sata_axi_aresetn => GND_1,
      sata_axi_arid(0) => Xillybus_AXI_Master_0_SATA_AXI_ARID(0),
      sata_axi_arlen(7 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARLEN(7 downto 0),
      sata_axi_arlock => Xillybus_AXI_Master_0_SATA_AXI_ARLOCK,
      sata_axi_arprot(2 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARPROT(2 downto 0),
      sata_axi_arqos(3 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARQOS(3 downto 0),
      sata_axi_arready => Xillybus_AXI_Master_0_SATA_AXI_ARREADY,
      sata_axi_arsize(2 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_ARSIZE(2 downto 0),
      sata_axi_aruser(0) => Xillybus_AXI_Master_0_SATA_AXI_ARUSER(0),
      sata_axi_arvalid => Xillybus_AXI_Master_0_SATA_AXI_ARVALID,
      sata_axi_awaddr(63 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWADDR(63 downto 0),
      sata_axi_awburst(1 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWBURST(1 downto 0),
      sata_axi_awcache(3 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWCACHE(3 downto 0),
      sata_axi_awid(0) => Xillybus_AXI_Master_0_SATA_AXI_AWID(0),
      sata_axi_awlen(7 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWLEN(7 downto 0),
      sata_axi_awlock => Xillybus_AXI_Master_0_SATA_AXI_AWLOCK,
      sata_axi_awprot(2 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWPROT(2 downto 0),
      sata_axi_awqos(3 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWQOS(3 downto 0),
      sata_axi_awready => Xillybus_AXI_Master_0_SATA_AXI_AWREADY,
      sata_axi_awsize(2 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_AWSIZE(2 downto 0),
      sata_axi_awuser(0) => Xillybus_AXI_Master_0_SATA_AXI_AWUSER(0),
      sata_axi_awvalid => Xillybus_AXI_Master_0_SATA_AXI_AWVALID,
      sata_axi_bid(0) => Xillybus_AXI_Master_0_SATA_AXI_BID(0),
      sata_axi_bready => Xillybus_AXI_Master_0_SATA_AXI_BREADY,
      sata_axi_bresp(1 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_BRESP(1 downto 0),
      sata_axi_buser(0) => Xillybus_AXI_Master_0_SATA_AXI_BUSER(0),
      sata_axi_bvalid => Xillybus_AXI_Master_0_SATA_AXI_BVALID,
      sata_axi_error => NLW_Xillybus_AXI_Master_0_sata_axi_error_UNCONNECTED,
      sata_axi_init_axi_txn => GND_1,
      sata_axi_rdata(511 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_RDATA(511 downto 0),
      sata_axi_rid(0) => Xillybus_AXI_Master_0_SATA_AXI_RID(0),
      sata_axi_rlast => Xillybus_AXI_Master_0_SATA_AXI_RLAST,
      sata_axi_rready => Xillybus_AXI_Master_0_SATA_AXI_RREADY,
      sata_axi_rresp(1 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_RRESP(1 downto 0),
      sata_axi_ruser(0) => Xillybus_AXI_Master_0_SATA_AXI_RUSER(0),
      sata_axi_rvalid => Xillybus_AXI_Master_0_SATA_AXI_RVALID,
      sata_axi_txn_done => NLW_Xillybus_AXI_Master_0_sata_axi_txn_done_UNCONNECTED,
      sata_axi_wdata(511 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_WDATA(511 downto 0),
      sata_axi_wlast => Xillybus_AXI_Master_0_SATA_AXI_WLAST,
      sata_axi_wready => Xillybus_AXI_Master_0_SATA_AXI_WREADY,
      sata_axi_wstrb(63 downto 0) => Xillybus_AXI_Master_0_SATA_AXI_WSTRB(63 downto 0),
      sata_axi_wuser(0) => Xillybus_AXI_Master_0_SATA_AXI_WUSER(0),
      sata_axi_wvalid => Xillybus_AXI_Master_0_SATA_AXI_WVALID,
      user_mem_8_addr(4 downto 0) => xillybus_0_user_mem_8_addr(4 downto 0),
      user_mem_8_addr_update => xillybus_0_user_mem_8_addr_update,
      user_r_mem_8_data(7 downto 0) => Xillybus_AXI_Master_0_user_r_mem_8_data(7 downto 0),
      user_r_mem_8_empty => Xillybus_AXI_Master_0_user_r_mem_8_empty,
      user_r_mem_8_eof => Xillybus_AXI_Master_0_user_r_mem_8_eof,
      user_r_mem_8_open => xillybus_0_user_r_mem_8_open,
      user_r_mem_8_rden => xillybus_0_user_r_mem_8_rden,
      user_r_read_32_data(31 downto 0) => Xillybus_AXI_Master_0_user_r_read_32_data(31 downto 0),
      user_r_read_32_empty => Xillybus_AXI_Master_0_user_r_read_32_empty,
      user_r_read_32_eof => Xillybus_AXI_Master_0_user_r_read_32_eof,
      user_r_read_32_open => xillybus_0_user_r_read_32_open,
      user_r_read_32_rden => xillybus_0_user_r_read_32_rden,
      user_r_read_8_data(7 downto 0) => Xillybus_AXI_Master_0_user_r_read_8_data(7 downto 0),
      user_r_read_8_empty => Xillybus_AXI_Master_0_user_r_read_8_empty,
      user_r_read_8_eof => Xillybus_AXI_Master_0_user_r_read_8_eof,
      user_r_read_8_open => xillybus_0_user_r_read_8_open,
      user_r_read_8_rden => xillybus_0_user_r_read_8_rden,
      user_w_mem_8_data(7 downto 0) => xillybus_0_user_w_mem_8_data(7 downto 0),
      user_w_mem_8_full => Xillybus_AXI_Master_0_user_w_mem_8_full,
      user_w_mem_8_open => xillybus_0_user_w_mem_8_open,
      user_w_mem_8_wren => xillybus_0_user_w_mem_8_wren,
      user_w_write_32_data(31 downto 0) => xillybus_0_user_w_write_32_data(31 downto 0),
      user_w_write_32_full => Xillybus_AXI_Master_0_user_w_write_32_full,
      user_w_write_32_open => xillybus_0_user_w_write_32_open,
      user_w_write_32_wren => xillybus_0_user_w_write_32_wren,
      user_w_write_8_data(7 downto 0) => xillybus_0_user_w_write_8_data(7 downto 0),
      user_w_write_8_full => Xillybus_AXI_Master_0_user_w_write_8_full,
      user_w_write_8_open => xillybus_0_user_w_write_8_open,
      user_w_write_8_wren => xillybus_0_user_w_write_8_wren
    );
clk_wiz: component design_1_clk_wiz_0
    port map (
      clk_in1 => clock_rtl_1,
      clk_out1 => clk_wiz_clk_out1,
      locked => clk_wiz_locked,
      reset => reset_rtl_1
    );
rst_clk_wiz_100M: component design_1_rst_clk_wiz_100M_0
    port map (
      aux_reset_in => VCC_1,
      bus_struct_reset(0) => NLW_rst_clk_wiz_100M_bus_struct_reset_UNCONNECTED(0),
      dcm_locked => clk_wiz_locked,
      ext_reset_in => reset_rtl_0_1,
      interconnect_aresetn(0) => NLW_rst_clk_wiz_100M_interconnect_aresetn_UNCONNECTED(0),
      mb_debug_sys_rst => GND_1,
      mb_reset => NLW_rst_clk_wiz_100M_mb_reset_UNCONNECTED,
      peripheral_aresetn(0) => rst_clk_wiz_100M_peripheral_aresetn(0),
      peripheral_reset(0) => NLW_rst_clk_wiz_100M_peripheral_reset_UNCONNECTED(0),
      slowest_sync_clk => clk_wiz_clk_out1
    );
xillybus_0: component design_1_xillybus_0_1
    port map (
      GPIO_LED(3 downto 0) => NLW_xillybus_0_GPIO_LED_UNCONNECTED(3 downto 0),
      PCIE_PERST_B_LS => GND_1,
      PCIE_REFCLK_N => GND_1,
      PCIE_REFCLK_P => GND_1,
      PCIE_RX_N(3) => GND_1,
      PCIE_RX_N(2) => GND_1,
      PCIE_RX_N(1) => GND_1,
      PCIE_RX_N(0) => GND_1,
      PCIE_RX_P(3) => GND_1,
      PCIE_RX_P(2) => GND_1,
      PCIE_RX_P(1) => GND_1,
      PCIE_RX_P(0) => GND_1,
      PCIE_TX_N(3 downto 0) => NLW_xillybus_0_PCIE_TX_N_UNCONNECTED(3 downto 0),
      PCIE_TX_P(3 downto 0) => NLW_xillybus_0_PCIE_TX_P_UNCONNECTED(3 downto 0),
      bus_clk => NLW_xillybus_0_bus_clk_UNCONNECTED,
      quiesce => NLW_xillybus_0_quiesce_UNCONNECTED,
      user_mem_8_addr(4 downto 0) => xillybus_0_user_mem_8_addr(4 downto 0),
      user_mem_8_addr_update => xillybus_0_user_mem_8_addr_update,
      user_r_mem_8_data(7 downto 0) => Xillybus_AXI_Master_0_user_r_mem_8_data(7 downto 0),
      user_r_mem_8_empty => Xillybus_AXI_Master_0_user_r_mem_8_empty,
      user_r_mem_8_eof => Xillybus_AXI_Master_0_user_r_mem_8_eof,
      user_r_mem_8_open => xillybus_0_user_r_mem_8_open,
      user_r_mem_8_rden => xillybus_0_user_r_mem_8_rden,
      user_r_read_32_data(31 downto 0) => Xillybus_AXI_Master_0_user_r_read_32_data(31 downto 0),
      user_r_read_32_empty => Xillybus_AXI_Master_0_user_r_read_32_empty,
      user_r_read_32_eof => Xillybus_AXI_Master_0_user_r_read_32_eof,
      user_r_read_32_open => xillybus_0_user_r_read_32_open,
      user_r_read_32_rden => xillybus_0_user_r_read_32_rden,
      user_r_read_8_data(7 downto 0) => Xillybus_AXI_Master_0_user_r_read_8_data(7 downto 0),
      user_r_read_8_empty => Xillybus_AXI_Master_0_user_r_read_8_empty,
      user_r_read_8_eof => Xillybus_AXI_Master_0_user_r_read_8_eof,
      user_r_read_8_open => xillybus_0_user_r_read_8_open,
      user_r_read_8_rden => xillybus_0_user_r_read_8_rden,
      user_w_mem_8_data(7 downto 0) => xillybus_0_user_w_mem_8_data(7 downto 0),
      user_w_mem_8_full => Xillybus_AXI_Master_0_user_w_mem_8_full,
      user_w_mem_8_open => xillybus_0_user_w_mem_8_open,
      user_w_mem_8_wren => xillybus_0_user_w_mem_8_wren,
      user_w_write_32_data(31 downto 0) => xillybus_0_user_w_write_32_data(31 downto 0),
      user_w_write_32_full => Xillybus_AXI_Master_0_user_w_write_32_full,
      user_w_write_32_open => xillybus_0_user_w_write_32_open,
      user_w_write_32_wren => xillybus_0_user_w_write_32_wren,
      user_w_write_8_data(7 downto 0) => xillybus_0_user_w_write_8_data(7 downto 0),
      user_w_write_8_full => Xillybus_AXI_Master_0_user_w_write_8_full,
      user_w_write_8_open => xillybus_0_user_w_write_8_open,
      user_w_write_8_wren => xillybus_0_user_w_write_8_wren
    );
end STRUCTURE;
