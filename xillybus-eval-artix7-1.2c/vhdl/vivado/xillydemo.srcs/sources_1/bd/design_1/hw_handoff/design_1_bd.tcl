
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2014.4
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   puts "ERROR: This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl

# If you do not already have a project created,
# you can create a project using the following command:
#    create_project project_1 myproj -part xc7a200tfbg676-2


# CHANGE DESIGN NAME HERE
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# CHECKING IF PROJECT EXISTS
if { [get_projects -quiet] eq "" } {
   puts "ERROR: Please open or create a project!"
   return 1
}


# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "ERROR: Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      puts "INFO: Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   puts "INFO: Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   puts "INFO: Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   puts "INFO: Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

puts "INFO: Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   puts $errMsg
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports

  # Create ports
  set DRP_CLK_IN_N [ create_bd_port -dir I DRP_CLK_IN_N ]
  set DRP_CLK_IN_P [ create_bd_port -dir I DRP_CLK_IN_P ]
  set GPIO_LED [ create_bd_port -dir O -from 3 -to 0 GPIO_LED ]
  set PCIE_PERST_B_LS [ create_bd_port -dir I PCIE_PERST_B_LS ]
  set PCIE_REFCLK_N [ create_bd_port -dir I PCIE_REFCLK_N ]
  set PCIE_REFCLK_P [ create_bd_port -dir I PCIE_REFCLK_P ]
  set PCIE_RX_N [ create_bd_port -dir I -from 3 -to 0 PCIE_RX_N ]
  set PCIE_RX_P [ create_bd_port -dir I -from 3 -to 0 PCIE_RX_P ]
  set PCIE_TX_N [ create_bd_port -dir O -from 3 -to 0 PCIE_TX_N ]
  set PCIE_TX_P [ create_bd_port -dir O -from 3 -to 0 PCIE_TX_P ]
  set Q0_CLK1_GTREFCLK_PAD_N_IN [ create_bd_port -dir I Q0_CLK1_GTREFCLK_PAD_N_IN ]
  set Q0_CLK1_GTREFCLK_PAD_P_IN [ create_bd_port -dir I Q0_CLK1_GTREFCLK_PAD_P_IN ]
  set RXN0_IN [ create_bd_port -dir I RXN0_IN ]
  set RXP0_IN [ create_bd_port -dir I RXP0_IN ]
  set TXN0_OUT [ create_bd_port -dir O TXN0_OUT ]
  set TXP0_OUT [ create_bd_port -dir O TXP0_OUT ]
  set bus_clk [ create_bd_port -dir O -type clk bus_clk ]
  set clock_rtl [ create_bd_port -dir I -type clk clock_rtl ]
  set_property -dict [ list CONFIG.FREQ_HZ {100000000} CONFIG.PHASE {0.000}  ] $clock_rtl
  set quiesce [ create_bd_port -dir O quiesce ]
  set reset_rtl [ create_bd_port -dir I -type rst reset_rtl ]
  set_property -dict [ list CONFIG.POLARITY {ACTIVE_HIGH}  ] $reset_rtl
  set reset_rtl_0 [ create_bd_port -dir I -type rst reset_rtl_0 ]
  set_property -dict [ list CONFIG.POLARITY {ACTIVE_LOW}  ] $reset_rtl_0

  # Create instance: AXI4_groundhog_SATA_0, and set properties
  set AXI4_groundhog_SATA_0 [ create_bd_cell -type ip -vlnv KoCMoC.dd:user:AXI4_groundhog_SATA:1.0 AXI4_groundhog_SATA_0 ]

  # Create instance: Xillybus_AXI_Master_0, and set properties
  set Xillybus_AXI_Master_0 [ create_bd_cell -type ip -vlnv KoCMoC.dd:user:Xillybus_AXI_Master:1.0 Xillybus_AXI_Master_0 ]

  # Create instance: clk_wiz, and set properties
  set clk_wiz [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:5.1 clk_wiz ]

  # Create instance: rst_clk_wiz_100M, and set properties
  set rst_clk_wiz_100M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_clk_wiz_100M ]

  # Create instance: xillybus_0, and set properties
  set xillybus_0 [ create_bd_cell -type ip -vlnv KoCMoC.dd:user:xillybus:1.0 xillybus_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net Xillybus_AXI_Master_0_SATA_AXI [get_bd_intf_pins AXI4_groundhog_SATA_0/S00_AXI] [get_bd_intf_pins Xillybus_AXI_Master_0/SATA_AXI]

  # Create port connections
  connect_bd_net -net AXI4_groundhog_SATA_0_TXN0_OUT [get_bd_ports TXN0_OUT] [get_bd_pins AXI4_groundhog_SATA_0/TXN0_OUT]
  connect_bd_net -net AXI4_groundhog_SATA_0_TXP0_OUT [get_bd_ports TXP0_OUT] [get_bd_pins AXI4_groundhog_SATA_0/TXP0_OUT]
  connect_bd_net -net DRP_CLK_IN_N_1 [get_bd_ports DRP_CLK_IN_N] [get_bd_pins AXI4_groundhog_SATA_0/DRP_CLK_IN_N]
  connect_bd_net -net DRP_CLK_IN_P_1 [get_bd_ports DRP_CLK_IN_P] [get_bd_pins AXI4_groundhog_SATA_0/DRP_CLK_IN_P]
  connect_bd_net -net Q0_CLK1_GTREFCLK_PAD_N_IN_1 [get_bd_ports Q0_CLK1_GTREFCLK_PAD_N_IN] [get_bd_pins AXI4_groundhog_SATA_0/Q0_CLK1_GTREFCLK_PAD_N_IN]
  connect_bd_net -net Q0_CLK1_GTREFCLK_PAD_P_IN_1 [get_bd_ports Q0_CLK1_GTREFCLK_PAD_P_IN] [get_bd_pins AXI4_groundhog_SATA_0/Q0_CLK1_GTREFCLK_PAD_P_IN]
  connect_bd_net -net RXN0_IN_1 [get_bd_ports RXN0_IN] [get_bd_pins AXI4_groundhog_SATA_0/RXN0_IN]
  connect_bd_net -net RXP0_IN_1 [get_bd_ports RXP0_IN] [get_bd_pins AXI4_groundhog_SATA_0/RXP0_IN]
  connect_bd_net -net Xillybus_AXI_Master_0_user_r_mem_8_data [get_bd_pins Xillybus_AXI_Master_0/user_r_mem_8_data] [get_bd_pins xillybus_0/user_r_mem_8_data]
  connect_bd_net -net Xillybus_AXI_Master_0_user_r_mem_8_empty [get_bd_pins Xillybus_AXI_Master_0/user_r_mem_8_empty] [get_bd_pins xillybus_0/user_r_mem_8_empty]
  connect_bd_net -net Xillybus_AXI_Master_0_user_r_mem_8_eof [get_bd_pins Xillybus_AXI_Master_0/user_r_mem_8_eof] [get_bd_pins xillybus_0/user_r_mem_8_eof]
  connect_bd_net -net Xillybus_AXI_Master_0_user_r_read_32_data [get_bd_pins Xillybus_AXI_Master_0/user_r_read_32_data] [get_bd_pins xillybus_0/user_r_read_32_data]
  connect_bd_net -net Xillybus_AXI_Master_0_user_r_read_32_empty [get_bd_pins Xillybus_AXI_Master_0/user_r_read_32_empty] [get_bd_pins xillybus_0/user_r_read_32_empty]
  connect_bd_net -net Xillybus_AXI_Master_0_user_r_read_32_eof [get_bd_pins Xillybus_AXI_Master_0/user_r_read_32_eof] [get_bd_pins xillybus_0/user_r_read_32_eof]
  connect_bd_net -net Xillybus_AXI_Master_0_user_r_read_8_data [get_bd_pins Xillybus_AXI_Master_0/user_r_read_8_data] [get_bd_pins xillybus_0/user_r_read_8_data]
  connect_bd_net -net Xillybus_AXI_Master_0_user_r_read_8_empty [get_bd_pins Xillybus_AXI_Master_0/user_r_read_8_empty] [get_bd_pins xillybus_0/user_r_read_8_empty]
  connect_bd_net -net Xillybus_AXI_Master_0_user_r_read_8_eof [get_bd_pins Xillybus_AXI_Master_0/user_r_read_8_eof] [get_bd_pins xillybus_0/user_r_read_8_eof]
  connect_bd_net -net Xillybus_AXI_Master_0_user_w_mem_8_full [get_bd_pins Xillybus_AXI_Master_0/user_w_mem_8_full] [get_bd_pins xillybus_0/user_w_mem_8_full]
  connect_bd_net -net Xillybus_AXI_Master_0_user_w_write_32_full [get_bd_pins Xillybus_AXI_Master_0/user_w_write_32_full] [get_bd_pins xillybus_0/user_w_write_32_full]
  connect_bd_net -net Xillybus_AXI_Master_0_user_w_write_8_full [get_bd_pins Xillybus_AXI_Master_0/user_w_write_8_full] [get_bd_pins xillybus_0/user_w_write_8_full]
  connect_bd_net -net clk_wiz_clk_out1 [get_bd_pins AXI4_groundhog_SATA_0/s00_axi_aclk] [get_bd_pins Xillybus_AXI_Master_0/bram_axi_aclk] [get_bd_pins Xillybus_AXI_Master_0/dram_axi_aclk] [get_bd_pins Xillybus_AXI_Master_0/sata_axi_aclk] [get_bd_pins clk_wiz/clk_out1] [get_bd_pins rst_clk_wiz_100M/slowest_sync_clk]
  connect_bd_net -net clk_wiz_locked [get_bd_pins clk_wiz/locked] [get_bd_pins rst_clk_wiz_100M/dcm_locked]
  connect_bd_net -net clock_rtl_1 [get_bd_ports clock_rtl] [get_bd_pins clk_wiz/clk_in1]
  connect_bd_net -net reset_rtl_0_1 [get_bd_ports reset_rtl_0] [get_bd_pins rst_clk_wiz_100M/ext_reset_in]
  connect_bd_net -net reset_rtl_1 [get_bd_ports reset_rtl] [get_bd_pins clk_wiz/reset]
  connect_bd_net -net rst_clk_wiz_100M_peripheral_aresetn [get_bd_pins AXI4_groundhog_SATA_0/s00_axi_aresetn] [get_bd_pins Xillybus_AXI_Master_0/dram_axi_aresetn] [get_bd_pins rst_clk_wiz_100M/peripheral_aresetn]
  connect_bd_net -net xillybus_0_user_mem_8_addr [get_bd_pins Xillybus_AXI_Master_0/user_mem_8_addr] [get_bd_pins xillybus_0/user_mem_8_addr]
  connect_bd_net -net xillybus_0_user_mem_8_addr_update [get_bd_pins Xillybus_AXI_Master_0/user_mem_8_addr_update] [get_bd_pins xillybus_0/user_mem_8_addr_update]
  connect_bd_net -net xillybus_0_user_r_mem_8_open [get_bd_pins Xillybus_AXI_Master_0/user_r_mem_8_open] [get_bd_pins xillybus_0/user_r_mem_8_open]
  connect_bd_net -net xillybus_0_user_r_mem_8_rden [get_bd_pins Xillybus_AXI_Master_0/user_r_mem_8_rden] [get_bd_pins xillybus_0/user_r_mem_8_rden]
  connect_bd_net -net xillybus_0_user_r_read_32_open [get_bd_pins Xillybus_AXI_Master_0/user_r_read_32_open] [get_bd_pins xillybus_0/user_r_read_32_open]
  connect_bd_net -net xillybus_0_user_r_read_32_rden [get_bd_pins Xillybus_AXI_Master_0/user_r_read_32_rden] [get_bd_pins xillybus_0/user_r_read_32_rden]
  connect_bd_net -net xillybus_0_user_r_read_8_open [get_bd_pins Xillybus_AXI_Master_0/user_r_read_8_open] [get_bd_pins xillybus_0/user_r_read_8_open]
  connect_bd_net -net xillybus_0_user_r_read_8_rden [get_bd_pins Xillybus_AXI_Master_0/user_r_read_8_rden] [get_bd_pins xillybus_0/user_r_read_8_rden]
  connect_bd_net -net xillybus_0_user_w_mem_8_data [get_bd_pins Xillybus_AXI_Master_0/user_w_mem_8_data] [get_bd_pins xillybus_0/user_w_mem_8_data]
  connect_bd_net -net xillybus_0_user_w_mem_8_open [get_bd_pins Xillybus_AXI_Master_0/user_w_mem_8_open] [get_bd_pins xillybus_0/user_w_mem_8_open]
  connect_bd_net -net xillybus_0_user_w_mem_8_wren [get_bd_pins Xillybus_AXI_Master_0/user_w_mem_8_wren] [get_bd_pins xillybus_0/user_w_mem_8_wren]
  connect_bd_net -net xillybus_0_user_w_write_32_data [get_bd_pins Xillybus_AXI_Master_0/user_w_write_32_data] [get_bd_pins xillybus_0/user_w_write_32_data]
  connect_bd_net -net xillybus_0_user_w_write_32_open [get_bd_pins Xillybus_AXI_Master_0/user_w_write_32_open] [get_bd_pins xillybus_0/user_w_write_32_open]
  connect_bd_net -net xillybus_0_user_w_write_32_wren [get_bd_pins Xillybus_AXI_Master_0/user_w_write_32_wren] [get_bd_pins xillybus_0/user_w_write_32_wren]
  connect_bd_net -net xillybus_0_user_w_write_8_data [get_bd_pins Xillybus_AXI_Master_0/user_w_write_8_data] [get_bd_pins xillybus_0/user_w_write_8_data]
  connect_bd_net -net xillybus_0_user_w_write_8_open [get_bd_pins Xillybus_AXI_Master_0/user_w_write_8_open] [get_bd_pins xillybus_0/user_w_write_8_open]
  connect_bd_net -net xillybus_0_user_w_write_8_wren [get_bd_pins Xillybus_AXI_Master_0/user_w_write_8_wren] [get_bd_pins xillybus_0/user_w_write_8_wren]

  # Create address segments
  

  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


