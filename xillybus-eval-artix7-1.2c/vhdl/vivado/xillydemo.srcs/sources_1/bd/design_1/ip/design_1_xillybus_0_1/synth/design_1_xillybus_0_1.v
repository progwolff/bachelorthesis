// (c) Copyright 1995-2015 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: KoCMoC.dd:user:xillybus:1.0
// IP Revision: 2

(* X_CORE_INFO = "xillybus,Vivado 2014.4" *)
(* CHECK_LICENSE_TYPE = "design_1_xillybus_0_1,xillybus,{}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module design_1_xillybus_0_1 (
  PCIE_TX_P,
  PCIE_TX_N,
  PCIE_RX_P,
  PCIE_RX_N,
  PCIE_REFCLK_P,
  PCIE_REFCLK_N,
  PCIE_PERST_B_LS,
  bus_clk,
  quiesce,
  GPIO_LED,
  user_r_read_32_rden,
  user_r_read_32_data,
  user_r_read_32_empty,
  user_r_read_32_eof,
  user_r_read_32_open,
  user_w_write_32_wren,
  user_w_write_32_data,
  user_w_write_32_full,
  user_w_write_32_open,
  user_r_read_8_rden,
  user_r_read_8_data,
  user_r_read_8_empty,
  user_r_read_8_eof,
  user_r_read_8_open,
  user_w_write_8_wren,
  user_w_write_8_data,
  user_w_write_8_full,
  user_w_write_8_open,
  user_r_mem_8_rden,
  user_r_mem_8_data,
  user_r_mem_8_empty,
  user_r_mem_8_eof,
  user_r_mem_8_open,
  user_w_mem_8_wren,
  user_w_mem_8_data,
  user_w_mem_8_full,
  user_w_mem_8_open,
  user_mem_8_addr,
  user_mem_8_addr_update
);

output wire [3 : 0] PCIE_TX_P;
output wire [3 : 0] PCIE_TX_N;
input wire [3 : 0] PCIE_RX_P;
input wire [3 : 0] PCIE_RX_N;
input wire PCIE_REFCLK_P;
input wire PCIE_REFCLK_N;
input wire PCIE_PERST_B_LS;
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 bus_signal_clock CLK" *)
output wire bus_clk;
output wire quiesce;
output wire [3 : 0] GPIO_LED;
output wire user_r_read_32_rden;
input wire [31 : 0] user_r_read_32_data;
input wire user_r_read_32_empty;
input wire user_r_read_32_eof;
output wire user_r_read_32_open;
output wire user_w_write_32_wren;
output wire [31 : 0] user_w_write_32_data;
input wire user_w_write_32_full;
output wire user_w_write_32_open;
output wire user_r_read_8_rden;
input wire [7 : 0] user_r_read_8_data;
input wire user_r_read_8_empty;
input wire user_r_read_8_eof;
output wire user_r_read_8_open;
output wire user_w_write_8_wren;
output wire [7 : 0] user_w_write_8_data;
input wire user_w_write_8_full;
output wire user_w_write_8_open;
output wire user_r_mem_8_rden;
input wire [7 : 0] user_r_mem_8_data;
input wire user_r_mem_8_empty;
input wire user_r_mem_8_eof;
output wire user_r_mem_8_open;
output wire user_w_mem_8_wren;
output wire [7 : 0] user_w_mem_8_data;
input wire user_w_mem_8_full;
output wire user_w_mem_8_open;
output wire [4 : 0] user_mem_8_addr;
output wire user_mem_8_addr_update;

  xillybus inst (
    .PCIE_TX_P(PCIE_TX_P),
    .PCIE_TX_N(PCIE_TX_N),
    .PCIE_RX_P(PCIE_RX_P),
    .PCIE_RX_N(PCIE_RX_N),
    .PCIE_REFCLK_P(PCIE_REFCLK_P),
    .PCIE_REFCLK_N(PCIE_REFCLK_N),
    .PCIE_PERST_B_LS(PCIE_PERST_B_LS),
    .bus_clk(bus_clk),
    .quiesce(quiesce),
    .GPIO_LED(GPIO_LED),
    .user_r_read_32_rden(user_r_read_32_rden),
    .user_r_read_32_data(user_r_read_32_data),
    .user_r_read_32_empty(user_r_read_32_empty),
    .user_r_read_32_eof(user_r_read_32_eof),
    .user_r_read_32_open(user_r_read_32_open),
    .user_w_write_32_wren(user_w_write_32_wren),
    .user_w_write_32_data(user_w_write_32_data),
    .user_w_write_32_full(user_w_write_32_full),
    .user_w_write_32_open(user_w_write_32_open),
    .user_r_read_8_rden(user_r_read_8_rden),
    .user_r_read_8_data(user_r_read_8_data),
    .user_r_read_8_empty(user_r_read_8_empty),
    .user_r_read_8_eof(user_r_read_8_eof),
    .user_r_read_8_open(user_r_read_8_open),
    .user_w_write_8_wren(user_w_write_8_wren),
    .user_w_write_8_data(user_w_write_8_data),
    .user_w_write_8_full(user_w_write_8_full),
    .user_w_write_8_open(user_w_write_8_open),
    .user_r_mem_8_rden(user_r_mem_8_rden),
    .user_r_mem_8_data(user_r_mem_8_data),
    .user_r_mem_8_empty(user_r_mem_8_empty),
    .user_r_mem_8_eof(user_r_mem_8_eof),
    .user_r_mem_8_open(user_r_mem_8_open),
    .user_w_mem_8_wren(user_w_mem_8_wren),
    .user_w_mem_8_data(user_w_mem_8_data),
    .user_w_mem_8_full(user_w_mem_8_full),
    .user_w_mem_8_open(user_w_mem_8_open),
    .user_mem_8_addr(user_mem_8_addr),
    .user_mem_8_addr_update(user_mem_8_addr_update)
  );
endmodule
