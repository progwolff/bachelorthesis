-- (c) Copyright 1995-2015 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: KoCMoC.dd:user:Xillybus_AXI_Master:1.0
-- IP Revision: 4

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY design_1_Xillybus_AXI_Master_0_4 IS
  PORT (
    user_r_mem_8_rden : IN STD_LOGIC;
    user_r_mem_8_empty : OUT STD_LOGIC;
    user_r_mem_8_data : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    user_r_mem_8_eof : OUT STD_LOGIC;
    user_r_mem_8_open : IN STD_LOGIC;
    user_w_mem_8_wren : IN STD_LOGIC;
    user_w_mem_8_full : OUT STD_LOGIC;
    user_w_mem_8_data : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    user_w_mem_8_open : IN STD_LOGIC;
    user_mem_8_addr : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    user_mem_8_addr_update : IN STD_LOGIC;
    user_r_read_32_rden : IN STD_LOGIC;
    user_r_read_32_empty : OUT STD_LOGIC;
    user_r_read_32_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    user_r_read_32_eof : OUT STD_LOGIC;
    user_r_read_32_open : IN STD_LOGIC;
    user_r_read_8_rden : IN STD_LOGIC;
    user_r_read_8_empty : OUT STD_LOGIC;
    user_r_read_8_data : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    user_r_read_8_eof : OUT STD_LOGIC;
    user_r_read_8_open : IN STD_LOGIC;
    user_w_write_32_wren : IN STD_LOGIC;
    user_w_write_32_full : OUT STD_LOGIC;
    user_w_write_32_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    user_w_write_32_open : IN STD_LOGIC;
    user_w_write_8_wren : IN STD_LOGIC;
    user_w_write_8_full : OUT STD_LOGIC;
    user_w_write_8_data : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    user_w_write_8_open : IN STD_LOGIC;
    sata_axi_init_axi_txn : IN STD_LOGIC;
    sata_axi_txn_done : OUT STD_LOGIC;
    sata_axi_error : OUT STD_LOGIC;
    sata_axi_aclk : IN STD_LOGIC;
    sata_axi_aresetn : IN STD_LOGIC;
    sata_axi_awid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    sata_axi_awaddr : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    sata_axi_awlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    sata_axi_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    sata_axi_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    sata_axi_awlock : OUT STD_LOGIC;
    sata_axi_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    sata_axi_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    sata_axi_awqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    sata_axi_awuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    sata_axi_awvalid : OUT STD_LOGIC;
    sata_axi_awready : IN STD_LOGIC;
    sata_axi_wdata : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    sata_axi_wstrb : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    sata_axi_wlast : OUT STD_LOGIC;
    sata_axi_wuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    sata_axi_wvalid : OUT STD_LOGIC;
    sata_axi_wready : IN STD_LOGIC;
    sata_axi_bid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    sata_axi_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    sata_axi_buser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    sata_axi_bvalid : IN STD_LOGIC;
    sata_axi_bready : OUT STD_LOGIC;
    sata_axi_arid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    sata_axi_araddr : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    sata_axi_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    sata_axi_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    sata_axi_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    sata_axi_arlock : OUT STD_LOGIC;
    sata_axi_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    sata_axi_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    sata_axi_arqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    sata_axi_aruser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    sata_axi_arvalid : OUT STD_LOGIC;
    sata_axi_arready : IN STD_LOGIC;
    sata_axi_rid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    sata_axi_rdata : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    sata_axi_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    sata_axi_rlast : IN STD_LOGIC;
    sata_axi_ruser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    sata_axi_rvalid : IN STD_LOGIC;
    sata_axi_rready : OUT STD_LOGIC;
    bram_axi_init_axi_txn : IN STD_LOGIC;
    bram_axi_txn_done : OUT STD_LOGIC;
    bram_axi_error : OUT STD_LOGIC;
    bram_axi_aclk : IN STD_LOGIC;
    bram_axi_aresetn : IN STD_LOGIC;
    bram_axi_awid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    bram_axi_awaddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_axi_awlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    bram_axi_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    bram_axi_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    bram_axi_awlock : OUT STD_LOGIC;
    bram_axi_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_axi_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    bram_axi_awqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_axi_awuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    bram_axi_awvalid : OUT STD_LOGIC;
    bram_axi_awready : IN STD_LOGIC;
    bram_axi_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_axi_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_axi_wlast : OUT STD_LOGIC;
    bram_axi_wuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    bram_axi_wvalid : OUT STD_LOGIC;
    bram_axi_wready : IN STD_LOGIC;
    bram_axi_bid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    bram_axi_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    bram_axi_buser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    bram_axi_bvalid : IN STD_LOGIC;
    bram_axi_bready : OUT STD_LOGIC;
    bram_axi_arid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    bram_axi_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_axi_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    bram_axi_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    bram_axi_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    bram_axi_arlock : OUT STD_LOGIC;
    bram_axi_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_axi_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    bram_axi_arqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_axi_aruser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    bram_axi_arvalid : OUT STD_LOGIC;
    bram_axi_arready : IN STD_LOGIC;
    bram_axi_rid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    bram_axi_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_axi_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    bram_axi_rlast : IN STD_LOGIC;
    bram_axi_ruser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    bram_axi_rvalid : IN STD_LOGIC;
    bram_axi_rready : OUT STD_LOGIC;
    dram_axi_init_axi_txn : IN STD_LOGIC;
    dram_axi_txn_done : OUT STD_LOGIC;
    dram_axi_error : OUT STD_LOGIC;
    dram_axi_aclk : IN STD_LOGIC;
    dram_axi_aresetn : IN STD_LOGIC;
    dram_axi_awid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    dram_axi_awaddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    dram_axi_awlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    dram_axi_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    dram_axi_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    dram_axi_awlock : OUT STD_LOGIC;
    dram_axi_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    dram_axi_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    dram_axi_awqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    dram_axi_awuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    dram_axi_awvalid : OUT STD_LOGIC;
    dram_axi_awready : IN STD_LOGIC;
    dram_axi_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    dram_axi_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    dram_axi_wlast : OUT STD_LOGIC;
    dram_axi_wuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    dram_axi_wvalid : OUT STD_LOGIC;
    dram_axi_wready : IN STD_LOGIC;
    dram_axi_bid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    dram_axi_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    dram_axi_buser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    dram_axi_bvalid : IN STD_LOGIC;
    dram_axi_bready : OUT STD_LOGIC;
    dram_axi_arid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    dram_axi_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    dram_axi_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    dram_axi_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    dram_axi_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    dram_axi_arlock : OUT STD_LOGIC;
    dram_axi_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    dram_axi_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    dram_axi_arqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    dram_axi_aruser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    dram_axi_arvalid : OUT STD_LOGIC;
    dram_axi_arready : IN STD_LOGIC;
    dram_axi_rid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    dram_axi_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    dram_axi_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    dram_axi_rlast : IN STD_LOGIC;
    dram_axi_ruser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    dram_axi_rvalid : IN STD_LOGIC;
    dram_axi_rready : OUT STD_LOGIC
  );
END design_1_Xillybus_AXI_Master_0_4;

ARCHITECTURE design_1_Xillybus_AXI_Master_0_4_arch OF design_1_Xillybus_AXI_Master_0_4 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : string;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_1_Xillybus_AXI_Master_0_4_arch: ARCHITECTURE IS "yes";

  COMPONENT Xillybus_AXI_Master_v1_0 IS
    GENERIC (
      C_DRAM_AXI_TARGET_SLAVE_BASE_ADDR : STD_LOGIC_VECTOR; -- Base address of targeted slave
      C_DRAM_AXI_BURST_LEN : INTEGER; -- Burst Length. Supports 1, 2, 4, 8, 16, 32, 64, 128, 256 burst lengths
      C_DRAM_AXI_ID_WIDTH : INTEGER; -- Thread ID Width
      C_DRAM_AXI_ADDR_WIDTH : INTEGER; -- Width of Address Bus
      C_DRAM_AXI_DATA_WIDTH : INTEGER; -- Width of Data Bus
      C_DRAM_AXI_AWUSER_WIDTH : INTEGER; -- Width of User Write Address Bus
      C_DRAM_AXI_ARUSER_WIDTH : INTEGER; -- Width of User Read Address Bus
      C_DRAM_AXI_WUSER_WIDTH : INTEGER; -- Width of User Write Data Bus
      C_DRAM_AXI_RUSER_WIDTH : INTEGER; -- Width of User Read Data Bus
      C_DRAM_AXI_BUSER_WIDTH : INTEGER; -- Width of User Response Bus
      C_SATA_AXI_TARGET_SLAVE_BASE_ADDR : STD_LOGIC_VECTOR; -- Base address of targeted slave
      C_SATA_AXI_BURST_LEN : INTEGER; -- Burst Length. Supports 1, 2, 4, 8, 16, 32, 64, 128, 256 burst lengths
      C_SATA_AXI_ID_WIDTH : INTEGER; -- Thread ID Width
      C_SATA_AXI_ADDR_WIDTH : INTEGER; -- Width of Address Bus
      C_SATA_AXI_DATA_WIDTH : INTEGER; -- Width of Data Bus
      C_SATA_AXI_AWUSER_WIDTH : INTEGER; -- Width of User Write Address Bus
      C_SATA_AXI_ARUSER_WIDTH : INTEGER; -- Width of User Read Address Bus
      C_SATA_AXI_WUSER_WIDTH : INTEGER; -- Width of User Write Data Bus
      C_SATA_AXI_RUSER_WIDTH : INTEGER; -- Width of User Read Data Bus
      C_SATA_AXI_BUSER_WIDTH : INTEGER; -- Width of User Response Bus
      C_BRAM_AXI_TARGET_SLAVE_BASE_ADDR : STD_LOGIC_VECTOR; -- Base address of targeted slave
      C_BRAM_AXI_BURST_LEN : INTEGER; -- Burst Length. Supports 1, 2, 4, 8, 16, 32, 64, 128, 256 burst lengths
      C_BRAM_AXI_ID_WIDTH : INTEGER; -- Thread ID Width
      C_BRAM_AXI_ADDR_WIDTH : INTEGER; -- Width of Address Bus
      C_BRAM_AXI_DATA_WIDTH : INTEGER; -- Width of Data Bus
      C_BRAM_AXI_AWUSER_WIDTH : INTEGER; -- Width of User Write Address Bus
      C_BRAM_AXI_ARUSER_WIDTH : INTEGER; -- Width of User Read Address Bus
      C_BRAM_AXI_WUSER_WIDTH : INTEGER; -- Width of User Write Data Bus
      C_BRAM_AXI_RUSER_WIDTH : INTEGER; -- Width of User Read Data Bus
      C_BRAM_AXI_BUSER_WIDTH : INTEGER -- Width of User Response Bus
    );
    PORT (
      user_r_mem_8_rden : IN STD_LOGIC;
      user_r_mem_8_empty : OUT STD_LOGIC;
      user_r_mem_8_data : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      user_r_mem_8_eof : OUT STD_LOGIC;
      user_r_mem_8_open : IN STD_LOGIC;
      user_w_mem_8_wren : IN STD_LOGIC;
      user_w_mem_8_full : OUT STD_LOGIC;
      user_w_mem_8_data : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      user_w_mem_8_open : IN STD_LOGIC;
      user_mem_8_addr : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      user_mem_8_addr_update : IN STD_LOGIC;
      user_r_read_32_rden : IN STD_LOGIC;
      user_r_read_32_empty : OUT STD_LOGIC;
      user_r_read_32_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      user_r_read_32_eof : OUT STD_LOGIC;
      user_r_read_32_open : IN STD_LOGIC;
      user_r_read_8_rden : IN STD_LOGIC;
      user_r_read_8_empty : OUT STD_LOGIC;
      user_r_read_8_data : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      user_r_read_8_eof : OUT STD_LOGIC;
      user_r_read_8_open : IN STD_LOGIC;
      user_w_write_32_wren : IN STD_LOGIC;
      user_w_write_32_full : OUT STD_LOGIC;
      user_w_write_32_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      user_w_write_32_open : IN STD_LOGIC;
      user_w_write_8_wren : IN STD_LOGIC;
      user_w_write_8_full : OUT STD_LOGIC;
      user_w_write_8_data : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      user_w_write_8_open : IN STD_LOGIC;
      sata_axi_init_axi_txn : IN STD_LOGIC;
      sata_axi_txn_done : OUT STD_LOGIC;
      sata_axi_error : OUT STD_LOGIC;
      sata_axi_aclk : IN STD_LOGIC;
      sata_axi_aresetn : IN STD_LOGIC;
      sata_axi_awid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      sata_axi_awaddr : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      sata_axi_awlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      sata_axi_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      sata_axi_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      sata_axi_awlock : OUT STD_LOGIC;
      sata_axi_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      sata_axi_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      sata_axi_awqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      sata_axi_awuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      sata_axi_awvalid : OUT STD_LOGIC;
      sata_axi_awready : IN STD_LOGIC;
      sata_axi_wdata : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      sata_axi_wstrb : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      sata_axi_wlast : OUT STD_LOGIC;
      sata_axi_wuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      sata_axi_wvalid : OUT STD_LOGIC;
      sata_axi_wready : IN STD_LOGIC;
      sata_axi_bid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      sata_axi_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      sata_axi_buser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      sata_axi_bvalid : IN STD_LOGIC;
      sata_axi_bready : OUT STD_LOGIC;
      sata_axi_arid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      sata_axi_araddr : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      sata_axi_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      sata_axi_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      sata_axi_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      sata_axi_arlock : OUT STD_LOGIC;
      sata_axi_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      sata_axi_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      sata_axi_arqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      sata_axi_aruser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      sata_axi_arvalid : OUT STD_LOGIC;
      sata_axi_arready : IN STD_LOGIC;
      sata_axi_rid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      sata_axi_rdata : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
      sata_axi_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      sata_axi_rlast : IN STD_LOGIC;
      sata_axi_ruser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      sata_axi_rvalid : IN STD_LOGIC;
      sata_axi_rready : OUT STD_LOGIC;
      bram_axi_init_axi_txn : IN STD_LOGIC;
      bram_axi_txn_done : OUT STD_LOGIC;
      bram_axi_error : OUT STD_LOGIC;
      bram_axi_aclk : IN STD_LOGIC;
      bram_axi_aresetn : IN STD_LOGIC;
      bram_axi_awid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      bram_axi_awaddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_axi_awlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      bram_axi_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      bram_axi_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      bram_axi_awlock : OUT STD_LOGIC;
      bram_axi_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_axi_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      bram_axi_awqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_axi_awuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      bram_axi_awvalid : OUT STD_LOGIC;
      bram_axi_awready : IN STD_LOGIC;
      bram_axi_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_axi_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_axi_wlast : OUT STD_LOGIC;
      bram_axi_wuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      bram_axi_wvalid : OUT STD_LOGIC;
      bram_axi_wready : IN STD_LOGIC;
      bram_axi_bid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      bram_axi_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      bram_axi_buser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      bram_axi_bvalid : IN STD_LOGIC;
      bram_axi_bready : OUT STD_LOGIC;
      bram_axi_arid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      bram_axi_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_axi_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      bram_axi_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      bram_axi_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      bram_axi_arlock : OUT STD_LOGIC;
      bram_axi_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_axi_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      bram_axi_arqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_axi_aruser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      bram_axi_arvalid : OUT STD_LOGIC;
      bram_axi_arready : IN STD_LOGIC;
      bram_axi_rid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      bram_axi_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_axi_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      bram_axi_rlast : IN STD_LOGIC;
      bram_axi_ruser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      bram_axi_rvalid : IN STD_LOGIC;
      bram_axi_rready : OUT STD_LOGIC;
      dram_axi_init_axi_txn : IN STD_LOGIC;
      dram_axi_txn_done : OUT STD_LOGIC;
      dram_axi_error : OUT STD_LOGIC;
      dram_axi_aclk : IN STD_LOGIC;
      dram_axi_aresetn : IN STD_LOGIC;
      dram_axi_awid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      dram_axi_awaddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      dram_axi_awlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      dram_axi_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      dram_axi_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      dram_axi_awlock : OUT STD_LOGIC;
      dram_axi_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      dram_axi_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      dram_axi_awqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      dram_axi_awuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      dram_axi_awvalid : OUT STD_LOGIC;
      dram_axi_awready : IN STD_LOGIC;
      dram_axi_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      dram_axi_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      dram_axi_wlast : OUT STD_LOGIC;
      dram_axi_wuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      dram_axi_wvalid : OUT STD_LOGIC;
      dram_axi_wready : IN STD_LOGIC;
      dram_axi_bid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      dram_axi_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      dram_axi_buser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      dram_axi_bvalid : IN STD_LOGIC;
      dram_axi_bready : OUT STD_LOGIC;
      dram_axi_arid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      dram_axi_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      dram_axi_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      dram_axi_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      dram_axi_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      dram_axi_arlock : OUT STD_LOGIC;
      dram_axi_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      dram_axi_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      dram_axi_arqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      dram_axi_aruser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      dram_axi_arvalid : OUT STD_LOGIC;
      dram_axi_arready : IN STD_LOGIC;
      dram_axi_rid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      dram_axi_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      dram_axi_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      dram_axi_rlast : IN STD_LOGIC;
      dram_axi_ruser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      dram_axi_rvalid : IN STD_LOGIC;
      dram_axi_rready : OUT STD_LOGIC
    );
  END COMPONENT Xillybus_AXI_Master_v1_0;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF design_1_Xillybus_AXI_Master_0_4_arch: ARCHITECTURE IS "Xillybus_AXI_Master_v1_0,Vivado 2014.4";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF design_1_Xillybus_AXI_Master_0_4_arch : ARCHITECTURE IS "design_1_Xillybus_AXI_Master_0_4,Xillybus_AXI_Master_v1_0,{}";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 SATA_AXI_CLK CLK, xilinx.com:signal:clock:1.0 sata_axi_signal_clock CLK";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 SATA_AXI_RST RST, xilinx.com:signal:reset:1.0 sata_axi_signal_reset RST";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_awid: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI AWID";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_awaddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI AWADDR";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_awlen: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI AWLEN";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_awsize: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI AWSIZE";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_awburst: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI AWBURST";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_awlock: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI AWLOCK";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_awcache: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI AWCACHE";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_awprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI AWPROT";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_awqos: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI AWQOS";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_awuser: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI AWUSER";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_awvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI AWVALID";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_awready: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI AWREADY";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_wdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_wstrb: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_wlast: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI WLAST";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_wuser: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI WUSER";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_wvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI WVALID";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_wready: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_bid: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI BID";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_bresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_buser: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI BUSER";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_bvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_bready: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_arid: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI ARID";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_araddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI ARADDR";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_arlen: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI ARLEN";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_arsize: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI ARSIZE";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_arburst: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI ARBURST";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_arlock: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI ARLOCK";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_arcache: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI ARCACHE";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_arprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI ARPROT";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_arqos: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI ARQOS";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_aruser: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI ARUSER";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_arvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_arready: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_rid: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI RID";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_rdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_rresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_rlast: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI RLAST";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_ruser: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI RUSER";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_rvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF sata_axi_rready: SIGNAL IS "xilinx.com:interface:aximm:1.0 SATA_AXI RREADY";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 BRAM_AXI_CLK CLK, xilinx.com:signal:clock:1.0 bram_axi_signal_clock CLK";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 BRAM_AXI_RST RST, xilinx.com:signal:reset:1.0 bram_axi_signal_reset RST";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_awid: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI AWID";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_awaddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI AWADDR";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_awlen: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI AWLEN";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_awsize: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI AWSIZE";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_awburst: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI AWBURST";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_awlock: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI AWLOCK";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_awcache: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI AWCACHE";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_awprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI AWPROT";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_awqos: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI AWQOS";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_awuser: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI AWUSER";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_awvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI AWVALID";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_awready: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI AWREADY";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_wdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_wstrb: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_wlast: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI WLAST";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_wuser: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI WUSER";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_wvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI WVALID";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_wready: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_bid: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI BID";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_bresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_buser: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI BUSER";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_bvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_bready: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_arid: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI ARID";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_araddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI ARADDR";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_arlen: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI ARLEN";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_arsize: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI ARSIZE";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_arburst: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI ARBURST";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_arlock: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI ARLOCK";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_arcache: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI ARCACHE";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_arprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI ARPROT";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_arqos: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI ARQOS";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_aruser: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI ARUSER";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_arvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_arready: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_rid: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI RID";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_rdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_rresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_rlast: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI RLAST";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_ruser: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI RUSER";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_rvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF bram_axi_rready: SIGNAL IS "xilinx.com:interface:aximm:1.0 BRAM_AXI RREADY";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 DRAM_AXI_CLK CLK, xilinx.com:signal:clock:1.0 dram_axi_signal_clock CLK";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 DRAM_AXI_RST RST, xilinx.com:signal:reset:1.0 dram_axi_signal_reset RST";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_awid: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI AWID";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_awaddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI AWADDR";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_awlen: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI AWLEN";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_awsize: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI AWSIZE";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_awburst: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI AWBURST";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_awlock: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI AWLOCK";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_awcache: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI AWCACHE";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_awprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI AWPROT";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_awqos: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI AWQOS";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_awuser: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI AWUSER";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_awvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI AWVALID";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_awready: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI AWREADY";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_wdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_wstrb: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_wlast: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI WLAST";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_wuser: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI WUSER";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_wvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI WVALID";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_wready: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_bid: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI BID";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_bresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_buser: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI BUSER";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_bvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_bready: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_arid: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI ARID";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_araddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI ARADDR";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_arlen: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI ARLEN";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_arsize: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI ARSIZE";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_arburst: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI ARBURST";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_arlock: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI ARLOCK";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_arcache: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI ARCACHE";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_arprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI ARPROT";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_arqos: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI ARQOS";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_aruser: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI ARUSER";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_arvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_arready: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_rid: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI RID";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_rdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_rresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_rlast: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI RLAST";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_ruser: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI RUSER";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_rvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF dram_axi_rready: SIGNAL IS "xilinx.com:interface:aximm:1.0 DRAM_AXI RREADY";
BEGIN
  U0 : Xillybus_AXI_Master_v1_0
    GENERIC MAP (
      C_DRAM_AXI_TARGET_SLAVE_BASE_ADDR => X"40000000",
      C_DRAM_AXI_BURST_LEN => 16,
      C_DRAM_AXI_ID_WIDTH => 1,
      C_DRAM_AXI_ADDR_WIDTH => 32,
      C_DRAM_AXI_DATA_WIDTH => 32,
      C_DRAM_AXI_AWUSER_WIDTH => 1,
      C_DRAM_AXI_ARUSER_WIDTH => 1,
      C_DRAM_AXI_WUSER_WIDTH => 1,
      C_DRAM_AXI_RUSER_WIDTH => 1,
      C_DRAM_AXI_BUSER_WIDTH => 1,
      C_SATA_AXI_TARGET_SLAVE_BASE_ADDR => X"40000000",
      C_SATA_AXI_BURST_LEN => 16,
      C_SATA_AXI_ID_WIDTH => 1,
      C_SATA_AXI_ADDR_WIDTH => 64,
      C_SATA_AXI_DATA_WIDTH => 512,
      C_SATA_AXI_AWUSER_WIDTH => 1,
      C_SATA_AXI_ARUSER_WIDTH => 1,
      C_SATA_AXI_WUSER_WIDTH => 1,
      C_SATA_AXI_RUSER_WIDTH => 1,
      C_SATA_AXI_BUSER_WIDTH => 1,
      C_BRAM_AXI_TARGET_SLAVE_BASE_ADDR => X"40000000",
      C_BRAM_AXI_BURST_LEN => 16,
      C_BRAM_AXI_ID_WIDTH => 1,
      C_BRAM_AXI_ADDR_WIDTH => 32,
      C_BRAM_AXI_DATA_WIDTH => 32,
      C_BRAM_AXI_AWUSER_WIDTH => 1,
      C_BRAM_AXI_ARUSER_WIDTH => 1,
      C_BRAM_AXI_WUSER_WIDTH => 1,
      C_BRAM_AXI_RUSER_WIDTH => 1,
      C_BRAM_AXI_BUSER_WIDTH => 1
    )
    PORT MAP (
      user_r_mem_8_rden => user_r_mem_8_rden,
      user_r_mem_8_empty => user_r_mem_8_empty,
      user_r_mem_8_data => user_r_mem_8_data,
      user_r_mem_8_eof => user_r_mem_8_eof,
      user_r_mem_8_open => user_r_mem_8_open,
      user_w_mem_8_wren => user_w_mem_8_wren,
      user_w_mem_8_full => user_w_mem_8_full,
      user_w_mem_8_data => user_w_mem_8_data,
      user_w_mem_8_open => user_w_mem_8_open,
      user_mem_8_addr => user_mem_8_addr,
      user_mem_8_addr_update => user_mem_8_addr_update,
      user_r_read_32_rden => user_r_read_32_rden,
      user_r_read_32_empty => user_r_read_32_empty,
      user_r_read_32_data => user_r_read_32_data,
      user_r_read_32_eof => user_r_read_32_eof,
      user_r_read_32_open => user_r_read_32_open,
      user_r_read_8_rden => user_r_read_8_rden,
      user_r_read_8_empty => user_r_read_8_empty,
      user_r_read_8_data => user_r_read_8_data,
      user_r_read_8_eof => user_r_read_8_eof,
      user_r_read_8_open => user_r_read_8_open,
      user_w_write_32_wren => user_w_write_32_wren,
      user_w_write_32_full => user_w_write_32_full,
      user_w_write_32_data => user_w_write_32_data,
      user_w_write_32_open => user_w_write_32_open,
      user_w_write_8_wren => user_w_write_8_wren,
      user_w_write_8_full => user_w_write_8_full,
      user_w_write_8_data => user_w_write_8_data,
      user_w_write_8_open => user_w_write_8_open,
      sata_axi_init_axi_txn => sata_axi_init_axi_txn,
      sata_axi_txn_done => sata_axi_txn_done,
      sata_axi_error => sata_axi_error,
      sata_axi_aclk => sata_axi_aclk,
      sata_axi_aresetn => sata_axi_aresetn,
      sata_axi_awid => sata_axi_awid,
      sata_axi_awaddr => sata_axi_awaddr,
      sata_axi_awlen => sata_axi_awlen,
      sata_axi_awsize => sata_axi_awsize,
      sata_axi_awburst => sata_axi_awburst,
      sata_axi_awlock => sata_axi_awlock,
      sata_axi_awcache => sata_axi_awcache,
      sata_axi_awprot => sata_axi_awprot,
      sata_axi_awqos => sata_axi_awqos,
      sata_axi_awuser => sata_axi_awuser,
      sata_axi_awvalid => sata_axi_awvalid,
      sata_axi_awready => sata_axi_awready,
      sata_axi_wdata => sata_axi_wdata,
      sata_axi_wstrb => sata_axi_wstrb,
      sata_axi_wlast => sata_axi_wlast,
      sata_axi_wuser => sata_axi_wuser,
      sata_axi_wvalid => sata_axi_wvalid,
      sata_axi_wready => sata_axi_wready,
      sata_axi_bid => sata_axi_bid,
      sata_axi_bresp => sata_axi_bresp,
      sata_axi_buser => sata_axi_buser,
      sata_axi_bvalid => sata_axi_bvalid,
      sata_axi_bready => sata_axi_bready,
      sata_axi_arid => sata_axi_arid,
      sata_axi_araddr => sata_axi_araddr,
      sata_axi_arlen => sata_axi_arlen,
      sata_axi_arsize => sata_axi_arsize,
      sata_axi_arburst => sata_axi_arburst,
      sata_axi_arlock => sata_axi_arlock,
      sata_axi_arcache => sata_axi_arcache,
      sata_axi_arprot => sata_axi_arprot,
      sata_axi_arqos => sata_axi_arqos,
      sata_axi_aruser => sata_axi_aruser,
      sata_axi_arvalid => sata_axi_arvalid,
      sata_axi_arready => sata_axi_arready,
      sata_axi_rid => sata_axi_rid,
      sata_axi_rdata => sata_axi_rdata,
      sata_axi_rresp => sata_axi_rresp,
      sata_axi_rlast => sata_axi_rlast,
      sata_axi_ruser => sata_axi_ruser,
      sata_axi_rvalid => sata_axi_rvalid,
      sata_axi_rready => sata_axi_rready,
      bram_axi_init_axi_txn => bram_axi_init_axi_txn,
      bram_axi_txn_done => bram_axi_txn_done,
      bram_axi_error => bram_axi_error,
      bram_axi_aclk => bram_axi_aclk,
      bram_axi_aresetn => bram_axi_aresetn,
      bram_axi_awid => bram_axi_awid,
      bram_axi_awaddr => bram_axi_awaddr,
      bram_axi_awlen => bram_axi_awlen,
      bram_axi_awsize => bram_axi_awsize,
      bram_axi_awburst => bram_axi_awburst,
      bram_axi_awlock => bram_axi_awlock,
      bram_axi_awcache => bram_axi_awcache,
      bram_axi_awprot => bram_axi_awprot,
      bram_axi_awqos => bram_axi_awqos,
      bram_axi_awuser => bram_axi_awuser,
      bram_axi_awvalid => bram_axi_awvalid,
      bram_axi_awready => bram_axi_awready,
      bram_axi_wdata => bram_axi_wdata,
      bram_axi_wstrb => bram_axi_wstrb,
      bram_axi_wlast => bram_axi_wlast,
      bram_axi_wuser => bram_axi_wuser,
      bram_axi_wvalid => bram_axi_wvalid,
      bram_axi_wready => bram_axi_wready,
      bram_axi_bid => bram_axi_bid,
      bram_axi_bresp => bram_axi_bresp,
      bram_axi_buser => bram_axi_buser,
      bram_axi_bvalid => bram_axi_bvalid,
      bram_axi_bready => bram_axi_bready,
      bram_axi_arid => bram_axi_arid,
      bram_axi_araddr => bram_axi_araddr,
      bram_axi_arlen => bram_axi_arlen,
      bram_axi_arsize => bram_axi_arsize,
      bram_axi_arburst => bram_axi_arburst,
      bram_axi_arlock => bram_axi_arlock,
      bram_axi_arcache => bram_axi_arcache,
      bram_axi_arprot => bram_axi_arprot,
      bram_axi_arqos => bram_axi_arqos,
      bram_axi_aruser => bram_axi_aruser,
      bram_axi_arvalid => bram_axi_arvalid,
      bram_axi_arready => bram_axi_arready,
      bram_axi_rid => bram_axi_rid,
      bram_axi_rdata => bram_axi_rdata,
      bram_axi_rresp => bram_axi_rresp,
      bram_axi_rlast => bram_axi_rlast,
      bram_axi_ruser => bram_axi_ruser,
      bram_axi_rvalid => bram_axi_rvalid,
      bram_axi_rready => bram_axi_rready,
      dram_axi_init_axi_txn => dram_axi_init_axi_txn,
      dram_axi_txn_done => dram_axi_txn_done,
      dram_axi_error => dram_axi_error,
      dram_axi_aclk => dram_axi_aclk,
      dram_axi_aresetn => dram_axi_aresetn,
      dram_axi_awid => dram_axi_awid,
      dram_axi_awaddr => dram_axi_awaddr,
      dram_axi_awlen => dram_axi_awlen,
      dram_axi_awsize => dram_axi_awsize,
      dram_axi_awburst => dram_axi_awburst,
      dram_axi_awlock => dram_axi_awlock,
      dram_axi_awcache => dram_axi_awcache,
      dram_axi_awprot => dram_axi_awprot,
      dram_axi_awqos => dram_axi_awqos,
      dram_axi_awuser => dram_axi_awuser,
      dram_axi_awvalid => dram_axi_awvalid,
      dram_axi_awready => dram_axi_awready,
      dram_axi_wdata => dram_axi_wdata,
      dram_axi_wstrb => dram_axi_wstrb,
      dram_axi_wlast => dram_axi_wlast,
      dram_axi_wuser => dram_axi_wuser,
      dram_axi_wvalid => dram_axi_wvalid,
      dram_axi_wready => dram_axi_wready,
      dram_axi_bid => dram_axi_bid,
      dram_axi_bresp => dram_axi_bresp,
      dram_axi_buser => dram_axi_buser,
      dram_axi_bvalid => dram_axi_bvalid,
      dram_axi_bready => dram_axi_bready,
      dram_axi_arid => dram_axi_arid,
      dram_axi_araddr => dram_axi_araddr,
      dram_axi_arlen => dram_axi_arlen,
      dram_axi_arsize => dram_axi_arsize,
      dram_axi_arburst => dram_axi_arburst,
      dram_axi_arlock => dram_axi_arlock,
      dram_axi_arcache => dram_axi_arcache,
      dram_axi_arprot => dram_axi_arprot,
      dram_axi_arqos => dram_axi_arqos,
      dram_axi_aruser => dram_axi_aruser,
      dram_axi_arvalid => dram_axi_arvalid,
      dram_axi_arready => dram_axi_arready,
      dram_axi_rid => dram_axi_rid,
      dram_axi_rdata => dram_axi_rdata,
      dram_axi_rresp => dram_axi_rresp,
      dram_axi_rlast => dram_axi_rlast,
      dram_axi_ruser => dram_axi_ruser,
      dram_axi_rvalid => dram_axi_rvalid,
      dram_axi_rready => dram_axi_rready
    );
END design_1_Xillybus_AXI_Master_0_4_arch;
