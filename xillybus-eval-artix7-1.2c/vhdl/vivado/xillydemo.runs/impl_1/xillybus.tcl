proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000
set_msg_config -id {HDL-1065} -limit 10000

start_step init_design
set rc [catch {
  create_msg_db init_design.pb
  set_param gui.test TreeTableDev
  debug::add_scope template.lib 1
  set_property design_mode GateLvl [current_fileset]
  set_property webtalk.parent_dir C:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.cache/wt [current_project]
  set_property parent.project_path C:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.xpr [current_project]
  set_property ip_repo_paths {
  c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.cache/ip
  c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c
  c:/Users/wolff/Desktop/bachelorthesis/XillybusAXI/Xillybus_AXI_Master_1.0
  c:/users/wolff/desktop/bachelorthesis/ip_repo/xillybus
  C:/Users/wolff/Desktop/bachelorthesis/ip_repo
} [current_project]
  set_property ip_output_repo c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.cache/ip [current_project]
  add_files -quiet C:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.runs/synth_1/xillybus.dcp
  set_property edif_extra_search_paths C:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/core [current_fileset]
  read_xdc -prop_thru_buffers -ref design_1_clk_wiz_0 -cells U0 c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.srcs/sources_1/bd/design_1/ip/design_1_clk_wiz_0/design_1_clk_wiz_0_board.xdc
  set_property processing_order EARLY [get_files c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.srcs/sources_1/bd/design_1/ip/design_1_clk_wiz_0/design_1_clk_wiz_0_board.xdc]
  read_xdc -ref design_1_clk_wiz_0 -cells U0 c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.srcs/sources_1/bd/design_1/ip/design_1_clk_wiz_0/design_1_clk_wiz_0.xdc
  set_property processing_order EARLY [get_files c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.srcs/sources_1/bd/design_1/ip/design_1_clk_wiz_0/design_1_clk_wiz_0.xdc]
  read_xdc -prop_thru_buffers -ref design_1_rst_clk_wiz_100M_0 c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.srcs/sources_1/bd/design_1/ip/design_1_rst_clk_wiz_100M_0/design_1_rst_clk_wiz_100M_0_board.xdc
  set_property processing_order EARLY [get_files c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.srcs/sources_1/bd/design_1/ip/design_1_rst_clk_wiz_100M_0/design_1_rst_clk_wiz_100M_0_board.xdc]
  read_xdc -ref design_1_rst_clk_wiz_100M_0 c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.srcs/sources_1/bd/design_1/ip/design_1_rst_clk_wiz_100M_0/design_1_rst_clk_wiz_100M_0.xdc
  set_property processing_order EARLY [get_files c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.srcs/sources_1/bd/design_1/ip/design_1_rst_clk_wiz_100M_0/design_1_rst_clk_wiz_100M_0.xdc]
  read_xdc -ref pcie_a7_vivado -cells inst c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.srcs/sources_1/bd/design_1/ip/design_1_xillybus_0_1/src/pcie_a7_vivado/source/pcie_a7_vivado-PCIE_X0Y0.xdc
  set_property processing_order EARLY [get_files c:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vhdl/vivado/xillydemo.srcs/sources_1/bd/design_1/ip/design_1_xillybus_0_1/src/pcie_a7_vivado/source/pcie_a7_vivado-PCIE_X0Y0.xdc]
  read_xdc -mode out_of_context C:/Users/wolff/Desktop/bachelorthesis/xillybus-eval-artix7-1.2c/vivado-essentials/xillydemo.xdc
  read_xdc C:/Users/wolff/Desktop/project_1/project_1.srcs/sources_1/ip/gtwizard_0/gtwizard_0.xdc
  link_design -top xillybus -part xc7a200tfbg676-2
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
}

start_step opt_design
set rc [catch {
  create_msg_db opt_design.pb
  catch {write_debug_probes -quiet -force debug_nets}
  opt_design 
  write_checkpoint -force xillybus_opt.dcp
  catch {report_drc -file xillybus_drc_opted.rpt}
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
}

start_step place_design
set rc [catch {
  create_msg_db place_design.pb
  place_design 
  write_checkpoint -force xillybus_placed.dcp
  catch { report_io -file xillybus_io_placed.rpt }
  catch { report_clock_utilization -file xillybus_clock_utilization_placed.rpt }
  catch { report_utilization -file xillybus_utilization_placed.rpt -pb xillybus_utilization_placed.pb }
  catch { report_control_sets -verbose -file xillybus_control_sets_placed.rpt }
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
}

start_step phys_opt_design
set rc [catch {
  create_msg_db phys_opt_design.pb
  phys_opt_design 
  write_checkpoint -force xillybus_physopt.dcp
  close_msg_db -file phys_opt_design.pb
} RESULT]
if {$rc} {
  step_failed phys_opt_design
  return -code error $RESULT
} else {
  end_step phys_opt_design
}

  set_msg_config -id {Route 35-39} -severity "critical warning" -new_severity warning
start_step route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force xillybus_routed.dcp
  catch { report_drc -file xillybus_drc_routed.rpt -pb xillybus_drc_routed.pb }
  catch { report_timing_summary -max_paths 10 -file xillybus_timing_summary_routed.rpt -rpx xillybus_timing_summary_routed.rpx }
  catch { report_power -file xillybus_power_routed.rpt -pb xillybus_power_summary_routed.pb }
  catch { report_route_status -file xillybus_route_status.rpt -pb xillybus_route_status.pb }
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
}

start_step post_route_phys_opt_design
set rc [catch {
  create_msg_db post_route_phys_opt_design.pb
  phys_opt_design 
  write_checkpoint -force xillybus_postroute_physopt.dcp
  catch { report_timing_summary -warn_on_violation -max_paths 10 -file xillybus_timing_summary_postroute_physopted.rpt -rpx xillybus_timing_summary_postroute_physopted.rpx }
  close_msg_db -file post_route_phys_opt_design.pb
} RESULT]
if {$rc} {
  step_failed post_route_phys_opt_design
  return -code error $RESULT
} else {
  end_step post_route_phys_opt_design
}

